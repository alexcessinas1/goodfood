FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY src/main/CSharp/*.csproj ./src/main/CSharp/
COPY src/test/CSharpTests/*.csproj ./src/test/CSharpTests/
RUN dotnet restore

# copy everything else and build app
COPY src/main/CSharp/. ./src/main/CSharp/
WORKDIR /app/src/main/CSharp
RUN dotnet publish -c Release -o out

# Run Unit Tests
FROM build AS unittest
WORKDIR /app/src/test/CSharpTests
COPY src/test/CSharpTests/. .
ENTRYPOINT ["dotnet", "test"]

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/src/main/CSharp/out ./
ENTRYPOINT ["dotnet", "CSharp.dll"]
