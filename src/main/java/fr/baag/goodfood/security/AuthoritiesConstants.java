package fr.baag.goodfood.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String CLIENT = "ROLE_CLIENT";

    public static final String EMPLOYE = "ROLE_EMPLOYE";

    public static final String FRANCHISE = "ROLE_FRANCHISE";

    private AuthoritiesConstants() {
    }
}
