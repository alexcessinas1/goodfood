package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.Client;
import fr.baag.goodfood.domain.enumeration.PersonneType;
import fr.baag.goodfood.repository.ClientRepository;
import fr.baag.goodfood.repository.AdresseRepository;
import fr.baag.goodfood.repository.PersonneMoraleRepository;
import fr.baag.goodfood.repository.PersonnePhysiqueRepository;
import fr.baag.goodfood.repository.search.ClientSearchRepository;
import fr.baag.goodfood.service.dto.ClientDTO;
import fr.baag.goodfood.service.mapper.ClientMapper;
import io.undertow.util.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Client}.
 */
@Service
@Transactional
public class ClientService {

    private final Logger log = LoggerFactory.getLogger(ClientService.class);

    private final ClientRepository clientRepository;

    private final ClientMapper clientMapper;

    private final ClientSearchRepository clientSearchRepository;

    private final AdresseRepository adresseRepository;
    private final PersonneMoraleRepository personneMoraleRepository;
    private final PersonnePhysiqueRepository personnePhysiqueRepository;

    public ClientService(ClientRepository clientRepository, ClientMapper clientMapper,
            ClientSearchRepository clientSearchRepository, AdresseRepository adresseRepository,
            PersonneMoraleRepository personneMoraleRepository, PersonnePhysiqueRepository personnePhysiqueRepository) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.clientSearchRepository = clientSearchRepository;
        this.adresseRepository = adresseRepository;
        this.personneMoraleRepository = personneMoraleRepository;
        this.personnePhysiqueRepository = personnePhysiqueRepository;
    }

    /**
     * Save a client.
     *
     * @param clientDTO the entity to save.
     * @return the persisted entity.
     * @throws BadRequestException
     */
    public ClientDTO save(ClientDTO clientDTO) throws BadRequestException {
        log.debug("Request to save Client : {}", clientDTO);
        Client client = clientMapper.toEntity(clientDTO);
        if (clientDTO.getAdresseFacturationId() != null) {
            adresseRepository.findById(clientDTO.getAdresseFacturationId()).ifPresent(client::adresseFacturation);
        }
        if (clientDTO.getAdresseLivraisonId() != null) {
            adresseRepository.findById(clientDTO.getAdresseLivraisonId()).ifPresent(client::adresseLivraison);
        }
        if (clientDTO.getPersonneType() == PersonneType.MORALE) {
            if (Objects.isNull(clientDTO.getPersonneMoraleId())) {
                throw new BadRequestException(
                        "Id personneMorale ne peut pas etre null pour créer un client Entreprise");
            }
            personneMoraleRepository.findById(clientDTO.getPersonneMoraleId()).ifPresent(client::personneMorale);
        } else {
            if (Objects.isNull(clientDTO.getPersonnePhysiqueId())) {
                throw new BadRequestException(
                        "Id personnePhysique ne peut pas etre null pour créer un client Physique");
            }
            personnePhysiqueRepository.findById(clientDTO.getPersonnePhysiqueId()).ifPresent(client::personnePhysique);
        }
        client = clientRepository.save(client);
        ClientDTO result = clientMapper.toDto(client);
        clientSearchRepository.save(client);
        return result;
    }

    /**
     * Get all the clients.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ClientDTO> findAll() {
        log.debug("Request to get all Clients");
        return clientRepository.findAll().stream().map(clientMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one client by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ClientDTO> findOne(Long id) {
        log.debug("Request to get Client : {}", id);
        return clientRepository.findById(id).map(clientMapper::toDto);
    }

    /**
     * Delete the client by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Client : {}", id);
        clientRepository.deleteById(id);
        clientSearchRepository.deleteById(id);
    }

    /**
     * Search for the client corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ClientDTO> search(String query) {
        log.debug("Request to search Clients for query {}", query);
        return StreamSupport.stream(clientSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .map(clientMapper::toDto).collect(Collectors.toList());
    }
}
