package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.CategorieAlimentation;
import fr.baag.goodfood.repository.CategorieAlimentationRepository;
import fr.baag.goodfood.repository.search.CategorieAlimentationSearchRepository;
import fr.baag.goodfood.service.dto.CategorieAlimentationDTO;
import fr.baag.goodfood.service.mapper.CategorieAlimentationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CategorieAlimentation}.
 */
@Service
@Transactional
public class CategorieAlimentationService {

    private final Logger log = LoggerFactory.getLogger(CategorieAlimentationService.class);

    private final CategorieAlimentationRepository categorieAlimentationRepository;

    private final CategorieAlimentationMapper categorieAlimentationMapper;

    private final CategorieAlimentationSearchRepository categorieAlimentationSearchRepository;

    public CategorieAlimentationService(CategorieAlimentationRepository categorieAlimentationRepository, CategorieAlimentationMapper categorieAlimentationMapper, CategorieAlimentationSearchRepository categorieAlimentationSearchRepository) {
        this.categorieAlimentationRepository = categorieAlimentationRepository;
        this.categorieAlimentationMapper = categorieAlimentationMapper;
        this.categorieAlimentationSearchRepository = categorieAlimentationSearchRepository;
    }

    /**
     * Save a categorieAlimentation.
     *
     * @param categorieAlimentationDTO the entity to save.
     * @return the persisted entity.
     */
    public CategorieAlimentationDTO save(CategorieAlimentationDTO categorieAlimentationDTO) {
        log.debug("Request to save CategorieAlimentation : {}", categorieAlimentationDTO);
        CategorieAlimentation categorieAlimentation = categorieAlimentationMapper.toEntity(categorieAlimentationDTO);
        categorieAlimentation = categorieAlimentationRepository.save(categorieAlimentation);
        CategorieAlimentationDTO result = categorieAlimentationMapper.toDto(categorieAlimentation);
        categorieAlimentationSearchRepository.save(categorieAlimentation);
        return result;
    }

    /**
     * Get all the categorieAlimentations.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieAlimentationDTO> findAll() {
        log.debug("Request to get all CategorieAlimentations");
        return categorieAlimentationRepository.findAll().stream()
            .map(categorieAlimentationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one categorieAlimentation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CategorieAlimentationDTO> findOne(Long id) {
        log.debug("Request to get CategorieAlimentation : {}", id);
        return categorieAlimentationRepository.findById(id)
            .map(categorieAlimentationMapper::toDto);
    }

    /**
     * Delete the categorieAlimentation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CategorieAlimentation : {}", id);
        categorieAlimentationRepository.deleteById(id);
        categorieAlimentationSearchRepository.deleteById(id);
    }

    /**
     * Search for the categorieAlimentation corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieAlimentationDTO> search(String query) {
        log.debug("Request to search CategorieAlimentations for query {}", query);
        return StreamSupport
            .stream(categorieAlimentationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(categorieAlimentationMapper::toDto)
        .collect(Collectors.toList());
    }
}
