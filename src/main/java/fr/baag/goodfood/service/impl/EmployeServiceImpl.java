package fr.baag.goodfood.service.impl;

import fr.baag.goodfood.service.EmployeService;
import fr.baag.goodfood.domain.Employe;
import fr.baag.goodfood.repository.EmployeRepository;
import fr.baag.goodfood.repository.search.EmployeSearchRepository;
import fr.baag.goodfood.service.dto.EmployeDTO;
import fr.baag.goodfood.service.mapper.EmployeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Employe}.
 */
@Service
@Transactional
public class EmployeServiceImpl implements EmployeService {

    private final Logger log = LoggerFactory.getLogger(EmployeServiceImpl.class);

    private final EmployeRepository employeRepository;

    private final EmployeMapper employeMapper;

    private final EmployeSearchRepository employeSearchRepository;

    public EmployeServiceImpl(EmployeRepository employeRepository, EmployeMapper employeMapper, EmployeSearchRepository employeSearchRepository) {
        this.employeRepository = employeRepository;
        this.employeMapper = employeMapper;
        this.employeSearchRepository = employeSearchRepository;
    }

    @Override
    public EmployeDTO save(EmployeDTO employeDTO) {
        log.debug("Request to save Employe : {}", employeDTO);
        Employe employe = employeMapper.toEntity(employeDTO);
        employe = employeRepository.save(employe);
        EmployeDTO result = employeMapper.toDto(employe);
        employeSearchRepository.save(employe);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeDTO> findAll() {
        log.debug("Request to get all Employes");
        return employeRepository.findAll().stream()
            .map(employeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<EmployeDTO> findOne(Long id) {
        log.debug("Request to get Employe : {}", id);
        return employeRepository.findById(id)
            .map(employeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Employe : {}", id);
        employeRepository.deleteById(id);
        employeSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeDTO> search(String query) {
        log.debug("Request to search Employes for query {}", query);
        return StreamSupport
            .stream(employeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(employeMapper::toDto)
        .collect(Collectors.toList());
    }
}
