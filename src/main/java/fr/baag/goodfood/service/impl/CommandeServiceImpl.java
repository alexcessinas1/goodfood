package fr.baag.goodfood.service.impl;

import fr.baag.goodfood.service.CommandeService;
import fr.baag.goodfood.domain.Commande;
import fr.baag.goodfood.repository.CommandeRepository;
import fr.baag.goodfood.repository.search.CommandeSearchRepository;
import fr.baag.goodfood.service.dto.CommandeDTO;
import fr.baag.goodfood.service.mapper.CommandeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Commande}.
 */
@Service
@Transactional
public class CommandeServiceImpl implements CommandeService {

    private final Logger log = LoggerFactory.getLogger(CommandeServiceImpl.class);

    private final CommandeRepository commandeRepository;

    private final CommandeMapper commandeMapper;

    private final CommandeSearchRepository commandeSearchRepository;

    public CommandeServiceImpl(CommandeRepository commandeRepository, CommandeMapper commandeMapper, CommandeSearchRepository commandeSearchRepository) {
        this.commandeRepository = commandeRepository;
        this.commandeMapper = commandeMapper;
        this.commandeSearchRepository = commandeSearchRepository;
    }

    @Override
    public CommandeDTO save(CommandeDTO commandeDTO) {
        log.debug("Request to save Commande : {}", commandeDTO);
        Commande commande = commandeMapper.toEntity(commandeDTO);
        commande = commandeRepository.save(commande);
        CommandeDTO result = commandeMapper.toDto(commande);
        commandeSearchRepository.save(commande);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommandeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commandes");
        return commandeRepository.findAll(pageable)
            .map(commandeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CommandeDTO> findOne(Long id) {
        log.debug("Request to get Commande : {}", id);
        return commandeRepository.findById(id)
            .map(commandeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Commande : {}", id);
        commandeRepository.deleteById(id);
        commandeSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommandeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Commandes for query {}", query);
        return commandeSearchRepository.search(queryStringQuery(query), pageable)
            .map(commandeMapper::toDto);
    }
}
