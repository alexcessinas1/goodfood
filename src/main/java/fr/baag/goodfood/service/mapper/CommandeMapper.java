package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.CommandeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commande} and its DTO {@link CommandeDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class, FranchiseMapper.class, FournisseurMapper.class})
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {

    @Mapping(source = "idClient.id", target = "idClient")
    @Mapping(source = "idFranchise.id", target = "idFranchise")
    @Mapping(source = "idFournisseur.id", target = "idFournisseur")
    CommandeDTO toDto(Commande commande);

    @Mapping(source = "idClient", target = "idClient")
    @Mapping(source = "idFranchise", target = "idFranchise")
    @Mapping(source = "idFournisseur", target = "idFournisseur")
    Commande toEntity(CommandeDTO commandeDTO);

    default Commande fromId(Long id) {
        if (id == null) {
            return null;
        }
        Commande commande = new Commande();
        commande.setId(id);
        return commande;
    }
}
