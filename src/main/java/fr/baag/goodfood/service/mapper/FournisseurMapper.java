package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.FournisseurDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Fournisseur} and its DTO {@link FournisseurDTO}.
 */
@Mapper(componentModel = "spring", uses = { AdresseMapper.class })
public interface FournisseurMapper extends EntityMapper<FournisseurDTO, Fournisseur> {

    @Mapping(source = "adresse.id", target = "idAdresseId")
    FournisseurDTO toDto(Fournisseur fournisseur);

    @Mapping(source = "idAdresseId", target = "adresse")
    @Mapping(target = "produitsFournisseur", ignore = true)
    @Mapping(target = "removeProduitsFournisseur", ignore = true)
    Fournisseur toEntity(FournisseurDTO fournisseurDTO);

    default Fournisseur fromId(Long id) {
        if (id == null) {
            return null;
        }
        Fournisseur fournisseur = new Fournisseur();
        fournisseur.setId(id);
        return fournisseur;
    }
}
