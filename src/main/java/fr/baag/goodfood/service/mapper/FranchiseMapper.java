package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.FranchiseDTO;
import fr.baag.goodfood.service.dto.SimpleFranchiseDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Franchise} and its DTO {@link FranchiseDTO}.
 */
@Mapper(componentModel = "spring", uses = {AdresseMapper.class, ProduitMapper.class})
public interface FranchiseMapper extends EntityMapper<FranchiseDTO, Franchise> {

    @Mapping(source = "adresseFranchiseId", target = "adresseFranchise")
    SimpleFranchiseDTO toSimpleDto(Franchise franchise);

    @Mapping(source = "adresseFranchiseId.id", target = "adresseFranchiseIdId")
    FranchiseDTO toDto(Franchise franchise);

    @Mapping(source = "adresseFranchiseIdId", target = "adresseFranchiseId")
    @Mapping(target = "removeStockFranchise", ignore = true)
    Franchise toEntity(FranchiseDTO franchiseDTO);

    default Franchise fromId(Long id) {
        if (id == null) {
            return null;
        }
        Franchise franchise = new Franchise();
        franchise.setId(id);
        return franchise;
    }
}
