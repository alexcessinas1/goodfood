package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.PersonneMoraleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PersonneMorale} and its DTO
 * {@link PersonneMoraleDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface PersonneMoraleMapper extends EntityMapper<PersonneMoraleDTO, PersonneMorale> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    PersonneMoraleDTO toDto(PersonneMorale personneMorale);

    @Mapping(source = "userId", target = "user")
    PersonneMorale toEntity(PersonneMoraleDTO personneMoraleDTO);

    default PersonneMorale fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonneMorale personneMorale = new PersonneMorale();
        personneMorale.setId(id);
        return personneMorale;
    }
}
