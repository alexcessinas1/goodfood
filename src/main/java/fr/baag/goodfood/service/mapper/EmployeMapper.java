package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.EmployeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Employe} and its DTO {@link EmployeDTO}.
 */
@Mapper(componentModel = "spring", uses = {FranchiseMapper.class, AdresseMapper.class, PersonnePhysiqueMapper.class, PersonneMoraleMapper.class})
public interface EmployeMapper extends EntityMapper<EmployeDTO, Employe> {

    @Mapping(source = "franchise.id", target = "franchiseId")
    @Mapping(source = "adresse.id", target = "adresseId")
    @Mapping(source = "personnePhysique.id", target = "personnePhysiqueId")
    @Mapping(source = "personneMorale.id", target = "personneMoraleId")
    EmployeDTO toDto(Employe employe);

    @Mapping(source = "franchiseId", target = "franchise")
    @Mapping(source = "adresseId", target = "adresse")
    @Mapping(source = "personnePhysiqueId", target = "personnePhysique")
    @Mapping(source = "personneMoraleId", target = "personneMorale")
    Employe toEntity(EmployeDTO employeDTO);

    default Employe fromId(Long id) {
        if (id == null) {
            return null;
        }
        Employe employe = new Employe();
        employe.setId(id);
        return employe;
    }
}
