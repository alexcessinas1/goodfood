package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.RecetteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Recette} and its DTO {@link RecetteDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RecetteMapper extends EntityMapper<RecetteDTO, Recette> {

    // @Mapping(target = "produits", ignore = true)
    // @Mapping(target = "removeProduits", ignore = true)
    Recette toEntity(RecetteDTO recetteDTO);

    RecetteDTO toDto(Recette recette);

    default Recette fromId(Long id) {
        if (id == null) {
            return null;
        }
        Recette recette = new Recette();
        recette.setId(id);
        return recette;
    }
}
