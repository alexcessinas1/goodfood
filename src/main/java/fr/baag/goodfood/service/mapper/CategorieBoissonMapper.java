package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.CategorieBoissonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategorieBoisson} and its DTO {@link CategorieBoissonDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategorieBoissonMapper extends EntityMapper<CategorieBoissonDTO, CategorieBoisson> {



    default CategorieBoisson fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategorieBoisson categorieBoisson = new CategorieBoisson();
        categorieBoisson.setId(id);
        return categorieBoisson;
    }
}
