package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.PersonnePhysiqueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PersonnePhysique} and its DTO
 * {@link PersonnePhysiqueDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface PersonnePhysiqueMapper extends EntityMapper<PersonnePhysiqueDTO, PersonnePhysique> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    PersonnePhysiqueDTO toDto(PersonnePhysique personnePhysique);

    @Mapping(source = "userId", target = "user")
    PersonnePhysique toEntity(PersonnePhysiqueDTO personnePhysiqueDTO);

    default PersonnePhysique fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonnePhysique personnePhysique = new PersonnePhysique();
        personnePhysique.setId(id);
        return personnePhysique;
    }
}
