package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PlateauRepas} and its DTO
 * {@link PlateauRepasDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProduitMapper.class })
public interface PlateauRepasMapper extends EntityMapper<PlateauRepasDTO, PlateauRepas> {

    @Mapping(source = "boisson.id", target = "boissonId")
    PlateauRepasDTO toDto(PlateauRepas plateauRepas);

    @Mapping(source = "boissonId", target = "boisson")
    PlateauRepas toEntity(PlateauRepasDTO plateauRepasDTO);

    default PlateauRepas fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlateauRepas plateauRepas = new PlateauRepas();
        plateauRepas.setId(id);
        return plateauRepas;
    }
}
