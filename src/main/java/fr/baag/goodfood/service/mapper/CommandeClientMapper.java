package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.CommandeClientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CommandeClient} and its DTO {@link CommandeClientDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class, EmployeMapper.class, CommandeMapper.class})
public interface CommandeClientMapper extends EntityMapper<CommandeClientDTO, CommandeClient> {

    @Mapping(source = "client.id", target = "clientId")
    @Mapping(source = "employe.id", target = "employeId")
    @Mapping(source = "commande.id", target = "commandeId")
    CommandeClientDTO toDto(CommandeClient commandeClient);

    @Mapping(source = "clientId", target = "client")
    @Mapping(source = "employeId", target = "employe")
    @Mapping(source = "commandeId", target = "commande")
    @Mapping(target = "plateauRepas", ignore = true)
    @Mapping(target = "removePlateauRepas", ignore = true)
    CommandeClient toEntity(CommandeClientDTO commandeClientDTO);

    default CommandeClient fromId(Long id) {
        if (id == null) {
            return null;
        }
        CommandeClient commandeClient = new CommandeClient();
        commandeClient.setId(id);
        return commandeClient;
    }
}
