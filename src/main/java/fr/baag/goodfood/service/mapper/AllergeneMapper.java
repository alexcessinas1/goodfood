package fr.baag.goodfood.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import fr.baag.goodfood.domain.Allergene;
import fr.baag.goodfood.service.dto.AllergeneDTO;

/**
 * Mapper for the entity {@link Allergene} and its DTO {@link AllergeneDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AllergeneMapper extends EntityMapper<AllergeneDTO, Allergene> {

	@Mapping(source = "nom", target = "nom")
	AllergeneDTO toDto(Allergene allergene);

	@Mapping(source = "nom", target = "nom")
	Allergene toEntity(AllergeneDTO allergeneDto);

	default Allergene fromId(Long id) {
		if (id == null) {
			return null;
		}
		Allergene allergene = new Allergene();
		allergene.setId(id);
		return allergene;
	}
}