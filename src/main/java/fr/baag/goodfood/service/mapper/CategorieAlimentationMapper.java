package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.CategorieAlimentationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategorieAlimentation} and its DTO {@link CategorieAlimentationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategorieAlimentationMapper extends EntityMapper<CategorieAlimentationDTO, CategorieAlimentation> {



    default CategorieAlimentation fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategorieAlimentation categorieAlimentation = new CategorieAlimentation();
        categorieAlimentation.setId(id);
        return categorieAlimentation;
    }
}
