package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.CategorieFournitureDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategorieFourniture} and its DTO {@link CategorieFournitureDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategorieFournitureMapper extends EntityMapper<CategorieFournitureDTO, CategorieFourniture> {



    default CategorieFourniture fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategorieFourniture categorieFourniture = new CategorieFourniture();
        categorieFourniture.setId(id);
        return categorieFourniture;
    }
}
