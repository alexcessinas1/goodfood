package fr.baag.goodfood.service.mapper;

import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.ProduitDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Produit} and its DTO {@link ProduitDTO}.
 */
@Mapper(componentModel = "spring", uses = { CategorieBoissonMapper.class, CategorieAlimentationMapper.class,
        CategorieFournitureMapper.class, AllergeneMapper.class })
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {

    @Mapping(source = "boisson.id", target = "boissonId")
    @Mapping(source = "aliment.id", target = "alimentId")
    @Mapping(source = "fourniture.id", target = "fournitureId")
    ProduitDTO toDto(Produit produit);

    @Mapping(source = "boissonId", target = "boisson")
    @Mapping(source = "alimentId", target = "aliment")
    @Mapping(source = "fournitureId", target = "fourniture")
    Produit toEntity(ProduitDTO produitDTO);

    default Produit fromId(Long id) {
        if (id == null) {
            return null;
        }
        Produit produit = new Produit();
        produit.setId(id);
        return produit;
    }
}
