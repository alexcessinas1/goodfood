package fr.baag.goodfood.service.mapper;


import fr.baag.goodfood.domain.*;
import fr.baag.goodfood.service.dto.ClientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Client} and its DTO {@link ClientDTO}.
 */
@Mapper(componentModel = "spring", uses = {AdresseMapper.class, PersonnePhysiqueMapper.class, PersonneMoraleMapper.class})
public interface ClientMapper extends EntityMapper<ClientDTO, Client> {

    @Mapping(source = "adresseLivraison.id", target = "adresseLivraisonId")
    @Mapping(source = "adresseFacturation.id", target = "adresseFacturationId")
    @Mapping(source = "personnePhysique.id", target = "personnePhysiqueId")
    @Mapping(source = "personneMorale.id", target = "personneMoraleId")
    ClientDTO toDto(Client client);

    @Mapping(source = "adresseLivraisonId", target = "adresseLivraison")
    @Mapping(source = "adresseFacturationId", target = "adresseFacturation")
    @Mapping(source = "personnePhysiqueId", target = "personnePhysique")
    @Mapping(source = "personneMoraleId", target = "personneMorale")
    Client toEntity(ClientDTO clientDTO);

    default Client fromId(Long id) {
        if (id == null) {
            return null;
        }
        Client client = new Client();
        client.setId(id);
        return client;
    }
}
