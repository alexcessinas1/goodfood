package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.CommandeClient;
import fr.baag.goodfood.repository.CommandeClientRepository;
import fr.baag.goodfood.repository.CommandeRepository;
import fr.baag.goodfood.repository.search.CommandeClientSearchRepository;
import fr.baag.goodfood.service.dto.CommandeClientDTO;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;
import fr.baag.goodfood.service.mapper.CommandeClientMapper;
import fr.baag.goodfood.service.mapper.PlateauRepasMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CommandeClient}.
 */
@Service
@Transactional
public class CommandeClientService {

    private final Logger log = LoggerFactory.getLogger(CommandeClientService.class);

    private final CommandeClientRepository commandeClientRepository;

    private final CommandeClientMapper commandeClientMapper;

    private final CommandeClientSearchRepository commandeClientSearchRepository;

    private final CommandeRepository commandeRepository;
    private final PlateauRepasService plateauRepasService;
    private final PlateauRepasMapper plateauRepasMapper;

    public CommandeClientService(CommandeClientRepository commandeClientRepository,
            CommandeClientMapper commandeClientMapper, CommandeClientSearchRepository commandeClientSearchRepository,
            CommandeRepository commandeRepository, PlateauRepasService plateauRepasService,
            PlateauRepasMapper plateauRepasMapper) {
        this.commandeClientRepository = commandeClientRepository;
        this.commandeClientMapper = commandeClientMapper;
        this.commandeClientSearchRepository = commandeClientSearchRepository;
        this.commandeRepository = commandeRepository;
        this.plateauRepasService = plateauRepasService;
        this.plateauRepasMapper = plateauRepasMapper;
    }

    public CommandeClientDTO save(CommandeClientDTO commandeClientDTO) {
        log.debug("Request to save CommandeClient : {}", commandeClientDTO);
        CommandeClient commandeClient = commandeClientMapper.toEntity(commandeClientDTO);
        Long commandeId = commandeClientDTO.getCommandeId();
        commandeRepository.findById(commandeId).ifPresent(commandeClient::commande);
        commandeClient = commandeClientRepository.save(commandeClient);
        CommandeClientDTO result = commandeClientMapper.toDto(commandeClient);
        commandeClientSearchRepository.save(commandeClient);
        return result;
    }

    @Transactional(readOnly = true)
    public List<CommandeClientDTO> findAll() {
        log.debug("Request to get all CommandeClients");
        return commandeClientRepository.findAll().stream().map(commandeClientMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public Optional<CommandeClientDTO> findOne(Long id) {
        log.debug("Request to get CommandeClient : {}", id);
        return commandeClientRepository.findById(id).map(commandeClientMapper::toDto);
    }

    public void delete(Long id) {
        log.debug("Request to delete CommandeClient : {}", id);
        commandeClientRepository.deleteById(id);
        commandeClientSearchRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<CommandeClientDTO> search(String query) {
        log.debug("Request to search CommandeClients for query {}", query);
        return StreamSupport.stream(commandeClientSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .map(commandeClientMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public CommandeClientDTO ajoutePlateauRepas(Long id, PlateauRepasDTO plateau) {
        Optional<CommandeClient> cmd = commandeClientRepository.findById(id);
        if (!cmd.isPresent()) {
            return null;
        }
        if (plateau.getId() == null) {
            plateau = plateauRepasService.save(plateau);
        }
        cmd.get().addPlateauRepas(plateauRepasMapper.toEntity(plateau));
        return commandeClientMapper.toDto(cmd.get());
    }

    @Transactional
    public CommandeClientDTO supprimePlateauRepas(Long id, PlateauRepasDTO plateau) {
        Optional<CommandeClient> cmd = commandeClientRepository.findById(id);
        if (!cmd.isPresent()) {
            return null;
        }
        if (plateau.getId() == null) {
            return null;
        }
        cmd.get().removePlateauRepas(plateauRepasMapper.toEntity(plateau));
        return commandeClientMapper.toDto(cmd.get());
    }
}
