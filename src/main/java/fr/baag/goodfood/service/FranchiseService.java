package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.Franchise;
import fr.baag.goodfood.repository.FranchiseRepository;
import fr.baag.goodfood.repository.search.FranchiseSearchRepository;
import fr.baag.goodfood.service.dto.FranchiseDTO;
import fr.baag.goodfood.service.dto.SimpleFranchiseDTO;
import fr.baag.goodfood.service.mapper.FranchiseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Franchise}.
 */
@Service
@Transactional
public class FranchiseService {

    private final Logger log = LoggerFactory.getLogger(FranchiseService.class);

    private final FranchiseRepository franchiseRepository;

    private final FranchiseMapper franchiseMapper;

    private final FranchiseSearchRepository franchiseSearchRepository;

    public FranchiseService(FranchiseRepository franchiseRepository, FranchiseMapper franchiseMapper, FranchiseSearchRepository franchiseSearchRepository) {
        this.franchiseRepository = franchiseRepository;
        this.franchiseMapper = franchiseMapper;
        this.franchiseSearchRepository = franchiseSearchRepository;
    }

    /**
     * Save a franchise.
     *
     * @param franchiseDTO the entity to save.
     * @return the persisted entity.
     */
    public FranchiseDTO save(FranchiseDTO franchiseDTO) {
        log.debug("Request to save Franchise : {}", franchiseDTO);
        Franchise franchise = franchiseMapper.toEntity(franchiseDTO);
        franchise = franchiseRepository.save(franchise);
        FranchiseDTO result = franchiseMapper.toDto(franchise);
        franchiseSearchRepository.save(franchise);
        return result;
    }

    /**
     * Get all the franchises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FranchiseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Franchises");
        return franchiseRepository.findAll(pageable)
            .map(franchiseMapper::toDto);
    }


    /**
     * Get all the franchises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SimpleFranchiseDTO> findAllWithAddress(Pageable pageable) {
        log.debug("Request to get all Franchises");
        return franchiseRepository.findAll(pageable).map(franchiseMapper::toSimpleDto);
    }


    /**
     * Get all the franchises with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<FranchiseDTO> findAllWithEagerRelationships(Pageable pageable) {
        return franchiseRepository.findAllWithEagerRelationships(pageable).map(franchiseMapper::toDto);
    }

    /**
     * Get one franchise by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FranchiseDTO> findOne(Long id) {
        log.debug("Request to get Franchise : {}", id);
        return franchiseRepository.findOneWithEagerRelationships(id)
            .map(franchiseMapper::toDto);
    }

    /**
     * Delete the franchise by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Franchise : {}", id);
        franchiseRepository.deleteById(id);
        franchiseSearchRepository.deleteById(id);
    }

    /**
     * Search for the franchise corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FranchiseDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Franchises for query {}", query);
        return franchiseSearchRepository.search(queryStringQuery(query), pageable)
            .map(franchiseMapper::toDto);
    }
}
