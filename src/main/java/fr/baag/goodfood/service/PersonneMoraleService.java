package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.PersonneMorale;
import fr.baag.goodfood.repository.PersonneMoraleRepository;
import fr.baag.goodfood.repository.UserRepository;
import fr.baag.goodfood.repository.search.PersonneMoraleSearchRepository;
import fr.baag.goodfood.service.dto.PersonneMoraleDTO;
import fr.baag.goodfood.service.mapper.PersonneMoraleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link PersonneMorale}.
 */
@Service
@Transactional
public class PersonneMoraleService {

    private final Logger log = LoggerFactory.getLogger(PersonneMoraleService.class);

    private final PersonneMoraleRepository personneMoraleRepository;

    private final PersonneMoraleMapper personneMoraleMapper;

    private final PersonneMoraleSearchRepository personneMoraleSearchRepository;

    private final UserRepository userRepository;

    public PersonneMoraleService(PersonneMoraleRepository personneMoraleRepository, PersonneMoraleMapper personneMoraleMapper, PersonneMoraleSearchRepository personneMoraleSearchRepository, UserRepository userRepository) {
        this.personneMoraleRepository = personneMoraleRepository;
        this.personneMoraleMapper = personneMoraleMapper;
        this.personneMoraleSearchRepository = personneMoraleSearchRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a personneMorale.
     *
     * @param personneMoraleDTO the entity to save.
     * @return the persisted entity.
     */
    public PersonneMoraleDTO save(PersonneMoraleDTO personneMoraleDTO) {
        log.debug("Request to save PersonneMorale : {}", personneMoraleDTO);
        PersonneMorale personneMorale = personneMoraleMapper.toEntity(personneMoraleDTO);
        Long userId = personneMoraleDTO.getUserId();
        userRepository.findById(userId).ifPresent(personneMorale::user);
        personneMorale = personneMoraleRepository.save(personneMorale);
        PersonneMoraleDTO result = personneMoraleMapper.toDto(personneMorale);
        personneMoraleSearchRepository.save(personneMorale);
        return result;
    }

    /**
     * Get all the personneMorales.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonneMoraleDTO> findAll() {
        log.debug("Request to get all PersonneMorales");
        return personneMoraleRepository.findAll().stream()
            .map(personneMoraleMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one personneMorale by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PersonneMoraleDTO> findOne(Long id) {
        log.debug("Request to get PersonneMorale : {}", id);
        return personneMoraleRepository.findById(id)
            .map(personneMoraleMapper::toDto);
    }

    /**
     * Delete the personneMorale by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PersonneMorale : {}", id);
        personneMoraleRepository.deleteById(id);
        personneMoraleSearchRepository.deleteById(id);
    }

    /**
     * Search for the personneMorale corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonneMoraleDTO> search(String query) {
        log.debug("Request to search PersonneMorales for query {}", query);
        return StreamSupport
            .stream(personneMoraleSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(personneMoraleMapper::toDto)
        .collect(Collectors.toList());
    }
}
