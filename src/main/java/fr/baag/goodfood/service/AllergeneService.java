package fr.baag.goodfood.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.baag.goodfood.domain.Allergene;
import fr.baag.goodfood.repository.AllergeneRepository;
import fr.baag.goodfood.repository.search.AllergeneSearchRepository;
import fr.baag.goodfood.service.dto.AllergeneDTO;
import fr.baag.goodfood.service.mapper.AllergeneMapper;

/**
 * Service Implementation for managing {@link Allergene}.
 */
@Service
@Transactional
public class AllergeneService {
	private final Logger log = LoggerFactory.getLogger(AllergeneService.class);
	private final AllergeneMapper allergeneMapper;
	private final AllergeneSearchRepository allergeneSearchRepository;
	private final AllergeneRepository allergeneRepository;

	public AllergeneService(AllergeneMapper allergeneMapper, AllergeneSearchRepository allergeneSearchRepository,
			AllergeneRepository allergeneRepository) {
		this.allergeneMapper = allergeneMapper;
		this.allergeneSearchRepository = allergeneSearchRepository;
		this.allergeneRepository = allergeneRepository;
	}

	/**
	 * Save an Allergene.
	 *
	 * @param allergeneDTO the entity to save.
	 * @return the persisted entity.
	 */
	public AllergeneDTO save(AllergeneDTO allergeneDTO) {
		log.debug("Request to save Allergene : {}", allergeneDTO);
		Allergene allergene = allergeneMapper.toEntity(allergeneDTO);
		allergene = allergeneRepository.save(allergene);
		AllergeneDTO result = allergeneMapper.toDto(allergene);
		allergeneSearchRepository.save(allergene);
		return result;
	}

}
