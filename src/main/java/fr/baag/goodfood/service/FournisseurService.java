package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.Fournisseur;
import fr.baag.goodfood.repository.FournisseurRepository;
import fr.baag.goodfood.repository.AdresseRepository;
import fr.baag.goodfood.repository.search.FournisseurSearchRepository;
import fr.baag.goodfood.service.dto.FournisseurDTO;
import fr.baag.goodfood.service.mapper.FournisseurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Fournisseur}.
 */
@Service
@Transactional
public class FournisseurService {

    private final Logger log = LoggerFactory.getLogger(FournisseurService.class);

    private final FournisseurRepository fournisseurRepository;

    private final FournisseurMapper fournisseurMapper;

    private final FournisseurSearchRepository fournisseurSearchRepository;

    private final AdresseRepository adresseRepository;

    public FournisseurService(FournisseurRepository fournisseurRepository, FournisseurMapper fournisseurMapper,
            FournisseurSearchRepository fournisseurSearchRepository, AdresseRepository adresseRepository) {
        this.fournisseurRepository = fournisseurRepository;
        this.fournisseurMapper = fournisseurMapper;
        this.fournisseurSearchRepository = fournisseurSearchRepository;
        this.adresseRepository = adresseRepository;
    }

    /**
     * Save a fournisseur.
     *
     * @param fournisseurDTO the entity to save.
     * @return the persisted entity.
     */
    public FournisseurDTO save(FournisseurDTO fournisseurDTO) {
        log.debug("Request to save Fournisseur : {}", fournisseurDTO);
        Fournisseur fournisseur = fournisseurMapper.toEntity(fournisseurDTO);
        Long adresseId = fournisseurDTO.getIdAdresseId();
        adresseRepository.findById(adresseId).ifPresent(fournisseur::adresse);
        fournisseur = fournisseurRepository.save(fournisseur);
        FournisseurDTO result = fournisseurMapper.toDto(fournisseur);
        fournisseurSearchRepository.save(fournisseur);
        return result;
    }

    /**
     * Get all the fournisseurs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FournisseurDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Fournisseurs");
        return fournisseurRepository.findAll(pageable).map(fournisseurMapper::toDto);
    }

    /**
     * Get one fournisseur by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FournisseurDTO> findOne(Long id) {
        log.debug("Request to get Fournisseur : {}", id);
        return fournisseurRepository.findById(id).map(fournisseurMapper::toDto);
    }

    /**
     * Delete the fournisseur by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Fournisseur : {}", id);
        fournisseurRepository.deleteById(id);
        fournisseurSearchRepository.deleteById(id);
    }

    /**
     * Search for the fournisseur corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FournisseurDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Fournisseurs for query {}", query);
        return fournisseurSearchRepository.search(queryStringQuery(query), pageable).map(fournisseurMapper::toDto);
    }
}
