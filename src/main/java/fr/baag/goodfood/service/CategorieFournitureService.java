package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.CategorieFourniture;
import fr.baag.goodfood.repository.CategorieFournitureRepository;
import fr.baag.goodfood.repository.search.CategorieFournitureSearchRepository;
import fr.baag.goodfood.service.dto.CategorieFournitureDTO;
import fr.baag.goodfood.service.mapper.CategorieFournitureMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CategorieFourniture}.
 */
@Service
@Transactional
public class CategorieFournitureService {

    private final Logger log = LoggerFactory.getLogger(CategorieFournitureService.class);

    private final CategorieFournitureRepository categorieFournitureRepository;

    private final CategorieFournitureMapper categorieFournitureMapper;

    private final CategorieFournitureSearchRepository categorieFournitureSearchRepository;

    public CategorieFournitureService(CategorieFournitureRepository categorieFournitureRepository, CategorieFournitureMapper categorieFournitureMapper, CategorieFournitureSearchRepository categorieFournitureSearchRepository) {
        this.categorieFournitureRepository = categorieFournitureRepository;
        this.categorieFournitureMapper = categorieFournitureMapper;
        this.categorieFournitureSearchRepository = categorieFournitureSearchRepository;
    }

    /**
     * Save a categorieFourniture.
     *
     * @param categorieFournitureDTO the entity to save.
     * @return the persisted entity.
     */
    public CategorieFournitureDTO save(CategorieFournitureDTO categorieFournitureDTO) {
        log.debug("Request to save CategorieFourniture : {}", categorieFournitureDTO);
        CategorieFourniture categorieFourniture = categorieFournitureMapper.toEntity(categorieFournitureDTO);
        categorieFourniture = categorieFournitureRepository.save(categorieFourniture);
        CategorieFournitureDTO result = categorieFournitureMapper.toDto(categorieFourniture);
        categorieFournitureSearchRepository.save(categorieFourniture);
        return result;
    }

    /**
     * Get all the categorieFournitures.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieFournitureDTO> findAll() {
        log.debug("Request to get all CategorieFournitures");
        return categorieFournitureRepository.findAll().stream()
            .map(categorieFournitureMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one categorieFourniture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CategorieFournitureDTO> findOne(Long id) {
        log.debug("Request to get CategorieFourniture : {}", id);
        return categorieFournitureRepository.findById(id)
            .map(categorieFournitureMapper::toDto);
    }

    /**
     * Delete the categorieFourniture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CategorieFourniture : {}", id);
        categorieFournitureRepository.deleteById(id);
        categorieFournitureSearchRepository.deleteById(id);
    }

    /**
     * Search for the categorieFourniture corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieFournitureDTO> search(String query) {
        log.debug("Request to search CategorieFournitures for query {}", query);
        return StreamSupport
            .stream(categorieFournitureSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(categorieFournitureMapper::toDto)
        .collect(Collectors.toList());
    }
}
