package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.PersonnePhysique;
import fr.baag.goodfood.repository.PersonnePhysiqueRepository;
import fr.baag.goodfood.repository.UserRepository;
import fr.baag.goodfood.repository.search.PersonnePhysiqueSearchRepository;
import fr.baag.goodfood.service.dto.PersonnePhysiqueDTO;
import fr.baag.goodfood.service.mapper.PersonnePhysiqueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link PersonnePhysique}.
 */
@Service
@Transactional
public class PersonnePhysiqueService {

    private final Logger log = LoggerFactory.getLogger(PersonnePhysiqueService.class);

    private final PersonnePhysiqueRepository personnePhysiqueRepository;

    private final PersonnePhysiqueMapper personnePhysiqueMapper;

    private final PersonnePhysiqueSearchRepository personnePhysiqueSearchRepository;

    private final UserRepository userRepository;

    public PersonnePhysiqueService(PersonnePhysiqueRepository personnePhysiqueRepository,
            PersonnePhysiqueMapper personnePhysiqueMapper,
            PersonnePhysiqueSearchRepository personnePhysiqueSearchRepository, UserRepository userRepository) {
        this.personnePhysiqueRepository = personnePhysiqueRepository;
        this.personnePhysiqueMapper = personnePhysiqueMapper;
        this.personnePhysiqueSearchRepository = personnePhysiqueSearchRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a personnePhysique.
     *
     * @param personnePhysiqueDTO the entity to save.
     * @return the persisted entity.
     */
    public PersonnePhysiqueDTO save(PersonnePhysiqueDTO personnePhysiqueDTO) {
        log.debug("Request to save PersonnePhysique : {}", personnePhysiqueDTO);
        PersonnePhysique personnePhysique = personnePhysiqueMapper.toEntity(personnePhysiqueDTO);
        if (personnePhysiqueDTO.getUserId() != null ){
            Long userId = personnePhysiqueDTO.getUserId();
            userRepository.findById(userId).ifPresent(personnePhysique::user);
        }
        personnePhysique = personnePhysiqueRepository.save(personnePhysique);
        PersonnePhysiqueDTO result = personnePhysiqueMapper.toDto(personnePhysique);
        personnePhysiqueSearchRepository.save(personnePhysique);
        return result;
    }

    /**
     * Get all the personnePhysiques.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonnePhysiqueDTO> findAll() {
        log.debug("Request to get all PersonnePhysiques");
        return personnePhysiqueRepository.findAll().stream().map(personnePhysiqueMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one personnePhysique by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PersonnePhysiqueDTO> findOne(Long id) {
        log.debug("Request to get PersonnePhysique : {}", id);
        return personnePhysiqueRepository.findById(id).map(personnePhysiqueMapper::toDto);
    }

    /**
     * Delete the personnePhysique by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PersonnePhysique : {}", id);
        personnePhysiqueRepository.deleteById(id);
        personnePhysiqueSearchRepository.deleteById(id);
    }

    /**
     * Search for the personnePhysique corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonnePhysiqueDTO> search(String query) {
        log.debug("Request to search PersonnePhysiques for query {}", query);
        return StreamSupport
                .stream(personnePhysiqueSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .map(personnePhysiqueMapper::toDto).collect(Collectors.toList());
    }
}
