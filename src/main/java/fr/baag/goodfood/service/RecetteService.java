package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.Recette;
import fr.baag.goodfood.repository.RecetteRepository;
import fr.baag.goodfood.repository.search.RecetteSearchRepository;
import fr.baag.goodfood.service.dto.RecetteDTO;
import fr.baag.goodfood.service.mapper.RecetteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Recette}.
 */
@Service
@Transactional
public class RecetteService {

    private final Logger log = LoggerFactory.getLogger(RecetteService.class);

    private final RecetteRepository recetteRepository;

    private final RecetteMapper recetteMapper;

    private final RecetteSearchRepository recetteSearchRepository;

    public RecetteService(RecetteRepository recetteRepository, RecetteMapper recetteMapper,
            RecetteSearchRepository recetteSearchRepository) {
        this.recetteRepository = recetteRepository;
        this.recetteMapper = recetteMapper;
        this.recetteSearchRepository = recetteSearchRepository;
    }

    /**
     * Save a recette.
     *
     * @param recetteDTO the entity to save.
     * @return the persisted entity.
     */
    public RecetteDTO save(RecetteDTO recetteDTO) {
        log.debug("Request to save Recette : {}", recetteDTO);
        Recette recette = recetteMapper.toEntity(recetteDTO);
        recette = recetteRepository.save(recette);
        RecetteDTO result = recetteMapper.toDto(recette);
        recetteSearchRepository.save(recette);
        return result;
    }

    /**
     * Get all the recettes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecetteDTO> findAll() {
        log.debug("Request to get all Recettes");
        return recetteRepository.findAll().stream().map(recetteMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one recette by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecetteDTO> findOne(Long id) {
        log.debug("Request to get Recette : {}", id);
        return recetteRepository.findById(id).map(recetteMapper::toDto);
    }

    /**
     * Delete the recette by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Recette : {}", id);
        recetteRepository.deleteById(id);
        recetteSearchRepository.deleteById(id);
    }

    /**
     * Search for the recette corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RecetteDTO> search(String query) {
        log.debug("Request to search Recettes for query {}", query);
        return StreamSupport.stream(recetteSearchRepository.search(queryStringQuery(query)).spliterator(),
                // .stream(recetteSearchRepository.search(queryStringQuery(query).defaultField("titre")).spliterator(),
                false).map(recetteMapper::toDto).collect(Collectors.toList());
    }
}
