package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.PlateauRepas;
import fr.baag.goodfood.repository.PlateauRepasRepository;
import fr.baag.goodfood.repository.search.PlateauRepasSearchRepository;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;
import fr.baag.goodfood.service.dto.RecetteDTO;
import fr.baag.goodfood.service.mapper.PlateauRepasMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link PlateauRepas}.
 */
@Service
@Transactional
public class PlateauRepasService {

    private final Logger log = LoggerFactory.getLogger(PlateauRepasService.class);

    private final PlateauRepasRepository plateauRepasRepository;

    private final PlateauRepasMapper plateauRepasMapper;

    private final PlateauRepasSearchRepository plateauRepasSearchRepository;

    public PlateauRepasService(PlateauRepasRepository plateauRepasRepository, PlateauRepasMapper plateauRepasMapper,
            PlateauRepasSearchRepository plateauRepasSearchRepository) {
        this.plateauRepasRepository = plateauRepasRepository;
        this.plateauRepasMapper = plateauRepasMapper;
        this.plateauRepasSearchRepository = plateauRepasSearchRepository;
    }

    /**
     * Save a plateauRepas.
     *
     * @param plateauRepasDTO the entity to save.
     * @return the persisted entity.
     */
    public PlateauRepasDTO save(PlateauRepasDTO plateauRepasDTO) {
        log.debug("Request to save PlateauRepas : {}", plateauRepasDTO);
        PlateauRepas plateauRepas = plateauRepasMapper.toEntity(plateauRepasDTO);
        plateauRepas = plateauRepasRepository.save(plateauRepas);
        PlateauRepasDTO result = plateauRepasMapper.toDto(plateauRepas);
        plateauRepasSearchRepository.save(plateauRepas);
        return result;
    }

    /**
     * Get all the plateauRepas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PlateauRepasDTO> findAll() {
        log.debug("Request to get all PlateauRepas");
        return plateauRepasRepository.findAll().stream().map(plateauRepasMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one plateauRepas by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PlateauRepasDTO> findOne(Long id) {
        log.debug("Request to get PlateauRepas : {}", id);
        return plateauRepasRepository.findById(id).map(plateauRepasMapper::toDto);
    }

    /**
     * Delete the plateauRepas by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PlateauRepas : {}", id);
        plateauRepasRepository.deleteById(id);
        plateauRepasSearchRepository.deleteById(id);
    }

    /**
     * Search for the plateauRepas corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PlateauRepasDTO> search(String query) {
        log.debug("Request to search PlateauRepas for query {}", query);
        return StreamSupport.stream(plateauRepasSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .map(plateauRepasMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Ajoute une recette à un PlateauRepas.
     *
     * @param recette La recette à ajouter au plateauRepas.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public PlateauRepasDTO ajouteRecette(PlateauRepasDTO plateau, RecetteDTO recette) {
        log.debug("Request to add recette #{} to PlateauRepas #{}", recette.getId(), plateau.getId());
        Set<RecetteDTO> recetteExistante = plateau.getRecettes().stream()
                .filter(r -> r.getType().equals(recette.getType())).collect(Collectors.toSet());
        if (!recetteExistante.isEmpty()) {
            return null;
        }
        plateau.getRecettes().add(recette);
        return plateauRepasMapper.toDto(plateauRepasRepository.save(plateauRepasMapper.toEntity(plateau)));
    }
}
