package fr.baag.goodfood.service;

import fr.baag.goodfood.domain.CategorieBoisson;
import fr.baag.goodfood.repository.CategorieBoissonRepository;
import fr.baag.goodfood.repository.search.CategorieBoissonSearchRepository;
import fr.baag.goodfood.service.dto.CategorieBoissonDTO;
import fr.baag.goodfood.service.mapper.CategorieBoissonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CategorieBoisson}.
 */
@Service
@Transactional
public class CategorieBoissonService {

    private final Logger log = LoggerFactory.getLogger(CategorieBoissonService.class);

    private final CategorieBoissonRepository categorieBoissonRepository;

    private final CategorieBoissonMapper categorieBoissonMapper;

    private final CategorieBoissonSearchRepository categorieBoissonSearchRepository;

    public CategorieBoissonService(CategorieBoissonRepository categorieBoissonRepository, CategorieBoissonMapper categorieBoissonMapper, CategorieBoissonSearchRepository categorieBoissonSearchRepository) {
        this.categorieBoissonRepository = categorieBoissonRepository;
        this.categorieBoissonMapper = categorieBoissonMapper;
        this.categorieBoissonSearchRepository = categorieBoissonSearchRepository;
    }

    /**
     * Save a categorieBoisson.
     *
     * @param categorieBoissonDTO the entity to save.
     * @return the persisted entity.
     */
    public CategorieBoissonDTO save(CategorieBoissonDTO categorieBoissonDTO) {
        log.debug("Request to save CategorieBoisson : {}", categorieBoissonDTO);
        CategorieBoisson categorieBoisson = categorieBoissonMapper.toEntity(categorieBoissonDTO);
        categorieBoisson = categorieBoissonRepository.save(categorieBoisson);
        CategorieBoissonDTO result = categorieBoissonMapper.toDto(categorieBoisson);
        categorieBoissonSearchRepository.save(categorieBoisson);
        return result;
    }

    /**
     * Get all the categorieBoissons.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieBoissonDTO> findAll() {
        log.debug("Request to get all CategorieBoissons");
        return categorieBoissonRepository.findAll().stream()
            .map(categorieBoissonMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one categorieBoisson by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CategorieBoissonDTO> findOne(Long id) {
        log.debug("Request to get CategorieBoisson : {}", id);
        return categorieBoissonRepository.findById(id)
            .map(categorieBoissonMapper::toDto);
    }

    /**
     * Delete the categorieBoisson by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CategorieBoisson : {}", id);
        categorieBoissonRepository.deleteById(id);
        categorieBoissonSearchRepository.deleteById(id);
    }

    /**
     * Search for the categorieBoisson corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CategorieBoissonDTO> search(String query) {
        log.debug("Request to search CategorieBoissons for query {}", query);
        return StreamSupport
            .stream(categorieBoissonSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(categorieBoissonMapper::toDto)
        .collect(Collectors.toList());
    }
}
