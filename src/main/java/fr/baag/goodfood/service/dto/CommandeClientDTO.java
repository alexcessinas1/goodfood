package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.CommandeClient} entity.
 */
public class CommandeClientDTO implements Serializable {
    
    private Long id;

    private String codePromo;


    private Long clientId;

    private Long employeId;

    private Long commandeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodePromo() {
        return codePromo;
    }

    public void setCodePromo(String codePromo) {
        this.codePromo = codePromo;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getEmployeId() {
        return employeId;
    }

    public void setEmployeId(Long employeId) {
        this.employeId = employeId;
    }

    public Long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Long commandeId) {
        this.commandeId = commandeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandeClientDTO)) {
            return false;
        }

        return id != null && id.equals(((CommandeClientDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommandeClientDTO{" +
            "id=" + getId() +
            ", codePromo='" + getCodePromo() + "'" +
            ", clientId=" + getClientId() +
            ", employeId=" + getEmployeId() +
            ", commandeId=" + getCommandeId() +
            "}";
    }
}
