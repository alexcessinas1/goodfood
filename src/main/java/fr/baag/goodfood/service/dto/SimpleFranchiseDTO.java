package fr.baag.goodfood.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Franchise} entity.
 */
public class SimpleFranchiseDTO implements Serializable {

    private Long id;

    private AdresseDTO adresseFranchise;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdresseDTO getAdresseFranchise() {
        return adresseFranchise;
    }

    public void setAdresseFranchise(AdresseDTO adresse) {
        this.adresseFranchise = adresse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SimpleFranchiseDTO)) {
            return false;
        }

        return id != null && id.equals(((SimpleFranchiseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FranchiseDTO{" +
            "id=" + getId() +
            ", adresseFranchiseIdId=" + getAdresseFranchise() +
            "}";
    }
}
