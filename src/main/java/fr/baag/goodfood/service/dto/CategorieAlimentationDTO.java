package fr.baag.goodfood.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.CategorieAlimentation} entity.
 */
public class CategorieAlimentationDTO implements Serializable {
    
    private Long id;

    private Boolean vegan;

    private Boolean vegetarien;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean isVegetarien() {
        return vegetarien;
    }

    public void setVegetarien(Boolean vegetarien) {
        this.vegetarien = vegetarien;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieAlimentationDTO)) {
            return false;
        }

        return id != null && id.equals(((CategorieAlimentationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieAlimentationDTO{" +
            "id=" + getId() +
            ", vegan='" + isVegan() + "'" +
            ", vegetarien='" + isVegetarien() + "'" +
            "}";
    }
}
