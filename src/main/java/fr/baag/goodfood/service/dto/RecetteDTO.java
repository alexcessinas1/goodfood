package fr.baag.goodfood.service.dto;

import java.io.Serializable;
import java.util.Set;

import fr.baag.goodfood.domain.enumeration.Plat;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Recette} entity.
 */
public class RecetteDTO implements Serializable {

    private Long id;

    private String titre;

    private String description;

    private Plat type;

    private Set<ProduitDTO> produits;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Plat getType() {
        return type;
    }

    public void setType(Plat type) {
        this.type = type;
    }

    public Set<ProduitDTO> getProduits() {
        return produits;
    }

    public void setProduits(Set<ProduitDTO> produits) {
        this.produits = produits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecetteDTO)) {
            return false;
        }

        return id != null && id.equals(((RecetteDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecetteDTO{" + "id=" + getId() + ", titre='" + getTitre() + "'" + ", description='" + getDescription()
                + "'" + ", type='" + getType() + "'" + "}";
    }

}
