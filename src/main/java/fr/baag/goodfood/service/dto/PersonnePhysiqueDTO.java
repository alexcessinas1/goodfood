package fr.baag.goodfood.service.dto;

import javax.persistence.Column;
import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.PersonnePhysique} entity.
 */
public class PersonnePhysiqueDTO implements Serializable {

    private Long id;

    private LocalDate dateNaissance;

    private String nom;

    private String email;

    private String phone;

    private Long userId;
    private String userLogin;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNom() {
        return nom;
    }

    public PersonnePhysiqueDTO setNom(String nom) {
        this.nom = nom;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PersonnePhysiqueDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public PersonnePhysiqueDTO setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonnePhysiqueDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonnePhysiqueDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "PersonnePhysiqueDTO{" +
                "id=" + id +
                ", dateNaissance=" + dateNaissance +
                ", nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", userId=" + userId +
                ", userLogin='" + userLogin + '\'' +
                '}';
    }
}
