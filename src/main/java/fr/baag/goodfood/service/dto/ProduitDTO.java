package fr.baag.goodfood.service.dto;

import java.time.LocalDate;
import java.util.Set;
import java.io.Serializable;
import fr.baag.goodfood.domain.enumeration.unite;
import fr.baag.goodfood.domain.enumeration.ProduitCategorie;
import fr.baag.goodfood.domain.Allergene;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Produit} entity.
 */
public class ProduitDTO implements Serializable {

    private Long id;

    private String eid;

    private String nom;

    private Float quantite;

    private unite quantiteUnite;

    private String description;

    private Float prixCatalogue;

    private Float prixStandard;

    private Boolean venteOuverte;

    private String origine;

    private ProduitCategorie produitCategorie;

    private Set<Allergene> allergenes;

    private String numeroLot;

    private LocalDate dateLot;

    private Long boissonId;

    private Long alimentId;

    private Long fournitureId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Float getQuantite() {
        return quantite;
    }

    public void setQuantite(Float quantite) {
        this.quantite = quantite;
    }

    public unite getQuantiteUnite() {
        return quantiteUnite;
    }

    public void setQuantiteUnite(unite quantiteUnite) {
        this.quantiteUnite = quantiteUnite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrixCatalogue() {
        return prixCatalogue;
    }

    public void setPrixCatalogue(Float prixCatalogue) {
        this.prixCatalogue = prixCatalogue;
    }

    public Float getPrixStandard() {
        return prixStandard;
    }

    public void setPrixStandard(Float prixStandard) {
        this.prixStandard = prixStandard;
    }

    public Boolean isVenteOuverte() {
        return venteOuverte;
    }

    public void setVenteOuverte(Boolean venteOuverte) {
        this.venteOuverte = venteOuverte;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public ProduitCategorie getProduitCategorie() {
        return produitCategorie;
    }

    public void setProduitCategorie(ProduitCategorie produitCategorie) {
        this.produitCategorie = produitCategorie;
    }

    public Set<Allergene> getAllergenes() {
        return allergenes;
    }

    public void setAllergenes(Set<Allergene> allergenes) {
        this.allergenes = allergenes;
    }

    public String getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public LocalDate getDateLot() {
        return dateLot;
    }

    public void setDateLot(LocalDate dateLot) {
        this.dateLot = dateLot;
    }

    public Long getBoissonId() {
        return boissonId;
    }

    public void setBoissonId(Long categorieBoissonId) {
        this.boissonId = categorieBoissonId;
    }

    public Long getAlimentId() {
        return alimentId;
    }

    public void setAlimentId(Long categorieAlimentationId) {
        this.alimentId = categorieAlimentationId;
    }

    public Long getFournitureId() {
        return fournitureId;
    }

    public void setFournitureId(Long categorieFournitureId) {
        this.fournitureId = categorieFournitureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitDTO)) {
            return false;
        }

        return id != null && id.equals(((ProduitDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitDTO{" + "id=" + getId() + ", eid='" + getEid() + "'" + ", nom='" + getNom() + "'" + ", quantite="
                + getQuantite() + ", quantiteUnite='" + getQuantiteUnite() + "'" + ", description='" + getDescription()
                + "'" + ", prixCatalogue=" + getPrixCatalogue() + ", prixStandard=" + getPrixStandard()
                + ", venteOuverte='" + isVenteOuverte() + "'" + ", origine='" + getOrigine() + "'"
                + ", produitCategorie='" + getProduitCategorie() + "'" + ", allergenes='" + getAllergenes() + "'"
                + ", numeroLot='" + getNumeroLot() + "'" + ", dateLot='" + getDateLot() + "'" + ", boissonId="
                + getBoissonId() + ", alimentId=" + getAlimentId() + ", fournitureId=" + getFournitureId() + "}";
    }

}
