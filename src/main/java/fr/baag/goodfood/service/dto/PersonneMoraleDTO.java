package fr.baag.goodfood.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.PersonneMorale} entity.
 */
public class PersonneMoraleDTO implements Serializable {

    private Long id;

    private String raisonSociale;

    private String siret;

    private Long userId;
    private String userLogin;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonneMoraleDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonneMoraleDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonneMoraleDTO{" + "id=" + getId() + ", raisonSociale='" + getRaisonSociale() + "'" + ", siret='"
                + getSiret() + ", userLogin='" + getUserLogin() + "'" + ", userId=" + getUserId() + "}";
    }
}
