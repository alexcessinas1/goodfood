package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import fr.baag.goodfood.domain.enumeration.PosteEnum;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Employe} entity.
 */
public class EmployeDTO implements Serializable {
    
    private Long id;

    private PosteEnum poste;

    private String notes;


    private Long franchiseId;

    private Long adresseId;

    private Long personnePhysiqueId;

    private Long personneMoraleId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PosteEnum getPoste() {
        return poste;
    }

    public void setPoste(PosteEnum poste) {
        this.poste = poste;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public Long getAdresseId() {
        return adresseId;
    }

    public void setAdresseId(Long adresseId) {
        this.adresseId = adresseId;
    }

    public Long getPersonnePhysiqueId() {
        return personnePhysiqueId;
    }

    public void setPersonnePhysiqueId(Long personnePhysiqueId) {
        this.personnePhysiqueId = personnePhysiqueId;
    }

    public Long getPersonneMoraleId() {
        return personneMoraleId;
    }

    public void setPersonneMoraleId(Long personneMoraleId) {
        this.personneMoraleId = personneMoraleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeDTO)) {
            return false;
        }

        return id != null && id.equals(((EmployeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeDTO{" +
            "id=" + getId() +
            ", poste='" + getPoste() + "'" +
            ", notes='" + getNotes() + "'" +
            ", franchiseId=" + getFranchiseId() +
            ", adresseId=" + getAdresseId() +
            ", personnePhysiqueId=" + getPersonnePhysiqueId() +
            ", personneMoraleId=" + getPersonneMoraleId() +
            "}";
    }
}
