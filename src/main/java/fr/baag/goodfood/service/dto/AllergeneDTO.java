package fr.baag.goodfood.service.dto;

import java.io.Serializable;
import fr.baag.goodfood.domain.enumeration.AllergeneEnum;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Allergene} entity.
 */
public class AllergeneDTO implements Serializable {
	private Long id;
	private AllergeneEnum nom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AllergeneEnum getNom() {
		return nom;
	}

	public void setNom(AllergeneEnum nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "AllergeneDTO [id=" + id + ", nom=" + nom + "]";
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AllergeneDTO)) {
			return false;
		}

		return id != null && id.equals(((AllergeneDTO) o).id);
	}
}
