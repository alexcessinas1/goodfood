package fr.baag.goodfood.service.dto;

import java.io.Serializable;
import fr.baag.goodfood.domain.enumeration.PersonneType;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Client} entity.
 */
public class ClientDTO implements Serializable {

    private Long id;

    private PersonneType personneType;

    private Long adresseLivraisonId;

    private Long adresseFacturationId;

    private Long personnePhysiqueId;

    private Long personneMoraleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonneType getPersonneType() {
        return personneType;
    }

    public void setPersonneType(PersonneType personneType) {
        this.personneType = personneType;
    }

    public Long getAdresseLivraisonId() {
        return adresseLivraisonId;
    }

    public void setAdresseLivraisonId(Long adresseId) {
        this.adresseLivraisonId = adresseId;
    }

    public Long getAdresseFacturationId() {
        return adresseFacturationId;
    }

    public void setAdresseFacturationId(Long adresseId) {
        this.adresseFacturationId = adresseId;
    }

    public Long getPersonnePhysiqueId() {
        return personnePhysiqueId;
    }

    public void setPersonnePhysiqueId(Long personnePhysiqueId) {
        this.personnePhysiqueId = personnePhysiqueId;
    }

    public Long getPersonneMoraleId() {
        return personneMoraleId;
    }

    public void setPersonneMoraleId(Long personneMoraleId) {
        this.personneMoraleId = personneMoraleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientDTO)) {
            return false;
        }

        return id != null && id.equals(((ClientDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientDTO{" + "id=" + getId() + ", personneType='" + getPersonneType() + "'" + ", adresseLivraisonId="
                + getAdresseLivraisonId() + ", adresseFacturationId=" + getAdresseFacturationId()
                + ", personnePhysiqueId=" + getPersonnePhysiqueId() + ", personneMoraleId=" + getPersonneMoraleId()
                + "}";
    }
}
