package fr.baag.goodfood.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.CategorieBoisson} entity.
 */
public class CategorieBoissonDTO implements Serializable {
    
    private Long id;

    private Float degreAlcool;

    private String type;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDegreAlcool() {
        return degreAlcool;
    }

    public void setDegreAlcool(Float degreAlcool) {
        this.degreAlcool = degreAlcool;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieBoissonDTO)) {
            return false;
        }

        return id != null && id.equals(((CategorieBoissonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieBoissonDTO{" +
            "id=" + getId() +
            ", degreAlcool=" + getDegreAlcool() +
            ", type='" + getType() + "'" +
            "}";
    }
}
