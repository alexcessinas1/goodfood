package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Set;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.PlateauRepas} entity.
 */
public class PlateauRepasDTO implements Serializable {

    private Long id;

    private Float prixHT;

    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private Float tva;

    private String composition;

    private Boolean couvert;

    private Boolean serviette;

    private Long boissonId;

    private Set<RecetteDTO> recettes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(Float prixHT) {
        this.prixHT = prixHT;
    }

    public Float getTva() {
        return tva;
    }

    public void setTva(Float tva) {
        this.tva = tva;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public Boolean getCouvert() {
        return couvert;
    }

    public void setCouvert(Boolean couvert) {
        this.couvert = couvert;
    }

    public Boolean getServiette() {
        return serviette;
    }

    public void setServiette(Boolean serviette) {
        this.serviette = serviette;
    }

    public Long getBoissonId() {
        return boissonId;
    }

    public void setBoissonId(Long produitId) {
        this.boissonId = produitId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlateauRepasDTO)) {
            return false;
        }

        return id != null && id.equals(((PlateauRepasDTO) o).id);
    }

    public Set<RecetteDTO> getRecettes() {
        return recettes;
    }

    public void setRecettes(Set<RecetteDTO> recettes) {
        this.recettes = recettes;
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlateauRepasDTO{" + "id=" + getId() + ", prixHT=" + getPrixHT() + ", tva=" + getTva()
                + ", composition='" + getComposition() + "'" + ", couvert='" + getCouvert() + "'" + ", serviette='"
                + getServiette() + "'" + ", boissonId=" + getBoissonId() + "}";
    }
}
