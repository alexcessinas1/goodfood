package fr.baag.goodfood.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.CategorieFourniture} entity.
 */
public class CategorieFournitureDTO implements Serializable {
    
    private Long id;

    private String type;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieFournitureDTO)) {
            return false;
        }

        return id != null && id.equals(((CategorieFournitureDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieFournitureDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
