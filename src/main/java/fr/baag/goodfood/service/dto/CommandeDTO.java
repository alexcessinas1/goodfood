package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import fr.baag.goodfood.domain.enumeration.Statut;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Commande} entity.
 */
public class CommandeDTO implements Serializable {

    private Long id;

    @NotNull
    private Float prixTotal;

    @NotNull
    private String dateCommande;

    @NotNull
    private String dateStatut;

    @NotNull
    private Statut statut;

    @NotNull
    private Boolean aLivrer;

    @NotNull
    private String datePaiement;


    private Long idClient;

    private Long idFranchise;

    private Long idFournisseur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Float prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getDateStatut() {
        return dateStatut;
    }

    public void setDateStatut(String dateStatut) {
        this.dateStatut = dateStatut;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Boolean isaLivrer() {
        return aLivrer;
    }

    public void setaLivrer(Boolean aLivrer) {
        this.aLivrer = aLivrer;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long clientId) {
        this.idClient = clientId;
    }

    public Long getIdFranchise() {
        return idFranchise;
    }

    public void setIdFranchise(Long franchiseId) {
        this.idFranchise = franchiseId;
    }

    public Long getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(Long fournisseurId) {
        this.idFournisseur = fournisseurId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandeDTO)) {
            return false;
        }

        return id != null && id.equals(((CommandeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommandeDTO{" +
            "id=" + getId() +
            ", prixTotal=" + getPrixTotal() +
            ", dateCommande='" + getDateCommande() + "'" +
            ", dateStatut='" + getDateStatut() + "'" +
            ", statut='" + getStatut() + "'" +
            ", aLivrer='" + isaLivrer() + "'" +
            ", datePaiement='" + getDatePaiement() + "'" +
            ", idClient=" + getIdClient() +
            ", idFranchise=" + getIdFranchise() +
            ", idFournisseur=" + getIdFournisseur() +
            "}";
    }
}
