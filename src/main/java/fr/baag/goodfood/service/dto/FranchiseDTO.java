package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Franchise} entity.
 */
public class FranchiseDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String telephone;

    @NotNull
    private String email;

    @NotNull
    private String horaire;

    @NotNull
    private String siret;


    private Long adresseFranchiseIdId;
    private Set<ProduitDTO> stockFranchises = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public Long getAdresseFranchiseIdId() {
        return adresseFranchiseIdId;
    }

    public void setAdresseFranchiseIdId(Long adresseId) {
        this.adresseFranchiseIdId = adresseId;
    }

    public Set<ProduitDTO> getStockFranchises() {
        return stockFranchises;
    }

    public void setStockFranchises(Set<ProduitDTO> produits) {
        this.stockFranchises = produits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FranchiseDTO)) {
            return false;
        }

        return id != null && id.equals(((FranchiseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FranchiseDTO{" +
            "id=" + getId() +
            ", telephone='" + getTelephone() + "'" +
            ", email='" + getEmail() + "'" +
            ", horaire='" + getHoraire() + "'" +
            ", siret='" + getSiret() + "'" +
            ", adresseFranchiseIdId=" + getAdresseFranchiseIdId() +
            ", stockFranchises='" + getStockFranchises() + "'" +
            "}";
    }
}
