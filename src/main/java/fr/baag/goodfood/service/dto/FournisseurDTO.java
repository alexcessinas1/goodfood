package fr.baag.goodfood.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.baag.goodfood.domain.Fournisseur} entity.
 */
public class FournisseurDTO implements Serializable {

    private Long id;

    @NotNull
    private String prenom;

    @NotNull
    private String nom;

    @NotNull
    private String email;

    private String titre;

    @NotNull
    private String telEntreprise;

    private String telMaison;

    private String telMobile;

    private String numeroFax;

    private String pageWeb;

    private String notes;

    @NotNull
    private Long idAdresseId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTelEntreprise() {
        return telEntreprise;
    }

    public void setTelEntreprise(String telEntreprise) {
        this.telEntreprise = telEntreprise;
    }

    public String getTelMaison() {
        return telMaison;
    }

    public void setTelMaison(String telMaison) {
        this.telMaison = telMaison;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getNumeroFax() {
        return numeroFax;
    }

    public void setNumeroFax(String numeroFax) {
        this.numeroFax = numeroFax;
    }

    public String getPageWeb() {
        return pageWeb;
    }

    public void setPageWeb(String pageWeb) {
        this.pageWeb = pageWeb;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getIdAdresseId() {
        return idAdresseId;
    }

    public void setIdAdresseId(Long adresseId) {
        this.idAdresseId = adresseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FournisseurDTO)) {
            return false;
        }

        return id != null && id.equals(((FournisseurDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FournisseurDTO{" + "id=" + getId() + ", prenom='" + getPrenom() + "'" + ", nom='" + getNom() + "'"
                + ", email='" + getEmail() + "'" + ", titre='" + getTitre() + "'" + ", telEntreprise='"
                + getTelEntreprise() + "'" + ", telMaison='" + getTelMaison() + "'" + ", telMobile='" + getTelMobile()
                + "'" + ", numeroFax='" + getNumeroFax() + "'" + ", pageWeb='" + getPageWeb() + "'" + ", notes='"
                + getNotes() + "'" + ", idAdresseId=" + getIdAdresseId() + "}";
    }
}
