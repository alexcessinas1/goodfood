package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.CommandeClient;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CommandeClient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommandeClientRepository extends JpaRepository<CommandeClient, Long> {
}
