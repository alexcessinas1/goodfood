package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.CategorieFourniture;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CategorieFourniture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieFournitureRepository extends JpaRepository<CategorieFourniture, Long> {
}
