package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.PersonneMorale;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PersonneMorale entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonneMoraleRepository extends JpaRepository<PersonneMorale, Long> {
}
