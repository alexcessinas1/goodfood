package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.CategorieAlimentation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CategorieAlimentation} entity.
 */
public interface CategorieAlimentationSearchRepository extends ElasticsearchRepository<CategorieAlimentation, Long> {
}
