package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.PersonnePhysique;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link PersonnePhysique} entity.
 */
public interface PersonnePhysiqueSearchRepository extends ElasticsearchRepository<PersonnePhysique, Long> {
}
