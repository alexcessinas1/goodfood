package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.Allergene;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Allergene} entity.
 */
public interface AllergeneSearchRepository extends ElasticsearchRepository<Allergene, Long> {
}
