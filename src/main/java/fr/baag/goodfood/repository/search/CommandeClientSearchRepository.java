package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.CommandeClient;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CommandeClient} entity.
 */
public interface CommandeClientSearchRepository extends ElasticsearchRepository<CommandeClient, Long> {
}
