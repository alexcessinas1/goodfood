package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.Franchise;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Franchise} entity.
 */
public interface FranchiseSearchRepository extends ElasticsearchRepository<Franchise, Long> {
}
