package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.CategorieFourniture;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CategorieFourniture} entity.
 */
public interface CategorieFournitureSearchRepository extends ElasticsearchRepository<CategorieFourniture, Long> {
}
