package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.CategorieBoisson;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CategorieBoisson} entity.
 */
public interface CategorieBoissonSearchRepository extends ElasticsearchRepository<CategorieBoisson, Long> {
}
