package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.PlateauRepas;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link PlateauRepas} entity.
 */
public interface PlateauRepasSearchRepository extends ElasticsearchRepository<PlateauRepas, Long> {
}
