package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.PersonneMorale;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link PersonneMorale} entity.
 */
public interface PersonneMoraleSearchRepository extends ElasticsearchRepository<PersonneMorale, Long> {
}
