package fr.baag.goodfood.repository.search;

import fr.baag.goodfood.domain.Recette;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Recette} entity.
 */
public interface RecetteSearchRepository extends ElasticsearchRepository<Recette, Long> {
}
