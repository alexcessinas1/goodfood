package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.PlateauRepas;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PlateauRepas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlateauRepasRepository extends JpaRepository<PlateauRepas, Long> {
}
