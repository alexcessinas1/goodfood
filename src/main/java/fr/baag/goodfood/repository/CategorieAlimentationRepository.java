package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.CategorieAlimentation;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CategorieAlimentation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieAlimentationRepository extends JpaRepository<CategorieAlimentation, Long> {
}
