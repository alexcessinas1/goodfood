package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.CategorieBoisson;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CategorieBoisson entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorieBoissonRepository extends JpaRepository<CategorieBoisson, Long> {
}
