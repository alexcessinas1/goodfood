package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.PersonnePhysique;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PersonnePhysique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonnePhysiqueRepository extends JpaRepository<PersonnePhysique, Long> {
}
