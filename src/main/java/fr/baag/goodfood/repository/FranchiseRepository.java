package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.Franchise;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Franchise entity.
 */
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

    @Query(value = "select distinct franchise from Franchise franchise left join fetch franchise.stockFranchises",
        countQuery = "select count(distinct franchise) from Franchise franchise")
    Page<Franchise> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct franchise from Franchise franchise left join fetch franchise.stockFranchises")
    List<Franchise> findAllWithEagerRelationships();

    @Query("select franchise from Franchise franchise left join fetch franchise.stockFranchises where franchise.id =:id")
    Optional<Franchise> findOneWithEagerRelationships(@Param("id") Long id);
}
