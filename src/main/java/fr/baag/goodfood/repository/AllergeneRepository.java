package fr.baag.goodfood.repository;

import fr.baag.goodfood.domain.Allergene;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Allergene entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AllergeneRepository extends JpaRepository<Allergene, Long> {
}
