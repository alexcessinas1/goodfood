package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.domain.Franchise;
import fr.baag.goodfood.service.FranchiseService;
import fr.baag.goodfood.service.dto.SimpleFranchiseDTO;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.FranchiseDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.Franchise}.
 */
@RestController
@RequestMapping("/api")
public class FranchiseResource {

    private final Logger log = LoggerFactory.getLogger(FranchiseResource.class);

    private static final String ENTITY_NAME = "franchise";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FranchiseService franchiseService;

    public FranchiseResource(FranchiseService franchiseService) {
        this.franchiseService = franchiseService;
    }

    /**
     * {@code POST  /franchises} : Create a new franchise.
     *
     * @param franchiseDTO the franchiseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new franchiseDTO, or with status {@code 400 (Bad Request)} if the franchise has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/franchises")
    public ResponseEntity<FranchiseDTO> createFranchise(@Valid @RequestBody FranchiseDTO franchiseDTO) throws URISyntaxException {
        log.debug("REST request to save Franchise : {}", franchiseDTO);
        if (franchiseDTO.getId() != null) {
            throw new BadRequestAlertException("A new franchise cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FranchiseDTO result = franchiseService.save(franchiseDTO);
        return ResponseEntity.created(new URI("/api/franchises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /franchises} : Updates an existing franchise.
     *
     * @param franchiseDTO the franchiseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated franchiseDTO,
     * or with status {@code 400 (Bad Request)} if the franchiseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the franchiseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/franchises")
    public ResponseEntity<FranchiseDTO> updateFranchise(@Valid @RequestBody FranchiseDTO franchiseDTO) throws URISyntaxException {
        log.debug("REST request to update Franchise : {}", franchiseDTO);
        if (franchiseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FranchiseDTO result = franchiseService.save(franchiseDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, franchiseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /franchises} : get all the franchises.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of franchises in body.
     */
    @GetMapping("/franchises")
    public ResponseEntity<List<FranchiseDTO>> getAllFranchises(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Franchises");
        Page<FranchiseDTO> page;
        if (eagerload) {
            page = franchiseService.findAllWithEagerRelationships(pageable);
        } else {
            page = franchiseService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/franchisesAddress")
    public ResponseEntity<List<SimpleFranchiseDTO>> getAllFranchisesAddress(Pageable pageable) {
        log.debug("REST request to get a page of Franchises");
        Page<SimpleFranchiseDTO> page = franchiseService.findAllWithAddress(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /franchises/:id} : get the "id" franchise.
     *
     * @param id the id of the franchiseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the franchiseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/franchises/{id}")
    public ResponseEntity<FranchiseDTO> getFranchise(@PathVariable Long id) {
        log.debug("REST request to get Franchise : {}", id);
        Optional<FranchiseDTO> franchiseDTO = franchiseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(franchiseDTO);
    }

    /**
     * {@code DELETE  /franchises/:id} : delete the "id" franchise.
     *
     * @param id the id of the franchiseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/franchises/{id}")
    public ResponseEntity<Void> deleteFranchise(@PathVariable Long id) {
        log.debug("REST request to delete Franchise : {}", id);
        franchiseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/franchises?query=:query} : search for the franchise corresponding
     * to the query.
     *
     * @param query the query of the franchise search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/franchises")
    public ResponseEntity<List<FranchiseDTO>> searchFranchises(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Franchises for query {}", query);
        Page<FranchiseDTO> page = franchiseService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
