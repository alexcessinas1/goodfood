package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.CategorieFournitureService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.CategorieFournitureDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.CategorieFourniture}.
 */
@RestController
@RequestMapping("/api")
public class CategorieFournitureResource {

    private final Logger log = LoggerFactory.getLogger(CategorieFournitureResource.class);

    private static final String ENTITY_NAME = "categorieFourniture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategorieFournitureService categorieFournitureService;

    public CategorieFournitureResource(CategorieFournitureService categorieFournitureService) {
        this.categorieFournitureService = categorieFournitureService;
    }

    /**
     * {@code POST  /categorie-fournitures} : Create a new categorieFourniture.
     *
     * @param categorieFournitureDTO the categorieFournitureDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categorieFournitureDTO, or with status {@code 400 (Bad Request)} if the categorieFourniture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categorie-fournitures")
    public ResponseEntity<CategorieFournitureDTO> createCategorieFourniture(@RequestBody CategorieFournitureDTO categorieFournitureDTO) throws URISyntaxException {
        log.debug("REST request to save CategorieFourniture : {}", categorieFournitureDTO);
        if (categorieFournitureDTO.getId() != null) {
            throw new BadRequestAlertException("A new categorieFourniture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategorieFournitureDTO result = categorieFournitureService.save(categorieFournitureDTO);
        return ResponseEntity.created(new URI("/api/categorie-fournitures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categorie-fournitures} : Updates an existing categorieFourniture.
     *
     * @param categorieFournitureDTO the categorieFournitureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categorieFournitureDTO,
     * or with status {@code 400 (Bad Request)} if the categorieFournitureDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categorieFournitureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categorie-fournitures")
    public ResponseEntity<CategorieFournitureDTO> updateCategorieFourniture(@RequestBody CategorieFournitureDTO categorieFournitureDTO) throws URISyntaxException {
        log.debug("REST request to update CategorieFourniture : {}", categorieFournitureDTO);
        if (categorieFournitureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategorieFournitureDTO result = categorieFournitureService.save(categorieFournitureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, categorieFournitureDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categorie-fournitures} : get all the categorieFournitures.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categorieFournitures in body.
     */
    @GetMapping("/categorie-fournitures")
    public List<CategorieFournitureDTO> getAllCategorieFournitures() {
        log.debug("REST request to get all CategorieFournitures");
        return categorieFournitureService.findAll();
    }

    /**
     * {@code GET  /categorie-fournitures/:id} : get the "id" categorieFourniture.
     *
     * @param id the id of the categorieFournitureDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categorieFournitureDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categorie-fournitures/{id}")
    public ResponseEntity<CategorieFournitureDTO> getCategorieFourniture(@PathVariable Long id) {
        log.debug("REST request to get CategorieFourniture : {}", id);
        Optional<CategorieFournitureDTO> categorieFournitureDTO = categorieFournitureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categorieFournitureDTO);
    }

    /**
     * {@code DELETE  /categorie-fournitures/:id} : delete the "id" categorieFourniture.
     *
     * @param id the id of the categorieFournitureDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categorie-fournitures/{id}")
    public ResponseEntity<Void> deleteCategorieFourniture(@PathVariable Long id) {
        log.debug("REST request to delete CategorieFourniture : {}", id);
        categorieFournitureService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/categorie-fournitures?query=:query} : search for the categorieFourniture corresponding
     * to the query.
     *
     * @param query the query of the categorieFourniture search.
     * @return the result of the search.
     */
    @GetMapping("/_search/categorie-fournitures")
    public List<CategorieFournitureDTO> searchCategorieFournitures(@RequestParam String query) {
        log.debug("REST request to search CategorieFournitures for query {}", query);
        return categorieFournitureService.search(query);
    }
}
