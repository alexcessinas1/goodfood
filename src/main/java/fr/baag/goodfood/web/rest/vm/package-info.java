/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.baag.goodfood.web.rest.vm;
