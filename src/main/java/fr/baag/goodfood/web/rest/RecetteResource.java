package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.RecetteService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.RecetteDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.Recette}.
 */
@RestController
@RequestMapping("/api")
public class RecetteResource {

    private final Logger log = LoggerFactory.getLogger(RecetteResource.class);

    private static final String ENTITY_NAME = "recette";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecetteService recetteService;

    public RecetteResource(RecetteService recetteService) {
        this.recetteService = recetteService;
    }

    /**
     * {@code POST  /recettes} : Create a new recette.
     *
     * @param recetteDTO the recetteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recetteDTO, or with status {@code 400 (Bad Request)} if the recette has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/recettes")
    public ResponseEntity<RecetteDTO> createRecette(@RequestBody RecetteDTO recetteDTO) throws URISyntaxException {
        log.debug("REST request to save Recette : {}", recetteDTO);
        if (recetteDTO.getId() != null) {
            throw new BadRequestAlertException("A new recette cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecetteDTO result = recetteService.save(recetteDTO);
        return ResponseEntity.created(new URI("/api/recettes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /recettes} : Updates an existing recette.
     *
     * @param recetteDTO the recetteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recetteDTO,
     * or with status {@code 400 (Bad Request)} if the recetteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recetteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/recettes")
    public ResponseEntity<RecetteDTO> updateRecette(@RequestBody RecetteDTO recetteDTO) throws URISyntaxException {
        log.debug("REST request to update Recette : {}", recetteDTO);
        if (recetteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecetteDTO result = recetteService.save(recetteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recetteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /recettes} : get all the recettes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recettes in body.
     */
    @GetMapping("/recettes")
    public List<RecetteDTO> getAllRecettes() {
        log.debug("REST request to get all Recettes");
        return recetteService.findAll();
    }

    /**
     * {@code GET  /recettes/:id} : get the "id" recette.
     *
     * @param id the id of the recetteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recetteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/recettes/{id}")
    public ResponseEntity<RecetteDTO> getRecette(@PathVariable Long id) {
        log.debug("REST request to get Recette : {}", id);
        Optional<RecetteDTO> recetteDTO = recetteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recetteDTO);
    }

    /**
     * {@code DELETE  /recettes/:id} : delete the "id" recette.
     *
     * @param id the id of the recetteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/recettes/{id}")
    public ResponseEntity<Void> deleteRecette(@PathVariable Long id) {
        log.debug("REST request to delete Recette : {}", id);
        recetteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/recettes?query=:query} : search for the recette corresponding
     * to the query.
     *
     * @param query the query of the recette search.
     * @return the result of the search.
     */
    @GetMapping("/_search/recettes")
    public List<RecetteDTO> searchRecettes(@RequestParam String query) {
        log.debug("REST request to search Recettes for query {}", query);
        return recetteService.search(query);
    }
}
