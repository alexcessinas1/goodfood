package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.domain.Allergene;
import fr.baag.goodfood.repository.AllergeneRepository;
import fr.baag.goodfood.repository.search.AllergeneSearchRepository;
import fr.baag.goodfood.service.AllergeneService;
import fr.baag.goodfood.service.dto.AllergeneDTO;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.Allergene}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AllergeneResource {

    private final Logger log = LoggerFactory.getLogger(AllergeneResource.class);

    private static final String ENTITY_NAME = "allergene";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AllergeneRepository allergeneRepository;

    private final AllergeneSearchRepository allergeneSearchRepository;
    private final AllergeneService allergeneService;

    public AllergeneResource(AllergeneRepository allergeneRepository,
            AllergeneSearchRepository allergeneSearchRepository, AllergeneService allergeneService) {
        this.allergeneRepository = allergeneRepository;
        this.allergeneSearchRepository = allergeneSearchRepository;
        this.allergeneService = allergeneService;
    }

    /**
     * {@code POST  /allergenes} : Create a new allergene.
     *
     * @param allergene the allergene to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new allergene, or with status {@code 400 (Bad Request)} if
     *         the allergene has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/allergenes")
    public ResponseEntity<AllergeneDTO> createAllergene(@RequestBody AllergeneDTO allergeneDTO)
            throws URISyntaxException {
        log.debug("REST request to save Allergene : {}", allergeneDTO);
        if (allergeneDTO.getId() != null) {
            throw new BadRequestAlertException("A new allergene cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AllergeneDTO result = allergeneService.save(allergeneDTO);
        return ResponseEntity
                .created(new URI("/api/allergenes/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /allergenes} : Updates an existing allergene.
     *
     * @param allergene the allergene to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated allergene, or with status {@code 400 (Bad Request)} if
     *         the allergene is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the allergene couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/allergenes")
    public ResponseEntity<AllergeneDTO> updateAllergene(@RequestBody AllergeneDTO allergeneDTO)
            throws URISyntaxException {
        log.debug("REST request to update Allergene : {}", allergeneDTO);
        if (allergeneDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AllergeneDTO result = allergeneService.save(allergeneDTO);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, allergeneDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /allergenes} : get all the allergenes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of allergenes in body.
     */
    @GetMapping("/allergenes")
    public List<Allergene> getAllAllergenes() {
        log.debug("REST request to get all Allergenes");
        return allergeneRepository.findAll();
    }

    /**
     * {@code GET  /allergenes/:id} : get the "id" allergene.
     *
     * @param id the id of the allergene to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the allergene, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/allergenes/{id}")
    public ResponseEntity<Allergene> getAllergene(@PathVariable Long id) {
        log.debug("REST request to get Allergene : {}", id);
        Optional<Allergene> allergene = allergeneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(allergene);
    }

    /**
     * {@code DELETE  /allergenes/:id} : delete the "id" allergene.
     *
     * @param id the id of the allergene to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/allergenes/{id}")
    public ResponseEntity<Void> deleteAllergene(@PathVariable Long id) {
        log.debug("REST request to delete Allergene : {}", id);
        allergeneRepository.deleteById(id);
        allergeneSearchRepository.deleteById(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code SEARCH  /_search/allergenes?query=:query} : search for the allergene
     * corresponding to the query.
     *
     * @param query the query of the allergene search.
     * @return the result of the search.
     */
    @GetMapping("/_search/allergenes")
    public List<Allergene> searchAllergenes(@RequestParam String query) {
        log.debug("REST request to search Allergenes for query {}", query);
        return StreamSupport.stream(allergeneSearchRepository.search(queryStringQuery(query)).spliterator(), false)
                .collect(Collectors.toList());
    }
}
