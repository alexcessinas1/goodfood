package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.PlateauRepasService;
import fr.baag.goodfood.service.RecetteService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;
import fr.baag.goodfood.service.dto.RecetteDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.PlateauRepas}.
 */
@RestController
@RequestMapping("/api")
public class PlateauRepasResource {

    private final Logger log = LoggerFactory.getLogger(PlateauRepasResource.class);

    private static final String ENTITY_NAME = "plateauRepas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlateauRepasService plateauRepasService;
    private final RecetteService recetteService;

    public PlateauRepasResource(PlateauRepasService plateauRepasService, RecetteService recetteService) {
        this.plateauRepasService = plateauRepasService;
        this.recetteService = recetteService;
    }

    /**
     * {@code POST  /plateau-repas} : Create a new plateauRepas.
     *
     * @param plateauRepasDTO the plateauRepasDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new plateauRepasDTO, or with status
     *         {@code 400 (Bad Request)} if the plateauRepas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/plateau-repas")
    public ResponseEntity<PlateauRepasDTO> createPlateauRepas(@Valid @RequestBody PlateauRepasDTO plateauRepasDTO)
            throws URISyntaxException {
        log.debug("REST request to save PlateauRepas : {}", plateauRepasDTO);
        if (plateauRepasDTO.getId() != null) {
            throw new BadRequestAlertException("A new plateauRepas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlateauRepasDTO result = plateauRepasService.save(plateauRepasDTO);
        return ResponseEntity
                .created(new URI("/api/plateau-repas/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /plateau-repas} : Updates an existing plateauRepas.
     *
     * @param plateauRepasDTO the plateauRepasDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated plateauRepasDTO, or with status {@code 400 (Bad Request)}
     *         if the plateauRepasDTO is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the plateauRepasDTO couldn't
     *         be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/plateau-repas")
    public ResponseEntity<PlateauRepasDTO> updatePlateauRepas(@Valid @RequestBody PlateauRepasDTO plateauRepasDTO)
            throws URISyntaxException {
        log.debug("REST request to update PlateauRepas : {}", plateauRepasDTO);
        if (plateauRepasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id ", ENTITY_NAME, "idnull");
        }
        PlateauRepasDTO result = plateauRepasService.save(plateauRepasDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                plateauRepasDTO.getId().toString())).body(result);
    }

    /**
     * {@code GET  /plateau-repas} : get all the plateauRepas.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of plateauRepas in body.
     */
    @GetMapping("/plateau-repas")
    public List<PlateauRepasDTO> getAllPlateauRepas() {
        log.debug("REST request to get all PlateauRepas");
        return plateauRepasService.findAll();
    }

    /**
     * {@code GET  /plateau-repas/:id} : get the "id" plateauRepas.
     *
     * @param id the id of the plateauRepasDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the plateauRepasDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/plateau-repas/{id}")
    public ResponseEntity<PlateauRepasDTO> getPlateauRepas(@PathVariable Long id) {
        log.debug("REST request to get PlateauRepas : {}", id);
        Optional<PlateauRepasDTO> plateauRepasDTO = plateauRepasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plateauRepasDTO);
    }

    /**
     * {@code DELETE  /plateau-repas/:id} : delete the "id" plateauRepas.
     *
     * @param id the id of the plateauRepasDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/plateau-repas/{id}")
    public ResponseEntity<Void> deletePlateauRepas(@PathVariable Long id) {
        log.debug("REST request to delete PlateauRepas : {}", id);
        plateauRepasService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code SEARCH  /_search/plateau-repas?query=:query} : search for the
     * plateauRepas corresponding to the query.
     *
     * @param query the query of the plateauRepas search.
     * @return the result of the search.
     */
    @GetMapping("/_search/plateau-repas")
    public List<PlateauRepasDTO> searchPlateauRepas(@RequestParam String query) {
        log.debug("REST request to search PlateauRepas for query {}", query);
        return plateauRepasService.search(query);
    }

    /**
     * {@code PUT /commande-clients/{id}/plateau-repas/ajoute} : Ajoute un
     * PlateauRepas à an existing commandeClient.
     *
     * @param id              the id of the commandeClientDTO to retrieve.
     * @param plateauRepasDTO the plateau-repas à ajouter.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated commandeClientDTO, or with status
     *         {@code 400 (Bad Request)} if the id is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the commandeClientDTO couldn't
     *         be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */

    @PutMapping("/plateau-repas/{id}/recette/{idRecette}/ajoute")
    public ResponseEntity<PlateauRepasDTO> ajouteRecette(@PathVariable Long id, @PathVariable Long idRecette)
            throws URISyntaxException {
        log.debug("REST request to ajouter une recette au PlateauRepas {}: {}", id, idRecette);
        Optional<PlateauRepasDTO> plateauRepasDTO = plateauRepasService.findOne(id);
        if (!plateauRepasDTO.isPresent() && plateauRepasDTO.get().getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "notFound or id null");
        }
        Optional<RecetteDTO> recetteDTO = recetteService.findOne(idRecette);
        if (!recetteDTO.isPresent() && recetteDTO.get().getId() == null) {
            throw new BadRequestAlertException("Invalid id", "recette", "notFound or id null");
        }

        PlateauRepasDTO plateauSauve = plateauRepasService.ajouteRecette(plateauRepasDTO.get(), recetteDTO.get());
        if (plateauSauve == null) {
            throw new BadRequestAlertException("Le plateau repas contient déjà ce type de recette", ENTITY_NAME,
                    recetteDTO.get().getType().name());
        }
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plateauSauve.getId().toString()))
                .body(plateauSauve);
    }
}
