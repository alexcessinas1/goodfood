package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.CommandeClientService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.CommandeClientDTO;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.CommandeClient}.
 */
@RestController
@RequestMapping("/api")
public class CommandeClientResource {

    private final Logger log = LoggerFactory.getLogger(CommandeClientResource.class);

    private static final String ENTITY_NAME = "commandeClient";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommandeClientService commandeClientService;

    public CommandeClientResource(CommandeClientService commandeClientService) {
        this.commandeClientService = commandeClientService;
    }

    /**
     * {@code POST  /commande-clients} : Create a new commandeClient.
     *
     * @param commandeClientDTO the commandeClientDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new commandeClientDTO, or with status
     *         {@code 400 (Bad Request)} if the commandeClient has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commande-clients")
    public ResponseEntity<CommandeClientDTO> createCommandeClient(
            @Valid @RequestBody CommandeClientDTO commandeClientDTO) throws URISyntaxException {
        log.debug("REST request to save CommandeClient : {}", commandeClientDTO);
        if (commandeClientDTO.getId() != null) {
            throw new BadRequestAlertException("A new commandeClient cannot already have an ID", ENTITY_NAME,
                    "idexists");
        }
        if (Objects.isNull(commandeClientDTO.getCommandeId())) {
            throw new BadRequestAlertException("Une commandeClient doit avoir une commande de définie", ENTITY_NAME,
                    "null");
        }
        CommandeClientDTO result = commandeClientService.save(commandeClientDTO);
        return ResponseEntity
                .created(new URI("/api/commande-clients/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /commande-clients} : Updates an existing commandeClient.
     *
     * @param commandeClientDTO the commandeClientDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated commandeClientDTO, or with status
     *         {@code 400 (Bad Request)} if the commandeClientDTO is not valid, or
     *         with status {@code 500 (Internal Server Error)} if the
     *         commandeClientDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commande-clients")
    public ResponseEntity<CommandeClientDTO> updateCommandeClient(
            @Valid @RequestBody CommandeClientDTO commandeClientDTO) throws URISyntaxException {
        log.debug("REST request to update CommandeClient : {}", commandeClientDTO);
        if (commandeClientDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommandeClientDTO result = commandeClientService.save(commandeClientDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                commandeClientDTO.getId().toString())).body(result);
    }

    /**
     * {@code GET  /commande-clients} : get all the commandeClients.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of commandeClients in body.
     */
    @GetMapping("/commande-clients")
    public List<CommandeClientDTO> getAllCommandeClients() {
        log.debug("REST request to get all CommandeClients");
        return commandeClientService.findAll();
    }

    /**
     * {@code GET  /commande-clients/:id} : get the "id" commandeClient.
     *
     * @param id the id of the commandeClientDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the commandeClientDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commande-clients/{id}")
    public ResponseEntity<CommandeClientDTO> getCommandeClient(@PathVariable Long id) {
        log.debug("REST request to get CommandeClient : {}", id);
        Optional<CommandeClientDTO> commandeClientDTO = commandeClientService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commandeClientDTO);
    }

    /**
     * {@code PUT /commande-clients/{id}/plateau-repas/ajoute} : Ajoute un
     * PlateauRepas à an existing commandeClient.
     *
     * @param id              the id of the commandeClientDTO to retrieve.
     * @param plateauRepasDTO the plateau-repas à ajouter.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated commandeClientDTO, or with status
     *         {@code 400 (Bad Request)} if the id is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the commandeClientDTO couldn't
     *         be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commande-clients/{id}/plateau-repas/ajoute")
    public ResponseEntity<CommandeClientDTO> ajoutePlateauRepas(@PathVariable Long id,
            @Valid @RequestBody PlateauRepasDTO plateauRepasDTO) throws URISyntaxException {
        log.debug("REST request to ajouter un plateauRepas à CommandeClient {}: {}", id, plateauRepasDTO);
        Optional<CommandeClientDTO> commandeClientDTO = commandeClientService.findOne(id);
        if (!commandeClientDTO.isPresent() && commandeClientDTO.get().getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "notFound or id null");
        }
        CommandeClientDTO cmd = commandeClientService.ajoutePlateauRepas(id, plateauRepasDTO);
        if (cmd == null) {
            return ResponseEntity.unprocessableEntity().body(cmd);
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                cmd.getId().toString())).body(cmd);
    }

    /**
     * {@code DELETE  /commande-clients/:id} : delete the "id" commandeClient.
     *
     * @param id the id of the commandeClientDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commande-clients/{id}")
    public ResponseEntity<Void> deleteCommandeClient(@PathVariable Long id) {
        log.debug("REST request to delete CommandeClient : {}", id);
        commandeClientService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code SEARCH  /_search/commande-clients?query=:query} : search for the
     * commandeClient corresponding to the query.
     *
     * @param query the query of the commandeClient search.
     * @return the result of the search.
     */
    @GetMapping("/_search/commande-clients")
    public List<CommandeClientDTO> searchCommandeClients(@RequestParam String query) {
        log.debug("REST request to search CommandeClients for query {}", query);
        return commandeClientService.search(query);
    }
}
