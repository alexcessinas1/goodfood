package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.CategorieBoissonService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.CategorieBoissonDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.CategorieBoisson}.
 */
@RestController
@RequestMapping("/api")
public class CategorieBoissonResource {

    private final Logger log = LoggerFactory.getLogger(CategorieBoissonResource.class);

    private static final String ENTITY_NAME = "categorieBoisson";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategorieBoissonService categorieBoissonService;

    public CategorieBoissonResource(CategorieBoissonService categorieBoissonService) {
        this.categorieBoissonService = categorieBoissonService;
    }

    /**
     * {@code POST  /categorie-boissons} : Create a new categorieBoisson.
     *
     * @param categorieBoissonDTO the categorieBoissonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categorieBoissonDTO, or with status {@code 400 (Bad Request)} if the categorieBoisson has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categorie-boissons")
    public ResponseEntity<CategorieBoissonDTO> createCategorieBoisson(@RequestBody CategorieBoissonDTO categorieBoissonDTO) throws URISyntaxException {
        log.debug("REST request to save CategorieBoisson : {}", categorieBoissonDTO);
        if (categorieBoissonDTO.getId() != null) {
            throw new BadRequestAlertException("A new categorieBoisson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategorieBoissonDTO result = categorieBoissonService.save(categorieBoissonDTO);
        return ResponseEntity.created(new URI("/api/categorie-boissons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categorie-boissons} : Updates an existing categorieBoisson.
     *
     * @param categorieBoissonDTO the categorieBoissonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categorieBoissonDTO,
     * or with status {@code 400 (Bad Request)} if the categorieBoissonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categorieBoissonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categorie-boissons")
    public ResponseEntity<CategorieBoissonDTO> updateCategorieBoisson(@RequestBody CategorieBoissonDTO categorieBoissonDTO) throws URISyntaxException {
        log.debug("REST request to update CategorieBoisson : {}", categorieBoissonDTO);
        if (categorieBoissonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategorieBoissonDTO result = categorieBoissonService.save(categorieBoissonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, categorieBoissonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categorie-boissons} : get all the categorieBoissons.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categorieBoissons in body.
     */
    @GetMapping("/categorie-boissons")
    public List<CategorieBoissonDTO> getAllCategorieBoissons() {
        log.debug("REST request to get all CategorieBoissons");
        return categorieBoissonService.findAll();
    }

    /**
     * {@code GET  /categorie-boissons/:id} : get the "id" categorieBoisson.
     *
     * @param id the id of the categorieBoissonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categorieBoissonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categorie-boissons/{id}")
    public ResponseEntity<CategorieBoissonDTO> getCategorieBoisson(@PathVariable Long id) {
        log.debug("REST request to get CategorieBoisson : {}", id);
        Optional<CategorieBoissonDTO> categorieBoissonDTO = categorieBoissonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categorieBoissonDTO);
    }

    /**
     * {@code DELETE  /categorie-boissons/:id} : delete the "id" categorieBoisson.
     *
     * @param id the id of the categorieBoissonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categorie-boissons/{id}")
    public ResponseEntity<Void> deleteCategorieBoisson(@PathVariable Long id) {
        log.debug("REST request to delete CategorieBoisson : {}", id);
        categorieBoissonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/categorie-boissons?query=:query} : search for the categorieBoisson corresponding
     * to the query.
     *
     * @param query the query of the categorieBoisson search.
     * @return the result of the search.
     */
    @GetMapping("/_search/categorie-boissons")
    public List<CategorieBoissonDTO> searchCategorieBoissons(@RequestParam String query) {
        log.debug("REST request to search CategorieBoissons for query {}", query);
        return categorieBoissonService.search(query);
    }
}
