package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.service.CategorieAlimentationService;
import fr.baag.goodfood.web.rest.errors.BadRequestAlertException;
import fr.baag.goodfood.service.dto.CategorieAlimentationDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.baag.goodfood.domain.CategorieAlimentation}.
 */
@RestController
@RequestMapping("/api")
public class CategorieAlimentationResource {

    private final Logger log = LoggerFactory.getLogger(CategorieAlimentationResource.class);

    private static final String ENTITY_NAME = "categorieAlimentation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategorieAlimentationService categorieAlimentationService;

    public CategorieAlimentationResource(CategorieAlimentationService categorieAlimentationService) {
        this.categorieAlimentationService = categorieAlimentationService;
    }

    /**
     * {@code POST  /categorie-alimentations} : Create a new categorieAlimentation.
     *
     * @param categorieAlimentationDTO the categorieAlimentationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categorieAlimentationDTO, or with status {@code 400 (Bad Request)} if the categorieAlimentation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/categorie-alimentations")
    public ResponseEntity<CategorieAlimentationDTO> createCategorieAlimentation(@RequestBody CategorieAlimentationDTO categorieAlimentationDTO) throws URISyntaxException {
        log.debug("REST request to save CategorieAlimentation : {}", categorieAlimentationDTO);
        if (categorieAlimentationDTO.getId() != null) {
            throw new BadRequestAlertException("A new categorieAlimentation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategorieAlimentationDTO result = categorieAlimentationService.save(categorieAlimentationDTO);
        return ResponseEntity.created(new URI("/api/categorie-alimentations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /categorie-alimentations} : Updates an existing categorieAlimentation.
     *
     * @param categorieAlimentationDTO the categorieAlimentationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categorieAlimentationDTO,
     * or with status {@code 400 (Bad Request)} if the categorieAlimentationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categorieAlimentationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/categorie-alimentations")
    public ResponseEntity<CategorieAlimentationDTO> updateCategorieAlimentation(@RequestBody CategorieAlimentationDTO categorieAlimentationDTO) throws URISyntaxException {
        log.debug("REST request to update CategorieAlimentation : {}", categorieAlimentationDTO);
        if (categorieAlimentationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategorieAlimentationDTO result = categorieAlimentationService.save(categorieAlimentationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, categorieAlimentationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /categorie-alimentations} : get all the categorieAlimentations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categorieAlimentations in body.
     */
    @GetMapping("/categorie-alimentations")
    public List<CategorieAlimentationDTO> getAllCategorieAlimentations() {
        log.debug("REST request to get all CategorieAlimentations");
        return categorieAlimentationService.findAll();
    }

    /**
     * {@code GET  /categorie-alimentations/:id} : get the "id" categorieAlimentation.
     *
     * @param id the id of the categorieAlimentationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categorieAlimentationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/categorie-alimentations/{id}")
    public ResponseEntity<CategorieAlimentationDTO> getCategorieAlimentation(@PathVariable Long id) {
        log.debug("REST request to get CategorieAlimentation : {}", id);
        Optional<CategorieAlimentationDTO> categorieAlimentationDTO = categorieAlimentationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categorieAlimentationDTO);
    }

    /**
     * {@code DELETE  /categorie-alimentations/:id} : delete the "id" categorieAlimentation.
     *
     * @param id the id of the categorieAlimentationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/categorie-alimentations/{id}")
    public ResponseEntity<Void> deleteCategorieAlimentation(@PathVariable Long id) {
        log.debug("REST request to delete CategorieAlimentation : {}", id);
        categorieAlimentationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/categorie-alimentations?query=:query} : search for the categorieAlimentation corresponding
     * to the query.
     *
     * @param query the query of the categorieAlimentation search.
     * @return the result of the search.
     */
    @GetMapping("/_search/categorie-alimentations")
    public List<CategorieAlimentationDTO> searchCategorieAlimentations(@RequestParam String query) {
        log.debug("REST request to search CategorieAlimentations for query {}", query);
        return categorieAlimentationService.search(query);
    }
}
