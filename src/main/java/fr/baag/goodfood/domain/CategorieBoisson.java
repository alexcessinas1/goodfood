package fr.baag.goodfood.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A CategorieBoisson.
 */
@Entity
@Table(name = "categorie_boisson")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "categorieboisson")
public class CategorieBoisson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "degre_alcool")
    private Float degreAlcool;

    @Column(name = "type")
    private String type;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDegreAlcool() {
        return degreAlcool;
    }

    public CategorieBoisson degreAlcool(Float degreAlcool) {
        this.degreAlcool = degreAlcool;
        return this;
    }

    public void setDegreAlcool(Float degreAlcool) {
        this.degreAlcool = degreAlcool;
    }

    public String getType() {
        return type;
    }

    public CategorieBoisson type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieBoisson)) {
            return false;
        }
        return id != null && id.equals(((CategorieBoisson) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieBoisson{" +
            "id=" + getId() +
            ", degreAlcool=" + getDegreAlcool() +
            ", type='" + getType() + "'" +
            "}";
    }
}
