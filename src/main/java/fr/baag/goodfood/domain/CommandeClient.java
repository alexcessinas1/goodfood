package fr.baag.goodfood.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CommandeClient.
 */
@Entity
@Table(name = "commande_client")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "commandeclient")
public class CommandeClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code_promo")
    private String codePromo;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "client_id")
    @JsonIgnoreProperties(value = "commandeClients", allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = "commandeClients", allowSetters = true)
    private Employe employe;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(name = "commande_id")
    private Commande commande;

    @OneToMany(mappedBy = "commandeClient")
    private Set<PlateauRepas> plateauRepas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodePromo() {
        return codePromo;
    }

    public CommandeClient codePromo(String codePromo) {
        this.codePromo = codePromo;
        return this;
    }

    public void setCodePromo(String codePromo) {
        this.codePromo = codePromo;
    }

    public Client getClient() {
        return client;
    }

    public CommandeClient client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }

    public CommandeClient employe(Employe employe) {
        this.employe = employe;
        return this;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Commande getCommande() {
        return commande;
    }

    public CommandeClient commande(Commande commande) {
        this.commande = commande;
        return this;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Set<PlateauRepas> getPlateauRepas() {
        return plateauRepas;
    }

    public CommandeClient plateauRepas(Set<PlateauRepas> plateauRepas) {
        this.plateauRepas = plateauRepas;
        return this;
    }

    public CommandeClient addPlateauRepas(PlateauRepas plateauRepas) {
        this.plateauRepas.add(plateauRepas);
        plateauRepas.setCommandeClient(this);
        return this;
    }

    public CommandeClient removePlateauRepas(PlateauRepas plateauRepas) {
        this.plateauRepas.remove(plateauRepas);
        plateauRepas.setCommandeClient(null);
        return this;
    }

    public void setPlateauRepas(Set<PlateauRepas> plateauRepas) {
        this.plateauRepas = plateauRepas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandeClient)) {
            return false;
        }
        return id != null && id.equals(((CommandeClient) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommandeClient{" + "id=" + getId() + ", codePromo='" + getCodePromo() + "'" + "}";
    }
}
