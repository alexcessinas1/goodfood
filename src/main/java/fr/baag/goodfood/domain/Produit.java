package fr.baag.goodfood.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import fr.baag.goodfood.domain.enumeration.unite;

import fr.baag.goodfood.domain.enumeration.ProduitCategorie;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "produit")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "eid")
    private String eid;

    @Column(name = "nom")
    private String nom;

    @Column(name = "quantite")
    private Float quantite;

    @Enumerated(EnumType.STRING)
    @Column(name = "quantite_unite")
    private unite quantiteUnite;

    @Column(name = "description")
    private String description;

    @Column(name = "prix_catalogue")
    private Float prixCatalogue;

    @Column(name = "prix_standard")
    private Float prixStandard;

    @Column(name = "vente_ouverte")
    private Boolean venteOuverte;

    @Column(name = "origine")
    private String origine;

    @Enumerated(EnumType.STRING)
    @Column(name = "produit_categorie")
    private ProduitCategorie produitCategorie;

    @ManyToMany
    @JoinTable(name = "produit_allergene", joinColumns = @JoinColumn(name = "produit_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "allergene_id", referencedColumnName = "id"))
    private Set<Allergene> allergenes = new HashSet<>();

    @Column(name = "numero_lot")
    private String numeroLot;

    @Column(name = "date_lot")
    private LocalDate dateLot;

    @ManyToOne
    @JsonIgnoreProperties(value = "produits", allowSetters = true)
    private CategorieBoisson boisson;

    @ManyToOne
    @JsonIgnoreProperties(value = "produits", allowSetters = true)
    private CategorieAlimentation aliment;

    @ManyToOne
    @JsonIgnoreProperties(value = "produits", allowSetters = true)
    private CategorieFourniture fourniture;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public Produit eid(String eid) {
        this.eid = eid;
        return this;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getNom() {
        return nom;
    }

    public Produit nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Float getQuantite() {
        return quantite;
    }

    public Produit quantite(Float quantite) {
        this.quantite = quantite;
        return this;
    }

    public void setQuantite(Float quantite) {
        this.quantite = quantite;
    }

    public unite getQuantiteUnite() {
        return quantiteUnite;
    }

    public Produit quantiteUnite(unite quantiteUnite) {
        this.quantiteUnite = quantiteUnite;
        return this;
    }

    public void setQuantiteUnite(unite quantiteUnite) {
        this.quantiteUnite = quantiteUnite;
    }

    public String getDescription() {
        return description;
    }

    public Produit description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrixCatalogue() {
        return prixCatalogue;
    }

    public Produit prixCatalogue(Float prixCatalogue) {
        this.prixCatalogue = prixCatalogue;
        return this;
    }

    public void setPrixCatalogue(Float prixCatalogue) {
        this.prixCatalogue = prixCatalogue;
    }

    public Float getPrixStandard() {
        return prixStandard;
    }

    public Produit prixStandard(Float prixStandard) {
        this.prixStandard = prixStandard;
        return this;
    }

    public void setPrixStandard(Float prixStandard) {
        this.prixStandard = prixStandard;
    }

    public Boolean isVenteOuverte() {
        return venteOuverte;
    }

    public Produit venteOuverte(Boolean venteOuverte) {
        this.venteOuverte = venteOuverte;
        return this;
    }

    public void setVenteOuverte(Boolean venteOuverte) {
        this.venteOuverte = venteOuverte;
    }

    public String getOrigine() {
        return origine;
    }

    public Produit origine(String origine) {
        this.origine = origine;
        return this;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public ProduitCategorie getProduitCategorie() {
        return produitCategorie;
    }

    public Produit produitCategorie(ProduitCategorie produitCategorie) {
        this.produitCategorie = produitCategorie;
        return this;
    }

    public void setProduitCategorie(ProduitCategorie produitCategorie) {
        this.produitCategorie = produitCategorie;
    }

    public Set<Allergene> getAllergenes() {
        return allergenes;
    }

    public void setAllergenes(Set<Allergene> allergenes) {
        this.allergenes = allergenes;
    }

    public Produit allergenes(Set<Allergene> allergenes) {
        this.allergenes = allergenes;
        return this;
    }

    public String getNumeroLot() {
        return numeroLot;
    }

    public Produit numeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
        return this;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public LocalDate getDateLot() {
        return dateLot;
    }

    public Produit dateLot(LocalDate dateLot) {
        this.dateLot = dateLot;
        return this;
    }

    public void setDateLot(LocalDate dateLot) {
        this.dateLot = dateLot;
    }

    public CategorieBoisson getBoisson() {
        return boisson;
    }

    public Produit boisson(CategorieBoisson categorieBoisson) {
        this.boisson = categorieBoisson;
        return this;
    }

    public void setBoisson(CategorieBoisson categorieBoisson) {
        this.boisson = categorieBoisson;
    }

    public CategorieAlimentation getAliment() {
        return aliment;
    }

    public Produit aliment(CategorieAlimentation categorieAlimentation) {
        this.aliment = categorieAlimentation;
        return this;
    }

    public void setAliment(CategorieAlimentation categorieAlimentation) {
        this.aliment = categorieAlimentation;
    }

    public CategorieFourniture getFourniture() {
        return fourniture;
    }

    public Produit fourniture(CategorieFourniture categorieFourniture) {
        this.fourniture = categorieFourniture;
        return this;
    }

    public void setFourniture(CategorieFourniture categorieFourniture) {
        this.fourniture = categorieFourniture;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" + "id=" + getId() + ", eid='" + getEid() + "'" + ", nom='" + getNom() + "'" + ", quantite="
                + getQuantite() + ", quantiteUnite='" + getQuantiteUnite() + "'" + ", description='" + getDescription()
                + "'" + ", prixCatalogue=" + getPrixCatalogue() + ", prixStandard=" + getPrixStandard()
                + ", venteOuverte='" + isVenteOuverte() + "'" + ", origine='" + getOrigine() + "'"
                + ", produitCategorie='" + getProduitCategorie() + "'" + ", allergenes='" + getAllergenes() + "'"
                + ", numeroLot='" + getNumeroLot() + "'" + ", dateLot='" + getDateLot() + "'" + "}";
    }
}
