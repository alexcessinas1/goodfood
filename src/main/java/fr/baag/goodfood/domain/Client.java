package fr.baag.goodfood.domain;

import javax.persistence.*;

import org.hibernate.annotations.Where;
import java.io.Serializable;

import fr.baag.goodfood.domain.enumeration.PersonneType;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "personne_type")
    private PersonneType personneType;

    @OneToOne(optional = true)
    @JoinColumn(name = "adresse_livraison_id")
    private Adresse adresseLivraison;

    @OneToOne(optional = true)
    @JoinColumn(name = "adresse_facturation_id")
    private Adresse adresseFacturation;

    @ManyToOne
    @JoinColumn(name = "personne_physique_id")
    @Where(clause = "personne_type = 'PHYSIQUE'")
    private PersonnePhysique personnePhysique;

    @ManyToOne
    @JoinColumn(name = "personne_morale_id")
    @Where(clause = "personne_type = 'MORALE'")
    private PersonneMorale personneMorale;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonneType getPersonneType() {
        return personneType;
    }

    public Client personneType(PersonneType personneType) {
        this.personneType = personneType;
        return this;
    }

    public void setPersonneType(PersonneType personneType) {
        this.personneType = personneType;
    }

    public Adresse getAdresseLivraison() {
        return adresseLivraison;
    }

    public Client adresseLivraison(Adresse adresse) {
        this.adresseLivraison = adresse;
        return this;
    }

    public void setAdresseLivraison(Adresse adresse) {
        this.adresseLivraison = adresse;
    }

    public Adresse getAdresseFacturation() {
        return adresseFacturation;
    }

    public Client adresseFacturation(Adresse adresse) {
        this.adresseFacturation = adresse;
        return this;
    }

    public void setAdresseFacturation(Adresse adresse) {
        this.adresseFacturation = adresse;
    }

    public PersonnePhysique getPersonnePhysique() {
        return personnePhysique;
    }

    public Client personnePhysique(PersonnePhysique personnePhysique) {
        this.personnePhysique = personnePhysique;
        return this;
    }

    public void setPersonnePhysique(PersonnePhysique personnePhysique) {
        this.personnePhysique = personnePhysique;
    }

    public PersonneMorale getPersonneMorale() {
        return personneMorale;
    }

    public Client personneMorale(PersonneMorale personneMorale) {
        this.personneMorale = personneMorale;
        return this;
    }

    public void setPersonneMorale(PersonneMorale personneMorale) {
        this.personneMorale = personneMorale;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" + "id=" + getId() + ", personneType='" + getPersonneType() + "'" + "}";
    }
}
