package fr.baag.goodfood.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A CategorieAlimentation.
 */
@Entity
@Table(name = "categorie_alimentation")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "categoriealimentation")
public class CategorieAlimentation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "vegan")
    private Boolean vegan;

    @Column(name = "vegetarien")
    private Boolean vegetarien;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isVegan() {
        return vegan;
    }

    public CategorieAlimentation vegan(Boolean vegan) {
        this.vegan = vegan;
        return this;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean isVegetarien() {
        return vegetarien;
    }

    public CategorieAlimentation vegetarien(Boolean vegetarien) {
        this.vegetarien = vegetarien;
        return this;
    }

    public void setVegetarien(Boolean vegetarien) {
        this.vegetarien = vegetarien;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieAlimentation)) {
            return false;
        }
        return id != null && id.equals(((CategorieAlimentation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieAlimentation{" +
            "id=" + getId() +
            ", vegan='" + isVegan() + "'" +
            ", vegetarien='" + isVegetarien() + "'" +
            "}";
    }
}
