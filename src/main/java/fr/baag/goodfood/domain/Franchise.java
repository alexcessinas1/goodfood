package fr.baag.goodfood.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Franchise.
 */
@Entity
@Table(name = "franchise")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "franchise")
public class Franchise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "horaire", nullable = false)
    private String horaire;

    @NotNull
    @Column(name = "siret", nullable = false)
    private String siret;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = false)
    private Adresse adresseFranchiseId;

    @ManyToMany
    @JoinTable(name = "franchise_stock_franchise", joinColumns = @JoinColumn(name = "franchise_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "stock_franchise_id", referencedColumnName = "id"))
    private Set<Produit> stockFranchises = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelephone() {
        return telephone;
    }

    public Franchise telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public Franchise email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoraire() {
        return horaire;
    }

    public Franchise horaire(String horaire) {
        this.horaire = horaire;
        return this;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public String getSiret() {
        return siret;
    }

    public Franchise siret(String siret) {
        this.siret = siret;
        return this;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public Adresse getAdresseFranchiseId() {
        return adresseFranchiseId;
    }

    public Franchise adresseFranchiseId(Adresse adresse) {
        this.adresseFranchiseId = adresse;
        return this;
    }

    public void setAdresseFranchiseId(Adresse adresse) {
        this.adresseFranchiseId = adresse;
    }

    public Set<Produit> getStockFranchises() {
        return stockFranchises;
    }

    public Franchise stockFranchises(Set<Produit> produits) {
        this.stockFranchises = produits;
        return this;
    }

    public Franchise addStockFranchise(Produit produit) {
        this.stockFranchises.add(produit);
        // produit.getFranchiseStocks().add(this);
        return this;
    }

    public Franchise removeStockFranchise(Produit produit) {
        this.stockFranchises.remove(produit);
        // produit.getFranchiseStocks().remove(this);
        return this;
    }

    public void setStockFranchises(Set<Produit> produits) {
        this.stockFranchises = produits;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Franchise)) {
            return false;
        }
        return id != null && id.equals(((Franchise) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Franchise{" + "id=" + getId() + ", telephone='" + getTelephone() + "'" + ", email='" + getEmail() + "'"
                + ", horaire='" + getHoraire() + "'" + ", siret='" + getSiret() + "'" + "}";
    }
}
