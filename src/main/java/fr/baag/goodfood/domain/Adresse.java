package fr.baag.goodfood.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A Adresse.
 */
@Entity
@Table(name = "adresse")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "adresse")
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_voie")
    private String numeroVoie;

    @Column(name = "nom_voie")
    private String nomVoie;

    @Column(name = "code_postal")
    private String codePostal;

    @Column(name = "ville")
    private String ville;

    @Column(name = "complement_adresse")
    private String complementAdresse;

    @Column(name = "info_complementaire")
    private String infoComplementaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroVoie() {
        return numeroVoie;
    }

    public Adresse numeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
        return this;
    }

    public void setNumeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
    }

    public String getNomVoie() {
        return nomVoie;
    }

    public Adresse nomVoie(String nomVoie) {
        this.nomVoie = nomVoie;
        return this;
    }

    public void setNomVoie(String nomVoie) {
        this.nomVoie = nomVoie;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public Adresse codePostal(String codePostal) {
        this.codePostal = codePostal;
        return this;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public Adresse ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getComplementAdresse() {
        return complementAdresse;
    }

    public Adresse complementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
        return this;
    }

    public void setComplementAdresse(String complementAdresse) {
        this.complementAdresse = complementAdresse;
    }

    public String getInfoComplementaire() {
        return infoComplementaire;
    }

    public Adresse infoComplementaire(String infoComplementaire) {
        this.infoComplementaire = infoComplementaire;
        return this;
    }

    public void setInfoComplementaire(String infoComplementaire) {
        this.infoComplementaire = infoComplementaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Adresse)) {
            return false;
        }
        return id != null && id.equals(((Adresse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Adresse{" +
            "id=" + getId() +
            ", numeroVoie='" + getNumeroVoie() + "'" +
            ", nomVoie='" + getNomVoie() + "'" +
            ", codePostal='" + getCodePostal() + "'" +
            ", ville='" + getVille() + "'" +
            ", complementAdresse='" + getComplementAdresse() + "'" +
            ", infoComplementaire='" + getInfoComplementaire() + "'" +
            "}";
    }
}
