package fr.baag.goodfood.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import fr.baag.goodfood.domain.enumeration.Statut;

/**
 * A Commande.
 */
@Entity
@Table(name = "commande")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "commande")
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "prix_total", nullable = false)
    private Float prixTotal;

    @Column(name = "date_commande", nullable = false)
    private String dateCommande;

    @NotNull
    @Column(name = "date_statut", nullable = false)
    private String dateStatut;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statut", nullable = false)
    private Statut statut;

    @NotNull
    @Column(name = "a_livrer", nullable = false)
    private Boolean aLivrer;

    @Column(name = "date_paiement", nullable = false)
    private String datePaiement;

    @ManyToOne
    @JoinColumn(name = "id_client")
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Client idClient;

    @ManyToOne
    @JoinColumn(name = "id_franchise")
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Franchise idFranchise;

    @ManyToOne
    @JoinColumn(name = "id_fournisseur")
    @JsonIgnoreProperties(value = "commandes", allowSetters = true)
    private Fournisseur idFournisseur;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getPrixTotal() {
        return prixTotal;
    }

    public Commande prixTotal(Float prixTotal) {
        this.prixTotal = prixTotal;
        return this;
    }

    public void setPrixTotal(Float prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public Commande dateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
        return this;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getDateStatut() {
        return dateStatut;
    }

    public Commande dateStatut(String dateStatut) {
        this.dateStatut = dateStatut;
        return this;
    }

    public void setDateStatut(String dateStatut) {
        this.dateStatut = dateStatut;
    }

    public Statut getStatut() {
        return statut;
    }

    public Commande statut(Statut statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Boolean isaLivrer() {
        return aLivrer;
    }

    public Commande aLivrer(Boolean aLivrer) {
        this.aLivrer = aLivrer;
        return this;
    }

    public void setaLivrer(Boolean aLivrer) {
        this.aLivrer = aLivrer;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public Commande datePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
        return this;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Client getIdClient() {
        return idClient;
    }

    public Commande idClient(Client client) {
        this.idClient = client;
        return this;
    }

    public void setIdClient(Client client) {
        this.idClient = client;
    }

    public Franchise getIdFranchise() {
        return idFranchise;
    }

    public Commande idFranchise(Franchise franchise) {
        this.idFranchise = franchise;
        return this;
    }

    public void setIdFranchise(Franchise franchise) {
        this.idFranchise = franchise;
    }

    public Fournisseur getIdFournisseur() {
        return idFournisseur;
    }

    public Commande idFournisseur(Fournisseur fournisseur) {
        this.idFournisseur = fournisseur;
        return this;
    }

    public void setIdFournisseur(Fournisseur fournisseur) {
        this.idFournisseur = fournisseur;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commande)) {
            return false;
        }
        return id != null && id.equals(((Commande) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "Commande{" +
                "id=" + id +
                ", prixTotal=" + prixTotal +
                ", dateCommande='" + dateCommande + '\'' +
                ", dateStatut='" + dateStatut + '\'' +
                ", statut=" + statut +
                ", aLivrer=" + aLivrer +
                ", datePaiement='" + datePaiement + '\'' +
                ", idClient=" + idClient +
                ", idFranchise=" + idFranchise +
                ", idFournisseur=" + idFournisseur +
                '}';
    }
}
