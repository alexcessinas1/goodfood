package fr.baag.goodfood.domain.enumeration;

/**
 * The ProduitCategorie enumeration.
 */
public enum ProduitCategorie {
    BOISSON, ALIMENTATION, FOURNITURE
}
