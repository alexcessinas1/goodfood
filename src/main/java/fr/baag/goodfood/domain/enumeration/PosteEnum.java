package fr.baag.goodfood.domain.enumeration;

/**
 * The PosteEnum enumeration.
 */
public enum PosteEnum {
    LARBIN, CUISINIER, LIVREUR, MANAGER, DIRECTEUR, CHEF, PDG, TECHNICIEN, COMMERCIAL, NETTOYEUR, INGENIEUR
}
