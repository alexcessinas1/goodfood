package fr.baag.goodfood.domain.enumeration;

/**
 * The Statut enumeration.
 */
public enum Statut {
    CREE, EN_COURS, EN_PREPARATION, PREPARE, PAYEE, LIVRAISON, COMPLETE
}
