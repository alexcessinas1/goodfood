package fr.baag.goodfood.domain.enumeration;

/**
 * The Allergene enumeration.
 */
public enum AllergeneEnum {
    ARACHIDE, LAIT, OEUF, GLUTTEN, CHOUX_DE_BRUXELLES
}
