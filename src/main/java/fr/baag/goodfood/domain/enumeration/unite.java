package fr.baag.goodfood.domain.enumeration;

/**
 * The unite enumeration.
 */
public enum unite {
    LITRE, KG, UNITE
}
