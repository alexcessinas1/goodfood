package fr.baag.goodfood.domain.enumeration;

/**
 * The Plat enumeration.
 */
public enum Plat {
    ENTREE, PLAT, DESSERT
}
