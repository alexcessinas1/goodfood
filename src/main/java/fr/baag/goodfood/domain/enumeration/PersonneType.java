package fr.baag.goodfood.domain.enumeration;

/**
 * The PersonneType enumeration.
 */
public enum PersonneType {
    PHYSIQUE, MORALE
}
