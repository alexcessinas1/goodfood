package fr.baag.goodfood.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Fournisseur.
 */
@Entity
@Table(name = "fournisseur")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "fournisseur")
public class Fournisseur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "titre")
    private String titre;

    @NotNull
    @Column(name = "tel_entreprise", nullable = false)
    private String telEntreprise;

    @Column(name = "tel_maison")
    private String telMaison;

    @Column(name = "tel_mobile")
    private String telMobile;

    @Column(name = "numero_fax")
    private String numeroFax;

    @Column(name = "page_web")
    private String pageWeb;

    @Column(name = "notes")
    private String notes;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;

    @ManyToMany
    @NotNull
    @JoinTable(name = "fournisseur_produit", joinColumns = @JoinColumn(name = "fournisseur_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "produit_fournisseur_id", referencedColumnName = "id"))
    private Set<Produit> produitsFournisseur = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public Fournisseur prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public Fournisseur nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public Fournisseur email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitre() {
        return titre;
    }

    public Fournisseur titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTelEntreprise() {
        return telEntreprise;
    }

    public Fournisseur telEntreprise(String telEntreprise) {
        this.telEntreprise = telEntreprise;
        return this;
    }

    public void setTelEntreprise(String telEntreprise) {
        this.telEntreprise = telEntreprise;
    }

    public String getTelMaison() {
        return telMaison;
    }

    public Fournisseur telMaison(String telMaison) {
        this.telMaison = telMaison;
        return this;
    }

    public void setTelMaison(String telMaison) {
        this.telMaison = telMaison;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public Fournisseur telMobile(String telMobile) {
        this.telMobile = telMobile;
        return this;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getNumeroFax() {
        return numeroFax;
    }

    public Fournisseur numeroFax(String numeroFax) {
        this.numeroFax = numeroFax;
        return this;
    }

    public void setNumeroFax(String numeroFax) {
        this.numeroFax = numeroFax;
    }

    public String getPageWeb() {
        return pageWeb;
    }

    public Fournisseur pageWeb(String pageWeb) {
        this.pageWeb = pageWeb;
        return this;
    }

    public void setPageWeb(String pageWeb) {
        this.pageWeb = pageWeb;
    }

    public String getNotes() {
        return notes;
    }

    public Fournisseur notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public Fournisseur adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Set<Produit> getProduitsFournisseur() {
        return produitsFournisseur;
    }

    public Fournisseur produitsFournisseur(Set<Produit> produits) {
        this.produitsFournisseur = produits;
        return this;
    }

    public Fournisseur addProduitsFournisseur(Produit produit) {
        this.produitsFournisseur.add(produit);
        // produit.getFournisseurProduits().add(this);
        return this;
    }

    public Fournisseur removeProduitsFournisseur(Produit produit) {
        this.produitsFournisseur.remove(produit);
        // produit.getFournisseurProduits().remove(this);
        return this;
    }

    public void setProduitsFournisseur(Set<Produit> produits) {
        this.produitsFournisseur = produits;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fournisseur)) {
            return false;
        }
        return id != null && id.equals(((Fournisseur) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fournisseur{" + "id=" + getId() + ", prenom='" + getPrenom() + "'" + ", nom='" + getNom() + "'"
                + ", email='" + getEmail() + "'" + ", titre='" + getTitre() + "'" + ", telEntreprise='"
                + getTelEntreprise() + "'" + ", telMaison='" + getTelMaison() + "'" + ", telMobile='" + getTelMobile()
                + "'" + ", numeroFax='" + getNumeroFax() + "'" + ", pageWeb='" + getPageWeb() + "'" + ", notes='"
                + getNotes() + "'" + "}";
    }
}
