package fr.baag.goodfood.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import fr.baag.goodfood.domain.enumeration.Plat;

/**
 * A Recette.
 */
@Entity
@Table(name = "recette")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "recette")
public class Recette implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private Plat type;

    //@OneToMany(mappedBy = "recette")
    @ManyToMany
    @JoinTable(name = "recette_produit",
            joinColumns = @JoinColumn(name = "recette_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "produit_id", referencedColumnName = "id"))
    private Set<Produit> produits = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public Recette titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public Recette description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Plat getType() {
        return type;
    }

    public Recette type(Plat type) {
        this.type = type;
        return this;
    }

    public void setType(Plat type) {
        this.type = type;
    }

    public Set<Produit> getProduits() {
        return produits;
    }

    public Recette produits(Set<Produit> produits) {
        this.produits = produits;
        return this;
    }

    public Recette addProduits(Produit produit) {
        this.produits.add(produit);
        return this;
    }

    public Recette removeProduits(Produit produit) {
        this.produits.remove(produit);
        return this;
    }

    public void setProduits(Set<Produit> produits) {
        this.produits = produits;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recette)) {
            return false;
        }
        return id != null && id.equals(((Recette) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Recette{" + "id=" + getId() + ", titre='" + getTitre() + "'" + ", description='" + getDescription()
                + "'" + ", type='" + getType() + "'" + "}";
    }
}
