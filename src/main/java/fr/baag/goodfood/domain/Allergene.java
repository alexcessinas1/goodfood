package fr.baag.goodfood.domain;

import javax.persistence.*;

import java.io.Serializable;

import fr.baag.goodfood.domain.enumeration.AllergeneEnum;

/**
 * A Allergene.
 */
@Entity
@Table(name = "allergene")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "allergene")
public class Allergene implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "nom")
    private AllergeneEnum nom;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AllergeneEnum getNom() {
        return nom;
    }

    public Allergene nom(AllergeneEnum nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(AllergeneEnum nom) {
        this.nom = nom;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Allergene)) {
            return false;
        }
        return id != null && id.equals(((Allergene) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Allergene{" + "id=" + getId() + ", nom='" + getNom() + "'" + "}";
    }
}
