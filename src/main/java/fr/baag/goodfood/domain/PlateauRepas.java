package fr.baag.goodfood.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A PlateauRepas.
 */
@Entity
@Table(name = "plateau_repas")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "plateaurepas")
public class PlateauRepas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "prix_ht")
    private Float prixHT;

    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    @Column(name = "tva")
    private Float tva;

    @Column(name = "composition")
    private String composition;

    @Column(name = "couvert")
    private Boolean couvert;

    @Column(name = "serviette")
    private Boolean serviette;

    @ManyToOne
    @JsonIgnoreProperties(value = "plateauRepas", allowSetters = true)
    private Produit boisson;

    @ManyToMany
    @JoinTable(name = "plateau_repas_recette", joinColumns = @JoinColumn(name = "plateau_repas_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "recette_id", referencedColumnName = "id"))
    private Set<Recette> recettes = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "commande_client_id")
    private CommandeClient commandeClient;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getPrixHT() {
        return prixHT;
    }

    public PlateauRepas prixHT(Float prixHT) {
        this.prixHT = prixHT;
        return this;
    }

    public void setPrixHT(Float prixHT) {
        this.prixHT = prixHT;
    }

    public Float getTva() {
        return tva;
    }

    public PlateauRepas tva(Float tva) {
        this.tva = tva;
        return this;
    }

    public void setTva(Float tva) {
        this.tva = tva;
    }

    public String getComposition() {
        return composition;
    }

    public PlateauRepas composition(String composition) {
        this.composition = composition;
        return this;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public Boolean getCouvert() {
        return couvert;
    }

    public PlateauRepas couvert(Boolean couvert) {
        this.couvert = couvert;
        return this;
    }

    public void setCouvert(Boolean couvert) {
        this.couvert = couvert;
    }

    public Boolean getServiette() {
        return serviette;
    }

    public PlateauRepas serviette(Boolean serviette) {
        this.serviette = serviette;
        return this;
    }

    public void setServiette(Boolean serviette) {
        this.serviette = serviette;
    }

    public Produit getBoisson() {
        return boisson;
    }

    public PlateauRepas boisson(Produit produit) {
        this.boisson = produit;
        return this;
    }

    public void setBoisson(Produit produit) {
        this.boisson = produit;
    }

    public Set<Recette> getRecettes() {
        return recettes;
    }

    public PlateauRepas setRecettes(Set<Recette> recettes) {
        this.recettes = recettes;
        return this;
    }

    public PlateauRepas addRecettes(Recette recette) {
        this.recettes.add(recette);
        // TODO Check Si pas casser recette.getPlateauRepas().add(this);
        return this;
    }

    public PlateauRepas removeRecettes(Recette recette) {
        this.recettes.remove(recette);
        // TODO Check Si pas casser recette.getPlateauRepas().remove(this);
        return this;
    }

    public CommandeClient getCommandeClient() {
        return commandeClient;
    }

    public void setCommandeClient(CommandeClient commandeClient) {
        this.commandeClient = commandeClient;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlateauRepas)) {
            return false;
        }
        return id != null && id.equals(((PlateauRepas) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlateauRepas{" + "id=" + getId() + ", prixHT=" + getPrixHT() + ", tva=" + getTva() + ", composition='"
                + getComposition() + "'" + ", couvert='" + getCouvert() + "'" + ", serviette='" + getServiette() + "'"
                + "}";
    }
}
