package fr.baag.goodfood.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A CategorieFourniture.
 */
@Entity
@Table(name = "categorie_fourniture")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "categoriefourniture")
public class CategorieFourniture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    private String type;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public CategorieFourniture type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategorieFourniture)) {
            return false;
        }
        return id != null && id.equals(((CategorieFourniture) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategorieFourniture{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
