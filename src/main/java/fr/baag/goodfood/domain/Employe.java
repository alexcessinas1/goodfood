package fr.baag.goodfood.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import fr.baag.goodfood.domain.enumeration.PosteEnum;

/**
 * A Employe.
 */
@Entity
@Table(name = "employe")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "employe")
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "poste")
    private PosteEnum poste;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JsonIgnoreProperties(value = "employes", allowSetters = true)
    private Franchise franchise;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "employes", allowSetters = true)
    private Adresse adresse;

    @ManyToOne
    @JsonIgnoreProperties(value = "employes", allowSetters = true)
    private PersonnePhysique personnePhysique;

    @ManyToOne
    @JsonIgnoreProperties(value = "employes", allowSetters = true)
    private PersonneMorale personneMorale;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PosteEnum getPoste() {
        return poste;
    }

    public Employe poste(PosteEnum poste) {
        this.poste = poste;
        return this;
    }

    public void setPoste(PosteEnum poste) {
        this.poste = poste;
    }

    public String getNotes() {
        return notes;
    }

    public Employe notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public Employe franchise(Franchise franchise) {
        this.franchise = franchise;
        return this;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public Employe adresse(Adresse adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public PersonnePhysique getPersonnePhysique() {
        return personnePhysique;
    }

    public Employe personnePhysique(PersonnePhysique personnePhysique) {
        this.personnePhysique = personnePhysique;
        return this;
    }

    public void setPersonnePhysique(PersonnePhysique personnePhysique) {
        this.personnePhysique = personnePhysique;
    }

    public PersonneMorale getPersonneMorale() {
        return personneMorale;
    }

    public Employe personneMorale(PersonneMorale personneMorale) {
        this.personneMorale = personneMorale;
        return this;
    }

    public void setPersonneMorale(PersonneMorale personneMorale) {
        this.personneMorale = personneMorale;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employe)) {
            return false;
        }
        return id != null && id.equals(((Employe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Employe{" +
            "id=" + getId() +
            ", poste='" + getPoste() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
