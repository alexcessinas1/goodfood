using CSharp.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace CSharp.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
            {
            }
            public DbSet<Produit> Produit {get;set;}
        protected override void OnModelCreating(ModelBuilder builder)
        
        {
            base.OnModelCreating(builder);
        }
    }
}