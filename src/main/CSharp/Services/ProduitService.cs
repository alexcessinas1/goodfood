using System.Threading.Tasks;
using CSharp.Entities;
using CSharp.Repositories;

namespace CSharp.Services
{
    public class ProduitService
    {
        private readonly ProduitRepository _produitRepository;

        public ProduitService(ProduitRepository produitRepository)
        {
            _produitRepository = produitRepository;
        }

        public Produit GetProduitAsync()
        {
            Produit produit = _produitRepository.GetProduit().Result;
            produit.Description = "test";
            return produit;
        }
    }
}