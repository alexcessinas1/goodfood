using System.Threading.Tasks;
using CSharp.Data;
using CSharp.Entities;
using Microsoft.EntityFrameworkCore;

namespace CSharp.Repositories
{
    public class ProduitRepository
    {
        private readonly ApplicationDbContext _context;

        public ProduitRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Produit> GetProduit()
        {
            Produit produit = new Produit();
            produit = await _context.Produit.FirstOrDefaultAsync();
            return produit;
        }
    }
}