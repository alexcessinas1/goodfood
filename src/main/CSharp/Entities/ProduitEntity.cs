using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CSharp.Entities
{
    public enum quantiteUnite
    {
        UNITE, KG, LITRE
    }

    /// <summary>
    /// Entité des stocks d'un produit
    /// </summary>
    [Table("produit")]
    public class Produit
    {
        // Id unique d'un stock
        [KeyAttribute]
        [ColumnAttribute("id")]
        public int Id { get; set; }
        
        // Id unique d'un stock qui est affiché
        [KeyAttribute]
        [ColumnAttribute("eid")]
        public string eId { get; set; }

        // Nom du stock
        [Required]
        [ColumnAttribute("nom")]
        public string Nom { get; set; }

        // Stock du produit en question
        [Required]
        [ColumnAttribute("quantite")]
        public float Quantite { get; set; }

        // Unite de mesure du produit
        [Required]
        [ColumnAttribute("quantite_unite")]
        public string Quantite_Unite { get; set; }

        // Description du produit
        [ColumnAttribute("description")]
        public string Description { get; set; }

        // Prix catalogue du produit
        [ColumnAttribute("prix_catalogue")]
        public float Prix_Catalogue { get; set; }

        // Prix standard du produit
        [ColumnAttribute("prix_standard")]
        public float Prix_Standard { get; set; }

        // 
        [ColumnAttribute("vente_ouverte")]
        public bool Vente_Ouverte { get; set; }

        // Origine du produit
        [ColumnAttribute("origine")]
        public string Origine { get; set; }

        //TODO: Clé etrangere avec le produit
    }
}