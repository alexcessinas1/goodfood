using System.Collections.Generic;
using CSharp.Entities;
using CSharp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CSharp.Controllers
{
    /// <summary>
    /// Controlleur pour les stocks d'un produit
    /// </summary>
    [Route("produitsC#"), Produces("application/json")]
    [ApiController]
    public class ProduitController : ControllerBase
    {
        private readonly ProduitService _produitService;

        public ProduitController(ProduitService produitService)
        {
            _produitService = produitService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Produit>> GetProduit()
        {
            Produit produit = _produitService.GetProduitAsync();
            if (produit is null)
            {
                return NotFound();
            }

            return Ok(produit);
        }
    }
}