import { NgModule } from '@angular/core';

import './vendor';
import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { GoodFoodCoreModule } from 'app/core/core.module';
import { GoodFoodAppRoutingModule } from './app-routing.module';
import { GoodFoodHomeModule } from './home/home.module';
import { GoodFoodEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { GoodFoodModulesModule } from 'app/modules/modules.module';

@NgModule({
  imports: [
    GoodFoodSharedModule,
    GoodFoodCoreModule,
    GoodFoodHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    GoodFoodEntityModule,
    GoodFoodAppRoutingModule,
    GoodFoodModulesModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class GoodFoodAppModule {}
