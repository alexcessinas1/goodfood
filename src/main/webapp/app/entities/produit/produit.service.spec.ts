import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IProduit, Produit } from 'app/shared/model/produit.model';
import { unite } from 'app/shared/model/enumerations/unite.model';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { AllergeneEnum } from 'app/shared/model/enumerations/allergeneEnum.model';
import { Allergene } from 'app/shared/model/allergene.model';

describe('Service Tests', () => {
  describe('Produit Service', () => {
    let injector: TestBed;
    let service: ProduitService;
    let httpMock: HttpTestingController;
    let elemDefault: IProduit;
    let expectedResult: IProduit | IProduit[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ProduitService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Produit(
        0,
        'AAAAAAA',
        'AAAAAAA',
        0,
        unite.LITRE,
        'AAAAAAA',
        0,
        0,
        false,
        'AAAAAAA',
        ProduitCategorie.BOISSON,
        [new Allergene(1, AllergeneEnum.ARACHIDE)],
        'AAAAAAA',
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateLot: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Produit', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateLot: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateLot: currentDate,
          },
          returnedFromService
        );

        service.create(new Produit()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Produit', () => {
        const returnedFromService = Object.assign(
          {
            eid: 'BBBBBB',
            nom: 'BBBBBB',
            quantite: 1,
            quantiteUnite: 'BBBBBB',
            description: 'BBBBBB',
            prixCatalogue: 1,
            prixStandard: 1,
            venteOuverte: true,
            origine: 'BBBBBB',
            produitCategorie: 'BBBBBB',
            allergenes: 'BBBBBB',
            numeroLot: 'BBBBBB',
            dateLot: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateLot: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Produit', () => {
        const returnedFromService = Object.assign(
          {
            eid: 'BBBBBB',
            nom: 'BBBBBB',
            quantite: 1,
            quantiteUnite: 'BBBBBB',
            description: 'BBBBBB',
            prixCatalogue: 1,
            prixStandard: 1,
            venteOuverte: true,
            origine: 'BBBBBB',
            produitCategorie: 'BBBBBB',
            allergenes: 'BBBBBB',
            numeroLot: 'BBBBBB',
            dateLot: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateLot: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Produit', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
