import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProduit, Produit } from 'app/shared/model/produit.model';
import { ProduitService } from './produit.service';
import { ICategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { CategorieBoissonService } from 'app/entities/categorie-boisson/categorie-boisson.service';
import { ICategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { CategorieAlimentationService } from 'app/entities/categorie-alimentation/categorie-alimentation.service';
import { ICategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { CategorieFournitureService } from 'app/entities/categorie-fourniture/categorie-fourniture.service';
import { IAllergene } from 'app/shared/model/allergene.model';
import { AllergeneService } from '../allergene/allergene.service';

type SelectableEntity = ICategorieBoisson | ICategorieAlimentation | ICategorieFourniture;

@Component({
  selector: 'gf-produit-update',
  templateUrl: './produit-update.component.html',
})
export class ProduitUpdateComponent implements OnInit {
  isSaving = false;
  categorieboissons: ICategorieBoisson[] = [];
  categoriealimentations: ICategorieAlimentation[] = [];
  categoriefournitures: ICategorieFourniture[] = [];
  allergenes: IAllergene[] = [];
  dateLotDp: any;

  editForm = this.fb.group({
    id: [],
    eid: [],
    nom: [],
    quantite: [],
    quantiteUnite: [],
    description: [],
    prixCatalogue: [],
    prixStandard: [],
    venteOuverte: [],
    origine: [],
    produitCategorie: [],
    allergenes: [],
    numeroLot: [],
    dateLot: [],
    boissonId: [],
    alimentId: [],
    fournitureId: [],
  });

  constructor(
    protected produitService: ProduitService,
    protected categorieBoissonService: CategorieBoissonService,
    protected categorieAlimentationService: CategorieAlimentationService,
    protected categorieFournitureService: CategorieFournitureService,
    protected activatedRoute: ActivatedRoute,
    protected allergeneService: AllergeneService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ produit }) => {
      this.updateForm(produit);

      this.categorieBoissonService.query().subscribe((res: HttpResponse<ICategorieBoisson[]>) => (this.categorieboissons = res.body || []));

      this.categorieAlimentationService
        .query()
        .subscribe((res: HttpResponse<ICategorieAlimentation[]>) => (this.categoriealimentations = res.body || []));

      this.categorieFournitureService
        .query()
        .subscribe((res: HttpResponse<ICategorieFourniture[]>) => (this.categoriefournitures = res.body || []));
      this.allergeneService.query().subscribe((res: HttpResponse<IAllergene[]>) => (this.allergenes = res.body || []));
    });
  }

  updateForm(produit: IProduit): void {
    this.editForm.patchValue({
      id: produit.id,
      eid: produit.eid,
      nom: produit.nom,
      quantite: produit.quantite,
      quantiteUnite: produit.quantiteUnite,
      description: produit.description,
      prixCatalogue: produit.prixCatalogue,
      prixStandard: produit.prixStandard,
      venteOuverte: produit.venteOuverte,
      origine: produit.origine,
      produitCategorie: produit.produitCategorie,
      allergenes: produit.allergenes,
      numeroLot: produit.numeroLot,
      dateLot: produit.dateLot,
      boissonId: produit.boissonId,
      alimentId: produit.alimentId,
      fournitureId: produit.fournitureId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const produit = this.createFromForm();
    if (produit.id !== undefined) {
      this.subscribeToSaveResponse(this.produitService.update(produit));
    } else {
      this.subscribeToSaveResponse(this.produitService.create(produit));
    }
  }

  private createFromForm(): IProduit {
    return {
      ...new Produit(),
      id: this.editForm.get(['id'])!.value,
      eid: this.editForm.get(['eid'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      quantite: this.editForm.get(['quantite'])!.value,
      quantiteUnite: this.editForm.get(['quantiteUnite'])!.value,
      description: this.editForm.get(['description'])!.value,
      prixCatalogue: this.editForm.get(['prixCatalogue'])!.value,
      prixStandard: this.editForm.get(['prixStandard'])!.value,
      venteOuverte: this.editForm.get(['venteOuverte'])!.value,
      origine: this.editForm.get(['origine'])!.value,
      produitCategorie: this.editForm.get(['produitCategorie'])!.value,
      allergenes: this.editForm.get(['allergenes'])!.value,
      numeroLot: this.editForm.get(['numeroLot'])!.value,
      dateLot: this.editForm.get(['dateLot'])!.value,
      boissonId: this.editForm.get(['boissonId'])!.value,
      alimentId: this.editForm.get(['alimentId'])!.value,
      fournitureId: this.editForm.get(['fournitureId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduit>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
