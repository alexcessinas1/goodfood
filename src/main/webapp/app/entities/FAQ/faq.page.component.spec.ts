import { FaqPageComponent } from 'app/entities/FAQ/faq.page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

describe('FaqPageComponent', () => {
  let component: FaqPageComponent;
  let fixture: ComponentFixture<FaqPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FaqPageComponent],
    }).compileComponents();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(FaqPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should display original title', () => {
    expect(component.title).toBe("FAQ C'est ici !");
  });
  it('should have content in notice h3 ', () => {
    const h3 = fixture.debugElement.query(By.css('.h3')).nativeElement;
    expect(h3.innerHTML).not.toBeNull();
    expect(h3.innerHTML.length).toBeGreaterThan(8);
  });
});
