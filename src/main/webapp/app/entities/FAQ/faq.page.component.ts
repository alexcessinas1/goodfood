import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gf-faq',
  templateUrl: './faq.page.component.html',
  styleUrls: ['./faq.page.component.scss'],
  template: '<h3 class="h3">{{title}}</h3>',
  styles: [
    'h3 {padding: 5px; background-color: hsla(0, 0%, 80%, 0.10196078431372549);border: 1px solid hsla(0, 0%, 80%, 0.10196078431372549); color: #323e40; font-weight: bolder; text-align: center;}',
  ],
})
export class FaqPageComponent implements OnInit {
  public title = "FAQ C'est ici !";
  constructor() {}
  ngOnInit(): void {}
}
