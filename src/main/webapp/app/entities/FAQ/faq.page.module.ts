import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { FaqPageComponent } from './faq.page.component';
const faqPageRoute: Routes = [
  {
    path: '',
    component: FaqPageComponent,
    data: {
      authorities: [],
      pageTitle: 'goodFoodApp.faq.home.title',
    }
  }
];

@NgModule({
    imports: [GoodFoodSharedModule, RouterModule.forChild(faqPageRoute)],
    declarations: [FaqPageComponent],
    providers: []
})
export class GoodFoodContratPageModule {}
