import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICategorieAlimentation, CategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { CategorieAlimentationService } from './categorie-alimentation.service';

@Component({
  selector: 'gf-categorie-alimentation-update',
  templateUrl: './categorie-alimentation-update.component.html',
})
export class CategorieAlimentationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    vegan: [],
    vegetarien: [],
  });

  constructor(
    protected categorieAlimentationService: CategorieAlimentationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieAlimentation }) => {
      this.updateForm(categorieAlimentation);
    });
  }

  updateForm(categorieAlimentation: ICategorieAlimentation): void {
    this.editForm.patchValue({
      id: categorieAlimentation.id,
      vegan: categorieAlimentation.vegan,
      vegetarien: categorieAlimentation.vegetarien,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const categorieAlimentation = this.createFromForm();
    if (categorieAlimentation.id !== undefined) {
      this.subscribeToSaveResponse(this.categorieAlimentationService.update(categorieAlimentation));
    } else {
      this.subscribeToSaveResponse(this.categorieAlimentationService.create(categorieAlimentation));
    }
  }

  private createFromForm(): ICategorieAlimentation {
    return {
      ...new CategorieAlimentation(),
      id: this.editForm.get(['id'])!.value,
      vegan: this.editForm.get(['vegan'])!.value,
      vegetarien: this.editForm.get(['vegetarien'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategorieAlimentation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
