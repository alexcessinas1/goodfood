import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { CategorieAlimentationComponent } from './categorie-alimentation.component';
import { CategorieAlimentationDetailComponent } from './categorie-alimentation-detail.component';
import { CategorieAlimentationUpdateComponent } from './categorie-alimentation-update.component';
import { CategorieAlimentationDeleteDialogComponent } from './categorie-alimentation-delete-dialog.component';
import { categorieAlimentationRoute } from './categorie-alimentation.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(categorieAlimentationRoute)],
  declarations: [
    CategorieAlimentationComponent,
    CategorieAlimentationDetailComponent,
    CategorieAlimentationUpdateComponent,
    CategorieAlimentationDeleteDialogComponent,
  ],
  entryComponents: [CategorieAlimentationDeleteDialogComponent],
})
export class GoodFoodCategorieAlimentationModule {}
