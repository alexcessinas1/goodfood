import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { CategorieAlimentationService } from './categorie-alimentation.service';

@Component({
  templateUrl: './categorie-alimentation-delete-dialog.component.html',
})
export class CategorieAlimentationDeleteDialogComponent {
  categorieAlimentation?: ICategorieAlimentation;

  constructor(
    protected categorieAlimentationService: CategorieAlimentationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.categorieAlimentationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('categorieAlimentationListModification');
      this.activeModal.close();
    });
  }
}
