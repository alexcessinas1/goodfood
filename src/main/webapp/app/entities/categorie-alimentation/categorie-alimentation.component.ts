import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { CategorieAlimentationService } from './categorie-alimentation.service';
import { CategorieAlimentationDeleteDialogComponent } from './categorie-alimentation-delete-dialog.component';

@Component({
  selector: 'gf-categorie-alimentation',
  templateUrl: './categorie-alimentation.component.html',
})
export class CategorieAlimentationComponent implements OnInit, OnDestroy {
  categorieAlimentations?: ICategorieAlimentation[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected categorieAlimentationService: CategorieAlimentationService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.categorieAlimentationService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ICategorieAlimentation[]>) => (this.categorieAlimentations = res.body || []));
      return;
    }

    this.categorieAlimentationService
      .query()
      .subscribe((res: HttpResponse<ICategorieAlimentation[]>) => (this.categorieAlimentations = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCategorieAlimentations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICategorieAlimentation): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCategorieAlimentations(): void {
    this.eventSubscriber = this.eventManager.subscribe('categorieAlimentationListModification', () => this.loadAll());
  }

  delete(categorieAlimentation: ICategorieAlimentation): void {
    const modalRef = this.modalService.open(CategorieAlimentationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.categorieAlimentation = categorieAlimentation;
  }
}
