import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICategorieAlimentation, CategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { CategorieAlimentationService } from './categorie-alimentation.service';
import { CategorieAlimentationComponent } from './categorie-alimentation.component';
import { CategorieAlimentationDetailComponent } from './categorie-alimentation-detail.component';
import { CategorieAlimentationUpdateComponent } from './categorie-alimentation-update.component';

@Injectable({ providedIn: 'root' })
export class CategorieAlimentationResolve implements Resolve<ICategorieAlimentation> {
  constructor(private service: CategorieAlimentationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategorieAlimentation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((categorieAlimentation: HttpResponse<CategorieAlimentation>) => {
          if (categorieAlimentation.body) {
            return of(categorieAlimentation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CategorieAlimentation());
  }
}

export const categorieAlimentationRoute: Routes = [
  {
    path: '',
    component: CategorieAlimentationComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieAlimentation.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CategorieAlimentationDetailComponent,
    resolve: {
      categorieAlimentation: CategorieAlimentationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieAlimentation.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CategorieAlimentationUpdateComponent,
    resolve: {
      categorieAlimentation: CategorieAlimentationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieAlimentation.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CategorieAlimentationUpdateComponent,
    resolve: {
      categorieAlimentation: CategorieAlimentationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieAlimentation.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
