import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { CategorieAlimentationComponent } from 'app/entities/categorie-alimentation/categorie-alimentation.component';
import { CategorieAlimentationService } from 'app/entities/categorie-alimentation/categorie-alimentation.service';
import { CategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieAlimentation Management Component', () => {
    let comp: CategorieAlimentationComponent;
    let fixture: ComponentFixture<CategorieAlimentationComponent>;
    let service: CategorieAlimentationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieAlimentationComponent],
      })
        .overrideTemplate(CategorieAlimentationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieAlimentationComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieAlimentationService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CategorieAlimentation(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.categorieAlimentations && comp.categorieAlimentations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
