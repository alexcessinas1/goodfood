import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';

@Component({
  selector: 'gf-categorie-alimentation-detail',
  templateUrl: './categorie-alimentation-detail.component.html',
})
export class CategorieAlimentationDetailComponent implements OnInit {
  categorieAlimentation: ICategorieAlimentation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieAlimentation }) => (this.categorieAlimentation = categorieAlimentation));
  }

  previousState(): void {
    window.history.back();
  }
}
