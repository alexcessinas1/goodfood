import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';

type EntityResponseType = HttpResponse<ICategorieAlimentation>;
type EntityArrayResponseType = HttpResponse<ICategorieAlimentation[]>;

@Injectable({ providedIn: 'root' })
export class CategorieAlimentationService {
  public resourceUrl = SERVER_API_URL + 'api/categorie-alimentations';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/categorie-alimentations';

  constructor(protected http: HttpClient) {}

  create(categorieAlimentation: ICategorieAlimentation): Observable<EntityResponseType> {
    return this.http.post<ICategorieAlimentation>(this.resourceUrl, categorieAlimentation, { observe: 'response' });
  }

  update(categorieAlimentation: ICategorieAlimentation): Observable<EntityResponseType> {
    return this.http.put<ICategorieAlimentation>(this.resourceUrl, categorieAlimentation, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICategorieAlimentation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieAlimentation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieAlimentation[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
