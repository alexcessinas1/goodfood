import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { CategorieAlimentationUpdateComponent } from 'app/entities/categorie-alimentation/categorie-alimentation-update.component';
import { CategorieAlimentationService } from 'app/entities/categorie-alimentation/categorie-alimentation.service';
import { CategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieAlimentation Management Update Component', () => {
    let comp: CategorieAlimentationUpdateComponent;
    let fixture: ComponentFixture<CategorieAlimentationUpdateComponent>;
    let service: CategorieAlimentationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieAlimentationUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CategorieAlimentationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieAlimentationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieAlimentationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieAlimentation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieAlimentation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
