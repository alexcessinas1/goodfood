import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { CategorieAlimentationDetailComponent } from 'app/entities/categorie-alimentation/categorie-alimentation-detail.component';
import { CategorieAlimentation } from 'app/shared/model/categorie-alimentation.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieAlimentation Management Detail Component', () => {
    let comp: CategorieAlimentationDetailComponent;
    let fixture: ComponentFixture<CategorieAlimentationDetailComponent>;
    const route = ({ data: of({ categorieAlimentation: new CategorieAlimentation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieAlimentationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CategorieAlimentationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CategorieAlimentationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load categorieAlimentation on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.categorieAlimentation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
