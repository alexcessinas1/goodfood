import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'gf-contact',
  templateUrl: './contact.page.component.html',
  styleUrls: ['./contact.page.component.scss'],
  template: '<h3 class="h3">{{title}}</h3>',
  styles: [
    'h3 {padding: 5px; background-color: hsla(0, 0%, 80%, 0.10196078431372549);border: 1px solid hsla(0, 0%, 80%, 0.10196078431372549); color: #323e40; font-weight: bolder; text-align: center;}',
  ],
})
export class ContactPageComponent implements OnInit {
  submitted = false;
  isLoading = false;
  responseMessage = '';
  public title = "FAQ C'est ici !";

  form = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    message: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(256)]],
  });

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {}

  ngOnInit(): void {}

  onSubmit(): void {
    if (this.form.valid) {
      this.form.disable();
      const formData: any = new FormData();
      formData.append('name', this.form.get(['name'])!.value);
      formData.append('email', this.form.get('email')!.value);
      formData.append('message', this.form.get('message')!.value);
      this.isLoading = true;
      this.submitted = false;
      this.http
        .post(
          'https://script.google.com/macros/s/AKfycbwkjuXKQ9g1o1EJhfP1irBpyDmqkSvJ_JolGTb27uwVlM5wT3ucI43pBQMJzztlKY8g4g/exec',
          formData
        )
        .subscribe(response => {
          if (response['result'] === 'success') {
            this.responseMessage = "Merci pour votre message, l'équipe Good Food reviendra vers vous au plus vite.";
          } else {
            this.responseMessage = 'Oops! Désolé... actualiser la page et réeesayé.';
          }
          this.form.enable();
          this.submitted = true;
          this.isLoading = false;
        });
    }
  }
}
