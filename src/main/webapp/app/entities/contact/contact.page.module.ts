import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { ContactPageComponent } from './contact.page.component';

const contactPageRoute: Routes = [
  {
    path: '',
    component: ContactPageComponent,
    data: {
      authorities: [],
      pageTitle: 'goodFoodApp.contact.home.title',
    },
  },
];

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(contactPageRoute)],
  declarations: [ContactPageComponent],
  providers: [],
})
export class GoodFoodContactPageModule {}
