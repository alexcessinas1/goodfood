import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { ShopPageComponent } from './shop.page.component';
import { ToastModule } from 'primeng/toast';
import { GMapModule } from 'primeng/gmap';
import { MessageService } from 'primeng/api';
import { GoodFoodModulesModule } from 'app/modules/modules.module';
import { MapService } from 'app/shared/util/map.service';

const shopPageRoute: Routes = [
  {
    path: '',
    component: ShopPageComponent,
    data: {
      authorities: [],
      pageTitle: 'goodFoodApp.shop.home.title',
    },
  },
];

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(shopPageRoute), ToastModule, GMapModule, GoodFoodModulesModule],
  declarations: [ShopPageComponent],
  exports: [ShopPageComponent],
  providers: [MessageService, MapService],
})
export class GoodFoodShopPageModule {}
