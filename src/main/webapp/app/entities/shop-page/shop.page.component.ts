import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { MapService } from 'app/shared/util/map.service';

const centreFrance = { lat: 46.606111, lng: 1.875278 };

@Component({
  selector: 'gf-shop',
  templateUrl: './shop.page.component.html',
})
export class ShopPageComponent implements OnInit, OnChanges {
  @Input() isSelectMode = false;
  @Input() manualAddress: string | undefined;
  @Output() setGeoloc = new EventEmitter<google.maps.LatLngLiteral>();
  options: any;
  icon = goodfoodIcons;
  // @ts-ignore
  overlays: any[] = [];
  private iconHome = {
    path: faHome.icon[4] as string,
    fillColor: '#242222',
    fillOpacity: 1,
    anchor: new google.maps.Point(
      faHome.icon[0] / 2, // width
      faHome.icon[1] // height
    ),
    strokeWeight: 1,
    strokeColor: '#ffffff',
    scale: 0.075,
  };

  constructor(
    private franchiseService: FranchiseService,
    private adresseService: AdresseService,
    private readonly mapService: MapService
  ) {}

  ngOnInit(): void {
    this.options = { center: centreFrance, zoom: 6 };
    if (this.isSelectMode) {
      this.getLocation();
    }
    this.initShopMarker();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.manualAddress) {
      const currentManualAddress: string = changes.manualAddress.currentValue;
      const previousManualAddress: string = changes.manualAddress.previousValue;
      if (currentManualAddress !== previousManualAddress) {
        this.mapService.getGeoLocation(currentManualAddress).subscribe(value => {
          if (previousManualAddress) {
            this.overlays.pop();
          }
          this.overlays.push(
            new google.maps.Marker({
              position: { lat: value.lat(), lng: value.lng() },
              title: this.manualAddress,
              icon: this.iconHome,
            })
          );
          this.options = { center: { lat: value.lat(), lng: value.lng() }, zoom: 12 };
        });
      }
    }
  }

  /**
   * Initialise les marqueurs pour les differents franchiisés
   */
  initShopMarker(): void {
    this.franchiseService.query().subscribe(franchises => {
      franchises.body?.forEach(franchise => {
        if (franchise.adresseFranchiseIdId) {
          this.adresseService.find(franchise.adresseFranchiseIdId).subscribe(adresse => {
            this.mapService
              .getGeoLocation(`${adresse.body?.numeroVoie} ${adresse.body?.nomVoie} ${adresse.body?.ville} ${adresse.body?.codePostal}`)
              .subscribe(value => {
                this.overlays.push(
                  new google.maps.Marker({
                    position: { lat: value.lat(), lng: value.lng() },
                    title: `${adresse.body?.numeroVoie} ${adresse.body?.nomVoie}, ${adresse.body?.ville} - ${adresse.body?.codePostal}`,
                  })
                );
              });
          });
        }
      });
    });
  }

  /**
   * Recupere la localisation et met un marker et un cercle sur un rayon de 5km
   */
  getLocation(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        this.options = { center: { lat: position.coords.latitude, lng: position.coords.longitude }, zoom: 12 };
        this.overlays.push(
          new google.maps.Marker({
            position: { lat: position.coords.latitude, lng: position.coords.longitude },
            title: 'Ma position',
            icon: this.iconHome,
          })
        );
        this.setGeoloc.emit({ lat: position.coords.latitude, lng: position.coords.longitude });
      });
    }
  }
}
