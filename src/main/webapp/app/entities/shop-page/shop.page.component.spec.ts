import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { ShopPageComponent } from 'app/entities/shop-page/shop.page.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { setupGoogleMock, setupNavigator } from '../../../../../test/javascript/jest-global-mocks';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { Adresse } from 'app/shared/model/adresse.model';
import { Franchise } from 'app/shared/model/franchise.model';
import { GoodFoodModulesModule } from 'app/modules/modules.module';
import { of } from 'rxjs';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { fakeAsync, tick } from '@angular/core/testing';
import { MapService } from 'app/shared/util/map.service';
import { MessageService } from 'primeng/api';

describe('Component Tests', () => {
  describe('Shop Page Component', () => {
    let spectator: Spectator<ShopPageComponent>;
    // @ts-ignore
    const createComponent = createComponentFactory({
      component: ShopPageComponent,
      imports: [GoodFoodModulesModule, HttpClientTestingModule],
      providers: [MapService, MessageService],
    });

    const data = {
      lat: () => 125,
      lng: () => 125,
    };

    const adresses = [new Adresse({ id: 15, numeroVoie: '147', nomVoie: 'Rue de Limoges', codePostal: '16000', ville: 'Angoulême' })];
    const franchises = [new Franchise(15, '', '', '', '', 1)];

    // in test file.
    beforeAll(() => {
      setupGoogleMock();
      setupNavigator();
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize all marker', fakeAsync(() => {
      spectator.component.isSelectMode = false;
      const spyQueryFranchise = spyOn(spectator.inject(FranchiseService), 'query').and.returnValue(of({ body: franchises }));
      const spyFindAdresse = spyOn(spectator.inject(AdresseService), 'find').and.returnValue(of({ body: adresses }));
      const spyGetGeoloc = spyOn(spectator.inject(MapService), 'getGeoLocation').and.returnValue(of(data));

      spectator.component.ngOnInit();
      tick();
      expect(spyQueryFranchise).toHaveBeenCalledTimes(1);
      expect(spyFindAdresse).toHaveBeenCalledTimes(1);
      expect(spyGetGeoloc).toHaveBeenCalledTimes(1);
      expect(spectator.component.overlays.length).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should initialize all marker with location', fakeAsync(() => {
      spectator.component.isSelectMode = true;
      const spyQueryFranchise = spyOn(spectator.inject(FranchiseService), 'query').and.returnValue(of({ body: franchises }));
      const spyFindAdresse = spyOn(spectator.inject(AdresseService), 'find').and.returnValue(of({ body: adresses }));
      const spyGetGeoloc = spyOn(spectator.inject(MapService), 'getGeoLocation').and.returnValue(of(data));

      spectator.component.ngOnInit();
      tick();
      expect(spyQueryFranchise).toHaveBeenCalledTimes(1);
      expect(spyFindAdresse).toHaveBeenCalledTimes(1);
      expect(spyGetGeoloc).toHaveBeenCalledTimes(1);
      expect(spectator.component.overlays.length).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should initialize all marker with location without mock', fakeAsync(() => {
      spectator.component.isSelectMode = true;
      const spyQueryFranchise = spyOn(spectator.inject(FranchiseService), 'query').and.returnValue(of({ body: franchises }));
      const spyFindAdresse = spyOn(spectator.inject(AdresseService), 'find').and.returnValue(of({ body: adresses }));

      spectator.component.ngOnInit();
      tick();
      expect(spyQueryFranchise).toHaveBeenCalledTimes(1);
      expect(spyFindAdresse).toHaveBeenCalledTimes(1);
      expect(spectator.component).toBeDefined();
    }));
  });
});
