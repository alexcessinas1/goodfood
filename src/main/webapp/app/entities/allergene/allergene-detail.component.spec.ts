import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { AllergeneDetailComponent } from 'app/entities/allergene/allergene-detail.component';
import { Allergene } from 'app/shared/model/allergene.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Allergene Management Detail Component', () => {
    let comp: AllergeneDetailComponent;
    let fixture: ComponentFixture<AllergeneDetailComponent>;
    const route = ({ data: of({ allergene: new Allergene(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [AllergeneDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AllergeneDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AllergeneDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load allergene on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.allergene).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
