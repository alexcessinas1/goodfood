import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAllergene, Allergene } from 'app/shared/model/allergene.model';
import { AllergeneService } from './allergene.service';

@Component({
  selector: 'gf-allergene-update',
  templateUrl: './allergene-update.component.html',
})
export class AllergeneUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nom: [],
  });

  constructor(protected allergeneService: AllergeneService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allergene }) => {
      this.updateForm(allergene);
    });
  }

  updateForm(allergene: IAllergene): void {
    this.editForm.patchValue({
      id: allergene.id,
      nom: allergene.nom,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const allergene = this.createFromForm();
    if (allergene.id !== undefined) {
      this.subscribeToSaveResponse(this.allergeneService.update(allergene));
    } else {
      this.subscribeToSaveResponse(this.allergeneService.create(allergene));
    }
  }

  private createFromForm(): IAllergene {
    return {
      ...new Allergene(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAllergene>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
