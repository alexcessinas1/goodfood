import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { AllergeneComponent } from './allergene.component';
import { AllergeneDetailComponent } from './allergene-detail.component';
import { AllergeneUpdateComponent } from './allergene-update.component';
import { AllergeneDeleteDialogComponent } from './allergene-delete-dialog.component';
import { allergeneRoute } from './allergene.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(allergeneRoute)],
  declarations: [AllergeneComponent, AllergeneDetailComponent, AllergeneUpdateComponent, AllergeneDeleteDialogComponent],
  entryComponents: [AllergeneDeleteDialogComponent],
})
export class GoodFoodAllergeneModule {}
