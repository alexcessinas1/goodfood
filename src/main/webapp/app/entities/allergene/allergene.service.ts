import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IAllergene } from 'app/shared/model/allergene.model';

type EntityResponseType = HttpResponse<IAllergene>;
type EntityArrayResponseType = HttpResponse<IAllergene[]>;

@Injectable({ providedIn: 'root' })
export class AllergeneService {
  public resourceUrl = SERVER_API_URL + 'api/allergenes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/allergenes';

  constructor(protected http: HttpClient) {}

  create(allergene: IAllergene): Observable<EntityResponseType> {
    return this.http.post<IAllergene>(this.resourceUrl, allergene, { observe: 'response' });
  }

  update(allergene: IAllergene): Observable<EntityResponseType> {
    return this.http.put<IAllergene>(this.resourceUrl, allergene, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAllergene>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAllergene[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAllergene[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
