import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { AllergeneComponent } from 'app/entities/allergene/allergene.component';
import { AllergeneService } from 'app/entities/allergene/allergene.service';
import { Allergene } from 'app/shared/model/allergene.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Allergene Management Component', () => {
    let comp: AllergeneComponent;
    let fixture: ComponentFixture<AllergeneComponent>;
    let service: AllergeneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [AllergeneComponent],
      })
        .overrideTemplate(AllergeneComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AllergeneComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AllergeneService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Allergene(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.allergenes && comp.allergenes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
