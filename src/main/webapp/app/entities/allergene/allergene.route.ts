import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAllergene, Allergene } from 'app/shared/model/allergene.model';
import { AllergeneService } from './allergene.service';
import { AllergeneComponent } from './allergene.component';
import { AllergeneDetailComponent } from './allergene-detail.component';
import { AllergeneUpdateComponent } from './allergene-update.component';

@Injectable({ providedIn: 'root' })
export class AllergeneResolve implements Resolve<IAllergene> {
  constructor(private service: AllergeneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAllergene> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((allergene: HttpResponse<Allergene>) => {
          if (allergene.body) {
            return of(allergene.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Allergene());
  }
}

export const allergeneRoute: Routes = [
  {
    path: '',
    component: AllergeneComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.allergene.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AllergeneDetailComponent,
    resolve: {
      allergene: AllergeneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.allergene.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AllergeneUpdateComponent,
    resolve: {
      allergene: AllergeneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.allergene.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AllergeneUpdateComponent,
    resolve: {
      allergene: AllergeneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.allergene.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
