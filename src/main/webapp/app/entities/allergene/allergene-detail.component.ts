import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAllergene } from 'app/shared/model/allergene.model';

@Component({
  selector: 'gf-allergene-detail',
  templateUrl: './allergene-detail.component.html',
})
export class AllergeneDetailComponent implements OnInit {
  allergene: IAllergene | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allergene }) => (this.allergene = allergene));
  }

  previousState(): void {
    window.history.back();
  }
}
