import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAllergene } from 'app/shared/model/allergene.model';
import { AllergeneService } from './allergene.service';

@Component({
  templateUrl: './allergene-delete-dialog.component.html',
})
export class AllergeneDeleteDialogComponent {
  allergene?: IAllergene;

  constructor(protected allergeneService: AllergeneService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.allergeneService.delete(id).subscribe(() => {
      this.eventManager.broadcast('allergeneListModification');
      this.activeModal.close();
    });
  }
}
