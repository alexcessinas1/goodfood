import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAllergene } from 'app/shared/model/allergene.model';
import { AllergeneService } from './allergene.service';
import { AllergeneDeleteDialogComponent } from './allergene-delete-dialog.component';

@Component({
  selector: 'gf-allergene',
  templateUrl: './allergene.component.html',
})
export class AllergeneComponent implements OnInit, OnDestroy {
  allergenes?: IAllergene[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected allergeneService: AllergeneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.allergeneService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IAllergene[]>) => (this.allergenes = res.body || []));
      return;
    }

    this.allergeneService.query().subscribe((res: HttpResponse<IAllergene[]>) => (this.allergenes = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAllergenes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAllergene): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAllergenes(): void {
    this.eventSubscriber = this.eventManager.subscribe('allergeneListModification', () => this.loadAll());
  }

  delete(allergene: IAllergene): void {
    const modalRef = this.modalService.open(AllergeneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.allergene = allergene;
  }
}
