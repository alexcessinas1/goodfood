import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { AllergeneUpdateComponent } from 'app/entities/allergene/allergene-update.component';
import { AllergeneService } from 'app/entities/allergene/allergene.service';
import { Allergene } from 'app/shared/model/allergene.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Allergene Management Update Component', () => {
    let comp: AllergeneUpdateComponent;
    let fixture: ComponentFixture<AllergeneUpdateComponent>;
    let service: AllergeneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [AllergeneUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AllergeneUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AllergeneUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AllergeneService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Allergene(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Allergene();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
