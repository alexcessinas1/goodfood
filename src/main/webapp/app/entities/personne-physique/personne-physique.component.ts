import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPersonnePhysique } from 'app/shared/model/personne-physique.model';
import { PersonnePhysiqueService } from './personne-physique.service';
import { PersonnePhysiqueDeleteDialogComponent } from './personne-physique-delete-dialog.component';

@Component({
  selector: 'gf-personne-physique',
  templateUrl: './personne-physique.component.html',
})
export class PersonnePhysiqueComponent implements OnInit, OnDestroy {
  personnePhysiques?: IPersonnePhysique[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected personnePhysiqueService: PersonnePhysiqueService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.personnePhysiqueService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IPersonnePhysique[]>) => (this.personnePhysiques = res.body || []));
      return;
    }

    this.personnePhysiqueService.query().subscribe((res: HttpResponse<IPersonnePhysique[]>) => (this.personnePhysiques = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPersonnePhysiques();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPersonnePhysique): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPersonnePhysiques(): void {
    this.eventSubscriber = this.eventManager.subscribe('personnePhysiqueListModification', () => this.loadAll());
  }

  delete(personnePhysique: IPersonnePhysique): void {
    const modalRef = this.modalService.open(PersonnePhysiqueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.personnePhysique = personnePhysique;
  }
}
