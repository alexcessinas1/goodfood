import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPersonnePhysique, PersonnePhysique } from 'app/shared/model/personne-physique.model';
import { PersonnePhysiqueService } from './personne-physique.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'gf-personne-physique-update',
  templateUrl: './personne-physique-update.component.html',
})
export class PersonnePhysiqueUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  dateNaissanceDp: any;

  editForm = this.fb.group({
    id: [],
    dateNaissance: [],
    userId: [],
  });

  constructor(
    protected personnePhysiqueService: PersonnePhysiqueService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ personnePhysique }) => {
      this.updateForm(personnePhysique);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(personnePhysique: IPersonnePhysique): void {
    this.editForm.patchValue({
      id: personnePhysique.id,
      dateNaissance: personnePhysique.dateNaissance,
      userId: personnePhysique.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const personnePhysique = this.createFromForm();
    if (personnePhysique.id !== undefined) {
      this.subscribeToSaveResponse(this.personnePhysiqueService.update(personnePhysique));
    } else {
      this.subscribeToSaveResponse(this.personnePhysiqueService.create(personnePhysique));
    }
  }

  private createFromForm(): IPersonnePhysique {
    return {
      ...new PersonnePhysique(),
      id: this.editForm.get(['id'])!.value,
      dateNaissance: this.editForm.get(['dateNaissance'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPersonnePhysique>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
