import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { PersonnePhysiqueComponent } from 'app/entities/personne-physique/personne-physique.component';
import { PersonnePhysiqueService } from 'app/entities/personne-physique/personne-physique.service';
import { PersonnePhysique } from 'app/shared/model/personne-physique.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('PersonnePhysique Management Component', () => {
    let comp: PersonnePhysiqueComponent;
    let fixture: ComponentFixture<PersonnePhysiqueComponent>;
    let service: PersonnePhysiqueService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [PersonnePhysiqueComponent],
      })
        .overrideTemplate(PersonnePhysiqueComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PersonnePhysiqueComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PersonnePhysiqueService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PersonnePhysique({ id: 123 })],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.personnePhysiques && comp.personnePhysiques[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
