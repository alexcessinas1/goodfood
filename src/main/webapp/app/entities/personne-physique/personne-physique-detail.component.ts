import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPersonnePhysique } from 'app/shared/model/personne-physique.model';

@Component({
  selector: 'gf-personne-physique-detail',
  templateUrl: './personne-physique-detail.component.html',
})
export class PersonnePhysiqueDetailComponent implements OnInit {
  personnePhysique: IPersonnePhysique | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ personnePhysique }) => (this.personnePhysique = personnePhysique));
  }

  previousState(): void {
    window.history.back();
  }
}
