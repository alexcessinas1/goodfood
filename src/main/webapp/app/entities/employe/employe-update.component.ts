import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEmploye, Employe } from 'app/shared/model/employe.model';
import { EmployeService } from './employe.service';
import { IFranchise } from 'app/shared/model/franchise.model';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { IAdresse } from 'app/shared/model/adresse.model';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { IPersonnePhysique } from 'app/shared/model/personne-physique.model';
import { PersonnePhysiqueService } from 'app/entities/personne-physique/personne-physique.service';
import { IPersonneMorale } from 'app/shared/model/personne-morale.model';
import { PersonneMoraleService } from 'app/entities/personne-morale/personne-morale.service';

type SelectableEntity = IFranchise | IAdresse | IPersonnePhysique | IPersonneMorale;

@Component({
  selector: 'gf-employe-update',
  templateUrl: './employe-update.component.html',
})
export class EmployeUpdateComponent implements OnInit {
  isSaving = false;
  franchises: IFranchise[] = [];
  adresses: IAdresse[] = [];
  personnephysiques: IPersonnePhysique[] = [];
  personnemorales: IPersonneMorale[] = [];

  editForm = this.fb.group({
    id: [],
    poste: [],
    notes: [],
    franchiseId: [],
    adresseId: [null, Validators.required],
    personnePhysiqueId: [],
    personneMoraleId: [],
  });

  constructor(
    protected employeService: EmployeService,
    protected franchiseService: FranchiseService,
    protected adresseService: AdresseService,
    protected personnePhysiqueService: PersonnePhysiqueService,
    protected personneMoraleService: PersonneMoraleService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employe }) => {
      this.updateForm(employe);

      this.franchiseService.query().subscribe((res: HttpResponse<IFranchise[]>) => (this.franchises = res.body || []));

      this.adresseService.query().subscribe((res: HttpResponse<IAdresse[]>) => (this.adresses = res.body || []));

      this.personnePhysiqueService.query().subscribe((res: HttpResponse<IPersonnePhysique[]>) => (this.personnephysiques = res.body || []));

      this.personneMoraleService.query().subscribe((res: HttpResponse<IPersonneMorale[]>) => (this.personnemorales = res.body || []));
    });
  }

  updateForm(employe: IEmploye): void {
    this.editForm.patchValue({
      id: employe.id,
      poste: employe.poste,
      notes: employe.notes,
      franchiseId: employe.franchiseId,
      adresseId: employe.adresseId,
      personnePhysiqueId: employe.personnePhysiqueId,
      personneMoraleId: employe.personneMoraleId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const employe = this.createFromForm();
    if (employe.id !== undefined) {
      this.subscribeToSaveResponse(this.employeService.update(employe));
    } else {
      this.subscribeToSaveResponse(this.employeService.create(employe));
    }
  }

  private createFromForm(): IEmploye {
    return {
      ...new Employe(),
      id: this.editForm.get(['id'])!.value,
      poste: this.editForm.get(['poste'])!.value,
      notes: this.editForm.get(['notes'])!.value,
      franchiseId: this.editForm.get(['franchiseId'])!.value,
      adresseId: this.editForm.get(['adresseId'])!.value,
      personnePhysiqueId: this.editForm.get(['personnePhysiqueId'])!.value,
      personneMoraleId: this.editForm.get(['personneMoraleId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmploye>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
