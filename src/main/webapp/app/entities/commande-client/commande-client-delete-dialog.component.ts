import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { CommandeClientService } from './commande-client.service';

@Component({
  templateUrl: './commande-client-delete-dialog.component.html',
})
export class CommandeClientDeleteDialogComponent {
  commandeClient?: ICommandeClient;

  constructor(
    protected commandeClientService: CommandeClientService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commandeClientService.delete(id).subscribe(() => {
      this.eventManager.broadcast('commandeClientListModification');
      this.activeModal.close();
    });
  }
}
