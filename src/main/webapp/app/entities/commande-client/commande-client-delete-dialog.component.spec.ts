import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { CommandeClientDeleteDialogComponent } from 'app/entities/commande-client/commande-client-delete-dialog.component';
import { CommandeClientService } from 'app/entities/commande-client/commande-client.service';
import { MockActiveModal } from 'app/shared/util/helpers/mock-active-modal.service';
import { MockEventManager } from 'app/shared/util/helpers/mock-event-manager.service';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CommandeClient Management Delete Component', () => {
    let comp: CommandeClientDeleteDialogComponent;
    let fixture: ComponentFixture<CommandeClientDeleteDialogComponent>;
    let service: CommandeClientService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CommandeClientDeleteDialogComponent],
      })
        .overrideTemplate(CommandeClientDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommandeClientDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommandeClientService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
