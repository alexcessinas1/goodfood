import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { CommandeClientDetailComponent } from 'app/entities/commande-client/commande-client-detail.component';
import { CommandeClient } from 'app/shared/model/commande-client.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CommandeClient Management Detail Component', () => {
    let comp: CommandeClientDetailComponent;
    let fixture: ComponentFixture<CommandeClientDetailComponent>;
    const route = ({ data: of({ commandeClient: new CommandeClient({ id: 123 }) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CommandeClientDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CommandeClientDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommandeClientDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load commandeClient on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.commandeClient).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
