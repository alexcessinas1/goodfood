import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { CommandeClientComponent } from 'app/entities/commande-client/commande-client.component';
import { CommandeClientService } from 'app/entities/commande-client/commande-client.service';
import { CommandeClient } from 'app/shared/model/commande-client.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CommandeClient Management Component', () => {
    let comp: CommandeClientComponent;
    let fixture: ComponentFixture<CommandeClientComponent>;
    let service: CommandeClientService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CommandeClientComponent],
      })
        .overrideTemplate(CommandeClientComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommandeClientComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommandeClientService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CommandeClient({ id: 123 })],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.commandeClients && comp.commandeClients[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
