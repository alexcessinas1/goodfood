import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICommandeClient, CommandeClient } from 'app/shared/model/commande-client.model';
import { CommandeClientService } from './commande-client.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IEmploye } from 'app/shared/model/employe.model';
import { EmployeService } from 'app/entities/employe/employe.service';
import { ICommande } from 'app/shared/model/commande.model';
import { CommandeService } from 'app/entities/commande/commande.service';

type SelectableEntity = IClient | IEmploye | ICommande;

@Component({
  selector: 'gf-commande-client-update',
  templateUrl: './commande-client-update.component.html',
})
export class CommandeClientUpdateComponent implements OnInit {
  isSaving = false;
  clients: IClient[] = [];
  employes: IEmploye[] = [];
  commandes: ICommande[] = [];

  editForm = this.fb.group({
    id: [],
    codePromo: [],
    clientId: [null, Validators.required],
    employeId: [],
    commandeId: [null, Validators.required],
  });

  constructor(
    protected commandeClientService: CommandeClientService,
    protected clientService: ClientService,
    protected employeService: EmployeService,
    protected commandeService: CommandeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commandeClient }) => {
      this.updateForm(commandeClient);

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));

      this.employeService.query().subscribe((res: HttpResponse<IEmploye[]>) => (this.employes = res.body || []));

      this.commandeService
        .query({ filter: 'commandeclient-is-null' })
        .pipe(
          map((res: HttpResponse<ICommande[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICommande[]) => {
          if (!commandeClient.commandeId) {
            this.commandes = resBody;
          } else {
            this.commandeService
              .find(commandeClient.commandeId)
              .pipe(
                map((subRes: HttpResponse<ICommande>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICommande[]) => (this.commandes = concatRes));
          }
        });
    });
  }

  updateForm(commandeClient: ICommandeClient): void {
    this.editForm.patchValue({
      id: commandeClient.id,
      codePromo: commandeClient.codePromo,
      clientId: commandeClient.clientId,
      employeId: commandeClient.employeId,
      commandeId: commandeClient.commandeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commandeClient = this.createFromForm();
    if (commandeClient.id !== undefined) {
      this.subscribeToSaveResponse(this.commandeClientService.update(commandeClient));
    } else {
      this.subscribeToSaveResponse(this.commandeClientService.create(commandeClient));
    }
  }

  private createFromForm(): ICommandeClient {
    return {
      ...new CommandeClient(),
      id: this.editForm.get(['id'])!.value,
      codePromo: this.editForm.get(['codePromo'])!.value,
      clientId: this.editForm.get(['clientId'])!.value,
      employeId: this.editForm.get(['employeId'])!.value,
      commandeId: this.editForm.get(['commandeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommandeClient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
