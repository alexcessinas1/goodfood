import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICommandeClient, CommandeClient } from 'app/shared/model/commande-client.model';
import { CommandeClientService } from './commande-client.service';
import { CommandeClientComponent } from './commande-client.component';
import { CommandeClientDetailComponent } from './commande-client-detail.component';
import { CommandeClientUpdateComponent } from './commande-client-update.component';

@Injectable({ providedIn: 'root' })
export class CommandeClientResolve implements Resolve<ICommandeClient> {
  constructor(private service: CommandeClientService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommandeClient> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((commandeClient: HttpResponse<CommandeClient>) => {
          if (commandeClient.body) {
            return of(commandeClient.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CommandeClient());
  }
}

export const commandeClientRoute: Routes = [
  {
    path: '',
    component: CommandeClientComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.commandeClient.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CommandeClientDetailComponent,
    resolve: {
      commandeClient: CommandeClientResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.commandeClient.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CommandeClientUpdateComponent,
    resolve: {
      commandeClient: CommandeClientResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.commandeClient.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CommandeClientUpdateComponent,
    resolve: {
      commandeClient: CommandeClientResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.commandeClient.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
