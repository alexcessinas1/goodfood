import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { CommandeClientComponent } from './commande-client.component';
import { CommandeClientDetailComponent } from './commande-client-detail.component';
import { CommandeClientUpdateComponent } from './commande-client-update.component';
import { CommandeClientDeleteDialogComponent } from './commande-client-delete-dialog.component';
import { commandeClientRoute } from './commande-client.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(commandeClientRoute)],
  declarations: [
    CommandeClientComponent,
    CommandeClientDetailComponent,
    CommandeClientUpdateComponent,
    CommandeClientDeleteDialogComponent,
  ],
  entryComponents: [CommandeClientDeleteDialogComponent],
})
export class GoodFoodCommandeClientModule {}
