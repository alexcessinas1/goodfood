import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { CommandeClientService } from './commande-client.service';
import { CommandeClientDeleteDialogComponent } from './commande-client-delete-dialog.component';

@Component({
  selector: 'gf-commande-client',
  templateUrl: './commande-client.component.html',
})
export class CommandeClientComponent implements OnInit, OnDestroy {
  commandeClients?: ICommandeClient[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected commandeClientService: CommandeClientService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.commandeClientService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ICommandeClient[]>) => (this.commandeClients = res.body || []));
      return;
    }

    this.commandeClientService.query().subscribe((res: HttpResponse<ICommandeClient[]>) => (this.commandeClients = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCommandeClients();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICommandeClient): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCommandeClients(): void {
    this.eventSubscriber = this.eventManager.subscribe('commandeClientListModification', () => this.loadAll());
  }

  delete(commandeClient: ICommandeClient): void {
    const modalRef = this.modalService.open(CommandeClientDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.commandeClient = commandeClient;
  }
}
