import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICommandeClient } from 'app/shared/model/commande-client.model';

type EntityResponseType = HttpResponse<ICommandeClient>;
type EntityArrayResponseType = HttpResponse<ICommandeClient[]>;

@Injectable({ providedIn: 'root' })
export class CommandeClientService {
  public resourceUrl = SERVER_API_URL + 'api/commande-clients';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/commande-clients';

  constructor(protected http: HttpClient) {}

  create(commandeClient: ICommandeClient): Observable<EntityResponseType> {
    return this.http.post<ICommandeClient>(this.resourceUrl, commandeClient, { observe: 'response' });
  }

  update(commandeClient: ICommandeClient): Observable<EntityResponseType> {
    return this.http.put<ICommandeClient>(this.resourceUrl, commandeClient, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICommandeClient>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommandeClient[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommandeClient[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
