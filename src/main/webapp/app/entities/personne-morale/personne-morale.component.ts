import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPersonneMorale } from 'app/shared/model/personne-morale.model';
import { PersonneMoraleService } from './personne-morale.service';
import { PersonneMoraleDeleteDialogComponent } from './personne-morale-delete-dialog.component';

@Component({
  selector: 'gf-personne-morale',
  templateUrl: './personne-morale.component.html',
})
export class PersonneMoraleComponent implements OnInit, OnDestroy {
  personneMorales?: IPersonneMorale[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected personneMoraleService: PersonneMoraleService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.personneMoraleService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IPersonneMorale[]>) => (this.personneMorales = res.body || []));
      return;
    }

    this.personneMoraleService.query().subscribe((res: HttpResponse<IPersonneMorale[]>) => (this.personneMorales = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPersonneMorales();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPersonneMorale): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPersonneMorales(): void {
    this.eventSubscriber = this.eventManager.subscribe('personneMoraleListModification', () => this.loadAll());
  }

  delete(personneMorale: IPersonneMorale): void {
    const modalRef = this.modalService.open(PersonneMoraleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.personneMorale = personneMorale;
  }
}
