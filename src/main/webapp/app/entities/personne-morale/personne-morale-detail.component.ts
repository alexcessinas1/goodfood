import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPersonneMorale } from 'app/shared/model/personne-morale.model';

@Component({
  selector: 'gf-personne-morale-detail',
  templateUrl: './personne-morale-detail.component.html',
})
export class PersonneMoraleDetailComponent implements OnInit {
  personneMorale: IPersonneMorale | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ personneMorale }) => (this.personneMorale = personneMorale));
  }

  previousState(): void {
    window.history.back();
  }
}
