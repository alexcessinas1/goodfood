import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IPersonneMorale } from 'app/shared/model/personne-morale.model';

type EntityResponseType = HttpResponse<IPersonneMorale>;
type EntityArrayResponseType = HttpResponse<IPersonneMorale[]>;

@Injectable({ providedIn: 'root' })
export class PersonneMoraleService {
  public resourceUrl = SERVER_API_URL + 'api/personne-morales';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/personne-morales';

  constructor(protected http: HttpClient) {}

  create(personneMorale: IPersonneMorale): Observable<EntityResponseType> {
    return this.http.post<IPersonneMorale>(this.resourceUrl, personneMorale, { observe: 'response' });
  }

  update(personneMorale: IPersonneMorale): Observable<EntityResponseType> {
    return this.http.put<IPersonneMorale>(this.resourceUrl, personneMorale, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPersonneMorale>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPersonneMorale[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPersonneMorale[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
