import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPersonneMorale, PersonneMorale } from 'app/shared/model/personne-morale.model';
import { PersonneMoraleService } from './personne-morale.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'gf-personne-morale-update',
  templateUrl: './personne-morale-update.component.html',
})
export class PersonneMoraleUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    raisonSociale: [],
    siret: [],
    userId: [],
  });

  constructor(
    protected personneMoraleService: PersonneMoraleService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ personneMorale }) => {
      this.updateForm(personneMorale);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(personneMorale: IPersonneMorale): void {
    this.editForm.patchValue({
      id: personneMorale.id,
      raisonSociale: personneMorale.raisonSociale,
      siret: personneMorale.siret,
      userId: personneMorale.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const personneMorale = this.createFromForm();
    if (personneMorale.id !== undefined) {
      this.subscribeToSaveResponse(this.personneMoraleService.update(personneMorale));
    } else {
      this.subscribeToSaveResponse(this.personneMoraleService.create(personneMorale));
    }
  }

  private createFromForm(): IPersonneMorale {
    return {
      ...new PersonneMorale(),
      id: this.editForm.get(['id'])!.value,
      raisonSociale: this.editForm.get(['raisonSociale'])!.value,
      siret: this.editForm.get(['siret'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPersonneMorale>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
