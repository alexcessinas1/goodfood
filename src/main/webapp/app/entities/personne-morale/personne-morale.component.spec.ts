import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { PersonneMoraleComponent } from 'app/entities/personne-morale/personne-morale.component';
import { PersonneMoraleService } from 'app/entities/personne-morale/personne-morale.service';
import { PersonneMorale } from 'app/shared/model/personne-morale.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('PersonneMorale Management Component', () => {
    let comp: PersonneMoraleComponent;
    let fixture: ComponentFixture<PersonneMoraleComponent>;
    let service: PersonneMoraleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [PersonneMoraleComponent],
      })
        .overrideTemplate(PersonneMoraleComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PersonneMoraleComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PersonneMoraleService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PersonneMorale(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.personneMorales && comp.personneMorales[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
