import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPersonneMorale, PersonneMorale } from 'app/shared/model/personne-morale.model';
import { PersonneMoraleService } from './personne-morale.service';
import { PersonneMoraleComponent } from './personne-morale.component';
import { PersonneMoraleDetailComponent } from './personne-morale-detail.component';
import { PersonneMoraleUpdateComponent } from './personne-morale-update.component';

@Injectable({ providedIn: 'root' })
export class PersonneMoraleResolve implements Resolve<IPersonneMorale> {
  constructor(private service: PersonneMoraleService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPersonneMorale> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((personneMorale: HttpResponse<PersonneMorale>) => {
          if (personneMorale.body) {
            return of(personneMorale.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PersonneMorale());
  }
}

export const personneMoraleRoute: Routes = [
  {
    path: '',
    component: PersonneMoraleComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.personneMorale.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PersonneMoraleDetailComponent,
    resolve: {
      personneMorale: PersonneMoraleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.personneMorale.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PersonneMoraleUpdateComponent,
    resolve: {
      personneMorale: PersonneMoraleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.personneMorale.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PersonneMoraleUpdateComponent,
    resolve: {
      personneMorale: PersonneMoraleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.personneMorale.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
