import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { PlateauRepasComponent } from 'app/entities/plateau-repas/plateau-repas.component';
import { PlateauRepasService } from 'app/entities/plateau-repas/plateau-repas.service';
import { PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('PlateauRepas Management Component', () => {
    let comp: PlateauRepasComponent;
    let fixture: ComponentFixture<PlateauRepasComponent>;
    let service: PlateauRepasService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [PlateauRepasComponent],
      })
        .overrideTemplate(PlateauRepasComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlateauRepasComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlateauRepasService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PlateauRepas({ id: 123 })],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.plateauRepas && comp.plateauRepas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
