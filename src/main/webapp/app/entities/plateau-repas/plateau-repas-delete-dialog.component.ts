import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPlateauRepas } from 'app/shared/model/plateau-repas.model';
import { PlateauRepasService } from './plateau-repas.service';

@Component({
  templateUrl: './plateau-repas-delete-dialog.component.html',
})
export class PlateauRepasDeleteDialogComponent {
  plateauRepas?: IPlateauRepas;

  constructor(
    protected plateauRepasService: PlateauRepasService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.plateauRepasService.delete(id).subscribe(() => {
      this.eventManager.broadcast('plateauRepasListModification');
      this.activeModal.close();
    });
  }
}
