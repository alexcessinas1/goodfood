import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPlateauRepas } from 'app/shared/model/plateau-repas.model';

@Component({
  selector: 'gf-plateau-repas-detail',
  templateUrl: './plateau-repas-detail.component.html',
})
export class PlateauRepasDetailComponent implements OnInit {
  plateauRepas: IPlateauRepas | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ plateauRepas }) => (this.plateauRepas = plateauRepas));
  }

  previousState(): void {
    window.history.back();
  }
}
