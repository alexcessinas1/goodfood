import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPlateauRepas } from 'app/shared/model/plateau-repas.model';
import { PlateauRepasService } from './plateau-repas.service';
import { PlateauRepasDeleteDialogComponent } from './plateau-repas-delete-dialog.component';

@Component({
  selector: 'gf-plateau-repas',
  templateUrl: './plateau-repas.component.html',
})
export class PlateauRepasComponent implements OnInit, OnDestroy {
  plateauRepas?: IPlateauRepas[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected plateauRepasService: PlateauRepasService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.plateauRepasService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IPlateauRepas[]>) => (this.plateauRepas = res.body || []));
      return;
    }

    this.plateauRepasService.query().subscribe((res: HttpResponse<IPlateauRepas[]>) => (this.plateauRepas = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPlateauRepas();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPlateauRepas): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPlateauRepas(): void {
    this.eventSubscriber = this.eventManager.subscribe('plateauRepasListModification', () => this.loadAll());
  }

  delete(plateauRepas: IPlateauRepas): void {
    const modalRef = this.modalService.open(PlateauRepasDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.plateauRepas = plateauRepas;
  }
}
