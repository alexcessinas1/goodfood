import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { PlateauRepasDetailComponent } from 'app/entities/plateau-repas/plateau-repas-detail.component';
import { PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('PlateauRepas Management Detail Component', () => {
    let comp: PlateauRepasDetailComponent;
    let fixture: ComponentFixture<PlateauRepasDetailComponent>;
    const route = ({ data: of({ plateauRepas: new PlateauRepas({ id: 123 }) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [PlateauRepasDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PlateauRepasDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PlateauRepasDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load plateauRepas on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.plateauRepas).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
