import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPlateauRepas, PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { PlateauRepasService } from './plateau-repas.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IRecette } from 'app/shared/model/recette.model';
import { RecetteService } from '../recette/recette.service';

@Component({
  selector: 'gf-plateau-repas-update',
  templateUrl: './plateau-repas-update.component.html',
})
export class PlateauRepasUpdateComponent implements OnInit {
  isSaving = false;
  produits: IProduit[] = [];
  recetteListe: IRecette[] = [];

  editForm = this.fb.group({
    id: [],
    prixHT: [],
    tva: [null, [Validators.min(0), Validators.max(100)]],
    composition: [],
    couvert: [],
    serviette: [],
    boissonId: [],
    recettes: [],
  });

  constructor(
    protected plateauRepasService: PlateauRepasService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    protected recetteService: RecetteService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ plateauRepas }) => {
      this.updateForm(plateauRepas);

      this.produitService.query().subscribe((res: HttpResponse<IProduit[]>) => (this.produits = res.body || []));
      this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => (this.recetteListe = res.body || []));
    });
  }

  updateForm(plateauRepas: IPlateauRepas): void {
    this.editForm.patchValue({
      id: plateauRepas?.id,
      prixHT: plateauRepas?.prixHT,
      tva: plateauRepas?.tva,
      composition: plateauRepas?.composition,
      couvert: plateauRepas?.couvert,
      serviette: plateauRepas?.serviette,
      boissonId: plateauRepas?.boissonId,
      recettes: plateauRepas?.recettes,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const plateauRepas = this.createFromForm();
    if (plateauRepas.id !== undefined) {
      this.subscribeToSaveResponse(this.plateauRepasService.update(plateauRepas));
    } else {
      this.subscribeToSaveResponse(this.plateauRepasService.create(plateauRepas));
    }
  }

  private createFromForm(): IPlateauRepas {
    return {
      ...new PlateauRepas(),
      id: this.editForm.get(['id'])!.value,
      prixHT: this.editForm.get(['prixHT'])!.value,
      tva: this.editForm.get(['tva'])!.value,
      composition: this.editForm.get(['composition'])!.value,
      couvert: this.editForm.get(['couvert'])!.value,
      serviette: this.editForm.get(['serviette'])!.value,
      boissonId: this.editForm.get(['boissonId'])!.value,
      recettes: this.editForm.get('recettes')!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPlateauRepas>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProduit): any {
    return item.id;
  }
  containsRecetteId(obj1?: IRecette, obj2?: IRecette): boolean {
    return obj1?.id === obj2?.id;
  }
}
