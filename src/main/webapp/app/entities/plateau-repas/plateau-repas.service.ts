import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IPlateauRepas } from 'app/shared/model/plateau-repas.model';

type EntityResponseType = HttpResponse<IPlateauRepas>;
type EntityArrayResponseType = HttpResponse<IPlateauRepas[]>;

@Injectable({ providedIn: 'root' })
export class PlateauRepasService {
  public resourceUrl = SERVER_API_URL + 'api/plateau-repas';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/plateau-repas';

  constructor(protected http: HttpClient) {}

  create(plateauRepas: IPlateauRepas): Observable<EntityResponseType> {
    return this.http.post<IPlateauRepas>(this.resourceUrl, plateauRepas, { observe: 'response' });
  }

  update(plateauRepas: IPlateauRepas): Observable<EntityResponseType> {
    return this.http.put<IPlateauRepas>(this.resourceUrl, plateauRepas, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPlateauRepas>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPlateauRepas[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPlateauRepas[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
