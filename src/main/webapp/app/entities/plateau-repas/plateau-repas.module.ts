import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { PlateauRepasComponent } from './plateau-repas.component';
import { PlateauRepasDetailComponent } from './plateau-repas-detail.component';
import { PlateauRepasUpdateComponent } from './plateau-repas-update.component';
import { PlateauRepasDeleteDialogComponent } from './plateau-repas-delete-dialog.component';
import { plateauRepasRoute } from './plateau-repas.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(plateauRepasRoute)],
  declarations: [PlateauRepasComponent, PlateauRepasDetailComponent, PlateauRepasUpdateComponent, PlateauRepasDeleteDialogComponent],
  entryComponents: [PlateauRepasDeleteDialogComponent],
})
export class GoodFoodPlateauRepasModule {}
