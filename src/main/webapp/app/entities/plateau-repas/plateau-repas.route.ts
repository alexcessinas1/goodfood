import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPlateauRepas, PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { PlateauRepasService } from './plateau-repas.service';
import { PlateauRepasComponent } from './plateau-repas.component';
import { PlateauRepasDetailComponent } from './plateau-repas-detail.component';
import { PlateauRepasUpdateComponent } from './plateau-repas-update.component';

@Injectable({ providedIn: 'root' })
export class PlateauRepasResolve implements Resolve<IPlateauRepas> {
  constructor(private service: PlateauRepasService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPlateauRepas> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((plateauRepas: HttpResponse<PlateauRepas>) => {
          if (plateauRepas.body) {
            return of(plateauRepas.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PlateauRepas());
  }
}

export const plateauRepasRoute: Routes = [
  {
    path: '',
    component: PlateauRepasComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.plateauRepas.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PlateauRepasDetailComponent,
    resolve: {
      plateauRepas: PlateauRepasResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.plateauRepas.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PlateauRepasUpdateComponent,
    resolve: {
      plateauRepas: PlateauRepasResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.plateauRepas.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PlateauRepasUpdateComponent,
    resolve: {
      plateauRepas: PlateauRepasResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.plateauRepas.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
