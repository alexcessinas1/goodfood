import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PlateauRepasService } from 'app/entities/plateau-repas/plateau-repas.service';
import { IPlateauRepas, PlateauRepas } from 'app/shared/model/plateau-repas.model';

describe('Service Tests', () => {
  describe('PlateauRepas Service', () => {
    let injector: TestBed;
    let service: PlateauRepasService;
    let httpMock: HttpTestingController;
    let elemDefault: IPlateauRepas;
    let expectedResult: IPlateauRepas | IPlateauRepas[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(PlateauRepasService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new PlateauRepas({
        id: 0,
        prixHT: 0,
        tva: 0,
        composition: 'AAAAAA',
        couvert: true,
        serviette: true,
      });
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PlateauRepas', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new PlateauRepas()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PlateauRepas', () => {
        const returnedFromService = Object.assign(
          {
            prixHT: 1,
            tva: 1,
            composition: 'BBBBBB',
            couvert: true,
            serviette: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PlateauRepas', () => {
        const returnedFromService = Object.assign(
          {
            prixHT: 1,
            tva: 1,
            composition: 'BBBBBB',
            couvert: true,
            serviette: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PlateauRepas', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
