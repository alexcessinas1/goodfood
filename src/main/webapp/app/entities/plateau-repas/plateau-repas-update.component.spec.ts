import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { PlateauRepasUpdateComponent } from 'app/entities/plateau-repas/plateau-repas-update.component';
import { PlateauRepasService } from 'app/entities/plateau-repas/plateau-repas.service';
import { PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { RecetteService } from 'app/entities/recette/recette.service';
import { ActivatedRoute } from '@angular/router';
import { Recette } from 'app/shared/model/recette.model';
import { Plat } from 'app/shared/model/enumerations/plat.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { Produit } from 'app/shared/model/produit.model';
import { GoodFoodTestModule } from 'app/shared/test.module';
import { MockActivatedRoute } from 'app/shared/util/helpers/mock-route.service';

describe('Component Tests', () => {
  describe('PlateauRepas Management Update Component', () => {
    let comp: PlateauRepasUpdateComponent;
    let fixture: ComponentFixture<PlateauRepasUpdateComponent>;
    let service: PlateauRepasService;
    let serviceRecette: RecetteService;
    let serviceProduit: ProduitService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [PlateauRepasUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PlateauRepasUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlateauRepasUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlateauRepasService);
      serviceRecette = fixture.debugElement.injector.get(RecetteService);
      serviceProduit = fixture.debugElement.injector.get(ProduitService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlateauRepas({ id: 123 });
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlateauRepas();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });

    describe('initialisation', () => {
      it('Le composant doit mettre a jour son formulaire avec les donnees de la route', fakeAsync(() => {
        const mockActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute) as MockActivatedRoute;
        mockActivatedRoute.setParameters({
          plateauRepas: new PlateauRepas({
            id: 4,
            prixHT: 10.75,
            tva: 20.6,
            composition: 'de la nourriture',
          }),
        });
        spyOn(serviceRecette, 'query').and.returnValue(
          of(new HttpResponse({ body: [new Recette(4, 'une recette', 'la description de recette', Plat.DESSERT)] }))
        );
        spyOn(serviceProduit, 'query').and.returnValue(of(new HttpResponse({ body: [new Produit(4, '4externe', 'patates', 1)] })));
        comp.ngOnInit();
        tick(); // simulate async

        expect(comp.editForm.get('id')?.value).toBe(4);
        expect(comp.recetteListe).toHaveLength(1);
        expect(comp.recetteListe[0].titre).toBe('une recette');
        expect(comp.produits).toHaveLength(1);
        expect(comp.produits[0].nom).toBe('patates');
      }));
      it('Le composant doit avoir une liste de produit vide si le serviceRecette ne retourne rien', fakeAsync(() => {
        const mockActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute) as MockActivatedRoute;
        mockActivatedRoute.setParameters({
          plateauRepas: new PlateauRepas({
            id: 4,
            prixHT: 10.75,
            tva: 20.6,
            composition: 'de la nourriture',
          }),
        });
        spyOn(serviceRecette, 'query').and.returnValue(of(new HttpResponse({ body: null })));
        spyOn(serviceProduit, 'query').and.returnValue(of(new HttpResponse({ body: null })));
        comp.ngOnInit();
        tick(); // simulate async

        expect(comp.recetteListe).toHaveLength(0);
        expect(comp.produits).toHaveLength(0);
      }));
    });

    describe('compareWith', () => {
      it('La fonction compareWith fonctionne', () => {
        expect(comp.containsRecetteId(undefined, undefined)).toBe(true);
        expect(comp.containsRecetteId(new Recette(2), undefined)).toBe(false);
        expect(comp.containsRecetteId(undefined, new Recette(2))).toBe(false);
        expect(comp.containsRecetteId(new Recette(2), new Recette(2))).toBe(true);
        expect(comp.containsRecetteId(new Recette(3), new Recette(2))).toBe(false);
        expect(comp.containsRecetteId(new Recette(), new Recette(2))).toBe(false);
        expect(comp.containsRecetteId(new Recette(2), new Recette())).toBe(false);
      });
    });
  });
});
