import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IClient, Client } from 'app/shared/model/client.model';
import { ClientService } from './client.service';
import { IAdresse } from 'app/shared/model/adresse.model';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { IPersonnePhysique } from 'app/shared/model/personne-physique.model';
import { PersonnePhysiqueService } from 'app/entities/personne-physique/personne-physique.service';
import { IPersonneMorale } from 'app/shared/model/personne-morale.model';
import { PersonneMoraleService } from 'app/entities/personne-morale/personne-morale.service';

type SelectableEntity = IAdresse | IPersonnePhysique | IPersonneMorale;

@Component({
  selector: 'gf-client-update',
  templateUrl: './client-update.component.html',
})
export class ClientUpdateComponent implements OnInit {
  isSaving = false;
  adresselivraisons: IAdresse[] = [];
  adressefacturations: IAdresse[] = [];
  personnephysiques: IPersonnePhysique[] = [];
  personnemorales: IPersonneMorale[] = [];

  editForm = this.fb.group({
    id: [],
    personneType: [],
    adresseLivraisonId: [],
    adresseFacturationId: [],
    personnePhysiqueId: [],
    personneMoraleId: [],
  });

  constructor(
    protected clientService: ClientService,
    protected adresseService: AdresseService,
    protected personnePhysiqueService: PersonnePhysiqueService,
    protected personneMoraleService: PersonneMoraleService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ client }) => {
      this.updateForm(client);

      this.adresseService
        .query({ filter: 'client-is-null' })
        .pipe(
          map((res: HttpResponse<IAdresse[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAdresse[]) => {
          if (!client.adresseLivraisonId) {
            this.adresselivraisons = resBody;
          } else {
            this.adresseService
              .find(client.adresseLivraisonId)
              .pipe(
                map((subRes: HttpResponse<IAdresse>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAdresse[]) => (this.adresselivraisons = concatRes));
          }
        });

      this.adresseService
        .query({ filter: 'client-is-null' })
        .pipe(
          map((res: HttpResponse<IAdresse[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAdresse[]) => {
          if (!client.adresseFacturationId) {
            this.adressefacturations = resBody;
          } else {
            this.adresseService
              .find(client.adresseFacturationId)
              .pipe(
                map((subRes: HttpResponse<IAdresse>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAdresse[]) => (this.adressefacturations = concatRes));
          }
        });

      this.personnePhysiqueService
        .query({ filter: 'client-is-null' })
        .pipe(
          map((res: HttpResponse<IPersonnePhysique[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IPersonnePhysique[]) => {
          if (!client.personnePhysiqueId) {
            this.personnephysiques = resBody;
          } else {
            this.personnePhysiqueService
              .find(client.personnePhysiqueId)
              .pipe(
                map((subRes: HttpResponse<IPersonnePhysique>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IPersonnePhysique[]) => (this.personnephysiques = concatRes));
          }
        });

      this.personneMoraleService
        .query({ filter: 'client-is-null' })
        .pipe(
          map((res: HttpResponse<IPersonneMorale[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IPersonneMorale[]) => {
          if (!client.personneMoraleId) {
            this.personnemorales = resBody;
          } else {
            this.personneMoraleService
              .find(client.personneMoraleId)
              .pipe(
                map((subRes: HttpResponse<IPersonneMorale>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IPersonneMorale[]) => (this.personnemorales = concatRes));
          }
        });
    });
  }

  updateForm(client: IClient): void {
    this.editForm.patchValue({
      id: client.id,
      personneType: client.personneType,
      adresseLivraisonId: client.adresseLivraisonId,
      adresseFacturationId: client.adresseFacturationId,
      personnePhysiqueId: client.personnePhysiqueId,
      personneMoraleId: client.personneMoraleId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const client = this.createFromForm();
    if (client.id !== undefined) {
      this.subscribeToSaveResponse(this.clientService.update(client));
    } else {
      this.subscribeToSaveResponse(this.clientService.create(client));
    }
  }

  private createFromForm(): IClient {
    return {
      ...new Client(),
      id: this.editForm.get(['id'])!.value,
      personneType: this.editForm.get(['personneType'])!.value,
      adresseLivraisonId: this.editForm.get(['adresseLivraisonId'])!.value,
      adresseFacturationId: this.editForm.get(['adresseFacturationId'])!.value,
      personnePhysiqueId: this.editForm.get(['personnePhysiqueId'])!.value,
      personneMoraleId: this.editForm.get(['personneMoraleId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
