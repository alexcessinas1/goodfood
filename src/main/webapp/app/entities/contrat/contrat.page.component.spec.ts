import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { GoodFoodModulesModule } from 'app/modules/modules.module';
import { ContratPageComponent } from 'app/entities/contrat/contrat.page.component';
import { TabViewModule } from 'primeng/tabview';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { fakeAsync, tick } from '@angular/core/testing';

describe('Component Tests', () => {
  describe('Contrat Page Component', () => {
    let spectator: Spectator<ContratPageComponent>;
    const createComponent = createComponentFactory({
      component: ContratPageComponent,
      imports: [GoodFoodModulesModule, TabViewModule, RouterTestingModule]
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize mention_legales', fakeAsync(() => {
      spyOn(spectator.inject(ActivatedRoute).snapshot.paramMap, 'get').and.returnValue("mention_legales");
      spectator.component.ngOnInit();
      tick();
      expect(spectator.component.activeIndex).toBe(0);
      expect(spectator.component).toBeDefined();
    }));

    it('should initialize CU', fakeAsync(() => {
      spyOn(spectator.inject(ActivatedRoute).snapshot.paramMap, 'get').and.returnValue("CU");
      spectator.component.ngOnInit();
      tick();
      expect(spectator.component.activeIndex).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should initialize CGv', fakeAsync(() => {
      spyOn(spectator.inject(ActivatedRoute).snapshot.paramMap, 'get').and.returnValue("CGV");
      spectator.component.ngOnInit();
      tick();
      expect(spectator.component.activeIndex).toBe(2);
      expect(spectator.component).toBeDefined();
    }));

    it('should initialize donnees_personnelles', fakeAsync(() => {
      spyOn(spectator.inject(ActivatedRoute).snapshot.paramMap, 'get').and.returnValue("donnees_personnelles");
      spectator.component.ngOnInit();
      tick();
      expect(spectator.component.activeIndex).toBe(3);
      expect(spectator.component).toBeDefined();
    }));
  });
});
