import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'gf-contrat',
    templateUrl: './contrat.page.component.html',
    styleUrls:['./contrat.page.component.scss']
})
export class ContratPageComponent implements OnInit {

    activeIndex = 0;

    constructor(private route: ActivatedRoute) {}

    ngOnInit(): void {
        const fragment = this.route.snapshot.paramMap.get("fragment");
        if (fragment && fragment === 'mention_legales') {
            this.activeIndex = 0;
        } else if (fragment && fragment === 'CU'){
            this.activeIndex = 1;
        } else if (fragment && fragment === 'CGV'){
            this.activeIndex = 2;
        } else if (fragment && fragment === 'donnees_personnelles'){
            this.activeIndex = 3;
        }
    }
}
