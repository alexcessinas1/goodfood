import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { ContratPageComponent } from './contrat.page.component';
import { TabViewModule } from 'primeng/tabview';

const contactPageRoute: Routes = [
  {
    path: '',
    component: ContratPageComponent,
    data: {
      authorities: [],
      pageTitle: 'goodFoodApp.contrat.home.title',
    }
  }
];

@NgModule({
    imports: [GoodFoodSharedModule, RouterModule.forChild(contactPageRoute), TabViewModule],
    declarations: [ContratPageComponent],
    providers: []
})
export class GoodFoodContratPageModule {}
