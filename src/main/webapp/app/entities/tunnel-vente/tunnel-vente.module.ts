import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { tunnelVenteRoute } from './tunnel-vente.route';
import { ButtonModule } from 'primeng/button';
import { RatingModule } from 'primeng/rating';
import { DataViewModule } from 'primeng/dataview';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { StepsModule } from 'primeng/steps';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { EntreeComponent } from 'app/entities/tunnel-vente/etapes/entree/entree.component';
import { PlatComponent } from 'app/entities/tunnel-vente/etapes/plat/plat.component';
import { DessertComponent } from 'app/entities/tunnel-vente/etapes/dessert/dessert.component';
import { BoissonComponent } from 'app/entities/tunnel-vente/etapes/boisson/boisson.component';
import { PayementComponent } from 'app/entities/tunnel-vente/etapes/payement/payement.component';
import { TooltipModule } from 'primeng/tooltip';
import { DelivranceComponent } from 'app/entities/tunnel-vente/etapes/delivrance/delivrance.component';
import { GoodFoodShopPageModule } from 'app/entities/shop-page/shop.page.module';
import { GoodFoodModulesModule } from 'app/modules/modules.module';
import { RecapitulatifComponent } from 'app/entities/tunnel-vente/etapes/recapitulatif/recapitulatif.component';

@NgModule({
  imports: [
    GoodFoodSharedModule,
    RouterModule.forChild(tunnelVenteRoute),
    ButtonModule,
    RatingModule,
    DataViewModule,
    DropdownModule,
    InputTextModule,
    StepsModule,
    ToastModule,
    TooltipModule,
    GoodFoodShopPageModule,
    GoodFoodModulesModule,
  ],
  declarations: [
    DelivranceComponent,
    EntreeComponent,
    PlatComponent,
    DessertComponent,
    BoissonComponent,
    PayementComponent,
    RecapitulatifComponent,
  ],
  entryComponents: [],
  providers: [MessageService],
})
export class GoodFoodTunnelVenteModule {}
