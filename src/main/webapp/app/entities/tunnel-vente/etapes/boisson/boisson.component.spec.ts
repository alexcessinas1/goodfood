import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { BoissonComponent } from 'app/entities/tunnel-vente/etapes/boisson/boisson.component';
import { ToastModule } from 'primeng/toast';
import { DataViewModule } from 'primeng/dataview';
import { StepsModule } from 'primeng/steps';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'primeng/api';
import { ProduitService } from 'app/entities/produit/produit.service';
import { of } from 'rxjs';
import { Produit } from 'app/shared/model/produit.model';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { fakeAsync, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { PanierComponent } from 'app/modules/panier/panier.component';
import { AccordionModule } from 'primeng/accordion';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { DialogModule } from 'primeng/dialog';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { TranslateModule } from '@ngx-translate/core';

describe('Component Tests', () => {
  describe('Boisson Component', () => {
    let spectator: Spectator<BoissonComponent>;
    const createComponent = createComponentFactory({
      component: BoissonComponent,
      imports: [
        ToastModule,
        DataViewModule,
        StepsModule,
        TooltipModule,
        DropdownModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FontAwesomeTestingModule,
        AccordionModule,
        ReactiveFormsModule,
        DialogModule,
        NgxWebstorageModule.forRoot(),
        TranslateModule.forRoot(),
      ],
      declarations: [PanierComponent],
      providers: [MessageService],
    });

    const produit = { id: 15, produitCategorie: ProduitCategorie.BOISSON } as Produit;
    const produit1 = { id: 15, produitCategorie: ProduitCategorie.ALIMENTATION } as Produit;

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });

    it('should load products', fakeAsync(() => {
      const data = [produit, produit, produit];
      const spy = spyOn(spectator.inject(ProduitService), 'query').and.returnValue(of({ body: data }));

      spectator.component.ngOnInit();
      tick();
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spectator.component.datas).toMatchSnapshot();
      expect(spectator.component.items).toMatchSnapshot();
    }));

    it('should load products with [] response', fakeAsync(() => {
      const data = [produit1, produit1, produit1];
      const spy = spyOn(spectator.inject(ProduitService), 'query').and.returnValue(of({ body: data }));

      spectator.component.ngOnInit();
      tick();
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spectator.component.datas).toStrictEqual([]);
    }));

    it('should go to the next page', () => {
      const spy = spyOn(spectator.inject(Router), 'navigate').and.callThrough();
      spectator.component.nextPage();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should go to the previous page', () => {
      const spy = spyOn(spectator.inject(Router), 'navigate').and.callThrough();
      spectator.component.prevPage();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should go to the previous page', () => {
      expect(spectator.component.activeIndex).toBe(3);
      spectator.click('p-steps > div > ul > li:nth-child(1) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(0);
      spectator.click('p-steps > div > ul > li:nth-child(2) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(1);
      spectator.click('p-steps > div > ul > li:nth-child(3) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(2);
      spectator.click('p-steps > div > ul > li:nth-child(4) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(3);
      spectator.click('p-steps > div > ul > li:nth-child(5) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(4);
    });
  });
});
