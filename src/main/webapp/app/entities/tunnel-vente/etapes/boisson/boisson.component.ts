import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IProduit, Produit } from 'app/shared/model/produit.model';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { url } from 'app/entities/tunnel-vente/etapes/constant';
import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';

@Component({
  selector: 'gf-boisson',
  templateUrl: './boisson.component.html',
  styleUrls: ['./../../tunnel-vente.component.scss'],
})
export class BoissonComponent implements OnInit {
  // @ts-ignore
  datas: Produit[];
  // @ts-ignore
  items: MenuItem[];
  activeIndex = 3;
  private keyCustomerCommand: string | null = '';
  produitsPrice: { produit: IProduit; prix: number }[] = [];
  public commandeClient?: ICommandeClient;
  public indexSelectedPlateau = 0;

  constructor(private produitService: ProduitService, private router: Router, private tunnelService: TunnelVenteService) {}

  ngOnInit(): void {
    this.keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();

    if (this.keyCustomerCommand) {
      this.commandeClient = this.tunnelService.get(this.keyCustomerCommand) as ICommandeClient;
    }
    this.produitService.query().subscribe((res: HttpResponse<IProduit[]>) => {
      // @ts-ignore
      this.datas = res.body.filter(recette => recette.produitCategorie === ProduitCategorie.BOISSON) || [];
      this.datas.forEach(produit => {
        if (produit.prixCatalogue) {
          this.produitsPrice.push({ produit, prix: produit.prixCatalogue });
        }
      });
    });

    this.items = [
      {
        label: 'Entrée',
        command: () => {
          this.activeIndex = 0;
          this.router.navigate([`${url}entree`]);
        },
      },
      {
        label: 'Plat',
        command: () => {
          this.activeIndex = 1;
          this.router.navigate([`${url}plat`]);
        },
      },
      {
        label: 'Dessert',
        command: () => {
          this.activeIndex = 2;
          this.router.navigate([`${url}dessert`]);
        },
      },
      {
        label: 'Boisson',
        command: () => {
          this.activeIndex = 3;
          this.router.navigate([`${url}boisson`]);
        },
      },
      {
        label: 'Confirmation',
        command: () => {
          this.activeIndex = 4;
          this.router.navigate([`${url}payement`]);
        },
      },
    ];
  }

  /**
   * Navigue vers la prochaine page
   */
  nextPage(): void {
    this.router.navigate([`${url}payement`]);
  }

  /**
   * Navigue vers la page précédente
   */
  prevPage(): void {
    this.router.navigate([`${url}dessert`]);
  }

  /**
   * Ajoute une nouvelle boisson
   * @param newBoisson Produit de type boisson
   */
  addBoisson(newBoisson: Produit): void {
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[this.indexSelectedPlateau]) {
      this.commandeClient.plateauRepas[this.indexSelectedPlateau].boissonId = newBoisson.id;
      if (this.keyCustomerCommand) {
        this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
      }
    }
  }

  /**
   * Indique quel plateau est selectionner dans la liste
   * @param index
   */
  selectPlateau(index: number): void {
    this.indexSelectedPlateau = index;
  }

  /**
   * Calcul le prix pour une recette
   * @param id Identifiant de la recette
   */
  searchPrice(id: number): number {
    const recette = this.produitsPrice.filter(produitPrice => produitPrice.produit.id === id)[0];
    return recette.prix;
  }
}
