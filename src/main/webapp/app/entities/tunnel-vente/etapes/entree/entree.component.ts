import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { HttpResponse } from '@angular/common/http';
import { RecetteService } from 'app/entities/recette/recette.service';
import { IRecette, Recette } from 'app/shared/model/recette.model';
import { Router } from '@angular/router';
import { Plat } from 'app/shared/model/enumerations/plat.model';
import { url } from 'app/entities/tunnel-vente/etapes/constant';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';
import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';

@Component({
  selector: 'gf-entree',
  templateUrl: './entree.component.html',
  styleUrls: ['./../../tunnel-vente.component.scss'],
})
export class EntreeComponent implements OnInit {
  recettes: Recette[] = [];
  recettesPrice: { recette: IRecette; prix: number }[] = [];
  items!: MenuItem[];
  activeIndex = 0;
  public icon = goodfoodIcons;
  private keyCustomerCommand: string | null = '';
  public indexSelectedPlateau = 0;
  public commandeClient?: ICommandeClient;

  constructor(private recetteService: RecetteService, private router: Router, private tunnelService: TunnelVenteService) {}

  ngOnInit(): void {
    this.keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();
    if (this.keyCustomerCommand) {
      this.commandeClient = this.tunnelService.get(this.keyCustomerCommand) as ICommandeClient;
    }
    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => {
      if (res.body) {
        this.recettes = res.body.filter(recette => recette.type === Plat.ENTREE) || [];
      }
      this.recettes.forEach(recette => {
        let prix = 0;
        recette.produits?.forEach(price => {
          if (price.prixCatalogue) {
            prix += price.prixCatalogue;
          }
        });
        if (recette.id) {
          this.recettesPrice.push({ recette, prix });
        }
      });
    });

    this.items = [
      {
        label: 'Entrée',
        command: () => {
          this.activeIndex = 0;
          this.router.navigate([`${url}entree`]);
        },
      },
      {
        label: 'Plat',
        command: () => {
          this.activeIndex = 1;
          this.router.navigate([`${url}plat`]);
        },
      },
      {
        label: 'Dessert',
        command: () => {
          this.activeIndex = 2;
          this.router.navigate([`${url}dessert`]);
        },
      },
      {
        label: 'Boisson',
        command: () => {
          this.activeIndex = 3;
          this.router.navigate([`${url}boisson`]);
        },
      },
      {
        label: 'Confirmation',
        command: () => {
          this.activeIndex = 4;
          this.router.navigate([`${url}payement`]);
        },
      },
    ];
  }

  searchPrice(id: number): number {
    const recette = this.recettesPrice.filter(recettePrice => recettePrice.recette.id === id)[0];
    return recette.prix;
  }

  nextPage(): void {
    this.router.navigate([`${url}plat`]);
  }

  prevPage(): void {
    this.router.navigate([`${url}delivrance`]);
  }

  addEntree(newRecette: Recette): void {
    if (
      this.commandeClient &&
      this.commandeClient.plateauRepas &&
      this.commandeClient.plateauRepas[this.indexSelectedPlateau] &&
      this.commandeClient.plateauRepas[this.indexSelectedPlateau].recettes
    ) {
      // @ts-ignore
      this.commandeClient?.plateauRepas[this.indexSelectedPlateau].recettes.push(newRecette);
      if (this.keyCustomerCommand) {
        this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
      }
    }
  }

  selectPlateau(index: number): void {
    this.indexSelectedPlateau = index;
  }
}
