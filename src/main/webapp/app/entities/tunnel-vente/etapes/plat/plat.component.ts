import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { RecetteService } from 'app/entities/recette/recette.service';
import { IRecette, Recette } from 'app/shared/model/recette.model';
import { Router } from '@angular/router';
import { Plat } from 'app/shared/model/enumerations/plat.model';
import { url } from 'app/entities/tunnel-vente/etapes/constant';
import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { MenuItem } from 'primeng/api';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';

@Component({
  selector: 'gf-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./../../tunnel-vente.component.scss'],
})
export class PlatComponent implements OnInit {
  // @ts-ignore
  datas: Recette[];

  // @ts-ignore
  sortOrder: number;

  // @ts-ignore
  sortField: string;
  // @ts-ignore
  items: MenuItem[];
  // @ts-ignore
  activeIndex = 1;
  activeIndexPlateau!: number;
  private keyCustomerCommand: string | null = '';
  recettesPrice: { recette: IRecette; prix: number }[] = [];
  public commandeClient?: ICommandeClient;
  public indexSelectedPlateau = 0;

  constructor(private recetteService: RecetteService, private router: Router, private tunnelService: TunnelVenteService) {}

  ngOnInit(): void {
    if (this.tunnelService.isStored('selectedPlateau')) {
      this.activeIndexPlateau = this.tunnelService.get('selectedPlateau') as number;
    } else {
      this.activeIndexPlateau = 0;
    }
    this.keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();

    if (this.keyCustomerCommand) {
      this.commandeClient = this.tunnelService.get(this.keyCustomerCommand) as ICommandeClient;
    }
    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => {
      // @ts-ignore
      this.datas = res.body.filter(recette => recette.type === Plat.PLAT) || [];
      this.datas.forEach(recette => {
        let prix = 0;
        recette.produits?.forEach(price => {
          if (price.prixCatalogue) {
            prix += price.prixCatalogue;
          }
        });
        if (recette.id) {
          this.recettesPrice.push({ recette, prix });
        }
      });
    });

    this.items = [
      {
        label: 'Entrée',
        command: () => {
          this.activeIndex = 0;
          this.router.navigate([`${url}entree`]);
        },
      },
      {
        label: 'Plat',
        command: () => {
          this.activeIndex = 1;
          this.router.navigate([`${url}plat`]);
        },
      },
      {
        label: 'Dessert',
        command: () => {
          this.activeIndex = 2;
          this.router.navigate([`${url}dessert`]);
        },
      },
      {
        label: 'Boisson',
        command: () => {
          this.activeIndex = 3;
          this.router.navigate([`${url}boisson`]);
        },
      },
      {
        label: 'Confirmation',
        command: () => {
          this.activeIndex = 4;
          this.router.navigate([`${url}payement`]);
        },
      },
    ];
  }

  onSortChange(event: any): void {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  nextPage(): void {
    this.router.navigate([`${url}dessert`]);
  }

  prevPage(): void {
    this.router.navigate([`${url}entree`]);
  }

  addPlat(newRecette: Recette): void {
    if (
      this.commandeClient &&
      this.commandeClient.plateauRepas &&
      this.commandeClient.plateauRepas[this.indexSelectedPlateau] &&
      this.commandeClient.plateauRepas[this.indexSelectedPlateau].recettes
    ) {
      // @ts-ignore
      this.commandeClient?.plateauRepas[this.indexSelectedPlateau].recettes.push(newRecette);
      if (this.keyCustomerCommand) {
        this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
      }
    }
  }
  searchPrice(id: number): number {
    const recette = this.recettesPrice.filter(recettePrice => recettePrice.recette.id === id)[0];
    return recette.prix;
  }

  selectPlateau(index: number): void {
    this.indexSelectedPlateau = index;
  }
}
