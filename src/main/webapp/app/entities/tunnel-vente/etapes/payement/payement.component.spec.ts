import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { ToastModule } from 'primeng/toast';
import { DataViewModule } from 'primeng/dataview';
import { StepsModule } from 'primeng/steps';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'primeng/api';
import { PayementComponent } from 'app/entities/tunnel-vente/etapes/payement/payement.component';
import { Router } from '@angular/router';
import { PanierComponent } from 'app/modules/panier/panier.component';
import { AccordionModule } from 'primeng/accordion';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { DialogModule } from 'primeng/dialog';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { TranslateModule } from '@ngx-translate/core';

describe('Component Tests', () => {
  describe('Payement Component', () => {
    let spectator: Spectator<PayementComponent>;
    const createComponent = createComponentFactory({
      component: PayementComponent,
      imports: [
        ToastModule,
        DataViewModule,
        StepsModule,
        TooltipModule,
        DropdownModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        AccordionModule,
        FontAwesomeTestingModule,
        ReactiveFormsModule,
        DialogModule,
        NgxWebstorageModule.forRoot(),
        TranslateModule.forRoot(),
      ],
      declarations: [PanierComponent],
      providers: [MessageService],
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });

    it('should go to the previous page', () => {
      const spy = spyOn(spectator.inject(Router), 'navigate').and.callThrough();
      spectator.component.prevPage();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should go to the previous page', () => {
      expect(spectator.component.activeIndex).toBe(4);
      spectator.click('p-steps > div > ul > li:nth-child(1) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(0);
      spectator.click('p-steps > div > ul > li:nth-child(2) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(1);
      spectator.click('p-steps > div > ul > li:nth-child(3) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(2);
      spectator.click('p-steps > div > ul > li:nth-child(4) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(3);
      spectator.click('p-steps > div > ul > li:nth-child(5) > a');
      spectator.detectChanges();
      expect(spectator.component.activeIndex).toBe(4);
    });
  });
});
