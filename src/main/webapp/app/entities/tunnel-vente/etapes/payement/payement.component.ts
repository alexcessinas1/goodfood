import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { url } from 'app/entities/tunnel-vente/etapes/constant';

@Component({
  selector: 'gf-payement',
  templateUrl: './payement.component.html',
  styleUrls: ['./../../tunnel-vente.component.scss'],
})
export class PayementComponent implements OnInit {
  // @ts-ignore
  items: MenuItem[];
  // @ts-ignore
  activeIndex = 4;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.items = [
      {
        label: 'Entrée',
        command: () => {
          this.activeIndex = 0;
          this.router.navigate([`${url}entree`]);
        },
      },
      {
        label: 'Plat',
        command: () => {
          this.activeIndex = 1;
          this.router.navigate([`${url}plat`]);
        },
      },
      {
        label: 'Dessert',
        command: () => {
          this.activeIndex = 2;
          this.router.navigate([`${url}dessert`]);
        },
      },
      {
        label: 'Boisson',
        command: () => {
          this.activeIndex = 3;
          this.router.navigate([`${url}boisson`]);
        },
      },
      {
        label: 'Confirmation',
        command: () => {
          this.activeIndex = 4;
          this.router.navigate([`${url}payement`]);
        },
      },
    ];
  }

  prevPage(): void {
    this.router.navigate([`${url}boisson`]);
  }
}
