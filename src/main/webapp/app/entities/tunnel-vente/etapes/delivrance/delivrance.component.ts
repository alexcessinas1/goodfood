import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { url } from 'app/entities/tunnel-vente/etapes/constant';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';
import { CommandeClient, ICommandeClient } from 'app/shared/model/commande-client.model';
import { FormBuilder, Validators } from '@angular/forms';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { Commande, ICommande } from 'app/shared/model/commande.model';
import { Statut } from 'app/shared/model/enumerations/statut.model';
import { AccountService } from 'app/core/auth/account.service';
import { ClientService } from 'app/entities/client/client.service';
import { UserService } from 'app/core/user/user.service';
import { IClient } from 'app/shared/model/client.model';
import { PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { MapService } from 'app/shared/util/map.service';
import { ISimpleFranchise } from 'app/shared/model/franchiseAdresse.model';
import DistanceMatrixResponse = google.maps.DistanceMatrixResponse;
import LatLngLiteral = google.maps.LatLngLiteral;
import { Adresse } from 'app/shared/model/adresse.model';

@Component({
  selector: 'gf-delivrance',
  templateUrl: './delivrance.component.html',
  styleUrls: ['./delivrance.component.scss'],
  providers: [MapService],
})
export class DelivranceComponent implements OnInit {
  icons = goodfoodIcons;
  public showMaps = false;
  public noValue = false;
  public manualAddress: string | undefined;
  public delivery: boolean | undefined;
  public franchiseAddressList: ISimpleFranchise[] = [];
  public currentClient?: IClient | null;

  /**
   * Déclaration des Validators pour le formulaire
   */
  searchDeliveryForm = this.fb.group({
    numberVoie: ['', Validators.required],
    nameStreet: ['', Validators.required],
    city: ['', Validators.required],
    zip: ['', Validators.required],
  });

  /**
   * Déclaration des Validators pour le formulaire
   */
  searchFranchiseForm = this.fb.group({
    cityOrZip: ['', Validators.required],
  });

  private cmdC!: ICommandeClient;
  private cmd!: ICommande;

  constructor(
    private router: Router,
    private readonly fb: FormBuilder,
    private tunnelService: TunnelVenteService,
    private readonly franchiseService: FranchiseService,
    private readonly adresseService: AdresseService,
    private readonly accountService: AccountService,
    private readonly userService: UserService,
    private readonly clientService: ClientService,
    private readonly mapService: MapService
  ) {}

  /**
   * Supprime les choses deja dans le local storage
   * Créé les cles pour retrouver la commandes et la commande client
   * Si un client est connecté on recupere les informations
   */
  ngOnInit(): void {
    this.tunnelService.removeAll();
    this.tunnelService.saveKeyCommand(Math.random().toString(36).substring(7));
    this.tunnelService.saveKeyAdresseLivraison(Math.random().toString(36).substring(7));
    this.tunnelService.saveKeyPersonnePhysique(Math.random().toString(36).substring(7));
    this.tunnelService.saveKeyClient(Math.random().toString(36).substring(7));
    this.tunnelService.saveKeyCustomerCommand(Math.random().toString(36).substring(7));
    if (this.accountService.isAuthenticated()) {
      this.accountService.getAuthenticationState().subscribe(account => {
        if (account) {
          this.userService
            .find(account?.login)
            .subscribe(user => this.clientService.find(user.id).subscribe(client => (this.currentClient = client.body)));
        }
      });
    }
  }

  /**
   * Selectionne le mode "Livraison"
   */
  toDelivery(): void {
    this.showMaps = true;
    this.delivery = true;
  }

  /**
   * Selectionne le mode "A Emporter"
   */
  toTakeAway(): void {
    this.showMaps = true;
    this.delivery = false;
  }

  /**
   * Passe à la page home
   */
  homePage(): void {
    this.router.navigate([`/`]);
  }

  /**
   * Création de l'adresse via les informations du formulaire
   */
  public searchDeliveryAddress(): void {
    if (
      this.searchDeliveryForm.get(['numberVoie'])!.value &&
      this.searchDeliveryForm.get(['nameStreet'])!.value &&
      this.searchDeliveryForm.get(['city'])!.value &&
      this.searchDeliveryForm.get(['zip'])!.value
    ) {
      this.manualAddress = `${this.searchDeliveryForm.get(['numberVoie'])!.value} ${this.searchDeliveryForm.get(['nameStreet'])!.value} ${
        this.searchDeliveryForm.get(['city'])!.value
      } ${this.searchDeliveryForm.get(['zip'])!.value}`;
      this.tunnelService.set(
        this.tunnelService.getKeyAdresseLivraison(),
        new Adresse({
          ville: this.searchDeliveryForm.get(['city'])!.value,
          codePostal: this.searchDeliveryForm.get(['zip'])!.value,
          nomVoie: this.searchDeliveryForm.get(['nameStreet'])!.value,
          numeroVoie: this.searchDeliveryForm.get(['numberVoie'])!.value,
        })
      );
      this.mapService.getGeoLocation(this.manualAddress).subscribe(coordonneOrigin => {
        this.getFranchiseIn5Km(coordonneOrigin);
      });
    }
  }

  /**
   * Récupère les différents franchise dans un rayon de 5km autour de la position fourni
   * @param coordonneOrigin LatLngLiteral
   */
  public getFranchiseIn5Km(coordonneOrigin: LatLngLiteral): void {
    this.franchiseService.getWithAddress().subscribe(franchises => {
      const addresses = franchises?.body?.map(
        franchise =>
          `${franchise.adresseFranchise?.numeroVoie} ${franchise.adresseFranchise?.nomVoie} ${franchise.adresseFranchise?.ville} ${franchise.adresseFranchise?.codePostal}`
      );
      this.mapService.getDistance(coordonneOrigin, addresses ?? []).subscribe((res: DistanceMatrixResponse) => {
        this.franchiseAddressList = [];
        if (res.rows && res.rows[0] && res.rows[0] && res.rows[0].elements) {
          res.rows.forEach(row => {
            row.elements.forEach((elem, index) => {
              if (elem.distance.value <= 5000 && franchises.body) {
                this.franchiseAddressList.push(franchises.body[index]);
              }
            });
          });
        }
      });
    });
  }

  /**
   * Selectionne le franchise et fait la tambouille
   * @param franchise { franchise: IFranchise; address: IAdresse }
   */
  public selectFranchise(franchise: ISimpleFranchise): void {
    const keyCommand = this.tunnelService.getKeyCommand();
    const keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();

    this.cmd = new Commande({
      aLivrer: this.delivery,
      statut: Statut.EN_COURS,
      idFranchise: franchise.id,
      dateStatut: Date.now().toString(),
      dateCommande: Date.now().toString(),
      idClient: this.currentClient ? this.currentClient?.id : undefined,
    });

    this.cmdC = new CommandeClient({
      plateauRepas: [new PlateauRepas({ recettes: [] })],
      clientId: this.currentClient ? this.currentClient?.id : undefined,
    });

    if (keyCommand !== null && keyCustomerCommand !== null) {
      this.saveOnLocal(keyCommand, keyCustomerCommand);
    }
    this.router.navigate([`${url}entree`]);
  }

  /**
   * Save la command et la command client dans le localStorage
   * @param keyCommand Cle pour retrouver la commande
   * @param keyCustomerCommand Cle pour retrouver la commande client
   */
  public saveOnLocal(keyCommand: string, keyCustomerCommand: string): void {
    this.tunnelService.set(keyCommand, this.cmd);
    this.tunnelService.set(keyCustomerCommand, this.cmdC);
  }

  /**
   * Pour afficher et cacher la carte au click
   */
  public toggleShowMaps(): void {
    this.showMaps = false;
    this.franchiseAddressList = [];
  }

  /**
   * Fait la recherche pour trouver les franchises soit par la ville ou le code postal
   */
  public searchFranchiseAddress(): void {
    this.franchiseAddressList = [];
    const cityOrZip = this.searchFranchiseForm.get(['cityOrZip'])!.value;
    this.franchiseService.query().subscribe(franchises => {
      franchises.body?.forEach(franchise => {
        if (franchise.adresseFranchiseIdId) {
          this.adresseService.find(franchise.adresseFranchiseIdId).subscribe(address => {
            if (
              cityOrZip.match('(?:0[1-9]|[13-8][0-9]|2[ab1-9]|9[0-5])(?:[0-9]{3})?|9[78][1-9](?:[0-9]{2})?') &&
              address.body &&
              address.body.codePostal &&
              address.body.codePostal === cityOrZip
            ) {
              this.franchiseAddressList.push({ id: franchise.id, adresseFranchise: address.body });
            } else if (typeof cityOrZip === 'string' && address.body && address.body.ville && address.body.ville === cityOrZip) {
              this.franchiseAddressList.push({ id: franchise.id, adresseFranchise: address.body });
            }
          });
        }
      });
    });
  }
}
