import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { DelivranceComponent } from 'app/entities/tunnel-vente/etapes/delivrance/delivrance.component';
import { ToastModule } from 'primeng/toast';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoodFoodShopPageModule } from 'app/entities/shop-page/shop.page.module';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { TooltipModule } from 'primeng/tooltip';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('Component Tests', () => {
  describe('DelivranceComponent', () => {
    let spectator: Spectator<DelivranceComponent>;
    const createComponent = createComponentFactory({
      component: DelivranceComponent,
      imports: [
        ToastModule,
        FontAwesomeTestingModule,
        FormsModule,
        ReactiveFormsModule,
        GoodFoodShopPageModule,
        NgxWebstorageModule.forRoot(),
        TranslateModule.forRoot(),
        RouterTestingModule,
        TooltipModule,
        HttpClientTestingModule,
      ],
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });
  });
});
