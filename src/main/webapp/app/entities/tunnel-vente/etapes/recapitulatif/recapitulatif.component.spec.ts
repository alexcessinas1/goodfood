import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { RecapitulatifComponent } from 'app/entities/tunnel-vente/etapes/recapitulatif/recapitulatif.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Component Tests', () => {
  describe('RecapitulatifComponent', () => {
    let spectator: Spectator<RecapitulatifComponent>;
    const createComponent = createComponentFactory({
      component: RecapitulatifComponent,
      imports: [
          RouterTestingModule,
        NgxWebstorageModule.forRoot(),
        FontAwesomeTestingModule,
          HttpClientTestingModule
      ],
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });
  });
});
