import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';
import { Adresse, IAdresse } from 'app/shared/model/adresse.model';
import { CommandeClient, ICommandeClient } from 'app/shared/model/commande-client.model';
import { Commande, ICommande } from 'app/shared/model/commande.model';
import { IPersonnePhysique, PersonnePhysique } from 'app/shared/model/personne-physique.model';
import { Client, IClient } from 'app/shared/model/client.model';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { IProduit } from 'app/shared/model/produit.model';
import { IRecette, Recette } from 'app/shared/model/recette.model';
import { HttpResponse } from '@angular/common/http';
import { RecetteService } from 'app/entities/recette/recette.service';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';

@Component({
  selector: 'gf-recapitulatif',
  templateUrl: './recapitulatif.component.html',
  styleUrls: ['./recapitulatif.component.scss'],
})
export class RecapitulatifComponent implements OnInit, OnDestroy {
  private keyCustomerCommand: string | null = '';
  private keyCommand: string | null = '';
  private keyPersonnePhysique: string | null = '';
  private keyClient: string | null = '';
  private keyAdresseLivraison: string | null = '';
  public boissonsList: IProduit[] = [];
  public recettesList: Recette[] = [];

  public icon = goodfoodIcons;

  prixHT = 0;

  public commandeClient: ICommandeClient = new CommandeClient();
  public commande: ICommande = new Commande();
  public adresseLivraison: IAdresse = new Adresse();
  public personnePhysique: IPersonnePhysique = new PersonnePhysique();
  public client: IClient = new Client();
  public franchiseAdresse: IAdresse = new Adresse();

  constructor(
    private readonly route: ActivatedRoute,
    private tunnelService: TunnelVenteService,
    public readonly franchise: FranchiseService,
    public readonly addressService: AdresseService,
    private readonly recetteService: RecetteService,
    private readonly produitService: ProduitService
  ) {}

  ngOnDestroy(): void {
    this.tunnelService.removeAll();
  }

  ngOnInit(): void {
    this.keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();
    this.keyCommand = this.tunnelService.getKeyCommand();
    this.keyPersonnePhysique = this.tunnelService.getKeyPersonnePhysique();
    this.keyClient = this.tunnelService.getKeyClient();

    if (this.keyCustomerCommand) {
      this.commandeClient = this.tunnelService.get(this.keyCustomerCommand) as CommandeClient;
    }
    if (this.keyCommand) {
      this.commande = this.tunnelService.get(this.keyCommand) as Commande;
      if (this.commande && this.commande.aLivrer) {
        this.keyAdresseLivraison = this.tunnelService.getKeyAdresseLivraison();
        if (this.keyAdresseLivraison) {
          this.adresseLivraison = this.tunnelService.get(this.keyAdresseLivraison) as Adresse;
        }
      }
    }
    if (this.keyPersonnePhysique) {
      this.personnePhysique = this.tunnelService.get(this.keyPersonnePhysique) as PersonnePhysique;
    }
    if (this.keyClient) {
      this.client = this.tunnelService.get(this.keyClient) as Client;
    }
    if (this.commande && this.commande.idFranchise) {
      this.franchise.find(this.commande.idFranchise).subscribe(franchise => {
        if (franchise && franchise.body && franchise.body.id) {
          this.addressService.find(franchise.body.id).subscribe(address => {
            if (address && address.body) {
              this.franchiseAdresse = address.body;
            }
          });
        }
      });
    }

    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => {
      if (res.body) {
        this.recettesList = res.body || [];
      }
    });
    this.produitService.query().subscribe(produit => {
      if (produit.body) {
        this.boissonsList = produit.body.filter(produitA => produitA.produitCategorie === ProduitCategorie.BOISSON);
      }
    });

    if (this.commande && this.commande.prixTotal) {
      this.prixHT = this.commande.prixTotal / 1.2;
    }
  }

  printInvoice(): void {
    setTimeout(() => {
      window.print();
    });
  }

  /**
   * Calcul le prix pour un recette
   * @param recetteId Identifiant de cette recette
   */
  searchPrice(recetteId: number | undefined): number {
    let prix = 0;
    const recetteA = this.recettesList.find(recette => recette.id === recetteId);
    if (recetteA && recetteA.produits) {
      // @ts-ignore
      recetteA.produits.forEach(produit => {
        if (produit.prixCatalogue) {
          prix += produit.prixCatalogue;
          if (recetteId) {
            return prix;
          }
        }
      });
    }
    return prix;
  }

  /**
   * Calcul le prix du plateau repas
   * @param indexPlateau Position du plateau dans la liste
   */
  searchPlateauPrice(indexPlateau: number): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[indexPlateau]) {
      const plateauA = this.commandeClient?.plateauRepas[indexPlateau];
      if (plateauA) {
        plateauA.recettes?.forEach(recette => {
          if (recette.produits) {
            recette.produits.forEach(produit => {
              if (produit.prixCatalogue) {
                prix += produit.prixCatalogue;
              }
            });
          }
        });
        prix += this.searchBoissonPrice(indexPlateau);
      }
    }
    return prix;
  }

  /**
   * Calcul le prix de la boisson d'un plateau repas
   * @param indexPlateau Position du plateau dans la liste
   */
  searchBoissonPrice(indexPlateau: number): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[indexPlateau]) {
      const boissonId = this.commandeClient.plateauRepas[indexPlateau].boissonId;
      if (boissonId) {
        const boissonA = this.boissonsList.filter(boisson => boisson.id === boissonId)[0];
        if (boissonA && boissonA.prixCatalogue) {
          prix = boissonA.prixCatalogue;
        }
      }
    }
    return prix;
  }

  /**
   * Calcul le prix de la boisson d'un plateau repas
   * @param indexPlateau Position du plateau dans la liste
   */
  searchBoissonName(indexPlateau: number): string {
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[indexPlateau]) {
      const boissonId = this.commandeClient.plateauRepas[indexPlateau].boissonId;
      if (boissonId) {
        const boissonA = this.boissonsList.filter(boisson => boisson.id === boissonId)[0];
        if (boissonA && boissonA.nom) {
          return boissonA.nom;
        }
      }
    }
    return '';
  }

  /**
   * Calcul le prix total de la commande
   */
  searchTotalPrice(): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas) {
      this.commandeClient.plateauRepas.forEach(plateauA => {
        if (plateauA && plateauA.recettes) {
          plateauA.recettes.forEach(recetteA => {
            if (recetteA && recetteA.produits) {
              recetteA.produits.forEach(produitA => {
                if (produitA && produitA.prixCatalogue) {
                  prix += produitA.prixCatalogue;
                }
              });
            }
          });
        }
        if (plateauA.boissonId) {
          const boissonA = this.boissonsList.filter(boisson => boisson.id === plateauA.boissonId)[0];
          if (boissonA && boissonA.prixCatalogue) {
            prix += boissonA.prixCatalogue;
          }
        }
      });
    }
    return prix;
  }
}
