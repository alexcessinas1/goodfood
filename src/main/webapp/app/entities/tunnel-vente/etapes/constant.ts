export const url = 'tunnel-vente/etape/';

export const sortOptions = [
  { label: 'Prix décroissant', value: '!price' },
  { label: 'Prix croissant', value: 'price' },
];
