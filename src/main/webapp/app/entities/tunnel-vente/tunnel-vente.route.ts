import { Routes } from '@angular/router';
import { EntreeComponent } from 'app/entities/tunnel-vente/etapes/entree/entree.component';
import { PlatComponent } from 'app/entities/tunnel-vente/etapes/plat/plat.component';
import { DessertComponent } from 'app/entities/tunnel-vente/etapes/dessert/dessert.component';
import { BoissonComponent } from 'app/entities/tunnel-vente/etapes/boisson/boisson.component';
import { PayementComponent } from 'app/entities/tunnel-vente/etapes/payement/payement.component';
import { DelivranceComponent } from 'app/entities/tunnel-vente/etapes/delivrance/delivrance.component';
import { RecapitulatifComponent } from 'app/entities/tunnel-vente/etapes/recapitulatif/recapitulatif.component';

const title = 'goodFoodApp.plateauRepas.home.title';

export const tunnelVenteRoute: Routes = [
  {
    path: 'etape/delivrance',
    component: DelivranceComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/entree',
    component: EntreeComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/plat',
    component: PlatComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/dessert',
    component: DessertComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/boisson',
    component: BoissonComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/payement',
    component: PayementComponent,
    data: {
      pageTitle: title,
    },
  },
  {
    path: 'etape/recapitulatif',
    component: RecapitulatifComponent,
    data: {
      pageTitle: title,
    },
  },
];
