import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { ICommande } from 'app/shared/model/commande.model';
import { IPersonnePhysique } from 'app/shared/model/personne-physique.model';
import { Adresse } from 'app/shared/model/adresse.model';

@Injectable({ providedIn: 'root' })
export class TunnelVenteService {
  constructor(private readonly localStorageService: LocalStorageService) {}

  /**
   * Récupère la commande dans le localstorage par rapport à une clé donnée.
   * @param key - la clé des critères dans le localstorage.
   */
  public get(key: string): ICommandeClient | ICommande | IPersonnePhysique | Adresse | null | string {
    if (this.isStored(key)) {
      return this.localStorageService.retrieve(key);
    }
    return null;
  }

  /**
   * Enregistre la commande pour une clé donnée.
   * @param key - La clé de référence dans le localstorage.
   * @param commande - La commande à enregister
   */
  public set(key: string | null, commande: ICommandeClient | ICommande | IPersonnePhysique | Adresse | string): void {
    if (key) {
      this.localStorageService.store(key, commande);
    }
  }

  /**
   * Sauvegarde la cle pour retrouver la commande dans le local storage
   */
  public saveKeyCommand(value: string): void {
    if (value && !this.isStored('keyCommande')) {
      this.localStorageService.store('keyCommande', value);
    }
  }

  /**
   * Recupere la cle pour retrouver la commande dans le local storage
   */
  public getKeyCommand(): string | null {
    if (this.isStored('keyCommande')) {
      return this.localStorageService.retrieve('keyCommande');
    }
    return null;
  }

  /**
   * Sauvegarde la cle pour retrouver la commande client dans le local storage
   */
  public saveKeyCustomerCommand(value: string): void {
    if (value && !this.isStored('keyCustomerCommande')) {
      this.localStorageService.store('keyCustomerCommande', value);
    }
  }

  /**
   * Recupere la cle pour retrouver la commande client dans le local storage
   */
  public getKeyCustomerCommand(): string | null {
    if (this.isStored('keyCustomerCommande')) {
      return this.localStorageService.retrieve('keyCustomerCommande');
    }
    return null;
  }

  public saveKeyPersonnePhysique(value: string): void {
    if (value && !this.isStored('keyPersonnePhysique')) {
      this.localStorageService.store('keyPersonnePhysique', value);
    }
  }

  public getKeyPersonnePhysique(): string | null {
    if (this.isStored('keyPersonnePhysique')) {
      return this.localStorageService.retrieve('keyPersonnePhysique');
    }
    return null;
  }

  public saveKeyClient(value: string): void {
    if (value && !this.isStored('keyClient')) {
      this.localStorageService.store('keyClient', value);
    }
  }

  public getKeyClient(): string | null {
    if (this.isStored('keyClient')) {
      return this.localStorageService.retrieve('keyClient');
    }
    return null;
  }

  public saveKeyAdresseLivraison(value: string): void {
    if (value && !this.isStored('keyAdresseLivraison')) {
      this.localStorageService.store('keyAdresseLivraison', value);
    }
  }

  public getKeyAdresseLivraison(): string | null {
    if (this.isStored('keyAdresseLivraison')) {
      return this.localStorageService.retrieve('keyAdresseLivraison');
    }
    return null;
  }

  /**
   * Efface la commande du localstorage pour la clé donnée.
   * @param key - La clé de référence dans le localstorage.
   */
  public remove(key: string): void {
    this.localStorageService.clear(key);
  }

  /**
   * Supprime tout
   */
  public removeAll(): void {
    const keyCustomerCommand = this.getKeyCustomerCommand();
    const keyCommand = this.getKeyCommand();
    const keyAdresseLivraison = this.getKeyAdresseLivraison();
    const keyPersonnePhysique = this.getKeyPersonnePhysique();
    const keyClient = this.getKeyClient();
    if (this.isStored('keyCustomerCommande') && keyCustomerCommand) {
      this.localStorageService.clear(keyCustomerCommand);
      this.localStorageService.clear('keyCustomerCommande');
    }
    if (this.isStored('keyCommande') && keyCommand) {
      this.localStorageService.clear(keyCommand);
      this.localStorageService.clear('keyCommande');
    }
    if (this.isStored('keyAdresseLivraison') && keyAdresseLivraison) {
      this.localStorageService.clear(keyAdresseLivraison);
      this.localStorageService.clear('keyAdresseLivraison');
    }
    if (this.isStored('keyPersonnePhysique') && keyPersonnePhysique) {
      this.localStorageService.clear(keyPersonnePhysique);
      this.localStorageService.clear('keyPersonnePhysique');
    }
    if (this.isStored('keyClient') && keyClient) {
      this.localStorageService.clear(keyClient);
      this.localStorageService.clear('keyClient');
    }
  }

  /**
   * Vérifie si la commande est bien renseigner pour une clé donnée.
   * @param key - La clé de référence dans le localstorage.
   */
  public isStored(key: string): boolean {
    if (!key) {
      return false;
    }
    return !!this.localStorageService.retrieve(key);
  }
}
