import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFranchise, Franchise } from 'app/shared/model/franchise.model';
import { FranchiseService } from './franchise.service';
import { IAdresse } from 'app/shared/model/adresse.model';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';

type SelectableEntity = IAdresse | IProduit;

@Component({
  selector: 'gf-franchise-update',
  templateUrl: './franchise-update.component.html',
})
export class FranchiseUpdateComponent implements OnInit {
  isSaving = false;
  adressefranchiseids: IAdresse[] = [];
  produits: IProduit[] = [];

  editForm = this.fb.group({
    id: [],
    telephone: [null, [Validators.required]],
    email: [null, [Validators.required]],
    horaire: [null, [Validators.required]],
    siret: [null, [Validators.required]],
    adresseFranchiseIdId: [null, Validators.required],
    stockFranchises: [null, Validators.required],
  });

  constructor(
    protected franchiseService: FranchiseService,
    protected adresseService: AdresseService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ franchise }) => {
      this.updateForm(franchise);

      this.adresseService
        .query({ filter: 'franchiseadresseid-is-null' })
        .pipe(
          map((res: HttpResponse<IAdresse[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAdresse[]) => {
          if (!franchise.adresseFranchiseIdId) {
            this.adressefranchiseids = resBody;
          } else {
            this.adresseService
              .find(franchise.adresseFranchiseIdId)
              .pipe(
                map((subRes: HttpResponse<IAdresse>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAdresse[]) => (this.adressefranchiseids = concatRes));
          }
        });

      this.produitService.query().subscribe((res: HttpResponse<IProduit[]>) => (this.produits = res.body || []));
    });
  }

  updateForm(franchise: IFranchise): void {
    this.editForm.patchValue({
      id: franchise.id,
      telephone: franchise.telephone,
      email: franchise.email,
      horaire: franchise.horaire,
      siret: franchise.siret,
      adresseFranchiseIdId: franchise.adresseFranchiseIdId,
      stockFranchises: franchise.stockFranchises,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const franchise = this.createFromForm();
    if (franchise.id !== undefined) {
      this.subscribeToSaveResponse(this.franchiseService.update(franchise));
    } else {
      this.subscribeToSaveResponse(this.franchiseService.create(franchise));
    }
  }

  private createFromForm(): IFranchise {
    return {
      ...new Franchise(),
      id: this.editForm.get(['id'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      email: this.editForm.get(['email'])!.value,
      horaire: this.editForm.get(['horaire'])!.value,
      siret: this.editForm.get(['siret'])!.value,
      adresseFranchiseIdId: this.editForm.get(['adresseFranchiseIdId'])!.value,
      stockFranchises: this.editForm.get(['stockFranchises'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFranchise>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProduit[], option: IProduit): IProduit {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
