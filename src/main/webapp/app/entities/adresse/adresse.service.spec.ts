import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { IAdresse, Adresse } from 'app/shared/model/adresse.model';

describe('Service Tests', () => {
  describe('Adresse Service', () => {
    let injector: TestBed;
    let service: AdresseService;
    let httpMock: HttpTestingController;
    let elemDefault: IAdresse;
    let expectedResult: IAdresse | IAdresse[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(AdresseService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Adresse({
        id: 0,
        infoComplementaire: 'AAAAAAA',
        numeroVoie: 'AAAAAAA',
        complementAdresse: 'AAAAAAA',
        codePostal: 'AAAAAAA',
        ville: 'AAAAAAA',
        nomVoie: 'AAAAAAA',
      });
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Adresse', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Adresse()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Adresse', () => {
        const returnedFromService = Object.assign(
          {
            numeroVoie: 'BBBBBB',
            nomVoie: 'BBBBBB',
            codePostal: 'BBBBBB',
            ville: 'BBBBBB',
            complementAdresse: 'BBBBBB',
            infoComplementaire: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Adresse', () => {
        const returnedFromService = Object.assign(
          {
            numeroVoie: 'BBBBBB',
            nomVoie: 'BBBBBB',
            codePostal: 'BBBBBB',
            ville: 'BBBBBB',
            complementAdresse: 'BBBBBB',
            infoComplementaire: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Adresse', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
