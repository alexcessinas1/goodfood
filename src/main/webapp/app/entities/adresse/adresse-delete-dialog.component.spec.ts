import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { AdresseDeleteDialogComponent } from 'app/entities/adresse/adresse-delete-dialog.component';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { MockEventManager } from 'app/shared/util/helpers/mock-event-manager.service';
import { MockActiveModal } from 'app/shared/util/helpers/mock-active-modal.service';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Adresse Management Delete Component', () => {
    let comp: AdresseDeleteDialogComponent;
    let fixture: ComponentFixture<AdresseDeleteDialogComponent>;
    let service: AdresseService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [AdresseDeleteDialogComponent],
      })
        .overrideTemplate(AdresseDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AdresseDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AdresseService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
