import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { RecetteComponent } from 'app/entities/recette/recette.component';
import { RecetteService } from 'app/entities/recette/recette.service';
import { Recette } from 'app/shared/model/recette.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Recette Management Component', () => {
    let comp: RecetteComponent;
    let fixture: ComponentFixture<RecetteComponent>;
    let service: RecetteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [RecetteComponent],
      })
        .overrideTemplate(RecetteComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecetteComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RecetteService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Recette(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.recettes && comp.recettes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
