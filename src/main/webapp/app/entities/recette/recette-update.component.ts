import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRecette, Recette } from 'app/shared/model/recette.model';
import { RecetteService } from './recette.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from '../produit/produit.service';

@Component({
  selector: 'gf-recette-update',
  templateUrl: './recette-update.component.html',
})
export class RecetteUpdateComponent implements OnInit {
  isSaving = false;
  produitsListe: IProduit[] = [];

  editForm = this.fb.group({
    id: [],
    titre: [],
    description: [],
    type: [],
    produits: [],
  });

  constructor(
    protected recetteService: RecetteService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recette }) => {
      this.updateForm(recette);

      this.produitService.query().subscribe((res: HttpResponse<IProduit[]>) => (this.produitsListe = res.body || []));
    });
  }

  updateForm(recette: IRecette): void {
    this.editForm.patchValue({
      id: recette?.id,
      titre: recette?.titre,
      description: recette?.description,
      type: recette?.type,
      produits: recette?.produits,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recette = this.createFromForm();
    if (recette.id !== undefined) {
      this.subscribeToSaveResponse(this.recetteService.update(recette));
    } else {
      this.subscribeToSaveResponse(this.recetteService.create(recette));
    }
  }

  private createFromForm(): IRecette {
    return {
      ...new Recette(),
      id: this.editForm.get(['id'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      description: this.editForm.get(['description'])!.value,
      type: this.editForm.get(['type'])!.value,
      produits: this.editForm.get('produits')!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecette>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
  containsProduitId(obj1?: IProduit, obj2?: IProduit): boolean {
    return obj1?.id === obj2?.id;
  }
}
