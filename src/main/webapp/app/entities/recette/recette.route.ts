import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRecette, Recette } from 'app/shared/model/recette.model';
import { RecetteService } from './recette.service';
import { RecetteComponent } from './recette.component';
import { RecetteDetailComponent } from './recette-detail.component';
import { RecetteUpdateComponent } from './recette-update.component';

@Injectable({ providedIn: 'root' })
export class RecetteResolve implements Resolve<IRecette> {
  constructor(private service: RecetteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecette> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((recette: HttpResponse<Recette>) => {
          if (recette.body) {
            return of(recette.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Recette());
  }
}

export const recetteRoute: Routes = [
  {
    path: '',
    component: RecetteComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.recette.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RecetteDetailComponent,
    resolve: {
      recette: RecetteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.recette.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RecetteUpdateComponent,
    resolve: {
      recette: RecetteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.recette.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RecetteUpdateComponent,
    resolve: {
      recette: RecetteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.recette.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
