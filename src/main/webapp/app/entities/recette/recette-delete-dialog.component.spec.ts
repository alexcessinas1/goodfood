import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { RecetteDeleteDialogComponent } from 'app/entities/recette/recette-delete-dialog.component';
import { RecetteService } from 'app/entities/recette/recette.service';
import { MockEventManager } from 'app/shared/util/helpers/mock-event-manager.service';
import { MockActiveModal } from 'app/shared/util/helpers/mock-active-modal.service';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Recette Management Delete Component', () => {
    let comp: RecetteDeleteDialogComponent;
    let fixture: ComponentFixture<RecetteDeleteDialogComponent>;
    let service: RecetteService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [RecetteDeleteDialogComponent],
      })
        .overrideTemplate(RecetteDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecetteDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RecetteService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
