import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecette } from 'app/shared/model/recette.model';
import { RecetteService } from './recette.service';
import { RecetteDeleteDialogComponent } from './recette-delete-dialog.component';

@Component({
  selector: 'gf-recette',
  templateUrl: './recette.component.html',
})
export class RecetteComponent implements OnInit, OnDestroy {
  recettes?: IRecette[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected recetteService: RecetteService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.recetteService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IRecette[]>) => (this.recettes = res.body || []));
      return;
    }

    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => (this.recettes = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInRecettes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRecette): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRecettes(): void {
    this.eventSubscriber = this.eventManager.subscribe('recetteListModification', () => this.loadAll());
  }

  delete(recette: IRecette): void {
    const modalRef = this.modalService.open(RecetteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.recette = recette;
  }
}
