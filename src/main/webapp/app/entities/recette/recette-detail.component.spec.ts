import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecetteDetailComponent } from 'app/entities/recette/recette-detail.component';
import { Recette } from 'app/shared/model/recette.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('Recette Management Detail Component', () => {
    let comp: RecetteDetailComponent;
    let fixture: ComponentFixture<RecetteDetailComponent>;
    const route = ({ data: of({ recette: new Recette(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [RecetteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(RecetteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecetteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load recette on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.recette).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
