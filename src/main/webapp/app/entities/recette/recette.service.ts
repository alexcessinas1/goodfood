import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IRecette } from 'app/shared/model/recette.model';

type EntityResponseType = HttpResponse<IRecette>;
type EntityArrayResponseType = HttpResponse<IRecette[]>;

@Injectable({ providedIn: 'root' })
export class RecetteService {
  public resourceUrl = SERVER_API_URL + 'api/recettes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/recettes';

  constructor(protected http: HttpClient) {}

  create(recette: IRecette): Observable<EntityResponseType> {
    return this.http.post<IRecette>(this.resourceUrl, recette, { observe: 'response' });
  }

  update(recette: IRecette): Observable<EntityResponseType> {
    return this.http.put<IRecette>(this.resourceUrl, recette, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecette>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecette[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecette[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
