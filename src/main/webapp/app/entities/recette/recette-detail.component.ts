import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecette } from 'app/shared/model/recette.model';

@Component({
  selector: 'gf-recette-detail',
  templateUrl: './recette-detail.component.html',
})
export class RecetteDetailComponent implements OnInit {
  recette: IRecette | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recette }) => (this.recette = recette));
  }

  previousState(): void {
    window.history.back();
  }
}
