import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { RecetteUpdateComponent } from 'app/entities/recette/recette-update.component';
import { RecetteService } from 'app/entities/recette/recette.service';
import { Recette } from 'app/shared/model/recette.model';
import { ActivatedRoute } from '@angular/router';
import { ProduitService } from 'app/entities/produit/produit.service';
import { Produit } from 'app/shared/model/produit.model';
import { GoodFoodTestModule } from 'app/shared/test.module';
import { MockActivatedRoute } from 'app/shared/util/helpers/mock-route.service';

describe('Component Tests', () => {
  describe('Recette Management Update Component', () => {
    let comp: RecetteUpdateComponent;
    let fixture: ComponentFixture<RecetteUpdateComponent>;
    let service: RecetteService;
    let serviceProduit: ProduitService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [RecetteUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(RecetteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecetteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RecetteService);
      serviceProduit = fixture.debugElement.injector.get(ProduitService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Recette(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Recette();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });

    describe('initialisation', () => {
      it('Le composant doit mettre a jour son formulaire avec les donnees de la route', fakeAsync(() => {
        const mockActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute) as MockActivatedRoute;
        mockActivatedRoute.setParameters({ recette: new Recette(15, 'un joli titre', 'une jolie description') });
        spyOn(serviceProduit, 'query').and.returnValue(of(new HttpResponse({ body: [new Produit(4, '4externe', 'patates', 1)] })));
        comp.ngOnInit();
        tick(); // simulate async

        expect(comp.editForm.get('id')?.value).toBe(15);
        expect(comp.produitsListe).toHaveLength(1);
        expect(comp.produitsListe[0].nom).toBe('patates');
      }));
      it('Le composant doit avoir une liste de produit vide si le serviceProduit ne retourne rien', fakeAsync(() => {
        const mockActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute) as MockActivatedRoute;
        mockActivatedRoute.setParameters({ recette: new Recette(15, 'un joli titre', 'une jolie description') });
        spyOn(serviceProduit, 'query').and.returnValue(of(new HttpResponse({ body: null })));
        comp.ngOnInit();
        tick(); // simulate async

        expect(comp.produitsListe).toHaveLength(0);
      }));
    });

    describe('compareWith', () => {
      it('La fonction compareWith fonctionne', () => {
        expect(comp.containsProduitId(undefined, undefined)).toBe(true);
        expect(comp.containsProduitId(new Produit(2), undefined)).toBe(false);
        expect(comp.containsProduitId(undefined, new Produit(2))).toBe(false);
        expect(comp.containsProduitId(new Produit(2), new Produit(2))).toBe(true);
        expect(comp.containsProduitId(new Produit(3), new Produit(2))).toBe(false);
        expect(comp.containsProduitId(new Produit(), new Produit(2))).toBe(false);
        expect(comp.containsProduitId(new Produit(2), new Produit())).toBe(false);
      });
    });
  });
});
