import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { RecetteComponent } from './recette.component';
import { RecetteDetailComponent } from './recette-detail.component';
import { RecetteUpdateComponent } from './recette-update.component';
import { RecetteDeleteDialogComponent } from './recette-delete-dialog.component';
import { recetteRoute } from './recette.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(recetteRoute)],
  declarations: [RecetteComponent, RecetteDetailComponent, RecetteUpdateComponent, RecetteDeleteDialogComponent],
  entryComponents: [RecetteDeleteDialogComponent],
})
export class GoodFoodRecetteModule {}
