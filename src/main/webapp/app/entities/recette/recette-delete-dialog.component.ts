import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRecette } from 'app/shared/model/recette.model';
import { RecetteService } from './recette.service';

@Component({
  templateUrl: './recette-delete-dialog.component.html',
})
export class RecetteDeleteDialogComponent {
  recette?: IRecette;

  constructor(protected recetteService: RecetteService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recetteService.delete(id).subscribe(() => {
      this.eventManager.broadcast('recetteListModification');
      this.activeModal.close();
    });
  }
}
