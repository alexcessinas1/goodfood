import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'personne-physique',
        loadChildren: () => import('./personne-physique/personne-physique.module').then(m => m.GoodFoodPersonnePhysiqueModule),
      },
      {
        path: 'adresse',
        loadChildren: () => import('./adresse/adresse.module').then(m => m.GoodFoodAdresseModule),
      },
      {
        path: 'personne-morale',
        loadChildren: () => import('./personne-morale/personne-morale.module').then(m => m.GoodFoodPersonneMoraleModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.GoodFoodClientModule),
      },
      {
        path: 'categorie-boisson',
        loadChildren: () => import('./categorie-boisson/categorie-boisson.module').then(m => m.GoodFoodCategorieBoissonModule),
      },
      {
        path: 'categorie-alimentation',
        loadChildren: () =>
          import('./categorie-alimentation/categorie-alimentation.module').then(m => m.GoodFoodCategorieAlimentationModule),
      },
      {
        path: 'categorie-fourniture',
        loadChildren: () => import('./categorie-fourniture/categorie-fourniture.module').then(m => m.GoodFoodCategorieFournitureModule),
      },
      {
        path: 'produit',
        loadChildren: () => import('./produit/produit.module').then(m => m.GoodFoodProduitModule),
      },
      {
        path: 'recette',
        loadChildren: () => import('./recette/recette.module').then(m => m.GoodFoodRecetteModule),
      },
      {
        path: 'contrat',
        loadChildren: () => import('./contrat/contrat.page.module').then(m => m.GoodFoodContratPageModule),
      },
      {
        path: 'faq',
        loadChildren: () => import('./FAQ/faq.page.module').then(m => m.GoodFoodContratPageModule),
      },
      {
        path: 'contact',
        loadChildren: () => import('./contact/contact.page.module').then(m => m.GoodFoodContactPageModule),
      },
      {
        path: 'nos-magasins',
        loadChildren: () => import('./shop-page/shop.page.module').then(m => m.GoodFoodShopPageModule),
      },
      {
        path: 'plateau-repas',
        loadChildren: () => import('./plateau-repas/plateau-repas.module').then(m => m.GoodFoodPlateauRepasModule),
      },
      {
        path: 'allergene',
        loadChildren: () => import('./allergene/allergene.module').then(m => m.GoodFoodAllergeneModule),
      },
      {
        path: 'tunnel-vente',
        loadChildren: () => import('./tunnel-vente/tunnel-vente.module').then(m => m.GoodFoodTunnelVenteModule),
      },
      {
        path: 'franchise',
        loadChildren: () => import('./franchise/franchise.module').then(m => m.GoodFoodFranchiseModule),
      },
      {
        path: 'fournisseur',
        loadChildren: () => import('./fournisseur/fournisseur.module').then(m => m.GoodFoodFournisseurModule),
      },
      {
        path: 'commande',
        loadChildren: () => import('./commande/commande.module').then(m => m.GoodFoodCommandeModule),
      },
      {
        path: 'employe',
        loadChildren: () => import('./employe/employe.module').then(m => m.GoodFoodEmployeModule),
      },
      {
        path: 'commande-client',
        loadChildren: () => import('./commande-client/commande-client.module').then(m => m.GoodFoodCommandeClientModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class GoodFoodEntityModule {}
