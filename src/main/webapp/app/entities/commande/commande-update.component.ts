import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICommande, Commande } from 'app/shared/model/commande.model';
import { CommandeService } from './commande.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { IFranchise } from 'app/shared/model/franchise.model';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { IFournisseur } from 'app/shared/model/fournisseur.model';
import { FournisseurService } from 'app/entities/fournisseur/fournisseur.service';

type SelectableEntity = IClient | IFranchise | IFournisseur;

@Component({
  selector: 'gf-commande-update',
  templateUrl: './commande-update.component.html',
})
export class CommandeUpdateComponent implements OnInit {
  isSaving = false;
  clients: IClient[] = [];
  franchises: IFranchise[] = [];
  fournisseurs: IFournisseur[] = [];

  editForm = this.fb.group({
    id: [],
    prixTotal: [null, [Validators.required]],
    dateCommande: [null, [Validators.required]],
    dateStatut: [null, [Validators.required]],
    statut: [null, [Validators.required]],
    aLivrer: [null, [Validators.required]],
    datePaiement: [null, [Validators.required]],
    idClient: [],
    idFranchise: [],
    idFournisseur: [],
  });

  constructor(
    protected commandeService: CommandeService,
    protected clientService: ClientService,
    protected franchiseService: FranchiseService,
    protected fournisseurService: FournisseurService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commande }) => {
      this.updateForm(commande);

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));

      this.franchiseService.query().subscribe((res: HttpResponse<IFranchise[]>) => (this.franchises = res.body || []));

      this.fournisseurService.query().subscribe((res: HttpResponse<IFournisseur[]>) => (this.fournisseurs = res.body || []));
    });
  }

  updateForm(commande: ICommande): void {
    this.editForm.patchValue({
      id: commande.id,
      prixTotal: commande.prixTotal,
      dateCommande: commande.dateCommande,
      dateStatut: commande.dateStatut,
      statut: commande.statut,
      aLivrer: commande.aLivrer,
      datePaiement: commande.datePaiement,
      idClient: commande.idClient,
      idFranchise: commande.idFranchise,
      idFournisseur: commande.idFournisseur,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commande = this.createFromForm();
    if (commande.id !== undefined) {
      this.subscribeToSaveResponse(this.commandeService.update(commande));
    } else {
      this.subscribeToSaveResponse(this.commandeService.create(commande));
    }
  }

  private createFromForm(): ICommande {
    return {
      ...new Commande(),
      id: this.editForm.get(['id'])!.value,
      prixTotal: this.editForm.get(['prixTotal'])!.value,
      dateCommande: this.editForm.get(['dateCommande'])!.value,
      dateStatut: this.editForm.get(['dateStatut'])!.value,
      statut: this.editForm.get(['statut'])!.value,
      aLivrer: this.editForm.get(['aLivrer'])!.value,
      datePaiement: this.editForm.get(['datePaiement'])!.value,
      idClient: this.editForm.get(['idClient'])!.value,
      idFranchise: this.editForm.get(['idFranchise'])!.value,
      idFournisseur: this.editForm.get(['idFournisseur'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommande>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
