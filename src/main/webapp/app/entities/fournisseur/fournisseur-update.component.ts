import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFournisseur, Fournisseur } from 'app/shared/model/fournisseur.model';
import { FournisseurService } from './fournisseur.service';
import { IAdresse } from 'app/shared/model/adresse.model';
import { AdresseService } from 'app/entities/adresse/adresse.service';

@Component({
  selector: 'gf-fournisseur-update',
  templateUrl: './fournisseur-update.component.html',
})
export class FournisseurUpdateComponent implements OnInit {
  isSaving = false;
  idadresses: IAdresse[] = [];

  editForm = this.fb.group({
    id: [],
    prenom: [null, [Validators.required]],
    nom: [null, [Validators.required]],
    email: [null, [Validators.required]],
    titre: [],
    telEntreprise: [null, [Validators.required]],
    telMaison: [],
    telMobile: [],
    numeroFax: [],
    pageWeb: [],
    notes: [],
    idAdresseId: [null, Validators.required],
  });

  constructor(
    protected fournisseurService: FournisseurService,
    protected adresseService: AdresseService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fournisseur }) => {
      this.updateForm(fournisseur);

      this.adresseService
        .query({ filter: 'idfournisseur-is-null' })
        .pipe(
          map((res: HttpResponse<IAdresse[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAdresse[]) => {
          if (!fournisseur.idAdresseId) {
            this.idadresses = resBody;
          } else {
            this.adresseService
              .find(fournisseur.idAdresseId)
              .pipe(
                map((subRes: HttpResponse<IAdresse>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAdresse[]) => (this.idadresses = concatRes));
          }
        });
    });
  }

  updateForm(fournisseur: IFournisseur): void {
    this.editForm.patchValue({
      id: fournisseur.id,
      prenom: fournisseur.prenom,
      nom: fournisseur.nom,
      email: fournisseur.email,
      titre: fournisseur.titre,
      telEntreprise: fournisseur.telEntreprise,
      telMaison: fournisseur.telMaison,
      telMobile: fournisseur.telMobile,
      numeroFax: fournisseur.numeroFax,
      pageWeb: fournisseur.pageWeb,
      notes: fournisseur.notes,
      idAdresseId: fournisseur.idAdresseId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fournisseur = this.createFromForm();
    if (fournisseur.id !== undefined) {
      this.subscribeToSaveResponse(this.fournisseurService.update(fournisseur));
    } else {
      this.subscribeToSaveResponse(this.fournisseurService.create(fournisseur));
    }
  }

  private createFromForm(): IFournisseur {
    return {
      ...new Fournisseur(),
      id: this.editForm.get(['id'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      email: this.editForm.get(['email'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      telEntreprise: this.editForm.get(['telEntreprise'])!.value,
      telMaison: this.editForm.get(['telMaison'])!.value,
      telMobile: this.editForm.get(['telMobile'])!.value,
      numeroFax: this.editForm.get(['numeroFax'])!.value,
      pageWeb: this.editForm.get(['pageWeb'])!.value,
      notes: this.editForm.get(['notes'])!.value,
      idAdresseId: this.editForm.get(['idAdresseId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFournisseur>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IAdresse): any {
    return item.id;
  }
}
