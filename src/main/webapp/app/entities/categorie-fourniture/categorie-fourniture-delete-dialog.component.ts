import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { CategorieFournitureService } from './categorie-fourniture.service';

@Component({
  templateUrl: './categorie-fourniture-delete-dialog.component.html',
})
export class CategorieFournitureDeleteDialogComponent {
  categorieFourniture?: ICategorieFourniture;

  constructor(
    protected categorieFournitureService: CategorieFournitureService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.categorieFournitureService.delete(id).subscribe(() => {
      this.eventManager.broadcast('categorieFournitureListModification');
      this.activeModal.close();
    });
  }
}
