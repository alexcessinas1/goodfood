import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICategorieFourniture, CategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { CategorieFournitureService } from './categorie-fourniture.service';

@Component({
  selector: 'gf-categorie-fourniture-update',
  templateUrl: './categorie-fourniture-update.component.html',
})
export class CategorieFournitureUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    type: [],
  });

  constructor(
    protected categorieFournitureService: CategorieFournitureService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieFourniture }) => {
      this.updateForm(categorieFourniture);
    });
  }

  updateForm(categorieFourniture: ICategorieFourniture): void {
    this.editForm.patchValue({
      id: categorieFourniture.id,
      type: categorieFourniture.type,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const categorieFourniture = this.createFromForm();
    if (categorieFourniture.id !== undefined) {
      this.subscribeToSaveResponse(this.categorieFournitureService.update(categorieFourniture));
    } else {
      this.subscribeToSaveResponse(this.categorieFournitureService.create(categorieFourniture));
    }
  }

  private createFromForm(): ICategorieFourniture {
    return {
      ...new CategorieFourniture(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategorieFourniture>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
