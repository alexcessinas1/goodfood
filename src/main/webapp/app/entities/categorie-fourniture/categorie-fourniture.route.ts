import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICategorieFourniture, CategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { CategorieFournitureService } from './categorie-fourniture.service';
import { CategorieFournitureComponent } from './categorie-fourniture.component';
import { CategorieFournitureDetailComponent } from './categorie-fourniture-detail.component';
import { CategorieFournitureUpdateComponent } from './categorie-fourniture-update.component';

@Injectable({ providedIn: 'root' })
export class CategorieFournitureResolve implements Resolve<ICategorieFourniture> {
  constructor(private service: CategorieFournitureService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategorieFourniture> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((categorieFourniture: HttpResponse<CategorieFourniture>) => {
          if (categorieFourniture.body) {
            return of(categorieFourniture.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CategorieFourniture());
  }
}

export const categorieFournitureRoute: Routes = [
  {
    path: '',
    component: CategorieFournitureComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieFourniture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CategorieFournitureDetailComponent,
    resolve: {
      categorieFourniture: CategorieFournitureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieFourniture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CategorieFournitureUpdateComponent,
    resolve: {
      categorieFourniture: CategorieFournitureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieFourniture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CategorieFournitureUpdateComponent,
    resolve: {
      categorieFourniture: CategorieFournitureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieFourniture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
