import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { CategorieFournitureService } from './categorie-fourniture.service';
import { CategorieFournitureDeleteDialogComponent } from './categorie-fourniture-delete-dialog.component';

@Component({
  selector: 'gf-categorie-fourniture',
  templateUrl: './categorie-fourniture.component.html',
})
export class CategorieFournitureComponent implements OnInit, OnDestroy {
  categorieFournitures?: ICategorieFourniture[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected categorieFournitureService: CategorieFournitureService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.categorieFournitureService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ICategorieFourniture[]>) => (this.categorieFournitures = res.body || []));
      return;
    }

    this.categorieFournitureService
      .query()
      .subscribe((res: HttpResponse<ICategorieFourniture[]>) => (this.categorieFournitures = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCategorieFournitures();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICategorieFourniture): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCategorieFournitures(): void {
    this.eventSubscriber = this.eventManager.subscribe('categorieFournitureListModification', () => this.loadAll());
  }

  delete(categorieFourniture: ICategorieFourniture): void {
    const modalRef = this.modalService.open(CategorieFournitureDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.categorieFourniture = categorieFourniture;
  }
}
