import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { CategorieFournitureComponent } from 'app/entities/categorie-fourniture/categorie-fourniture.component';
import { CategorieFournitureService } from 'app/entities/categorie-fourniture/categorie-fourniture.service';
import { CategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieFourniture Management Component', () => {
    let comp: CategorieFournitureComponent;
    let fixture: ComponentFixture<CategorieFournitureComponent>;
    let service: CategorieFournitureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieFournitureComponent],
      })
        .overrideTemplate(CategorieFournitureComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieFournitureComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieFournitureService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CategorieFourniture(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.categorieFournitures && comp.categorieFournitures[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
