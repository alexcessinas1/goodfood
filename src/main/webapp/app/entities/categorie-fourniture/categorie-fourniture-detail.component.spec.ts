import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { CategorieFournitureDetailComponent } from 'app/entities/categorie-fourniture/categorie-fourniture-detail.component';
import { CategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieFourniture Management Detail Component', () => {
    let comp: CategorieFournitureDetailComponent;
    let fixture: ComponentFixture<CategorieFournitureDetailComponent>;
    const route = ({ data: of({ categorieFourniture: new CategorieFourniture(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieFournitureDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CategorieFournitureDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CategorieFournitureDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load categorieFourniture on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.categorieFourniture).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
