import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICategorieFourniture } from 'app/shared/model/categorie-fourniture.model';

type EntityResponseType = HttpResponse<ICategorieFourniture>;
type EntityArrayResponseType = HttpResponse<ICategorieFourniture[]>;

@Injectable({ providedIn: 'root' })
export class CategorieFournitureService {
  public resourceUrl = SERVER_API_URL + 'api/categorie-fournitures';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/categorie-fournitures';

  constructor(protected http: HttpClient) {}

  create(categorieFourniture: ICategorieFourniture): Observable<EntityResponseType> {
    return this.http.post<ICategorieFourniture>(this.resourceUrl, categorieFourniture, { observe: 'response' });
  }

  update(categorieFourniture: ICategorieFourniture): Observable<EntityResponseType> {
    return this.http.put<ICategorieFourniture>(this.resourceUrl, categorieFourniture, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICategorieFourniture>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieFourniture[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieFourniture[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
