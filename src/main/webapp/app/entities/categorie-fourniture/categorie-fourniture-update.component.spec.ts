import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { CategorieFournitureUpdateComponent } from 'app/entities/categorie-fourniture/categorie-fourniture-update.component';
import { CategorieFournitureService } from 'app/entities/categorie-fourniture/categorie-fourniture.service';
import { CategorieFourniture } from 'app/shared/model/categorie-fourniture.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieFourniture Management Update Component', () => {
    let comp: CategorieFournitureUpdateComponent;
    let fixture: ComponentFixture<CategorieFournitureUpdateComponent>;
    let service: CategorieFournitureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieFournitureUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CategorieFournitureUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieFournitureUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieFournitureService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieFourniture(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieFourniture();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
