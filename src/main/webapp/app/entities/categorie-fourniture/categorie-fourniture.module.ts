import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { CategorieFournitureComponent } from './categorie-fourniture.component';
import { CategorieFournitureDetailComponent } from './categorie-fourniture-detail.component';
import { CategorieFournitureUpdateComponent } from './categorie-fourniture-update.component';
import { CategorieFournitureDeleteDialogComponent } from './categorie-fourniture-delete-dialog.component';
import { categorieFournitureRoute } from './categorie-fourniture.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(categorieFournitureRoute)],
  declarations: [
    CategorieFournitureComponent,
    CategorieFournitureDetailComponent,
    CategorieFournitureUpdateComponent,
    CategorieFournitureDeleteDialogComponent,
  ],
  entryComponents: [CategorieFournitureDeleteDialogComponent],
})
export class GoodFoodCategorieFournitureModule {}
