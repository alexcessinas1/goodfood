import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICategorieFourniture } from 'app/shared/model/categorie-fourniture.model';

@Component({
  selector: 'gf-categorie-fourniture-detail',
  templateUrl: './categorie-fourniture-detail.component.html',
})
export class CategorieFournitureDetailComponent implements OnInit {
  categorieFourniture: ICategorieFourniture | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieFourniture }) => (this.categorieFourniture = categorieFourniture));
  }

  previousState(): void {
    window.history.back();
  }
}
