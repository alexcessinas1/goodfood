import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICategorieBoisson, CategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { CategorieBoissonService } from './categorie-boisson.service';

@Component({
  selector: 'gf-categorie-boisson-update',
  templateUrl: './categorie-boisson-update.component.html',
})
export class CategorieBoissonUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    degreAlcool: [],
    type: [],
  });

  constructor(
    protected categorieBoissonService: CategorieBoissonService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieBoisson }) => {
      this.updateForm(categorieBoisson);
    });
  }

  updateForm(categorieBoisson: ICategorieBoisson): void {
    this.editForm.patchValue({
      id: categorieBoisson.id,
      degreAlcool: categorieBoisson.degreAlcool,
      type: categorieBoisson.type,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const categorieBoisson = this.createFromForm();
    if (categorieBoisson.id !== undefined) {
      this.subscribeToSaveResponse(this.categorieBoissonService.update(categorieBoisson));
    } else {
      this.subscribeToSaveResponse(this.categorieBoissonService.create(categorieBoisson));
    }
  }

  private createFromForm(): ICategorieBoisson {
    return {
      ...new CategorieBoisson(),
      id: this.editForm.get(['id'])!.value,
      degreAlcool: this.editForm.get(['degreAlcool'])!.value,
      type: this.editForm.get(['type'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategorieBoisson>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
