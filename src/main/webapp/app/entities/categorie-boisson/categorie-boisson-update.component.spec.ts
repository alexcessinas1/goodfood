import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { CategorieBoissonUpdateComponent } from 'app/entities/categorie-boisson/categorie-boisson-update.component';
import { CategorieBoissonService } from 'app/entities/categorie-boisson/categorie-boisson.service';
import { CategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieBoisson Management Update Component', () => {
    let comp: CategorieBoissonUpdateComponent;
    let fixture: ComponentFixture<CategorieBoissonUpdateComponent>;
    let service: CategorieBoissonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieBoissonUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CategorieBoissonUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieBoissonUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieBoissonService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieBoisson(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CategorieBoisson();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
