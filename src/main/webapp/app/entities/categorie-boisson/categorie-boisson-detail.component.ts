import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICategorieBoisson } from 'app/shared/model/categorie-boisson.model';

@Component({
  selector: 'gf-categorie-boisson-detail',
  templateUrl: './categorie-boisson-detail.component.html',
})
export class CategorieBoissonDetailComponent implements OnInit {
  categorieBoisson: ICategorieBoisson | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ categorieBoisson }) => (this.categorieBoisson = categorieBoisson));
  }

  previousState(): void {
    window.history.back();
  }
}
