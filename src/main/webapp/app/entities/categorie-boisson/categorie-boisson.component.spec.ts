import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { CategorieBoissonComponent } from 'app/entities/categorie-boisson/categorie-boisson.component';
import { CategorieBoissonService } from 'app/entities/categorie-boisson/categorie-boisson.service';
import { CategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieBoisson Management Component', () => {
    let comp: CategorieBoissonComponent;
    let fixture: ComponentFixture<CategorieBoissonComponent>;
    let service: CategorieBoissonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieBoissonComponent],
      })
        .overrideTemplate(CategorieBoissonComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CategorieBoissonComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CategorieBoissonService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CategorieBoisson(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.categorieBoissons && comp.categorieBoissons[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
