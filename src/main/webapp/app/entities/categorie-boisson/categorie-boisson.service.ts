import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICategorieBoisson } from 'app/shared/model/categorie-boisson.model';

type EntityResponseType = HttpResponse<ICategorieBoisson>;
type EntityArrayResponseType = HttpResponse<ICategorieBoisson[]>;

@Injectable({ providedIn: 'root' })
export class CategorieBoissonService {
  public resourceUrl = SERVER_API_URL + 'api/categorie-boissons';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/categorie-boissons';

  constructor(protected http: HttpClient) {}

  create(categorieBoisson: ICategorieBoisson): Observable<EntityResponseType> {
    return this.http.post<ICategorieBoisson>(this.resourceUrl, categorieBoisson, { observe: 'response' });
  }

  update(categorieBoisson: ICategorieBoisson): Observable<EntityResponseType> {
    return this.http.put<ICategorieBoisson>(this.resourceUrl, categorieBoisson, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICategorieBoisson>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieBoisson[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICategorieBoisson[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
