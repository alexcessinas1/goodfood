import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { CategorieBoissonComponent } from './categorie-boisson.component';
import { CategorieBoissonDetailComponent } from './categorie-boisson-detail.component';
import { CategorieBoissonUpdateComponent } from './categorie-boisson-update.component';
import { CategorieBoissonDeleteDialogComponent } from './categorie-boisson-delete-dialog.component';
import { categorieBoissonRoute } from './categorie-boisson.route';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild(categorieBoissonRoute)],
  declarations: [
    CategorieBoissonComponent,
    CategorieBoissonDetailComponent,
    CategorieBoissonUpdateComponent,
    CategorieBoissonDeleteDialogComponent,
  ],
  entryComponents: [CategorieBoissonDeleteDialogComponent],
})
export class GoodFoodCategorieBoissonModule {}
