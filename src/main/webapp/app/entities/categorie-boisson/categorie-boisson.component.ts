import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { CategorieBoissonService } from './categorie-boisson.service';
import { CategorieBoissonDeleteDialogComponent } from './categorie-boisson-delete-dialog.component';

@Component({
  selector: 'gf-categorie-boisson',
  templateUrl: './categorie-boisson.component.html',
})
export class CategorieBoissonComponent implements OnInit, OnDestroy {
  categorieBoissons?: ICategorieBoisson[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected categorieBoissonService: CategorieBoissonService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.categorieBoissonService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ICategorieBoisson[]>) => (this.categorieBoissons = res.body || []));
      return;
    }

    this.categorieBoissonService.query().subscribe((res: HttpResponse<ICategorieBoisson[]>) => (this.categorieBoissons = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCategorieBoissons();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICategorieBoisson): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCategorieBoissons(): void {
    this.eventSubscriber = this.eventManager.subscribe('categorieBoissonListModification', () => this.loadAll());
  }

  delete(categorieBoisson: ICategorieBoisson): void {
    const modalRef = this.modalService.open(CategorieBoissonDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.categorieBoisson = categorieBoisson;
  }
}
