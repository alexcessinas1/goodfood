import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { CategorieBoissonService } from './categorie-boisson.service';

@Component({
  templateUrl: './categorie-boisson-delete-dialog.component.html',
})
export class CategorieBoissonDeleteDialogComponent {
  categorieBoisson?: ICategorieBoisson;

  constructor(
    protected categorieBoissonService: CategorieBoissonService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.categorieBoissonService.delete(id).subscribe(() => {
      this.eventManager.broadcast('categorieBoissonListModification');
      this.activeModal.close();
    });
  }
}
