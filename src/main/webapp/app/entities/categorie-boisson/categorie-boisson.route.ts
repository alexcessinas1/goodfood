import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICategorieBoisson, CategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { CategorieBoissonService } from './categorie-boisson.service';
import { CategorieBoissonComponent } from './categorie-boisson.component';
import { CategorieBoissonDetailComponent } from './categorie-boisson-detail.component';
import { CategorieBoissonUpdateComponent } from './categorie-boisson-update.component';

@Injectable({ providedIn: 'root' })
export class CategorieBoissonResolve implements Resolve<ICategorieBoisson> {
  constructor(private service: CategorieBoissonService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICategorieBoisson> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((categorieBoisson: HttpResponse<CategorieBoisson>) => {
          if (categorieBoisson.body) {
            return of(categorieBoisson.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CategorieBoisson());
  }
}

export const categorieBoissonRoute: Routes = [
  {
    path: '',
    component: CategorieBoissonComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieBoisson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CategorieBoissonDetailComponent,
    resolve: {
      categorieBoisson: CategorieBoissonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieBoisson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CategorieBoissonUpdateComponent,
    resolve: {
      categorieBoisson: CategorieBoissonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieBoisson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CategorieBoissonUpdateComponent,
    resolve: {
      categorieBoisson: CategorieBoissonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'goodFoodApp.categorieBoisson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
