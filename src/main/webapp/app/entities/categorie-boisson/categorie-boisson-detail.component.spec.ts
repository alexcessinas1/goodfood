import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { CategorieBoissonDetailComponent } from 'app/entities/categorie-boisson/categorie-boisson-detail.component';
import { CategorieBoisson } from 'app/shared/model/categorie-boisson.model';
import { GoodFoodTestModule } from 'app/shared/test.module';

describe('Component Tests', () => {
  describe('CategorieBoisson Management Detail Component', () => {
    let comp: CategorieBoissonDetailComponent;
    let fixture: ComponentFixture<CategorieBoissonDetailComponent>;
    const route = ({ data: of({ categorieBoisson: new CategorieBoisson(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GoodFoodTestModule],
        declarations: [CategorieBoissonDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CategorieBoissonDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CategorieBoissonDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load categorieBoisson on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.categorieBoisson).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
