import { Pipe, PipeTransform } from '@angular/core';

// Prend un array en parametres, retourne son champ "key" joint par "joinChar"
@Pipe({ name: 'concatString' })
export class ConcatStringPipe implements PipeTransform {
  transform(tableau: any[] | undefined, key: string, joinChar: string): string {
    if (tableau === undefined) {
      return 'AUCUN';
    }
    return tableau.map(val => val[key]).join(joinChar);
  }
}
