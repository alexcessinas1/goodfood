import { defer, Observable } from 'rxjs';

export class MapService {
  constructor() {}

  /**
   * Recupere les coordonnées GPS a partir d'une address
   * @param address string
   */
  public getGeoLocation(address: string): Observable<any> {
    const geocoder = new google.maps.Geocoder();
    return new Observable((observer: any) => {
      geocoder.geocode({ address }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          observer.next(results[0].geometry.location);
          observer.complete();
        }
      });
    });
  }

  public getDistance(origin: google.maps.LatLngLiteral, destination: string[]): Observable<any> {
    const matrix = new google.maps.DistanceMatrixService();
    return defer(() => {
      return matrix.getDistanceMatrix(
        {
          origins: [origin],
          destinations: destination,
          travelMode: google.maps.TravelMode.WALKING,
        },
        (response, status) => {
          if (status === 'OK') {
            return response;
          } else {
            return status;
          }
        }
      );
    });
  }
}
