import { ConcatStringPipe } from 'app/shared/util/string.pipe';

describe('stringConcatPipe', () => {
  const pipe = new ConcatStringPipe();

  it('normal', () => {
    expect(pipe.transform([{ nom: 'nom1' }, { nom: 'nom2' }], 'nom', ';')).toBe('nom1;nom2');
  });
  it('undefined tableau', () => {
    expect(pipe.transform(undefined, 'nom', ';')).toBe('AUCUN');
  });
});
