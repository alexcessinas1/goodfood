export interface ICategorieAlimentation {
  id?: number;
  vegan?: boolean;
  vegetarien?: boolean;
}

export class CategorieAlimentation implements ICategorieAlimentation {
  constructor(public id?: number, public vegan?: boolean, public vegetarien?: boolean) {
    this.vegan = this.vegan || false;
    this.vegetarien = this.vegetarien || false;
  }
}
