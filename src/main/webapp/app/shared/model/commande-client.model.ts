import { IPlateauRepas } from 'app/shared/model/plateau-repas.model';

export interface ICommandeClient {
  id?: number;
  codePromo?: string;
  clientId?: number;
  employeId?: number;
  commandeId?: number;
  plateauRepas?: IPlateauRepas[];
}

export class CommandeClient implements ICommandeClient {
  constructor(params?: Partial<ICommandeClient>) {
    Object.assign(this, params);
  }
}
