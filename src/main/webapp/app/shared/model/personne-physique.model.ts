import { Moment } from 'moment';

export interface IPersonnePhysique {
  id?: number;
  dateNaissance?: Moment;
  userLogin?: string;
  userId?: number;
  nom?: string;
  email?: string;
  phone?: string;
}

export class PersonnePhysique implements IPersonnePhysique {
  constructor(params?: Partial<IPersonnePhysique>) {
    Object.assign(this, params);
  }
}
