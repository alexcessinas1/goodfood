export interface ICategorieFourniture {
  id?: number;
  type?: string;
}

export class CategorieFourniture implements ICategorieFourniture {
  constructor(public id?: number, public type?: string) {}
}
