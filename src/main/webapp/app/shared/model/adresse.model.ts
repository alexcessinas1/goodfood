export interface IAdresse {
  id?: number;
  numeroVoie?: string;
  nomVoie?: string;
  codePostal?: string;
  ville?: string;
  complementAdresse?: string;
  infoComplementaire?: string;
}

export class Adresse implements IAdresse {
  constructor(params?: Partial<IAdresse>) {
    Object.assign(this, params);
  }
}
