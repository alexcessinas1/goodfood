export const enum ProduitCategorie {
  BOISSON = 'BOISSON',

  ALIMENTATION = 'ALIMENTATION',

  FOURNITURE = 'FOURNITURE',
}
