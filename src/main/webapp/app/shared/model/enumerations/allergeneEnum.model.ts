export const enum AllergeneEnum {
  ARACHIDE = 'ARACHIDE',

  LAIT = 'LAIT',

  OEUF = 'OEUF',

  GLUTTEN = 'GLUTTEN',

  CHOUX_DE_BRUXELLES = 'CHOUX_DE_BRUXELLES',
}
