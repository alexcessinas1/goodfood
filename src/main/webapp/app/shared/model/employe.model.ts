import { PosteEnum } from 'app/shared/model/enumerations/poste-enum.model';

export interface IEmploye {
  id?: number;
  poste?: PosteEnum;
  notes?: string;
  franchiseId?: number;
  adresseId?: number;
  personnePhysiqueId?: number;
  personneMoraleId?: number;
}

export class Employe implements IEmploye {
  constructor(
    public id?: number,
    public poste?: PosteEnum,
    public notes?: string,
    public franchiseId?: number,
    public adresseId?: number,
    public personnePhysiqueId?: number,
    public personneMoraleId?: number
  ) {}
}
