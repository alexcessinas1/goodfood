export interface IPersonneMorale {
  id?: number;
  raisonSociale?: string;
  siret?: string;
  userLogin?: string;
  userId?: number;
}

export class PersonneMorale implements IPersonneMorale {
  constructor(
    public id?: number,
    public raisonSociale?: string,
    public siret?: string,
    public userLogin?: string,
    public userId?: number
  ) {}
}
