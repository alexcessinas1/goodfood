import { PersonneType } from 'app/shared/model/enumerations/personne-type.model';

export interface IClient {
  id?: number;
  personneType?: PersonneType;
  adresseLivraisonId?: number;
  adresseFacturationId?: number;
  personnePhysiqueId?: number;
  personneMoraleId?: number;
}

export class Client implements IClient {
  constructor(params?: Partial<IClient>) {
    Object.assign(this, params);
  }
}
