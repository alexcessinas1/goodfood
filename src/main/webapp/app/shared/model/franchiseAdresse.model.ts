import { IAdresse } from 'app/shared/model/adresse.model';

export interface ISimpleFranchise {
  id?: number;
  adresseFranchise?: IAdresse;
}

export class FranchiseAdresse implements ISimpleFranchise {
  constructor(params?: Partial<ISimpleFranchise>) {
    Object.assign(this, params);
  }
}
