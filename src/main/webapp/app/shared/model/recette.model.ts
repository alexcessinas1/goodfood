import { IProduit } from 'app/shared/model/produit.model';
import { Plat } from 'app/shared/model/enumerations/plat.model';

export interface IRecette {
  id?: number;
  titre?: string;
  description?: string;
  type?: Plat;
  produits?: IProduit[];
}

export class Recette implements IRecette {
  constructor(public id?: number, public titre?: string, public description?: string, public type?: Plat, public produits?: IProduit[]) {}
}
