import { Statut } from 'app/shared/model/enumerations/statut.model';

export interface ICommande {
  id?: number;
  prixTotal?: number;
  dateCommande?: string;
  dateStatut?: string;
  statut?: Statut;
  aLivrer?: boolean;
  datePaiement?: string;
  idClient?: number;
  idFranchise?: number;
  idFournisseur?: number;
}

export class Commande implements ICommande {
  constructor(params?: Partial<ICommande>) {
    Object.assign(this, params);
  }
}
