import { IRecette } from 'app/shared/model/recette.model';

export interface IPlateauRepas {
  id?: number;
  prixHT?: number;
  tva?: number;
  composition?: string;
  couvert?: boolean;
  serviette?: boolean;
  boissonId?: number;
  recettes?: IRecette[];
  commandeClientId?: number;
}

export class PlateauRepas implements IPlateauRepas {
  constructor(params?: Partial<IPlateauRepas>) {
    Object.assign(this, params);
  }
}
