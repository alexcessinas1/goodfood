import { AllergeneEnum } from 'app/shared/model/enumerations/allergeneEnum.model';

export interface IAllergene {
  id?: number;
  nom?: AllergeneEnum;
}

export class Allergene implements IAllergene {
  constructor(public id?: number, public nom?: AllergeneEnum) {}
}
