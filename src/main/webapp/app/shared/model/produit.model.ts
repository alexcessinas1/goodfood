import { Moment } from 'moment';
import { unite } from 'app/shared/model/enumerations/unite.model';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { Allergene } from 'app/shared/model/allergene.model';

export interface IProduit {
  id?: number;
  eid?: string;
  nom?: string;
  quantite?: number;
  quantiteUnite?: unite;
  description?: string;
  prixCatalogue?: number;
  prixStandard?: number;
  venteOuverte?: boolean;
  origine?: string;
  produitCategorie?: ProduitCategorie;
  allergenes?: Allergene[];
  numeroLot?: string;
  dateLot?: Moment;
  boissonId?: number;
  alimentId?: number;
  fournitureId?: number;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public eid?: string,
    public nom?: string,
    public quantite?: number,
    public quantiteUnite?: unite,
    public description?: string,
    public prixCatalogue?: number,
    public prixStandard?: number,
    public venteOuverte?: boolean,
    public origine?: string,
    public produitCategorie?: ProduitCategorie,
    public allergenes?: Allergene[],
    public numeroLot?: string,
    public dateLot?: Moment,
    public boissonId?: number,
    public alimentId?: number,
    public fournitureId?: number
  ) {
    this.venteOuverte = this.venteOuverte || false;
  }
}
