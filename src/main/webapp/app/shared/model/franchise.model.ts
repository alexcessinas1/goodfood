import { IProduit } from 'app/shared/model/produit.model';

export interface IFranchise {
  id?: number;
  telephone?: string;
  email?: string;
  horaire?: string;
  siret?: string;
  adresseFranchiseIdId?: number;
  stockFranchises?: IProduit[];
}

export class Franchise implements IFranchise {
  constructor(
    public id?: number,
    public telephone?: string,
    public email?: string,
    public horaire?: string,
    public siret?: string,
    public adresseFranchiseIdId?: number,
    public stockFranchises?: IProduit[]
  ) {}
}
