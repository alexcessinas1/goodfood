import { IProduit } from 'app/shared/model/produit.model';

export interface IFournisseur {
  id?: number;
  prenom?: string;
  nom?: string;
  email?: string;
  titre?: string;
  telEntreprise?: string;
  telMaison?: string;
  telMobile?: string;
  numeroFax?: string;
  pageWeb?: string;
  notes?: string;
  idAdresseId?: number;
  idProduits?: IProduit[];
}

export class Fournisseur implements IFournisseur {
  constructor(
    public id?: number,
    public prenom?: string,
    public nom?: string,
    public email?: string,
    public titre?: string,
    public telEntreprise?: string,
    public telMaison?: string,
    public telMobile?: string,
    public numeroFax?: string,
    public pageWeb?: string,
    public notes?: string,
    public idAdresseId?: number,
    public idProduits?: IProduit[]
  ) {}
}
