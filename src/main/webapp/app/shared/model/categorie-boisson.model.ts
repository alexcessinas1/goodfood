export interface ICategorieBoisson {
  id?: number;
  degreAlcool?: number;
  type?: string;
}

export class CategorieBoisson implements ICategorieBoisson {
  constructor(public id?: number, public degreAlcool?: number, public type?: string) {}
}
