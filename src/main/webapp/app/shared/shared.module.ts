import { NgModule } from '@angular/core';
import { GoodFoodSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { ConcatStringPipe } from './util/string.pipe';

@NgModule({
  imports: [GoodFoodSharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ConcatStringPipe,
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    GoodFoodSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ConcatStringPipe,
  ],
})
export class GoodFoodSharedModule {}
