import { Component } from '@angular/core';

@Component({
  selector: 'gf-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['docs.scss'],
})
export class DocsComponent {}
