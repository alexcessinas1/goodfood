import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';
import { HttpResponse } from '@angular/common/http';
import { IRecette } from 'app/shared/model/recette.model';
import { RecetteService } from 'app/entities/recette/recette.service';
import { Router } from '@angular/router';

@Component({
  selector: 'gf-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  icons = goodfoodIcons;
  public recipes!: any[];

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private recetteService: RecetteService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => {
      this.recipes = res.body as any[];
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  goToShop(): void {
    this.router.navigate(['/nos-magasins']);
  }

  goToBuy(): void {
    this.router.navigate(['/tunnel-vente/etape/delivrance']);
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
