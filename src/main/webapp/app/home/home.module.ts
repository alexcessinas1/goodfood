import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GoodFoodSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { GoodFoodModulesModule } from 'app/modules/modules.module';

@NgModule({
  imports: [GoodFoodSharedModule, RouterModule.forChild([HOME_ROUTE]), GoodFoodModulesModule],
  declarations: [HomeComponent],
})
export class GoodFoodHomeModule {}
