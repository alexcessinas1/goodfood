import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { PanierComponent } from 'app/modules/panier/panier.component';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { AccordionModule } from 'primeng/accordion';
import { GoodFoodTestModule } from 'app/shared/test.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { DialogModule } from 'primeng/dialog';
import { MessageService } from 'primeng/api';
import { LocalStorageService, NgxWebstorageModule } from 'ngx-webstorage';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';
import { Commande } from 'app/shared/model/commande.model';
import { Adresse } from 'app/shared/model/adresse.model';
import { PersonnePhysique } from 'app/shared/model/personne-physique.model';
import { Client } from 'app/shared/model/client.model';
import { CommandeClient } from 'app/shared/model/commande-client.model';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { of } from 'rxjs';
import { RecetteService } from 'app/entities/recette/recette.service';
import { fakeAsync, tick } from '@angular/core/testing';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { ProduitService } from 'app/entities/produit/produit.service';
import { PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { Recette } from 'app/shared/model/recette.model';
import { PersonnePhysiqueService } from 'app/entities/personne-physique/personne-physique.service';
import { ClientService } from 'app/entities/client/client.service';
import { CommandeService } from 'app/entities/commande/commande.service';
import { CommandeClientService } from 'app/entities/commande-client/commande-client.service';
import { PlateauRepasService } from 'app/entities/plateau-repas/plateau-repas.service';

describe('Component Tests', () => {
  describe('Panier Component', () => {
    let spectator: Spectator<PanierComponent>;

    let localStorageService: LocalStorageService;
    const createComponent = createComponentFactory({
      component: PanierComponent,
      imports: [
        FontAwesomeTestingModule,
        AccordionModule,
        GoodFoodTestModule,
        FormsModule,
        ReactiveFormsModule,
        TooltipModule,
        DialogModule,
        NgxWebstorageModule.forRoot(),
      ],
      providers: [MessageService],
    });

    beforeEach(() => {
      spectator = createComponent();

      spyOn(spectator.inject(TunnelVenteService), 'getKeyAdresseLivraison').and.returnValue('AdresseLivraison');
      spyOn(spectator.inject(TunnelVenteService), 'getKeyCommand').and.returnValue('Command');
      spyOn(spectator.inject(TunnelVenteService), 'getKeyClient').and.returnValue('Client');
      spyOn(spectator.inject(TunnelVenteService), 'getKeyCustomerCommand').and.returnValue('CustomerCommand');
      spyOn(spectator.inject(TunnelVenteService), 'getKeyPersonnePhysique').and.returnValue('PersonnePhysique');

      spyOn(spectator.inject(FranchiseService), 'find').and.returnValue(of({ body: { adresseFranchiseIdId: 1 } }));
      spyOn(spectator.inject(AdresseService), 'find').and.returnValue(of({ body: {} }));
      spyOn(spectator.inject(AdresseService), 'create').and.returnValue(of({ body: { id: 12 } }));
      spyOn(spectator.inject(RecetteService), 'query').and.returnValue(of({ body: [{}] }));
      spyOn(spectator.inject(ProduitService), 'query').and.returnValue(of({ body: [{}] }));
    });

    const command = new Commande({ idFranchise: 1, aLivrer: true });
    const plateauRepas = [new PlateauRepas({ id: 1 }), new PlateauRepas({ id: 2 })];
    const customerCommand = new CommandeClient({ plateauRepas });

    it('should initialize', fakeAsync(() => {
      spyOn(spectator.inject(TunnelVenteService), 'get').and.returnValue(command);

      spectator.component.commande = command;

      spectator.component.ngOnInit();
      tick();

      // @ts-ignore
      expect(spectator.component.recetteService.query).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.franchiseService.find).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.adresseService.find).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.adresseService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.produitService.query).toHaveBeenCalled();

      expect(spectator.component.commande).toBeDefined();
      expect(spectator.component.commande.idFranchise).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should save 15', fakeAsync(() => {
      const spy = spyOn(spectator.inject(TunnelVenteService), 'set').and.callThrough();
      spectator.component.commandeClient = customerCommand;
      spectator.component.ngOnInit();
      tick();
      spectator.component.save();
      tick();
      expect(spy).toHaveBeenCalled();
      expect(spectator.component).toBeDefined();
    }));

    it('should remove recette', fakeAsync(() => {
      const recettes = [new Recette(1), new Recette(2)];
      const plateauRepa = new PlateauRepas({ id: 2, recettes });
      const result = new CommandeClient({ plateauRepas: [new PlateauRepas({ id: 12 }), plateauRepa] });
      spectator.component.commandeClient = result;
      expect(spectator.component.commandeClient).toBe(result);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].id).toBe(2);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].recettes[1].id).toBe(2);
      spectator.component.removeRecette(1, 1);
      tick();

      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].recettes).toBe(recettes);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].recettes.length).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should remove boisson', fakeAsync(() => {
      const plateauRepas5 = new PlateauRepas({ id: 2, boissonId: 12 });
      const result = new CommandeClient({ plateauRepas: [new PlateauRepas({ id: 12 }), plateauRepas5] });
      spectator.component.commandeClient = result;
      expect(spectator.component.commandeClient).toBe(result);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].id).toBe(2);
      spectator.component.removeBoisson(1);
      tick();

      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].boissonId).toBe(undefined);
      expect(spectator.component).toBeDefined();
    }));

    it('should remove plateau', fakeAsync(() => {
      const plateauRepas1 = new PlateauRepas({ id: 2 });
      const result = new CommandeClient({ plateauRepas: [new PlateauRepas({ id: 12 }), plateauRepas1] });
      spectator.component.commandeClient = result;
      expect(spectator.component.commandeClient).toBe(result);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[1].id).toBe(2);
      spectator.component.removePlateau(1);
      tick();

      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas?.length).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should remove plateau', fakeAsync(() => {
      const plateauRepas3 = new PlateauRepas({ id: 2 });
      const result = new CommandeClient({ plateauRepas: [plateauRepas3] });
      spectator.component.commandeClient = result;
      expect(spectator.component.commandeClient).toBe(result);
      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas[0].id).toBe(2);
      spectator.component.removePlateau(1);
      tick();

      // @ts-ignore
      expect(spectator.component.commandeClient.plateauRepas?.length).toBe(1);
      expect(spectator.component).toBeDefined();
    }));

    it('should valid command', fakeAsync(() => {
      spyOn(spectator.inject(PersonnePhysiqueService), 'create').and.returnValue(of({ body: { id: 12 } }));
      spyOn(spectator.inject(ClientService), 'create').and.returnValue(of({ body: { id: 12 } }));
      spyOn(spectator.inject(CommandeService), 'create').and.returnValue(of({ body: { id: 12 } }));
      spyOn(spectator.inject(CommandeClientService), 'create').and.returnValue(of({ body: { id: 12 } }));
      spyOn(spectator.inject(PlateauRepasService), 'create').and.returnValue(of({ body: { id: 12 }, ok: true }));
      spyOn(spectator.inject(MessageService), 'add').and.callThrough();
      spectator.component.contactForm.get('consent')?.setValue(true);
      spectator.component.ngOnInit();
      tick();
      spectator.component.commande = command;
      spectator.component.commandeClient = customerCommand;

      spectator.component.validCommand();
      tick();

      // @ts-ignore
      expect(spectator.component.personnePhysiqueService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.clientService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.commandeService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.commandeClientService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.plateauRepasService.create).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.messageService.add).toHaveBeenCalled();
      // @ts-ignore
      expect(spectator.component.router.navigate).toHaveBeenCalled();
      expect(spectator.component.contactForm.get('consent')?.value).toBe(true);
      expect(spectator.component).toBeDefined();
    }));
  });
});
