import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ICommandeClient } from 'app/shared/model/commande-client.model';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';
import { TunnelVenteService } from 'app/entities/tunnel-vente/tunnel-vente.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { FormBuilder, Validators } from '@angular/forms';
import { AccountService } from 'app/core/auth/account.service';
import { ICommande } from 'app/shared/model/commande.model';
import { FranchiseService } from 'app/entities/franchise/franchise.service';
import { AdresseService } from 'app/entities/adresse/adresse.service';
import { Adresse, IAdresse } from 'app/shared/model/adresse.model';
import { HttpResponse } from '@angular/common/http';
import { IRecette, Recette } from 'app/shared/model/recette.model';
import { RecetteService } from 'app/entities/recette/recette.service';
import { IPlateauRepas, PlateauRepas } from 'app/shared/model/plateau-repas.model';
import { ProduitCategorie } from 'app/shared/model/enumerations/produit-categorie.model';
import { IPersonnePhysique, PersonnePhysique } from 'app/shared/model/personne-physique.model';
import { UserService } from 'app/core/user/user.service';
import { Client, IClient } from 'app/shared/model/client.model';
import { PersonnePhysiqueService } from 'app/entities/personne-physique/personne-physique.service';
import { ClientService } from 'app/entities/client/client.service';
import { CommandeClientService } from 'app/entities/commande-client/commande-client.service';
import { CommandeService } from 'app/entities/commande/commande.service';
import { PlateauRepasService } from 'app/entities/plateau-repas/plateau-repas.service';
import { MessageService } from 'primeng/api';
import { Statut } from 'app/shared/model/enumerations/statut.model';
import { PersonneType } from 'app/shared/model/enumerations/personne-type.model';
import { Router } from '@angular/router';
import { url } from 'app/entities/tunnel-vente/etapes/constant';

@Component({
  selector: 'gf-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss'],
})
export class PanierComponent implements OnInit, OnChanges {
  @Output() indexSelectedPlateau: EventEmitter<number> = new EventEmitter<number>();
  @Input() validationMode = true;
  activeIndexPlateau!: number;
  public commandeClient: ICommandeClient | undefined;
  public commande: ICommande | undefined;
  public adresseFranchise: IAdresse = new Adresse();
  public boissonsList: IProduit[] = [];
  recettesList: Recette[] = [];
  prixPlateau: { plateau: IPlateauRepas; prix: number }[] = [];
  activeState: boolean[] = [true];
  displayTPE = false;
  public adresseLivraison: IAdresse = new Adresse();

  public totalPrice = 0;

  public icons = goodfoodIcons;
  /**
   * Déclaration des Validators pour le formulaire
   */
  contactForm = this.fb.group({
    name: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', Validators.required],
    isLiquid: [true, Validators.required],
    consent: [false, Validators.requiredTrue],
  });

  public keyCustomerCommand: string | null = '';
  public keyCommand: string | null = '';
  public keyPersonnePhysique: string | null = '';
  public keyClient: string | null = '';
  public keyAdresseLivraison: string | null = '';

  constructor(
    private readonly produitService: ProduitService,
    private readonly fb: FormBuilder,
    private loginModalService: LoginModalService,
    private tunnelService: TunnelVenteService,
    private readonly accountService: AccountService,
    private readonly userService: UserService,
    private readonly franchiseService: FranchiseService,
    private readonly adresseService: AdresseService,
    private recetteService: RecetteService,
    private readonly personnePhysiqueService: PersonnePhysiqueService,
    private readonly clientService: ClientService,
    private readonly commandeService: CommandeService,
    private readonly commandeClientService: CommandeClientService,
    private readonly plateauRepasService: PlateauRepasService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnChanges(): void {
    this.updateForm();
  }

  ngOnInit(): void {
    this.keyCustomerCommand = this.tunnelService.getKeyCustomerCommand();
    this.keyCommand = this.tunnelService.getKeyCommand();
    this.keyPersonnePhysique = this.tunnelService.getKeyPersonnePhysique();
    this.keyClient = this.tunnelService.getKeyClient();
    this.keyAdresseLivraison = this.tunnelService.getKeyAdresseLivraison();
    if (this.keyAdresseLivraison) {
      this.adresseLivraison = this.tunnelService.get(this.keyAdresseLivraison) as Adresse;
    }

    if (this.keyCommand) {
      this.commande = this.tunnelService.get(this.keyCommand) as ICommande;
      if (this.commande && this.commande.idFranchise) {
        this.franchiseService.find(this.commande.idFranchise).subscribe(franchise => {
          if (franchise.body && franchise.body.adresseFranchiseIdId) {
            this.adresseService.find(franchise.body.adresseFranchiseIdId).subscribe(adresse => {
              if (adresse.body) {
                this.adresseFranchise = adresse.body;
              }
            });
          }
        });
      }
    }

    if (this.keyCustomerCommand) {
      this.commandeClient = this.tunnelService.get(this.keyCustomerCommand) as ICommandeClient;
      this.produitService.query().subscribe(produit => {
        if (produit.body) {
          this.boissonsList = produit.body.filter(produitA => produitA.produitCategorie === ProduitCategorie.BOISSON);
        }
      });
    }

    if (this.accountService.isAuthenticated()) {
      this.accountService.getAuthenticationState().subscribe(account => {
        this.contactForm.patchValue({
          name: account?.lastName,
          phone: account?.mobilePhone,
          email: account?.email,
        });
      });
    }

    this.recetteService.query().subscribe((res: HttpResponse<IRecette[]>) => {
      if (res.body) {
        this.recettesList = res.body || [];
      }

      this.tunnelService.set(this.keyClient, new Client({}));
    });
    if (!!this.commande && this.commande.aLivrer && !!this.keyAdresseLivraison) {
      this.adresseService.create(this.tunnelService.get(this.keyAdresseLivraison) as Adresse).subscribe(adresseRes => {
        if (adresseRes && adresseRes.body && adresseRes.body.id && this.keyClient) {
          const client = this.tunnelService.get(this.keyClient) as IClient;
          if (client && client.adresseLivraisonId) {
            client.adresseLivraisonId = adresseRes.body.id;
          }
          this.tunnelService.set(this.keyClient, client);
        }
      });
    }
  }

  /**
   * Fait appel à la méthode du service de sauvegarde dans le local storage
   */
  save(): void {
    if (this.keyCustomerCommand) {
      this.tunnelService.set(this.keyCustomerCommand, this.commandeClient as ICommandeClient);
    }
  }

  /**
   * Supprime la recette d'un plateau repas
   * @param indexPlateau Position du plateau repas dans la liste
   * @param indexRecette Position de la recette dans la liste
   */
  removeRecette(indexPlateau: number, indexRecette: number): void {
    if (
      (indexPlateau || indexPlateau === 0) &&
      (indexRecette || indexRecette === 0) &&
      this.commandeClient &&
      this.commandeClient.plateauRepas &&
      this.commandeClient.plateauRepas[indexPlateau] &&
      this.commandeClient.plateauRepas[indexPlateau].recettes &&
      this.commandeClient.plateauRepas[indexPlateau].recettes
    ) {
      const recetteA = this.commandeClient.plateauRepas[indexPlateau].recettes;
      if (recetteA) {
        recetteA.splice(indexRecette, 1);
        this.save();
      }
    }
  }

  /**
   * Supprime la boisson d'un plateau repas
   * @param indexPlateau
   */
  removeBoisson(indexPlateau: number): void {
    if (
      (indexPlateau || indexPlateau === 0) &&
      this.commandeClient &&
      this.commandeClient.plateauRepas &&
      this.commandeClient.plateauRepas[indexPlateau]
    ) {
      this.commandeClient.plateauRepas[indexPlateau].boissonId = undefined;
      this.save();
    }
  }

  /**
   * Ouvre la modal de login pour que l'utilisateur se connecte
   */
  login(): void {
    this.loginModalService.open();
  }

  /**
   * Supprimer le plateau repas de la commande en cours
   * @param index Index de ce plateau repas
   */
  removePlateau(index: number): void {
    this.commandeClient?.plateauRepas?.splice(index, 1);
    if (this.commandeClient?.plateauRepas?.length === 0) {
      this.commandeClient.plateauRepas = [new PlateauRepas({ recettes: [] })];
    }
    if (this.keyCustomerCommand && this.commandeClient) {
      this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
    }
  }

  onTabClose(): void {
    this.indexSelectedPlateau.emit(0);
    this.activeState[0] = true;
  }

  /**
   * A l'ouverture de l'accordéon on sélectionne son index
   * @param event Index de l'accordéon
   */
  onTabOpen(event: any): void {
    this.indexSelectedPlateau.emit(event.index);
  }

  /**
   * Calcul le prix pour un recette
   * @param recetteId Identifiant de cette recette
   */
  searchPrice(recetteId: number | undefined): number {
    let prix = 0;
    const recetteA = this.recettesList.find(recette => recette.id === recetteId);
    if (recetteA && recetteA.produits) {
      // @ts-ignore
      recetteA.produits.forEach(produit => {
        if (produit.prixCatalogue) {
          prix += produit.prixCatalogue;
          if (recetteId) {
            return prix;
          }
        }
      });
    }
    return prix;
  }

  /**
   * Calcul le prix du plateau repas
   * @param indexPlateau Position du plateau dans la liste
   */
  searchPlateauPrice(indexPlateau: number): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[indexPlateau]) {
      const plateauA = this.commandeClient?.plateauRepas[indexPlateau];
      if (plateauA) {
        plateauA.recettes?.forEach(recette => {
          if (recette.produits) {
            recette.produits.forEach(produit => {
              if (produit.prixCatalogue) {
                prix += produit.prixCatalogue;
              }
            });
          }
        });
        prix += this.searchBoissonPrice(indexPlateau);
      }
    }
    return prix;
  }

  /**
   * Calcul le prix de la boisson d'un plateau repas
   * @param indexPlateau Position du plateau dans la liste
   */
  searchBoissonPrice(indexPlateau: number): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas[indexPlateau]) {
      const boissonId = this.commandeClient.plateauRepas[indexPlateau].boissonId;
      if (boissonId) {
        const boissonA = this.boissonsList.filter(boisson => boisson.id === boissonId)[0];
        if (boissonA && boissonA.prixCatalogue) {
          prix = boissonA.prixCatalogue;
        }
      }
    }
    return prix;
  }

  /**
   * Calcul le prix total de la commande
   */
  searchTotalPrice(): number {
    let prix = 0;
    if (this.commandeClient && this.commandeClient.plateauRepas && this.commandeClient.plateauRepas) {
      this.commandeClient.plateauRepas.forEach(plateauA => {
        if (plateauA && plateauA.recettes) {
          plateauA.recettes.forEach(recetteA => {
            if (recetteA && recetteA.produits) {
              recetteA.produits.forEach(produitA => {
                if (produitA && produitA.prixCatalogue) {
                  prix += produitA.prixCatalogue;
                }
              });
            }
          });
        }
        if (plateauA.boissonId) {
          const boissonA = this.boissonsList.filter(boisson => boisson.id === plateauA.boissonId)[0];
          if (boissonA && boissonA.prixCatalogue) {
            prix += boissonA.prixCatalogue;
          }
        }
      });
    }
    return prix;
  }

  updateForm(): void {
    if (this.accountService.isAuthenticated()) {
      this.accountService.getAuthenticationState().subscribe(account => {
        if (account && account.login) {
          this.userService.find(account?.login).subscribe(user => {
            this.tunnelService.set(
              this.keyPersonnePhysique,
              new PersonnePhysique({
                userId: user.id,
                email: user.email,
                phone: user.mobilePhone,
                nom: user.lastName,
              })
            );
          });
        }
      });
    } else if (this.keyPersonnePhysique) {
      this.tunnelService.set(
        this.keyPersonnePhysique,
        new PersonnePhysique({
          email: this.contactForm.get(['email'])?.value,
          phone: this.contactForm.get(['phone'])?.value,
          nom: this.contactForm.get(['name'])?.value,
        })
      );
    }
  }

  /**
   * Ajoute un plateau repas à la commande
   */
  addPlateau(): void {
    this.commandeClient?.plateauRepas?.push(new PlateauRepas({ recettes: [] }));
    if (this.commandeClient && this.keyCustomerCommand) {
      this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
    }
  }

  /**
   * Détermine si on peut ajouter ou non un nouveau plateau repas
   * Si le dernier plateau repas ne comporte aucune recette/ou boisson alors on ne peut pas en ajouter
   */
  showAddBtn(): boolean {
    return !(
      this.commandeClient &&
      this.commandeClient.plateauRepas &&
      this.commandeClient.plateauRepas[this.commandeClient.plateauRepas.length - 1] &&
      this.commandeClient.plateauRepas[this.commandeClient.plateauRepas.length - 1].recettes &&
      this.commandeClient.plateauRepas[this.commandeClient.plateauRepas.length - 1].recettes?.length === 0
    );
  }

  /**
   * Les différentes étapes de la validation de la commande du client
   */
  validCommand(): void {
    if (this.keyPersonnePhysique && this.contactForm.get('consent')?.value) {
      this.updateForm();

      const personne = this.tunnelService.get(this.keyPersonnePhysique) as IPersonnePhysique;
      this.personnePhysiqueService.create(personne).subscribe(personnePhysique => {
        if (personnePhysique.body && personnePhysique.body.id && this.keyClient) {
          const clientTaMere = this.tunnelService.get(this.keyClient) as IClient;
          clientTaMere.personnePhysiqueId = personnePhysique.body.id;
          clientTaMere.personneType = PersonneType.PHYSIQUE;
          this.tunnelService.set(this.keyPersonnePhysique, personnePhysique.body);
          this.clientService.create(clientTaMere).subscribe(client => {
            if (!!client && !!client.body && !!this.keyCommand && !!this.commande) {
              this.commande.idClient = client.body.id;
              this.commande.datePaiement = Date.now().toString();
              this.commande.dateStatut = Date.now().toString();
              this.commande.prixTotal = this.searchTotalPrice();
              this.tunnelService.set(this.keyClient, client.body);
              this.commandeService.create(this.commande).subscribe(commande => {
                if (!!commande && !!commande.body && !!this.commandeClient && !!client && !!client.body) {
                  this.commandeClient.clientId = client.body.id;
                  this.commandeClient.commandeId = commande.body.id;
                  this.tunnelService.set(this.keyCommand, commande.body);
                  this.commandeClientService.create(this.commandeClient).subscribe(commandeClient => {
                    if (!!this.commandeClient && !!this.commandeClient.plateauRepas && !!commandeClient && !!commandeClient.body) {
                      this.commandeClient.id = commandeClient.body.id;
                      this.tunnelService.set(this.keyCustomerCommand, this.commandeClient);
                      this.commandeClient.plateauRepas.forEach((plateauTaMere, index) => {
                        if (!!commandeClient && !!commandeClient.body && !!commandeClient.body.id) {
                          plateauTaMere.prixHT = this.searchPlateauPrice(index);
                          plateauTaMere.commandeClientId = commandeClient.body.id;
                          this.plateauRepasService.create(plateauTaMere).subscribe(plateau => {
                            if (plateau && plateau.ok) {
                              this.messageService.add({ severity: 'success', summary: 'Commande passée', detail: 'Bonne dégustation !' });
                              /* Router vers la page de récap */
                              if (!!commande && !!commande.body && !!commande.body.id) {
                                this.router.navigate([`${url}recapitulatif`]);
                              }
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  }

  /**
   * Simule le parcours pour le payement via TPE System ou payement à la réception
   */
  nextStep(): void {
    if (!this.contactForm.get('isLiquid')?.value) {
      this.displayTPE = true;
      setTimeout(() => {
        this.displayTPE = false;
        this.messageService.add({ severity: 'success', summary: 'Payement validé', detail: 'Merci !' });
        if (this.commande && this.commande.statut) {
          this.commande.statut = Statut.PAYEE;
          this.validCommand();
        }
      }, 5000);
    } else {
      if (this.commande && this.commande.statut) {
        this.commande.statut = Statut.COMPLETE;
        this.validCommand();
      }
    }
  }
}
