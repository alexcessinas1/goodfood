import { AfterViewInit, Component, Input, OnChanges, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { MessageService } from 'primeng/api';
import { goodfoodIcons } from 'app/core/icons/font-awesome-icons';
import { GMap } from 'primeng/gmap';

export type typeEvent = {
  overlay: {
    getTitle: (() => any) | undefined;
    getPosition: () => any;
  };
  map: {
    setCenter: (arg0: any) => void;
  };
};

export type Options = { center: { lat: number; lng: number }; zoom: number };

@Component({
  selector: 'gf-maps',
  templateUrl: './maps.component.html',
})
export class MapsComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() overlays: any[] = [];
  @Input() options!: { center: { lat: number; lng: number }; zoom: number };
  @ViewChild('gmap') gmap?: GMap;

  icon = goodfoodIcons;

  // @ts-ignore
  markerTitle: string;
  infoWindow: any;
  map?: google.maps.Map;

  constructor(private messageService: MessageService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options && this.map) {
      const currentOptions: Options = changes.options.currentValue;
      const previousOptions: Options = changes.options.previousValue;
      if (currentOptions.zoom !== previousOptions.zoom) {
        this.map.setZoom(currentOptions.zoom);
      }
      if (currentOptions.center.lat !== previousOptions.center.lat || currentOptions.center.lng !== previousOptions.center.lng) {
        this.map.setCenter(currentOptions.center);
      }
      if (changes.overlays) {
        this.overlays.forEach(overlay => {
          overlay.setMap(this.map);
        });
      }
    }
  }

  ngOnInit(): void {
    this.infoWindow = new google.maps.InfoWindow();
  }

  ngAfterViewInit(): void {
    this.map = this.gmap?.getMap();
  }

  handleOverlayClick(event: any): void {
    const isMarker = event.overlay.getTitle() !== undefined;

    if (isMarker) {
      // @ts-ignore
      const title = event.overlay.getTitle();
      this.infoWindow.setContent('' + title + '');
      this.infoWindow.open(event.map, event.overlay);
      event.map.setCenter(event.overlay.getPosition());
      event.map.setZoom(12);

      this.messageService.add({ severity: 'info', detail: title });
    } else {
      this.messageService.add({ severity: 'info', detail: '' });
    }
  }

  zoomIn(): void {
    if (this.map) {
      this.map.setZoom(this.map.getZoom() + 1);
    }
  }

  zoomOut(): void {
    if (this.map) {
      this.map.setZoom(this.map.getZoom() - 1);
    }
  }
}
