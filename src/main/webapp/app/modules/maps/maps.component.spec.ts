import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { GMapModule } from 'primeng/gmap';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { setupGoogleMock } from '../../../../../test/javascript/jest-global-mocks';
import { MapsComponent } from 'app/modules/maps/maps.component';

describe('Component Tests', () => {
  describe('Maps Component', () => {
    let spectator: Spectator<MapsComponent>;
    // @ts-ignore
    const createComponent = createComponentFactory({
      component: MapsComponent,
      imports: [ToastModule, GMapModule, FontAwesomeTestingModule, HttpClientTestingModule],
      providers: [MessageService],
    });

    // in test file.
    beforeAll(() => {
      setupGoogleMock();
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    xit('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });

    xit('should click on mark', () => {
      spectator.component.zoomOut();
      const spy = spyOn(spectator.inject(MessageService), 'add').and.callThrough();
      const event = { overlay: { getTitle: () => 'LALA', getPosition: () => void {} }, map: { setCenter: () => void {} } };
      spectator.component.infoWindow = { setContent: () => 'LALA', open: () => void {} };
      spectator.component.handleOverlayClick(event);
      expect(spectator.component).toBeDefined();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    xit('should click on not mark', () => {
      const map = { setZoom: () => void {}, getZoom: () => void {} };
      spectator.component.zoomIn();
      const spy = spyOn(spectator.inject(MessageService), 'add').and.callThrough();
      const event = { overlay: { getTitle: () => undefined, getPosition: () => void {} }, map: { setCenter: () => void {} } };
      spectator.component.infoWindow = { setContent: () => 'LALA', open: () => void {} };
      spectator.component.handleOverlayClick(event);
      expect(spectator.component).toBeDefined();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
