import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { CarouselComponent } from 'app/modules/carousel/carousel.component';
import { CarouselModule } from 'primeng/carousel';

describe('Component Tests', () => {
  describe('Carousel Component', () => {
    let spectator: Spectator<CarouselComponent>;
    const createComponent = createComponentFactory({
      component: CarouselComponent,
      imports: [CarouselModule],
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    it('should initialize', () => {
      expect(spectator.component).toBeDefined();
    });
  });
});
