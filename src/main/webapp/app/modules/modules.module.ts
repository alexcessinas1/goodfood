import { NgModule } from '@angular/core';
import { CarouselComponent } from 'app/modules/carousel/carousel.component';
import { CarouselModule } from 'primeng/carousel';
import { MapsComponent } from 'app/modules/maps/maps.component';
import { GMapModule } from 'primeng/gmap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PanierComponent } from 'app/modules/panier/panier.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'primeng/accordion';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { CaptchaModule } from 'primeng/captcha';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [
    CarouselModule,
    GMapModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    AccordionModule,
    TooltipModule,
    ConfirmDialogModule,
    DialogModule,
    CaptchaModule,
    InputTextModule,
  ],
  declarations: [CarouselComponent, MapsComponent, PanierComponent],
  entryComponents: [],
  providers: [],
  exports: [CarouselComponent, MapsComponent, PanierComponent],
})
export class GoodFoodModulesModule {}
