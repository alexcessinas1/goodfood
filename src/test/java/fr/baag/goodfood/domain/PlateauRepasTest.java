package fr.baag.goodfood.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class PlateauRepasTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlateauRepas.class);
        PlateauRepas plateauRepas1 = new PlateauRepas();
        plateauRepas1.setId(1L);
        PlateauRepas plateauRepas2 = new PlateauRepas();
        plateauRepas2.setId(plateauRepas1.getId());
        assertThat(plateauRepas1).isEqualTo(plateauRepas2);
        plateauRepas2.setId(2L);
        assertThat(plateauRepas1).isNotEqualTo(plateauRepas2);
        plateauRepas1.setId(null);
        assertThat(plateauRepas1).isNotEqualTo(plateauRepas2);
    }
}
