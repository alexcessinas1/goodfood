package fr.baag.goodfood.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieBoissonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieBoisson.class);
        CategorieBoisson categorieBoisson1 = new CategorieBoisson();
        categorieBoisson1.setId(1L);
        CategorieBoisson categorieBoisson2 = new CategorieBoisson();
        categorieBoisson2.setId(categorieBoisson1.getId());
        assertThat(categorieBoisson1).isEqualTo(categorieBoisson2);
        categorieBoisson2.setId(2L);
        assertThat(categorieBoisson1).isNotEqualTo(categorieBoisson2);
        categorieBoisson1.setId(null);
        assertThat(categorieBoisson1).isNotEqualTo(categorieBoisson2);
    }
}
