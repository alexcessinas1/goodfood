package fr.baag.goodfood.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieFournitureTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieFourniture.class);
        CategorieFourniture categorieFourniture1 = new CategorieFourniture();
        categorieFourniture1.setId(1L);
        CategorieFourniture categorieFourniture2 = new CategorieFourniture();
        categorieFourniture2.setId(categorieFourniture1.getId());
        assertThat(categorieFourniture1).isEqualTo(categorieFourniture2);
        categorieFourniture2.setId(2L);
        assertThat(categorieFourniture1).isNotEqualTo(categorieFourniture2);
        categorieFourniture1.setId(null);
        assertThat(categorieFourniture1).isNotEqualTo(categorieFourniture2);
    }
}
