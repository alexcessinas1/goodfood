package fr.baag.goodfood.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class AllergeneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Allergene.class);
        Allergene allergene1 = new Allergene();
        allergene1.setId(1L);
        Allergene allergene2 = new Allergene();
        allergene2.setId(allergene1.getId());
        assertThat(allergene1).isEqualTo(allergene2);
        allergene2.setId(2L);
        assertThat(allergene1).isNotEqualTo(allergene2);
        allergene1.setId(null);
        assertThat(allergene1).isNotEqualTo(allergene2);
    }
}
