package fr.baag.goodfood.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieAlimentationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieAlimentation.class);
        CategorieAlimentation categorieAlimentation1 = new CategorieAlimentation();
        categorieAlimentation1.setId(1L);
        CategorieAlimentation categorieAlimentation2 = new CategorieAlimentation();
        categorieAlimentation2.setId(categorieAlimentation1.getId());
        assertThat(categorieAlimentation1).isEqualTo(categorieAlimentation2);
        categorieAlimentation2.setId(2L);
        assertThat(categorieAlimentation1).isNotEqualTo(categorieAlimentation2);
        categorieAlimentation1.setId(null);
        assertThat(categorieAlimentation1).isNotEqualTo(categorieAlimentation2);
    }
}
