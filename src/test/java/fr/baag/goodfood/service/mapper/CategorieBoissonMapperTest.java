package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CategorieBoissonMapperTest {

    private CategorieBoissonMapper categorieBoissonMapper;

    @BeforeEach
    public void setUp() {
        categorieBoissonMapper = new CategorieBoissonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(categorieBoissonMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(categorieBoissonMapper.fromId(null)).isNull();
    }
}
