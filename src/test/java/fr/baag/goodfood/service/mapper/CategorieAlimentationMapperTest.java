package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CategorieAlimentationMapperTest {

    private CategorieAlimentationMapper categorieAlimentationMapper;

    @BeforeEach
    public void setUp() {
        categorieAlimentationMapper = new CategorieAlimentationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(categorieAlimentationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(categorieAlimentationMapper.fromId(null)).isNull();
    }
}
