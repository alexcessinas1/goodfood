package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RecetteMapperTest {

    private RecetteMapper recetteMapper;

    @BeforeEach
    public void setUp() {
        recetteMapper = new RecetteMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(recetteMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(recetteMapper.fromId(null)).isNull();
    }
}
