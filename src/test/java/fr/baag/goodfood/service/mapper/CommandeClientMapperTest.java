package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CommandeClientMapperTest {

    private CommandeClientMapper commandeClientMapper;

    @BeforeEach
    public void setUp() {
        commandeClientMapper = new CommandeClientMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(commandeClientMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(commandeClientMapper.fromId(null)).isNull();
    }
}
