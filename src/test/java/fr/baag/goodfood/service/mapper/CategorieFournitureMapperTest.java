package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CategorieFournitureMapperTest {

    private CategorieFournitureMapper categorieFournitureMapper;

    @BeforeEach
    public void setUp() {
        categorieFournitureMapper = new CategorieFournitureMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(categorieFournitureMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(categorieFournitureMapper.fromId(null)).isNull();
    }
}
