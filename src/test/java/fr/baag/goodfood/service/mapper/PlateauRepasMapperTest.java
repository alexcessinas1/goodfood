package fr.baag.goodfood.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PlateauRepasMapperTest {

    private PlateauRepasMapper plateauRepasMapper;

    @BeforeEach
    public void setUp() {
        plateauRepasMapper = new PlateauRepasMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(plateauRepasMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(plateauRepasMapper.fromId(null)).isNull();
    }
}
