package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieFournitureDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieFournitureDTO.class);
        CategorieFournitureDTO categorieFournitureDTO1 = new CategorieFournitureDTO();
        categorieFournitureDTO1.setId(1L);
        CategorieFournitureDTO categorieFournitureDTO2 = new CategorieFournitureDTO();
        assertThat(categorieFournitureDTO1).isNotEqualTo(categorieFournitureDTO2);
        categorieFournitureDTO2.setId(categorieFournitureDTO1.getId());
        assertThat(categorieFournitureDTO1).isEqualTo(categorieFournitureDTO2);
        categorieFournitureDTO2.setId(2L);
        assertThat(categorieFournitureDTO1).isNotEqualTo(categorieFournitureDTO2);
        categorieFournitureDTO1.setId(null);
        assertThat(categorieFournitureDTO1).isNotEqualTo(categorieFournitureDTO2);
    }
}
