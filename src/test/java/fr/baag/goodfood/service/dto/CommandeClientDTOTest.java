package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CommandeClientDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommandeClientDTO.class);
        CommandeClientDTO commandeClientDTO1 = new CommandeClientDTO();
        commandeClientDTO1.setId(1L);
        CommandeClientDTO commandeClientDTO2 = new CommandeClientDTO();
        assertThat(commandeClientDTO1).isNotEqualTo(commandeClientDTO2);
        commandeClientDTO2.setId(commandeClientDTO1.getId());
        assertThat(commandeClientDTO1).isEqualTo(commandeClientDTO2);
        commandeClientDTO2.setId(2L);
        assertThat(commandeClientDTO1).isNotEqualTo(commandeClientDTO2);
        commandeClientDTO1.setId(null);
        assertThat(commandeClientDTO1).isNotEqualTo(commandeClientDTO2);
    }
}
