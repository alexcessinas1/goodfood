package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieAlimentationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieAlimentationDTO.class);
        CategorieAlimentationDTO categorieAlimentationDTO1 = new CategorieAlimentationDTO();
        categorieAlimentationDTO1.setId(1L);
        CategorieAlimentationDTO categorieAlimentationDTO2 = new CategorieAlimentationDTO();
        assertThat(categorieAlimentationDTO1).isNotEqualTo(categorieAlimentationDTO2);
        categorieAlimentationDTO2.setId(categorieAlimentationDTO1.getId());
        assertThat(categorieAlimentationDTO1).isEqualTo(categorieAlimentationDTO2);
        categorieAlimentationDTO2.setId(2L);
        assertThat(categorieAlimentationDTO1).isNotEqualTo(categorieAlimentationDTO2);
        categorieAlimentationDTO1.setId(null);
        assertThat(categorieAlimentationDTO1).isNotEqualTo(categorieAlimentationDTO2);
    }
}
