package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class PlateauRepasDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlateauRepasDTO.class);
        PlateauRepasDTO plateauRepasDTO1 = new PlateauRepasDTO();
        plateauRepasDTO1.setId(1L);
        PlateauRepasDTO plateauRepasDTO2 = new PlateauRepasDTO();
        assertThat(plateauRepasDTO1).isNotEqualTo(plateauRepasDTO2);
        plateauRepasDTO2.setId(plateauRepasDTO1.getId());
        assertThat(plateauRepasDTO1).isEqualTo(plateauRepasDTO2);
        plateauRepasDTO2.setId(2L);
        assertThat(plateauRepasDTO1).isNotEqualTo(plateauRepasDTO2);
        plateauRepasDTO1.setId(null);
        assertThat(plateauRepasDTO1).isNotEqualTo(plateauRepasDTO2);
    }
}
