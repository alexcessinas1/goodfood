package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;

import fr.baag.goodfood.domain.enumeration.AllergeneEnum;
import fr.baag.goodfood.web.rest.TestUtil;
import static org.assertj.core.api.Assertions.assertThat;

public class AllergeneDTOTest {
	@Test
	public void dtoEqualsVerifier() throws Exception {
		TestUtil.equalsVerifier(AllergeneDTO.class);
		AllergeneDTO allergeneDTO1 = new AllergeneDTO();
		allergeneDTO1.setId(1L);
		AllergeneDTO allergeneDTO2 = new AllergeneDTO();
		assertThat(allergeneDTO1).isNotEqualTo(allergeneDTO2);
		allergeneDTO2.setId(allergeneDTO1.getId());
		assertThat(allergeneDTO1).isEqualTo(allergeneDTO2);
		allergeneDTO2.setId(2L);
		assertThat(allergeneDTO1).isNotEqualTo(allergeneDTO2);
		allergeneDTO1.setId(null);
		assertThat(allergeneDTO1).isNotEqualTo(allergeneDTO2);
		allergeneDTO1.setNom(AllergeneEnum.ARACHIDE);
		assertThat(allergeneDTO1.getNom()).isEqualTo(AllergeneEnum.ARACHIDE);
	}
}
