package fr.baag.goodfood.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.baag.goodfood.web.rest.TestUtil;

public class CategorieBoissonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategorieBoissonDTO.class);
        CategorieBoissonDTO categorieBoissonDTO1 = new CategorieBoissonDTO();
        categorieBoissonDTO1.setId(1L);
        CategorieBoissonDTO categorieBoissonDTO2 = new CategorieBoissonDTO();
        assertThat(categorieBoissonDTO1).isNotEqualTo(categorieBoissonDTO2);
        categorieBoissonDTO2.setId(categorieBoissonDTO1.getId());
        assertThat(categorieBoissonDTO1).isEqualTo(categorieBoissonDTO2);
        categorieBoissonDTO2.setId(2L);
        assertThat(categorieBoissonDTO1).isNotEqualTo(categorieBoissonDTO2);
        categorieBoissonDTO1.setId(null);
        assertThat(categorieBoissonDTO1).isNotEqualTo(categorieBoissonDTO2);
    }
}
