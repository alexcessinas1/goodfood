package fr.baag.goodfood.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link FranchiseSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class FranchiseSearchRepositoryMockConfiguration {

    @MockBean
    private FranchiseSearchRepository mockFranchiseSearchRepository;

}
