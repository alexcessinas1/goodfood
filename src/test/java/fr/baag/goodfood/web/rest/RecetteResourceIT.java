package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Recette;
import fr.baag.goodfood.repository.RecetteRepository;
import fr.baag.goodfood.repository.search.RecetteSearchRepository;
import fr.baag.goodfood.service.RecetteService;
import fr.baag.goodfood.service.dto.RecetteDTO;
import fr.baag.goodfood.service.mapper.RecetteMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.baag.goodfood.domain.enumeration.Plat;

/**
 * Integration tests for the {@link RecetteResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class RecetteResourceIT {

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Plat DEFAULT_TYPE = Plat.ENTREE;
    private static final Plat UPDATED_TYPE = Plat.PLAT;

    @Autowired
    private RecetteRepository recetteRepository;

    @Autowired
    private RecetteMapper recetteMapper;

    @Autowired
    private RecetteService recetteService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.RecetteSearchRepositoryMockConfiguration
     */
    @Autowired
    private RecetteSearchRepository mockRecetteSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRecetteMockMvc;

    private Recette recette;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Recette createEntity(EntityManager em) {
        Recette recette = new Recette().titre(DEFAULT_TITRE).description(DEFAULT_DESCRIPTION).type(DEFAULT_TYPE);
        return recette;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Recette createUpdatedEntity(EntityManager em) {
        Recette recette = new Recette().titre(UPDATED_TITRE).description(UPDATED_DESCRIPTION).type(UPDATED_TYPE);
        return recette;
    }

    @BeforeEach
    public void initTest() {
        recette = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecette() throws Exception {
        int databaseSizeBeforeCreate = recetteRepository.findAll().size();
        // Create the Recette
        RecetteDTO recetteDTO = recetteMapper.toDto(recette);
        restRecetteMockMvc.perform(post("/api/recettes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recetteDTO))).andExpect(status().isCreated());

        // Validate the Recette in the database
        List<Recette> recetteList = recetteRepository.findAll();
        assertThat(recetteList).hasSize(databaseSizeBeforeCreate + 1);
        Recette testRecette = recetteList.get(recetteList.size() - 1);
        assertThat(testRecette.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testRecette.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRecette.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the Recette in Elasticsearch
        verify(mockRecetteSearchRepository, times(1)).save(testRecette);
    }

    @Test
    @Transactional
    public void createRecetteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recetteRepository.findAll().size();

        // Create the Recette with an existing ID
        recette.setId(1L);
        RecetteDTO recetteDTO = recetteMapper.toDto(recette);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecetteMockMvc.perform(post("/api/recettes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recetteDTO))).andExpect(status().isBadRequest());

        // Validate the Recette in the database
        List<Recette> recetteList = recetteRepository.findAll();
        assertThat(recetteList).hasSize(databaseSizeBeforeCreate);

        // Validate the Recette in Elasticsearch
        verify(mockRecetteSearchRepository, times(0)).save(recette);
    }

    @Test
    @Transactional
    public void getAllRecettes() throws Exception {
        // Initialize the database
        recetteRepository.saveAndFlush(recette);

        // Get all the recetteList
        restRecetteMockMvc.perform(get("/api/recettes?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(recette.getId().intValue())))
                .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getRecette() throws Exception {
        // Initialize the database
        recetteRepository.saveAndFlush(recette);

        // Get the recette
        restRecetteMockMvc.perform(get("/api/recettes/{id}", recette.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(recette.getId().intValue()))
                .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
                .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
                .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRecette() throws Exception {
        // Get the recette
        restRecetteMockMvc.perform(get("/api/recettes/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecette() throws Exception {
        // Initialize the database
        recetteRepository.saveAndFlush(recette);

        int databaseSizeBeforeUpdate = recetteRepository.findAll().size();

        // Update the recette
        Recette updatedRecette = recetteRepository.findById(recette.getId()).get();
        // Disconnect from session so that the updates on updatedRecette are not
        // directly saved in db
        em.detach(updatedRecette);
        updatedRecette.titre(UPDATED_TITRE).description(UPDATED_DESCRIPTION).type(UPDATED_TYPE);
        RecetteDTO recetteDTO = recetteMapper.toDto(updatedRecette);

        restRecetteMockMvc.perform(put("/api/recettes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recetteDTO))).andExpect(status().isOk());

        // Validate the Recette in the database
        List<Recette> recetteList = recetteRepository.findAll();
        assertThat(recetteList).hasSize(databaseSizeBeforeUpdate);
        Recette testRecette = recetteList.get(recetteList.size() - 1);
        assertThat(testRecette.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testRecette.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRecette.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the Recette in Elasticsearch
        verify(mockRecetteSearchRepository, times(1)).save(testRecette);
    }

    @Test
    @Transactional
    public void updateNonExistingRecette() throws Exception {
        int databaseSizeBeforeUpdate = recetteRepository.findAll().size();

        // Create the Recette
        RecetteDTO recetteDTO = recetteMapper.toDto(recette);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecetteMockMvc.perform(put("/api/recettes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(recetteDTO))).andExpect(status().isBadRequest());

        // Validate the Recette in the database
        List<Recette> recetteList = recetteRepository.findAll();
        assertThat(recetteList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Recette in Elasticsearch
        verify(mockRecetteSearchRepository, times(0)).save(recette);
    }

    @Test
    @Transactional
    public void deleteRecette() throws Exception {
        // Initialize the database
        recetteRepository.save(recette);

        int databaseSizeBeforeDelete = recetteRepository.findAll().size();

        // Delete the recette
        restRecetteMockMvc.perform(delete("/api/recettes/{id}", recette.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Recette> recetteList = recetteRepository.findAll();
        assertThat(recetteList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Recette in Elasticsearch
        verify(mockRecetteSearchRepository, times(1)).deleteById(recette.getId());
    }

    @Test
    @Transactional
    public void searchRecette() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        recetteRepository.save(recette);
        when(mockRecetteSearchRepository.search(queryStringQuery("id:" + recette.getId())))
                .thenReturn(Collections.singletonList(recette));
        // Search the recette
        restRecetteMockMvc.perform(get("/api/_search/recettes?query=id:" + recette.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(recette.getId().intValue())))
                .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
}
