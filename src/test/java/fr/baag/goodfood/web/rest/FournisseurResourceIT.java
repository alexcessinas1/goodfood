package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Fournisseur;
import fr.baag.goodfood.domain.Adresse;
import fr.baag.goodfood.repository.FournisseurRepository;
import fr.baag.goodfood.repository.search.FournisseurSearchRepository;
import fr.baag.goodfood.service.FournisseurService;
import fr.baag.goodfood.service.dto.FournisseurDTO;
import fr.baag.goodfood.service.mapper.FournisseurMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FournisseurResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class FournisseurResourceIT {

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_ENTREPRISE = "AAAAAAAAAA";
    private static final String UPDATED_TEL_ENTREPRISE = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_MAISON = "AAAAAAAAAA";
    private static final String UPDATED_TEL_MAISON = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_MOBILE = "AAAAAAAAAA";
    private static final String UPDATED_TEL_MOBILE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_FAX = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_PAGE_WEB = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_WEB = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    @Autowired
    private FournisseurRepository fournisseurRepository;

    @Autowired
    private FournisseurMapper fournisseurMapper;

    @Autowired
    private FournisseurService fournisseurService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.FournisseurSearchRepositoryMockConfiguration
     */
    @Autowired
    private FournisseurSearchRepository mockFournisseurSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFournisseurMockMvc;

    private Fournisseur fournisseur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Fournisseur createEntity(EntityManager em) {
        Fournisseur fournisseur = new Fournisseur().prenom(DEFAULT_PRENOM).nom(DEFAULT_NOM).email(DEFAULT_EMAIL)
                .titre(DEFAULT_TITRE).telEntreprise(DEFAULT_TEL_ENTREPRISE).telMaison(DEFAULT_TEL_MAISON)
                .telMobile(DEFAULT_TEL_MOBILE).numeroFax(DEFAULT_NUMERO_FAX).pageWeb(DEFAULT_PAGE_WEB)
                .notes(DEFAULT_NOTES);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        fournisseur.setAdresse(adresse);
        return fournisseur;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Fournisseur createUpdatedEntity(EntityManager em) {
        Fournisseur fournisseur = new Fournisseur().prenom(UPDATED_PRENOM).nom(UPDATED_NOM).email(UPDATED_EMAIL)
                .titre(UPDATED_TITRE).telEntreprise(UPDATED_TEL_ENTREPRISE).telMaison(UPDATED_TEL_MAISON)
                .telMobile(UPDATED_TEL_MOBILE).numeroFax(UPDATED_NUMERO_FAX).pageWeb(UPDATED_PAGE_WEB)
                .notes(UPDATED_NOTES);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createUpdatedEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        fournisseur.setAdresse(adresse);
        return fournisseur;
    }

    @BeforeEach
    public void initTest() {
        fournisseur = createEntity(em);
    }

    @Test
    @Transactional
    public void createFournisseur() throws Exception {
        int databaseSizeBeforeCreate = fournisseurRepository.findAll().size();
        // Create the Fournisseur
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);
        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isCreated());

        // Validate the Fournisseur in the database
        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeCreate + 1);
        Fournisseur testFournisseur = fournisseurList.get(fournisseurList.size() - 1);
        assertThat(testFournisseur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testFournisseur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testFournisseur.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testFournisseur.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testFournisseur.getTelEntreprise()).isEqualTo(DEFAULT_TEL_ENTREPRISE);
        assertThat(testFournisseur.getTelMaison()).isEqualTo(DEFAULT_TEL_MAISON);
        assertThat(testFournisseur.getTelMobile()).isEqualTo(DEFAULT_TEL_MOBILE);
        assertThat(testFournisseur.getNumeroFax()).isEqualTo(DEFAULT_NUMERO_FAX);
        assertThat(testFournisseur.getPageWeb()).isEqualTo(DEFAULT_PAGE_WEB);
        assertThat(testFournisseur.getNotes()).isEqualTo(DEFAULT_NOTES);

        // Validate the Fournisseur in Elasticsearch
        verify(mockFournisseurSearchRepository, times(1)).save(testFournisseur);
    }

    @Test
    @Transactional
    public void createFournisseurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fournisseurRepository.findAll().size();

        // Create the Fournisseur with an existing ID
        fournisseur.setId(1L);
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        // Validate the Fournisseur in the database
        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeCreate);

        // Validate the Fournisseur in Elasticsearch
        verify(mockFournisseurSearchRepository, times(0)).save(fournisseur);
    }

    @Test
    @Transactional
    public void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = fournisseurRepository.findAll().size();
        // set the field null
        fournisseur.setPrenom(null);

        // Create the Fournisseur, which fails.
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = fournisseurRepository.findAll().size();
        // set the field null
        fournisseur.setNom(null);

        // Create the Fournisseur, which fails.
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = fournisseurRepository.findAll().size();
        // set the field null
        fournisseur.setEmail(null);

        // Create the Fournisseur, which fails.
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelEntrepriseIsRequired() throws Exception {
        int databaseSizeBeforeTest = fournisseurRepository.findAll().size();
        // set the field null
        fournisseur.setTelEntreprise(null);

        // Create the Fournisseur, which fails.
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        restFournisseurMockMvc.perform(post("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFournisseurs() throws Exception {
        // Initialize the database
        fournisseur = fournisseurMapper.toEntity(fournisseurService.save(fournisseurMapper.toDto(fournisseur)));

        // Get all the fournisseurList
        restFournisseurMockMvc.perform(get("/api/fournisseurs?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(fournisseur.getId().intValue())))
                .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
                .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
                .andExpect(jsonPath("$.[*].telEntreprise").value(hasItem(DEFAULT_TEL_ENTREPRISE)))
                .andExpect(jsonPath("$.[*].telMaison").value(hasItem(DEFAULT_TEL_MAISON)))
                .andExpect(jsonPath("$.[*].telMobile").value(hasItem(DEFAULT_TEL_MOBILE)))
                .andExpect(jsonPath("$.[*].numeroFax").value(hasItem(DEFAULT_NUMERO_FAX)))
                .andExpect(jsonPath("$.[*].pageWeb").value(hasItem(DEFAULT_PAGE_WEB)))
                .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)));
    }

    @Test
    @Transactional
    public void getFournisseur() throws Exception {
        // Initialize the database
        // fournisseurRepository.saveAndFlush(fournisseur);
        fournisseur = fournisseurMapper.toEntity(fournisseurService.save(fournisseurMapper.toDto(fournisseur)));
        // Get the fournisseur
        restFournisseurMockMvc.perform(get("/api/fournisseurs/{id}", fournisseur.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(fournisseur.getId().intValue()))
                .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM)).andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
                .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL)).andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
                .andExpect(jsonPath("$.telEntreprise").value(DEFAULT_TEL_ENTREPRISE))
                .andExpect(jsonPath("$.telMaison").value(DEFAULT_TEL_MAISON))
                .andExpect(jsonPath("$.telMobile").value(DEFAULT_TEL_MOBILE))
                .andExpect(jsonPath("$.numeroFax").value(DEFAULT_NUMERO_FAX))
                .andExpect(jsonPath("$.pageWeb").value(DEFAULT_PAGE_WEB))
                .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES));
    }

    @Test
    @Transactional
    public void getNonExistingFournisseur() throws Exception {
        // Get the fournisseur
        restFournisseurMockMvc.perform(get("/api/fournisseurs/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFournisseur() throws Exception {
        // Initialize the database
        fournisseur = fournisseurMapper.toEntity(fournisseurService.save(fournisseurMapper.toDto(fournisseur)));

        int databaseSizeBeforeUpdate = fournisseurRepository.findAll().size();

        // Update the fournisseur
        Fournisseur updatedFournisseur = fournisseurRepository.findById(fournisseur.getId()).get();
        // Disconnect from session so that the updates on updatedFournisseur are not
        // directly saved in db
        em.detach(updatedFournisseur);
        updatedFournisseur.prenom(UPDATED_PRENOM).nom(UPDATED_NOM).email(UPDATED_EMAIL).titre(UPDATED_TITRE)
                .telEntreprise(UPDATED_TEL_ENTREPRISE).telMaison(UPDATED_TEL_MAISON).telMobile(UPDATED_TEL_MOBILE)
                .numeroFax(UPDATED_NUMERO_FAX).pageWeb(UPDATED_PAGE_WEB).notes(UPDATED_NOTES);
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(updatedFournisseur);

        restFournisseurMockMvc.perform(put("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isOk());

        // Validate the Fournisseur in the database
        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeUpdate);
        Fournisseur testFournisseur = fournisseurList.get(fournisseurList.size() - 1);
        assertThat(testFournisseur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testFournisseur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testFournisseur.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testFournisseur.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testFournisseur.getTelEntreprise()).isEqualTo(UPDATED_TEL_ENTREPRISE);
        assertThat(testFournisseur.getTelMaison()).isEqualTo(UPDATED_TEL_MAISON);
        assertThat(testFournisseur.getTelMobile()).isEqualTo(UPDATED_TEL_MOBILE);
        assertThat(testFournisseur.getNumeroFax()).isEqualTo(UPDATED_NUMERO_FAX);
        assertThat(testFournisseur.getPageWeb()).isEqualTo(UPDATED_PAGE_WEB);
        assertThat(testFournisseur.getNotes()).isEqualTo(UPDATED_NOTES);

        // Validate the Fournisseur in Elasticsearch
        verify(mockFournisseurSearchRepository, times(2)).save(testFournisseur);
    }

    @Test
    @Transactional
    public void updateNonExistingFournisseur() throws Exception {
        int databaseSizeBeforeUpdate = fournisseurRepository.findAll().size();

        // Create the Fournisseur
        FournisseurDTO fournisseurDTO = fournisseurMapper.toDto(fournisseur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFournisseurMockMvc.perform(put("/api/fournisseurs").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(fournisseurDTO))).andExpect(status().isBadRequest());

        // Validate the Fournisseur in the database
        List<Fournisseur> fournisseurList = fournisseurRepository.findAll();
        assertThat(fournisseurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Fournisseur in Elasticsearch
        verify(mockFournisseurSearchRepository, times(0)).save(fournisseur);
    }

    @Test
    @Transactional
    public void deleteFournisseur() throws Exception {
        // Initialize the database
        fournisseur = fournisseurMapper.toEntity(fournisseurService.save(fournisseurMapper.toDto(fournisseur)));
        int databaseSizeBeforeDelete = fournisseurService.findAll(Pageable.unpaged()).toList().stream()
                .map(fdto -> fournisseurMapper.toEntity(fdto)).collect(Collectors.toList()).size();

        // Delete the fournisseur
        restFournisseurMockMvc
                .perform(delete("/api/fournisseurs/{id}", fournisseur.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fournisseur> fournisseurList = fournisseurService.findAll(Pageable.unpaged()).toList().stream()
                .map(fdto -> fournisseurMapper.toEntity(fdto)).collect(Collectors.toList());
        assertThat(fournisseurList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Fournisseur in Elasticsearch
        verify(mockFournisseurSearchRepository, times(1)).deleteById(fournisseur.getId());
    }

    @Test
    @Transactional
    public void searchFournisseur() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        fournisseur = fournisseurMapper.toEntity(fournisseurService.save(fournisseurMapper.toDto(fournisseur)));
        when(mockFournisseurSearchRepository.search(queryStringQuery("id:" + fournisseur.getId()),
                PageRequest.of(0, 20)))
                        .thenReturn(new PageImpl<>(Collections.singletonList(fournisseur), PageRequest.of(0, 1), 1));

        // Search the fournisseur
        restFournisseurMockMvc.perform(get("/api/_search/fournisseurs?query=id:" + fournisseur.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(fournisseur.getId().intValue())))
                .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
                .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
                .andExpect(jsonPath("$.[*].telEntreprise").value(hasItem(DEFAULT_TEL_ENTREPRISE)))
                .andExpect(jsonPath("$.[*].telMaison").value(hasItem(DEFAULT_TEL_MAISON)))
                .andExpect(jsonPath("$.[*].telMobile").value(hasItem(DEFAULT_TEL_MOBILE)))
                .andExpect(jsonPath("$.[*].numeroFax").value(hasItem(DEFAULT_NUMERO_FAX)))
                .andExpect(jsonPath("$.[*].pageWeb").value(hasItem(DEFAULT_PAGE_WEB)))
                .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)));
    }
}
