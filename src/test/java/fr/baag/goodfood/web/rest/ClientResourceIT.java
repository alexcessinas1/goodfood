package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Client;
import fr.baag.goodfood.domain.Adresse;
import fr.baag.goodfood.domain.PersonnePhysique;
import fr.baag.goodfood.domain.PersonneMorale;
import fr.baag.goodfood.repository.ClientRepository;
import fr.baag.goodfood.repository.search.ClientSearchRepository;
import fr.baag.goodfood.service.ClientService;
import fr.baag.goodfood.service.dto.ClientDTO;
import fr.baag.goodfood.service.mapper.ClientMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.baag.goodfood.domain.enumeration.PersonneType;
/**
 * Integration tests for the {@link ClientResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ClientResourceIT {

    private static final PersonneType DEFAULT_PERSONNE_TYPE = PersonneType.PHYSIQUE;
    private static final PersonneType UPDATED_PERSONNE_TYPE = PersonneType.MORALE;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private ClientService clientService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test package.
     *
     * @see fr.baag.goodfood.repository.search.ClientSearchRepositoryMockConfiguration
     */
    @Autowired
    private ClientSearchRepository mockClientSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClientMockMvc;

    private Client client;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Client createEntity(EntityManager em) {
        Client client = new Client()
            .personneType(DEFAULT_PERSONNE_TYPE);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        client.setAdresseLivraison(adresse);
        // Add required entity
        client.setAdresseFacturation(adresse);
        // Add required entity
        PersonnePhysique personnePhysique;
        if (TestUtil.findAll(em, PersonnePhysique.class).isEmpty()) {
            personnePhysique = PersonnePhysiqueResourceIT.createEntity(em);
            em.persist(personnePhysique);
            em.flush();
        } else {
            personnePhysique = TestUtil.findAll(em, PersonnePhysique.class).get(0);
        }
        client.setPersonnePhysique(personnePhysique);
        // Add required entity
        PersonneMorale personneMorale;
        if (TestUtil.findAll(em, PersonneMorale.class).isEmpty()) {
            personneMorale = PersonneMoraleResourceIT.createEntity(em);
            em.persist(personneMorale);
            em.flush();
        } else {
            personneMorale = TestUtil.findAll(em, PersonneMorale.class).get(0);
        }
        client.setPersonneMorale(personneMorale);
        return client;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Client createUpdatedEntity(EntityManager em) {
        Client client = new Client()
            .personneType(UPDATED_PERSONNE_TYPE);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createUpdatedEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        client.setAdresseLivraison(adresse);
        // Add required entity
        client.setAdresseFacturation(adresse);
        // Add required entity
        PersonnePhysique personnePhysique;
        if (TestUtil.findAll(em, PersonnePhysique.class).isEmpty()) {
            personnePhysique = PersonnePhysiqueResourceIT.createUpdatedEntity(em);
            em.persist(personnePhysique);
            em.flush();
        } else {
            personnePhysique = TestUtil.findAll(em, PersonnePhysique.class).get(0);
        }
        client.setPersonnePhysique(personnePhysique);
        // Add required entity
        PersonneMorale personneMorale;
        if (TestUtil.findAll(em, PersonneMorale.class).isEmpty()) {
            personneMorale = PersonneMoraleResourceIT.createUpdatedEntity(em);
            em.persist(personneMorale);
            em.flush();
        } else {
            personneMorale = TestUtil.findAll(em, PersonneMorale.class).get(0);
        }
        client.setPersonneMorale(personneMorale);
        return client;
    }

    @BeforeEach
    public void initTest() {
        client = createEntity(em);
    }

    @Test
    @Transactional
    public void createClient() throws Exception {
        int databaseSizeBeforeCreate = clientRepository.findAll().size();
        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);
        restClientMockMvc.perform(post("/api/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isCreated());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate + 1);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getPersonneType()).isEqualTo(DEFAULT_PERSONNE_TYPE);

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(1)).save(testClient);
    }

    @Test
    @Transactional
    public void createClientWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientRepository.findAll().size();

        // Create the Client with an existing ID
        client.setId(1L);
        ClientDTO clientDTO = clientMapper.toDto(client);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientMockMvc.perform(post("/api/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate);

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(0)).save(client);
    }

    @Test
    @Transactional
    public void updateClientMapsIdAssociationWithNewId() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);
        int databaseSizeBeforeCreate = clientRepository.findAll().size();

        // Add a new parent entity
        Adresse adresse = AdresseResourceIT.createUpdatedEntity(em);
        em.persist(adresse);
        em.flush();

        // Load the client
        Client updatedClient = clientRepository.findById(client.getId()).get();
        // Disconnect from session so that the updates on updatedClient are not directly saved in db
        em.detach(updatedClient);

        // Update the Adresse with new association value
        updatedClient.setAdresseLivraison(adresse);
        ClientDTO updatedClientDTO = clientMapper.toDto(updatedClient);

        // Update the entity
        restClientMockMvc.perform(put("/api/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientDTO)))
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeCreate);
        Client testClient = clientList.get(clientList.size() - 1);

        // Validate the id for MapsId, the ids must be same
        // Uncomment the following line for assertion. However, please note that there is a known issue and uncommenting will fail the test.
        // Please look at https://github.com/jhipster/generator-jhipster/issues/9100. You can modify this test as necessary.
        // assertThat(testClient.getId()).isEqualTo(testClient.getAdresse().getId());

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(1)).save(client);
    }

    @Test
    @Transactional
    public void getAllClients() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get all the clientList
        restClientMockMvc.perform(get("/api/clients?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].personneType").value(hasItem(DEFAULT_PERSONNE_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        // Get the client
        restClientMockMvc.perform(get("/api/clients/{id}", client.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(client.getId().intValue()))
            .andExpect(jsonPath("$.personneType").value(DEFAULT_PERSONNE_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingClient() throws Exception {
        // Get the client
        restClientMockMvc.perform(get("/api/clients/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Update the client
        Client updatedClient = clientRepository.findById(client.getId()).get();
        // Disconnect from session so that the updates on updatedClient are not directly saved in db
        em.detach(updatedClient);
        updatedClient
            .personneType(UPDATED_PERSONNE_TYPE);
        ClientDTO clientDTO = clientMapper.toDto(updatedClient);

        restClientMockMvc.perform(put("/api/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isOk());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);
        Client testClient = clientList.get(clientList.size() - 1);
        assertThat(testClient.getPersonneType()).isEqualTo(UPDATED_PERSONNE_TYPE);

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(1)).save(testClient);
    }

    @Test
    @Transactional
    public void updateNonExistingClient() throws Exception {
        int databaseSizeBeforeUpdate = clientRepository.findAll().size();

        // Create the Client
        ClientDTO clientDTO = clientMapper.toDto(client);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientMockMvc.perform(put("/api/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clientDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Client in the database
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(0)).save(client);
    }

    @Test
    @Transactional
    public void deleteClient() throws Exception {
        // Initialize the database
        clientRepository.saveAndFlush(client);

        int databaseSizeBeforeDelete = clientRepository.findAll().size();

        // Delete the client
        restClientMockMvc.perform(delete("/api/clients/{id}", client.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Client> clientList = clientRepository.findAll();
        assertThat(clientList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Client in Elasticsearch
        verify(mockClientSearchRepository, times(1)).deleteById(client.getId());
    }

    @Test
    @Transactional
    public void searchClient() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        clientRepository.saveAndFlush(client);
        when(mockClientSearchRepository.search(queryStringQuery("id:" + client.getId())))
            .thenReturn(Collections.singletonList(client));

        // Search the client
        restClientMockMvc.perform(get("/api/_search/clients?query=id:" + client.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(client.getId().intValue())))
            .andExpect(jsonPath("$.[*].personneType").value(hasItem(DEFAULT_PERSONNE_TYPE.toString())));
    }
}
