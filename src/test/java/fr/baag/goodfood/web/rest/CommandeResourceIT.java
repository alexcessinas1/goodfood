package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Commande;
import fr.baag.goodfood.domain.Client;
import fr.baag.goodfood.domain.Franchise;
import fr.baag.goodfood.domain.Fournisseur;
import fr.baag.goodfood.repository.CommandeRepository;
import fr.baag.goodfood.repository.search.CommandeSearchRepository;
import fr.baag.goodfood.service.CommandeService;
import fr.baag.goodfood.service.dto.CommandeDTO;
import fr.baag.goodfood.service.mapper.CommandeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.baag.goodfood.domain.enumeration.Statut;

/**
 * Integration tests for the {@link CommandeResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CommandeResourceIT {

    private static final Float DEFAULT_PRIX_TOTAL = 1F;
    private static final Float UPDATED_PRIX_TOTAL = 2F;

    private static final String DEFAULT_DATE_COMMANDE = "AAAAAAAAAA";
    private static final String UPDATED_DATE_COMMANDE = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_STATUT = "AAAAAAAAAA";
    private static final String UPDATED_DATE_STATUT = "BBBBBBBBBB";

    private static final Statut DEFAULT_STATUT = Statut.EN_COURS;
    private static final Statut UPDATED_STATUT = Statut.EN_PREPARATION;

    private static final Boolean DEFAULT_A_LIVRER = false;
    private static final Boolean UPDATED_A_LIVRER = true;

    private static final String DEFAULT_DATE_PAIEMENT = "AAAAAAAAAA";
    private static final String UPDATED_DATE_PAIEMENT = "BBBBBBBBBB";

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private CommandeMapper commandeMapper;

    @Autowired
    private CommandeService commandeService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.CommandeSearchRepositoryMockConfiguration
     */
    @Autowired
    private CommandeSearchRepository mockCommandeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommandeMockMvc;

    private Commande commande;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Commande createEntity(EntityManager em) {
        Commande commande = new Commande().prixTotal(DEFAULT_PRIX_TOTAL).dateCommande(DEFAULT_DATE_COMMANDE)
                .dateStatut(DEFAULT_DATE_STATUT).statut(DEFAULT_STATUT).aLivrer(DEFAULT_A_LIVRER)
                .datePaiement(DEFAULT_DATE_PAIEMENT);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createEntity(em);
            // em.persist(client);
            // em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        commande.setIdClient(client);
        // Add required entity
        Franchise franchise;
        if (TestUtil.findAll(em, Franchise.class).isEmpty()) {
            franchise = FranchiseResourceIT.createEntity(em);
            // em.persist(franchise);
            // em.flush();
        } else {
            franchise = TestUtil.findAll(em, Franchise.class).get(0);
        }
        commande.setIdFranchise(franchise);
        // Add required entity
        Fournisseur fournisseur;
        if (TestUtil.findAll(em, Fournisseur.class).isEmpty()) {
            fournisseur = FournisseurResourceIT.createEntity(em);
            // em.persist(fournisseur);
            // em.flush();
        } else {
            fournisseur = TestUtil.findAll(em, Fournisseur.class).get(0);
        }
        commande.setIdFournisseur(fournisseur);
        return commande;
    }

    public static Commande createEntitySansClient(EntityManager em) {
        Commande commande = new Commande().prixTotal(DEFAULT_PRIX_TOTAL).dateCommande(DEFAULT_DATE_COMMANDE)
                .dateStatut(DEFAULT_DATE_STATUT).statut(DEFAULT_STATUT).aLivrer(DEFAULT_A_LIVRER)
                .datePaiement(DEFAULT_DATE_PAIEMENT);
        // Add required entity
        Franchise franchise;
        if (TestUtil.findAll(em, Franchise.class).isEmpty()) {
            franchise = FranchiseResourceIT.createEntitySansProduit(em);
            em.persist(franchise);
            em.flush();
        } else {
            franchise = TestUtil.findAll(em, Franchise.class).get(0);
        }
        commande.setIdFranchise(franchise);
        return commande;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Commande createUpdatedEntity(EntityManager em) {
        Commande commande = new Commande().prixTotal(UPDATED_PRIX_TOTAL).dateCommande(UPDATED_DATE_COMMANDE)
                .dateStatut(UPDATED_DATE_STATUT).statut(UPDATED_STATUT).aLivrer(UPDATED_A_LIVRER)
                .datePaiement(UPDATED_DATE_PAIEMENT);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createUpdatedEntity(em);
            // em.persist(client);
            // em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        commande.setIdClient(client);
        // Add required entity
        Franchise franchise;
        if (TestUtil.findAll(em, Franchise.class).isEmpty()) {
            franchise = FranchiseResourceIT.createUpdatedEntity(em);
            // em.persist(franchise);
            // em.flush();
        } else {
            franchise = TestUtil.findAll(em, Franchise.class).get(0);
        }
        commande.setIdFranchise(franchise);
        // Add required entity
        Fournisseur fournisseur;
        if (TestUtil.findAll(em, Fournisseur.class).isEmpty()) {
            fournisseur = FournisseurResourceIT.createUpdatedEntity(em);
            // em.persist(fournisseur);
            // em.flush();
        } else {
            fournisseur = TestUtil.findAll(em, Fournisseur.class).get(0);
        }
        commande.setIdFournisseur(fournisseur);
        return commande;
    }

    @BeforeEach
    public void initTest() {
        commande = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommande() throws Exception {
        int databaseSizeBeforeCreate = commandeRepository.findAll().size();
        // Create the Commande
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);
        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isCreated());

        // Validate the Commande in the database
        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeCreate + 1);
        Commande testCommande = commandeList.get(commandeList.size() - 1);
        assertThat(testCommande.getPrixTotal()).isEqualTo(DEFAULT_PRIX_TOTAL);
        assertThat(testCommande.getDateCommande()).isEqualTo(DEFAULT_DATE_COMMANDE);
        assertThat(testCommande.getDateStatut()).isEqualTo(DEFAULT_DATE_STATUT);
        assertThat(testCommande.getStatut()).isEqualTo(DEFAULT_STATUT);
        assertThat(testCommande.isaLivrer()).isEqualTo(DEFAULT_A_LIVRER);
        assertThat(testCommande.getDatePaiement()).isEqualTo(DEFAULT_DATE_PAIEMENT);

        // Validate the Commande in Elasticsearch
        verify(mockCommandeSearchRepository, times(1)).save(testCommande);
    }

    @Test
    @Transactional
    public void createCommandeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commandeRepository.findAll().size();

        // Create the Commande with an existing ID
        commande.setId(1L);
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        // Validate the Commande in the database
        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Commande in Elasticsearch
        verify(mockCommandeSearchRepository, times(0)).save(commande);
    }

    @Test
    @Transactional
    public void checkPrixTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setPrixTotal(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateCommandeIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setDateCommande(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateStatutIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setDateStatut(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatutIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setStatut(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkaLivrerIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setaLivrer(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatePaiementIsRequired() throws Exception {
        int databaseSizeBeforeTest = commandeRepository.findAll().size();
        // set the field null
        commande.setDatePaiement(null);

        // Create the Commande, which fails.
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        restCommandeMockMvc.perform(post("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCommandes() throws Exception {
        // Initialize the database
        commande = createEntitySansClient(em);
        commandeRepository.save(commande);

        // Get all the commandeList
        restCommandeMockMvc.perform(get("/api/commandes?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(commande.getId().intValue())))
                .andExpect(jsonPath("$.[*].prixTotal").value(hasItem(DEFAULT_PRIX_TOTAL.doubleValue())))
                .andExpect(jsonPath("$.[*].dateCommande").value(hasItem(DEFAULT_DATE_COMMANDE)))
                .andExpect(jsonPath("$.[*].dateStatut").value(hasItem(DEFAULT_DATE_STATUT)))
                .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
                .andExpect(jsonPath("$.[*].aLivrer").value(hasItem(DEFAULT_A_LIVRER.booleanValue())))
                .andExpect(jsonPath("$.[*].datePaiement").value(hasItem(DEFAULT_DATE_PAIEMENT)));
    }

    @Test
    @Transactional
    public void getCommande() throws Exception {
        // Initialize the database
        commandeRepository.save(commande);

        // Get the commande
        restCommandeMockMvc.perform(get("/api/commandes/{id}", commande.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(commande.getId().intValue()))
                .andExpect(jsonPath("$.prixTotal").value(DEFAULT_PRIX_TOTAL.doubleValue()))
                .andExpect(jsonPath("$.dateCommande").value(DEFAULT_DATE_COMMANDE))
                .andExpect(jsonPath("$.dateStatut").value(DEFAULT_DATE_STATUT))
                .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()))
                .andExpect(jsonPath("$.aLivrer").value(DEFAULT_A_LIVRER.booleanValue()))
                .andExpect(jsonPath("$.datePaiement").value(DEFAULT_DATE_PAIEMENT));
    }

    @Test
    @Transactional
    public void getNonExistingCommande() throws Exception {
        // Get the commande
        restCommandeMockMvc.perform(get("/api/commandes/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommande() throws Exception {
        // Initialize the database
        commande = createEntitySansClient(em);
        commandeRepository.save(commande);

        int databaseSizeBeforeUpdate = commandeRepository.findAll().size();

        // Update the commande
        Commande updatedCommande = commandeRepository.findById(commande.getId()).get();
        // Disconnect from session so that the updates on updatedCommande are not
        // directly saved in db
        em.detach(updatedCommande);
        updatedCommande.prixTotal(UPDATED_PRIX_TOTAL).dateCommande(UPDATED_DATE_COMMANDE)
                .dateStatut(UPDATED_DATE_STATUT).statut(UPDATED_STATUT).aLivrer(UPDATED_A_LIVRER)
                .datePaiement(UPDATED_DATE_PAIEMENT);
        CommandeDTO commandeDTO = commandeMapper.toDto(updatedCommande);

        restCommandeMockMvc.perform(put("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isOk());

        // Validate the Commande in the database
        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeUpdate);
        Commande testCommande = commandeList.get(commandeList.size() - 1);
        assertThat(testCommande.getPrixTotal()).isEqualTo(UPDATED_PRIX_TOTAL);
        assertThat(testCommande.getDateCommande()).isEqualTo(UPDATED_DATE_COMMANDE);
        assertThat(testCommande.getDateStatut()).isEqualTo(UPDATED_DATE_STATUT);
        assertThat(testCommande.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testCommande.isaLivrer()).isEqualTo(UPDATED_A_LIVRER);
        assertThat(testCommande.getDatePaiement()).isEqualTo(UPDATED_DATE_PAIEMENT);

        // Validate the Commande in Elasticsearch
        verify(mockCommandeSearchRepository, times(1)).save(testCommande);
    }

    @Test
    @Transactional
    public void updateNonExistingCommande() throws Exception {
        int databaseSizeBeforeUpdate = commandeRepository.findAll().size();

        // Create the Commande
        CommandeDTO commandeDTO = commandeMapper.toDto(commande);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommandeMockMvc.perform(put("/api/commandes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeDTO))).andExpect(status().isBadRequest());

        // Validate the Commande in the database
        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Commande in Elasticsearch
        verify(mockCommandeSearchRepository, times(0)).save(commande);
    }

    @Test
    @Transactional
    public void deleteCommande() throws Exception {
        // Initialize the database
        commande = createEntitySansClient(em);
        commandeRepository.save(commande);

        int databaseSizeBeforeDelete = commandeRepository.findAll().size();

        // Delete the commande
        restCommandeMockMvc.perform(delete("/api/commandes/{id}", commande.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Commande> commandeList = commandeRepository.findAll();
        assertThat(commandeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Commande in Elasticsearch
        verify(mockCommandeSearchRepository, times(1)).deleteById(commande.getId());
    }

    @Test
    @Transactional
    public void searchCommande() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        commandeRepository.save(commande);
        when(mockCommandeSearchRepository.search(queryStringQuery("id:" + commande.getId()), PageRequest.of(0, 20)))
                .thenReturn(new PageImpl<>(Collections.singletonList(commande), PageRequest.of(0, 1), 1));

        // Search the commande
        restCommandeMockMvc.perform(get("/api/_search/commandes?query=id:" + commande.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(commande.getId().intValue())))
                .andExpect(jsonPath("$.[*].prixTotal").value(hasItem(DEFAULT_PRIX_TOTAL.doubleValue())))
                .andExpect(jsonPath("$.[*].dateCommande").value(hasItem(DEFAULT_DATE_COMMANDE)))
                .andExpect(jsonPath("$.[*].dateStatut").value(hasItem(DEFAULT_DATE_STATUT)))
                .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
                .andExpect(jsonPath("$.[*].aLivrer").value(hasItem(DEFAULT_A_LIVRER.booleanValue())))
                .andExpect(jsonPath("$.[*].datePaiement").value(hasItem(DEFAULT_DATE_PAIEMENT)));
    }
}
