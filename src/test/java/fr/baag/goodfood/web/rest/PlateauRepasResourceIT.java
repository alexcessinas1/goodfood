package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.PlateauRepas;
import fr.baag.goodfood.repository.PlateauRepasRepository;
import fr.baag.goodfood.repository.search.PlateauRepasSearchRepository;
import fr.baag.goodfood.service.PlateauRepasService;
import fr.baag.goodfood.service.dto.PlateauRepasDTO;
import fr.baag.goodfood.service.mapper.PlateauRepasMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PlateauRepasResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PlateauRepasResourceIT {

    private static final Float DEFAULT_PRIX_HT = 1F;
    private static final Float UPDATED_PRIX_HT = 2F;

    private static final Float DEFAULT_TVA = 0F;
    private static final Float UPDATED_TVA = 1F;

    private static final String DEFAULT_COMPOSITION = "AAAAAAAAAA";
    private static final String UPDATED_COMPOSITION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_COUVERT = false;
    private static final Boolean UPDATED_COUVERT = true;

    private static final Boolean DEFAULT_SERVIETTE = false;
    private static final Boolean UPDATED_SERVIETTE = true;

    @Autowired
    private PlateauRepasRepository plateauRepasRepository;

    @Autowired
    private PlateauRepasMapper plateauRepasMapper;

    @Autowired
    private PlateauRepasService plateauRepasService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.PlateauRepasSearchRepositoryMockConfiguration
     */
    @Autowired
    private PlateauRepasSearchRepository mockPlateauRepasSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPlateauRepasMockMvc;

    private PlateauRepas plateauRepas;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PlateauRepas createEntity(EntityManager em) {
        PlateauRepas plateauRepas = new PlateauRepas().prixHT(DEFAULT_PRIX_HT).tva(DEFAULT_TVA)
                .composition(DEFAULT_COMPOSITION).couvert(DEFAULT_COUVERT).serviette(DEFAULT_SERVIETTE);
        return plateauRepas;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PlateauRepas createUpdatedEntity(EntityManager em) {
        PlateauRepas plateauRepas = new PlateauRepas().prixHT(UPDATED_PRIX_HT).tva(UPDATED_TVA)
                .composition(UPDATED_COMPOSITION).couvert(UPDATED_COUVERT).serviette(UPDATED_SERVIETTE);
        return plateauRepas;
    }

    @BeforeEach
    public void initTest() {
        plateauRepas = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlateauRepas() throws Exception {
        int databaseSizeBeforeCreate = plateauRepasRepository.findAll().size();
        // Create the PlateauRepas
        PlateauRepasDTO plateauRepasDTO = plateauRepasMapper.toDto(plateauRepas);
        restPlateauRepasMockMvc.perform(post("/api/plateau-repas").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(plateauRepasDTO))).andExpect(status().isCreated());

        // Validate the PlateauRepas in the database
        List<PlateauRepas> plateauRepasList = plateauRepasRepository.findAll();
        assertThat(plateauRepasList).hasSize(databaseSizeBeforeCreate + 1);
        PlateauRepas testPlateauRepas = plateauRepasList.get(plateauRepasList.size() - 1);
        assertThat(testPlateauRepas.getPrixHT()).isEqualTo(DEFAULT_PRIX_HT);
        assertThat(testPlateauRepas.getTva()).isEqualTo(DEFAULT_TVA);
        assertThat(testPlateauRepas.getComposition()).isEqualTo(DEFAULT_COMPOSITION);
        assertThat(testPlateauRepas.getCouvert()).isEqualTo(DEFAULT_COUVERT);
        assertThat(testPlateauRepas.getServiette()).isEqualTo(DEFAULT_SERVIETTE);

        // Validate the PlateauRepas in Elasticsearch
        verify(mockPlateauRepasSearchRepository, times(1)).save(testPlateauRepas);
    }

    @Test
    @Transactional
    public void createPlateauRepasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = plateauRepasRepository.findAll().size();

        // Create the PlateauRepas with an existing ID
        plateauRepas.setId(1L);
        PlateauRepasDTO plateauRepasDTO = plateauRepasMapper.toDto(plateauRepas);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlateauRepasMockMvc
                .perform(post("/api/plateau-repas").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(plateauRepasDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PlateauRepas in the database
        List<PlateauRepas> plateauRepasList = plateauRepasRepository.findAll();
        assertThat(plateauRepasList).hasSize(databaseSizeBeforeCreate);

        // Validate the PlateauRepas in Elasticsearch
        verify(mockPlateauRepasSearchRepository, times(0)).save(plateauRepas);
    }

    @Test
    @Transactional
    public void getAllPlateauRepas() throws Exception {
        // Initialize the database
        plateauRepasRepository.saveAndFlush(plateauRepas);

        // Get all the plateauRepasList
        restPlateauRepasMockMvc.perform(get("/api/plateau-repas?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(plateauRepas.getId().intValue())))
                .andExpect(jsonPath("$.[*].prixHT").value(hasItem(DEFAULT_PRIX_HT.doubleValue())))
                .andExpect(jsonPath("$.[*].tva").value(hasItem(DEFAULT_TVA.doubleValue())))
                .andExpect(jsonPath("$.[*].composition").value(hasItem(DEFAULT_COMPOSITION)))
                .andExpect(jsonPath("$.[*].couvert").value(hasItem(DEFAULT_COUVERT.booleanValue())))
                .andExpect(jsonPath("$.[*].serviette").value(hasItem(DEFAULT_SERVIETTE.booleanValue())));
    }

    @Test
    @Transactional
    public void getPlateauRepas() throws Exception {
        // Initialize the database
        plateauRepasRepository.saveAndFlush(plateauRepas);

        // Get the plateauRepas
        restPlateauRepasMockMvc.perform(get("/api/plateau-repas/{id}", plateauRepas.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(plateauRepas.getId().intValue()))
                .andExpect(jsonPath("$.prixHT").value(DEFAULT_PRIX_HT.doubleValue()))
                .andExpect(jsonPath("$.tva").value(DEFAULT_TVA.doubleValue()))
                .andExpect(jsonPath("$.composition").value(DEFAULT_COMPOSITION))
                .andExpect(jsonPath("$.couvert").value(DEFAULT_COUVERT.booleanValue()))
                .andExpect(jsonPath("$.serviette").value(DEFAULT_SERVIETTE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPlateauRepas() throws Exception {
        // Get the plateauRepas
        restPlateauRepasMockMvc.perform(get("/api/plateau-repas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlateauRepas() throws Exception {
        // Initialize the database
        plateauRepasRepository.saveAndFlush(plateauRepas);

        int databaseSizeBeforeUpdate = plateauRepasRepository.findAll().size();

        // Update the plateauRepas
        PlateauRepas updatedPlateauRepas = plateauRepasRepository.findById(plateauRepas.getId()).get();
        // Disconnect from session so that the updates on updatedPlateauRepas are not
        // directly saved in db
        em.detach(updatedPlateauRepas);
        updatedPlateauRepas.prixHT(UPDATED_PRIX_HT).tva(UPDATED_TVA).composition(UPDATED_COMPOSITION)
                .couvert(UPDATED_COUVERT).serviette(UPDATED_SERVIETTE);
        PlateauRepasDTO plateauRepasDTO = plateauRepasMapper.toDto(updatedPlateauRepas);

        restPlateauRepasMockMvc.perform(put("/api/plateau-repas").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(plateauRepasDTO))).andExpect(status().isOk());

        // Validate the PlateauRepas in the database
        List<PlateauRepas> plateauRepasList = plateauRepasRepository.findAll();
        assertThat(plateauRepasList).hasSize(databaseSizeBeforeUpdate);
        PlateauRepas testPlateauRepas = plateauRepasList.get(plateauRepasList.size() - 1);
        assertThat(testPlateauRepas.getPrixHT()).isEqualTo(UPDATED_PRIX_HT);
        assertThat(testPlateauRepas.getTva()).isEqualTo(UPDATED_TVA);
        assertThat(testPlateauRepas.getComposition()).isEqualTo(UPDATED_COMPOSITION);
        assertThat(testPlateauRepas.getCouvert()).isEqualTo(UPDATED_COUVERT);
        assertThat(testPlateauRepas.getServiette()).isEqualTo(UPDATED_SERVIETTE);

        // Validate the PlateauRepas in Elasticsearch
        verify(mockPlateauRepasSearchRepository, times(1)).save(testPlateauRepas);
    }

    @Test
    @Transactional
    public void updateNonExistingPlateauRepas() throws Exception {
        int databaseSizeBeforeUpdate = plateauRepasRepository.findAll().size();

        // Create the PlateauRepas
        PlateauRepasDTO plateauRepasDTO = plateauRepasMapper.toDto(plateauRepas);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlateauRepasMockMvc
                .perform(put("/api/plateau-repas").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(plateauRepasDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PlateauRepas in the database
        List<PlateauRepas> plateauRepasList = plateauRepasRepository.findAll();
        assertThat(plateauRepasList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PlateauRepas in Elasticsearch
        verify(mockPlateauRepasSearchRepository, times(0)).save(plateauRepas);
    }

    @Test
    @Transactional
    public void deletePlateauRepas() throws Exception {
        // Initialize the database
        plateauRepasRepository.saveAndFlush(plateauRepas);

        int databaseSizeBeforeDelete = plateauRepasRepository.findAll().size();

        // Delete the plateauRepas
        restPlateauRepasMockMvc
                .perform(delete("/api/plateau-repas/{id}", plateauRepas.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PlateauRepas> plateauRepasList = plateauRepasRepository.findAll();
        assertThat(plateauRepasList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the PlateauRepas in Elasticsearch
        verify(mockPlateauRepasSearchRepository, times(1)).deleteById(plateauRepas.getId());
    }

    @Test
    @Transactional
    public void searchPlateauRepas() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        plateauRepasRepository.saveAndFlush(plateauRepas);
        when(mockPlateauRepasSearchRepository.search(queryStringQuery("id:" + plateauRepas.getId())))
                .thenReturn(Collections.singletonList(plateauRepas));

        // Search the plateauRepas
        restPlateauRepasMockMvc.perform(get("/api/_search/plateau-repas?query=id:" + plateauRepas.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(plateauRepas.getId().intValue())))
                .andExpect(jsonPath("$.[*].prixHT").value(hasItem(DEFAULT_PRIX_HT.doubleValue())))
                .andExpect(jsonPath("$.[*].tva").value(hasItem(DEFAULT_TVA.doubleValue())))
                .andExpect(jsonPath("$.[*].composition").value(hasItem(DEFAULT_COMPOSITION)))
                .andExpect(jsonPath("$.[*].couvert").value(hasItem(DEFAULT_COUVERT.booleanValue())))
                .andExpect(jsonPath("$.[*].serviette").value(hasItem(DEFAULT_SERVIETTE.booleanValue())));
    }
}
