package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.PersonneMorale;
import fr.baag.goodfood.domain.User;
import fr.baag.goodfood.repository.PersonneMoraleRepository;
import fr.baag.goodfood.repository.search.PersonneMoraleSearchRepository;
import fr.baag.goodfood.service.PersonneMoraleService;
import fr.baag.goodfood.service.dto.PersonneMoraleDTO;
import fr.baag.goodfood.service.mapper.PersonneMoraleMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PersonneMoraleResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonneMoraleResourceIT {

    private static final String DEFAULT_RAISON_SOCIALE = "AAAAAAAAAA";
    private static final String UPDATED_RAISON_SOCIALE = "BBBBBBBBBB";

    private static final String DEFAULT_SIRET = "AAAAAAAAAA";
    private static final String UPDATED_SIRET = "BBBBBBBBBB";

    @Autowired
    private PersonneMoraleRepository personneMoraleRepository;

    @Autowired
    private PersonneMoraleMapper personneMoraleMapper;

    @Autowired
    private PersonneMoraleService personneMoraleService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.PersonneMoraleSearchRepositoryMockConfiguration
     */
    @Autowired
    private PersonneMoraleSearchRepository mockPersonneMoraleSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonneMoraleMockMvc;

    private PersonneMorale personneMorale;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PersonneMorale createEntity(EntityManager em) {
        PersonneMorale personneMorale = new PersonneMorale().raisonSociale(DEFAULT_RAISON_SOCIALE).siret(DEFAULT_SIRET);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        personneMorale.setUser(user);
        return personneMorale;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PersonneMorale createUpdatedEntity(EntityManager em) {
        PersonneMorale personneMorale = new PersonneMorale().raisonSociale(UPDATED_RAISON_SOCIALE).siret(UPDATED_SIRET);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        personneMorale.setUser(user);
        return personneMorale;
    }

    @BeforeEach
    public void initTest() {
        personneMorale = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonneMorale() throws Exception {
        int databaseSizeBeforeCreate = personneMoraleRepository.findAll().size();
        // Create the PersonneMorale
        PersonneMoraleDTO personneMoraleDTO = personneMoraleMapper.toDto(personneMorale);
        restPersonneMoraleMockMvc.perform(post("/api/personne-morales").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(personneMoraleDTO))).andExpect(status().isCreated());

        // Validate the PersonneMorale in the database
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeCreate + 1);
        PersonneMorale testPersonneMorale = personneMoraleList.get(personneMoraleList.size() - 1);
        assertThat(testPersonneMorale.getRaisonSociale()).isEqualTo(DEFAULT_RAISON_SOCIALE);
        assertThat(testPersonneMorale.getSiret()).isEqualTo(DEFAULT_SIRET);

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(1)).save(testPersonneMorale);
    }

    @Test
    @Transactional
    public void createPersonneMoraleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personneMoraleRepository.findAll().size();

        // Create the PersonneMorale with an existing ID
        personneMorale.setId(1L);
        PersonneMoraleDTO personneMoraleDTO = personneMoraleMapper.toDto(personneMorale);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonneMoraleMockMvc
                .perform(post("/api/personne-morales").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(personneMoraleDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PersonneMorale in the database
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeCreate);

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(0)).save(personneMorale);
    }

    @Test
    @Transactional
    public void updatePersonneMoraleMapsIdAssociationWithNewId() throws Exception {
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);
        int databaseSizeBeforeCreate = personneMoraleRepository.findAll().size();

        // Add a new parent entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();

        // Load the personneMorale
        PersonneMorale updatedPersonneMorale = personneMoraleRepository.findById(personneMorale.getId()).get();
        // Disconnect from session so that the updates on updatedPersonneMorale are not
        // directly saved in db
        em.detach(updatedPersonneMorale);

        // Update the User with new association value
        updatedPersonneMorale.setUser(user);
        PersonneMoraleDTO updatedPersonneMoraleDTO = personneMoraleMapper.toDto(updatedPersonneMorale);

        // Update the entity
        restPersonneMoraleMockMvc
                .perform(put("/api/personne-morales").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(updatedPersonneMoraleDTO)))
                .andExpect(status().isOk());

        // Validate the PersonneMorale in the database
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeCreate);
        PersonneMorale testPersonneMorale = personneMoraleList.get(personneMoraleList.size() - 1);

        // Validate the id for MapsId, the ids must be same
        // Uncomment the following line for assertion. However, please note that there
        // is a known issue and uncommenting will fail the test.
        // Please look at https://github.com/jhipster/generator-jhipster/issues/9100.
        // You can modify this test as necessary.
        // assertThat(testPersonneMorale.getId()).isEqualTo(testPersonneMorale.getUser().getId());

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(1)).save(personneMorale);
    }

    @Test
    @Transactional
    public void getAllPersonneMorales() throws Exception {
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);

        // Get all the personneMoraleList
        restPersonneMoraleMockMvc.perform(get("/api/personne-morales?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personneMorale.getId().intValue())))
                .andExpect(jsonPath("$.[*].raisonSociale").value(hasItem(DEFAULT_RAISON_SOCIALE)))
                .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET)));
    }

    @Test
    @Transactional
    public void getPersonneMorale() throws Exception {
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);

        // Get the personneMorale
        restPersonneMoraleMockMvc.perform(get("/api/personne-morales/{id}", personneMorale.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(personneMorale.getId().intValue()))
                .andExpect(jsonPath("$.raisonSociale").value(DEFAULT_RAISON_SOCIALE))
                .andExpect(jsonPath("$.siret").value(DEFAULT_SIRET));
    }

    @Test
    @Transactional
    public void getNonExistingPersonneMorale() throws Exception {
        // Get the personneMorale
        restPersonneMoraleMockMvc.perform(get("/api/personne-morales/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonneMorale() throws Exception {
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);

        int databaseSizeBeforeUpdate = personneMoraleRepository.findAll().size();

        // Update the personneMorale
        PersonneMorale updatedPersonneMorale = personneMoraleRepository.findById(personneMorale.getId()).get();
        // Disconnect from session so that the updates on updatedPersonneMorale are not
        // directly saved in db
        em.detach(updatedPersonneMorale);
        updatedPersonneMorale.raisonSociale(UPDATED_RAISON_SOCIALE).siret(UPDATED_SIRET);
        PersonneMoraleDTO personneMoraleDTO = personneMoraleMapper.toDto(updatedPersonneMorale);

        restPersonneMoraleMockMvc.perform(put("/api/personne-morales").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(personneMoraleDTO))).andExpect(status().isOk());

        // Validate the PersonneMorale in the database
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeUpdate);
        PersonneMorale testPersonneMorale = personneMoraleList.get(personneMoraleList.size() - 1);
        assertThat(testPersonneMorale.getRaisonSociale()).isEqualTo(UPDATED_RAISON_SOCIALE);
        assertThat(testPersonneMorale.getSiret()).isEqualTo(UPDATED_SIRET);

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(1)).save(testPersonneMorale);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonneMorale() throws Exception {
        int databaseSizeBeforeUpdate = personneMoraleRepository.findAll().size();

        // Create the PersonneMorale
        PersonneMoraleDTO personneMoraleDTO = personneMoraleMapper.toDto(personneMorale);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonneMoraleMockMvc
                .perform(put("/api/personne-morales").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(personneMoraleDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PersonneMorale in the database
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(0)).save(personneMorale);
    }

    @Test
    @Transactional
    public void deletePersonneMorale() throws Exception {
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);

        int databaseSizeBeforeDelete = personneMoraleRepository.findAll().size();

        // Delete the personneMorale
        restPersonneMoraleMockMvc
                .perform(
                        delete("/api/personne-morales/{id}", personneMorale.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PersonneMorale> personneMoraleList = personneMoraleRepository.findAll();
        assertThat(personneMoraleList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the PersonneMorale in Elasticsearch
        verify(mockPersonneMoraleSearchRepository, times(1)).deleteById(personneMorale.getId());
    }

    @Test
    @Transactional
    public void searchPersonneMorale() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        personneMoraleRepository.saveAndFlush(personneMorale);
        when(mockPersonneMoraleSearchRepository.search(queryStringQuery("id:" + personneMorale.getId())))
                .thenReturn(Collections.singletonList(personneMorale));

        // Search the personneMorale
        restPersonneMoraleMockMvc.perform(get("/api/_search/personne-morales?query=id:" + personneMorale.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personneMorale.getId().intValue())))
                .andExpect(jsonPath("$.[*].raisonSociale").value(hasItem(DEFAULT_RAISON_SOCIALE)))
                .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET)));
    }
}
