package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.PersonnePhysique;
import fr.baag.goodfood.domain.User;
import fr.baag.goodfood.repository.PersonnePhysiqueRepository;
import fr.baag.goodfood.repository.search.PersonnePhysiqueSearchRepository;
import fr.baag.goodfood.service.PersonnePhysiqueService;
import fr.baag.goodfood.service.dto.PersonnePhysiqueDTO;
import fr.baag.goodfood.service.mapper.PersonnePhysiqueMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PersonnePhysiqueResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonnePhysiqueResourceIT {

    private static final LocalDate DEFAULT_DATE_NAISSANCE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PersonnePhysiqueRepository personnePhysiqueRepository;

    @Autowired
    private PersonnePhysiqueMapper personnePhysiqueMapper;

    @Autowired
    private PersonnePhysiqueService personnePhysiqueService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.PersonnePhysiqueSearchRepositoryMockConfiguration
     */
    @Autowired
    private PersonnePhysiqueSearchRepository mockPersonnePhysiqueSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonnePhysiqueMockMvc;

    private PersonnePhysique personnePhysique;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PersonnePhysique createEntity(EntityManager em) {
        PersonnePhysique personnePhysique = new PersonnePhysique().dateNaissance(DEFAULT_DATE_NAISSANCE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        personnePhysique.setUser(user);
        return personnePhysique;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static PersonnePhysique createUpdatedEntity(EntityManager em) {
        PersonnePhysique personnePhysique = new PersonnePhysique().dateNaissance(UPDATED_DATE_NAISSANCE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        personnePhysique.setUser(user);
        return personnePhysique;
    }

    @BeforeEach
    public void initTest() {
        personnePhysique = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonnePhysique() throws Exception {
        int databaseSizeBeforeCreate = personnePhysiqueRepository.findAll().size();
        // Create the PersonnePhysique
        PersonnePhysiqueDTO personnePhysiqueDTO = personnePhysiqueMapper.toDto(personnePhysique);
        restPersonnePhysiqueMockMvc
                .perform(post("/api/personne-physiques").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(personnePhysiqueDTO)))
                .andExpect(status().isCreated());

        // Validate the PersonnePhysique in the database
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeCreate + 1);
        PersonnePhysique testPersonnePhysique = personnePhysiqueList.get(personnePhysiqueList.size() - 1);
        assertThat(testPersonnePhysique.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(1)).save(testPersonnePhysique);
    }

    @Test
    @Transactional
    public void createPersonnePhysiqueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personnePhysiqueRepository.findAll().size();

        // Create the PersonnePhysique with an existing ID
        personnePhysique.setId(1L);
        PersonnePhysiqueDTO personnePhysiqueDTO = personnePhysiqueMapper.toDto(personnePhysique);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonnePhysiqueMockMvc
                .perform(post("/api/personne-physiques").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(personnePhysiqueDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PersonnePhysique in the database
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeCreate);

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(0)).save(personnePhysique);
    }

    @Test
    @Transactional
    public void updatePersonnePhysiqueMapsIdAssociationWithNewId() throws Exception {
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);
        int databaseSizeBeforeCreate = personnePhysiqueRepository.findAll().size();

        // Add a new parent entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();

        // Load the personnePhysique
        PersonnePhysique updatedPersonnePhysique = personnePhysiqueRepository.findById(personnePhysique.getId()).get();
        // Disconnect from session so that the updates on updatedPersonnePhysique are
        // not directly saved in db
        em.detach(updatedPersonnePhysique);

        // Update the User with new association value
        updatedPersonnePhysique.setUser(user);
        PersonnePhysiqueDTO updatedPersonnePhysiqueDTO = personnePhysiqueMapper.toDto(updatedPersonnePhysique);

        // Update the entity
        restPersonnePhysiqueMockMvc
                .perform(put("/api/personne-physiques").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(updatedPersonnePhysiqueDTO)))
                .andExpect(status().isOk());

        // Validate the PersonnePhysique in the database
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeCreate);
        PersonnePhysique testPersonnePhysique = personnePhysiqueList.get(personnePhysiqueList.size() - 1);

        // Validate the id for MapsId, the ids must be same
        // Uncomment the following line for assertion. However, please note that there
        // is a known issue and uncommenting will fail the test.
        // Please look at https://github.com/jhipster/generator-jhipster/issues/9100.
        // You can modify this test as necessary.
        // assertThat(testPersonnePhysique.getId()).isEqualTo(testPersonnePhysique.getUser().getId());

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(1)).save(personnePhysique);
    }

    @Test
    @Transactional
    public void getAllPersonnePhysiques() throws Exception {
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);

        // Get all the personnePhysiqueList
        restPersonnePhysiqueMockMvc.perform(get("/api/personne-physiques?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personnePhysique.getId().intValue())))
                .andExpect(jsonPath("$.[*].dateNaissance").value(hasItem(DEFAULT_DATE_NAISSANCE.toString())));
    }

    @Test
    @Transactional
    public void getPersonnePhysique() throws Exception {
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);

        // Get the personnePhysique
        restPersonnePhysiqueMockMvc.perform(get("/api/personne-physiques/{id}", personnePhysique.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(personnePhysique.getId().intValue()))
                .andExpect(jsonPath("$.dateNaissance").value(DEFAULT_DATE_NAISSANCE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonnePhysique() throws Exception {
        // Get the personnePhysique
        restPersonnePhysiqueMockMvc.perform(get("/api/personne-physiques/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonnePhysique() throws Exception {
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);

        int databaseSizeBeforeUpdate = personnePhysiqueRepository.findAll().size();

        // Update the personnePhysique
        PersonnePhysique updatedPersonnePhysique = personnePhysiqueRepository.findById(personnePhysique.getId()).get();
        // Disconnect from session so that the updates on updatedPersonnePhysique are
        // not directly saved in db
        em.detach(updatedPersonnePhysique);
        updatedPersonnePhysique.dateNaissance(UPDATED_DATE_NAISSANCE);
        PersonnePhysiqueDTO personnePhysiqueDTO = personnePhysiqueMapper.toDto(updatedPersonnePhysique);

        restPersonnePhysiqueMockMvc.perform(put("/api/personne-physiques").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(personnePhysiqueDTO))).andExpect(status().isOk());

        // Validate the PersonnePhysique in the database
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeUpdate);
        PersonnePhysique testPersonnePhysique = personnePhysiqueList.get(personnePhysiqueList.size() - 1);
        assertThat(testPersonnePhysique.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(1)).save(testPersonnePhysique);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonnePhysique() throws Exception {
        int databaseSizeBeforeUpdate = personnePhysiqueRepository.findAll().size();

        // Create the PersonnePhysique
        PersonnePhysiqueDTO personnePhysiqueDTO = personnePhysiqueMapper.toDto(personnePhysique);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonnePhysiqueMockMvc
                .perform(put("/api/personne-physiques").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(personnePhysiqueDTO)))
                .andExpect(status().isBadRequest());

        // Validate the PersonnePhysique in the database
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeUpdate);

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(0)).save(personnePhysique);
    }

    @Test
    @Transactional
    public void deletePersonnePhysique() throws Exception {
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);

        int databaseSizeBeforeDelete = personnePhysiqueRepository.findAll().size();

        // Delete the personnePhysique
        restPersonnePhysiqueMockMvc.perform(
                delete("/api/personne-physiques/{id}", personnePhysique.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PersonnePhysique> personnePhysiqueList = personnePhysiqueRepository.findAll();
        assertThat(personnePhysiqueList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the PersonnePhysique in Elasticsearch
        verify(mockPersonnePhysiqueSearchRepository, times(1)).deleteById(personnePhysique.getId());
    }

    @Test
    @Transactional
    public void searchPersonnePhysique() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        personnePhysiqueRepository.saveAndFlush(personnePhysique);
        when(mockPersonnePhysiqueSearchRepository.search(queryStringQuery("id:" + personnePhysique.getId())))
                .thenReturn(Collections.singletonList(personnePhysique));

        // Search the personnePhysique
        restPersonnePhysiqueMockMvc.perform(get("/api/_search/personne-physiques?query=id:" + personnePhysique.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personnePhysique.getId().intValue())))
                .andExpect(jsonPath("$.[*].dateNaissance").value(hasItem(DEFAULT_DATE_NAISSANCE.toString())));
    }
}
