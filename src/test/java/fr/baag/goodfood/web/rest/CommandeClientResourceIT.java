package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.CommandeClient;
import fr.baag.goodfood.domain.PersonneMorale;
import fr.baag.goodfood.domain.PersonnePhysique;
import fr.baag.goodfood.domain.enumeration.PersonneType;
import fr.baag.goodfood.domain.enumeration.Statut;
import fr.baag.goodfood.domain.Adresse;
import fr.baag.goodfood.domain.Client;
import fr.baag.goodfood.domain.Commande;
import fr.baag.goodfood.repository.CommandeClientRepository;
import fr.baag.goodfood.repository.UserRepository;
import fr.baag.goodfood.repository.search.CommandeClientSearchRepository;
import fr.baag.goodfood.service.AdresseService;
import fr.baag.goodfood.service.ClientService;
import fr.baag.goodfood.service.CommandeClientService;
import fr.baag.goodfood.service.CommandeService;
import fr.baag.goodfood.service.PersonneMoraleService;
import fr.baag.goodfood.service.PersonnePhysiqueService;
import fr.baag.goodfood.service.dto.AdresseDTO;
import fr.baag.goodfood.service.dto.ClientDTO;
import fr.baag.goodfood.service.dto.CommandeClientDTO;
import fr.baag.goodfood.service.dto.CommandeDTO;
import fr.baag.goodfood.service.dto.PersonnePhysiqueDTO;
import fr.baag.goodfood.service.mapper.AdresseMapper;
import fr.baag.goodfood.service.mapper.ClientMapper;
import fr.baag.goodfood.service.mapper.CommandeClientMapper;
import fr.baag.goodfood.service.mapper.CommandeMapper;
import fr.baag.goodfood.service.mapper.PersonneMoraleMapper;
import fr.baag.goodfood.service.mapper.PersonnePhysiqueMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CommandeClientResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CommandeClientResourceIT {

    private static final String DEFAULT_CODE_PROMO = "AAAAAAAAAA";
    private static final String UPDATED_CODE_PROMO = "BBBBBBBBBB";

    @Autowired
    private CommandeClientRepository commandeClientRepository;

    @Autowired
    private CommandeClientMapper commandeClientMapper;

    @Autowired
    private CommandeClientService commandeClientService;

    @Autowired
    private CommandeClientResource commandeClientResource;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientMapper clientMapper;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private CommandeMapper commandeMapper;

    @Autowired
    private AdresseService adresseService;

    @Autowired
    private AdresseMapper adresseMapper;

    @Autowired
    private PersonnePhysiqueService personnePhysiqueService;

    @Autowired
    private PersonnePhysiqueMapper personnePhysiqueMapper;

    @Autowired
    private UserRepository userRepository;
    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.CommandeClientSearchRepositoryMockConfiguration
     */
    @Autowired
    private CommandeClientSearchRepository mockCommandeClientSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommandeClientMockMvc;

    private CommandeClient commandeClient;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static CommandeClient createEntity(EntityManager em) {
        CommandeClient commandeClient = new CommandeClient().codePromo(DEFAULT_CODE_PROMO);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createEntity(em);
            // em.persist(client);
            // em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        commandeClient.setClient(client);
        // Add required entity
        Commande commande;
        if (TestUtil.findAll(em, Commande.class).isEmpty()) {
            commande = CommandeResourceIT.createEntity(em);
            // em.persist(commande);
            // em.flush();
        } else {
            commande = TestUtil.findAll(em, Commande.class).get(0);
        }
        commandeClient.setCommande(commande);
        return commandeClient;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public CommandeClient createEntityPropre() throws Exception {
        CommandeClient commandeClient = new CommandeClient();

        PersonnePhysique personnePhysique = new PersonnePhysique();
        personnePhysique.setUser(userRepository.findById(1L).get());
        PersonnePhysiqueDTO personnePhysiqueDTO = personnePhysiqueService.save(personnePhysiqueMapper.toDto(personnePhysique));

        Adresse adresse = new Adresse();
        AdresseDTO adresseDTO = adresseService.save(adresseMapper.toDto(adresse));

        Client client = new Client();

        client.setPersonnePhysique(personnePhysiqueMapper.toEntity(personnePhysiqueDTO));
        client.setPersonneType(PersonneType.PHYSIQUE);
        client.setAdresseFacturation(adresseMapper.toEntity(adresseDTO));
        client.setAdresseLivraison(adresseMapper.toEntity(adresseDTO));

        ClientDTO clientDTO = clientService.save(clientMapper.toDto(client));
        commandeClient.setClient(clientMapper.toEntity(clientDTO));

        Commande commande = new Commande();

        commande.setPrixTotal((float) Math.random());
        commande.setDateCommande("dateCommande");
        commande.setDateStatut("dateStatut");
        commande.setStatut(Statut.COMPLETE);
        commande.setaLivrer(true);
        commande.setDatePaiement("datePaiement");

        CommandeDTO commandeDTO = commandeService.save(commandeMapper.toDto(commande));

        commandeClient.setCommande(commandeMapper.toEntity(commandeDTO));
        commandeClient.setCodePromo(DEFAULT_CODE_PROMO);
        CommandeClientDTO commandeClientDTO2 = commandeClientService.save(commandeClientMapper.toDto(commandeClient));
        commandeClient = commandeClientMapper.toEntity(commandeClientDTO2);
        return commandeClient;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static CommandeClient createUpdatedEntity(EntityManager em) {
        CommandeClient commandeClient = new CommandeClient().codePromo(UPDATED_CODE_PROMO);
        // Add required entity
        Client client;
        if (TestUtil.findAll(em, Client.class).isEmpty()) {
            client = ClientResourceIT.createUpdatedEntity(em);
            em.persist(client);
            em.flush();
        } else {
            client = TestUtil.findAll(em, Client.class).get(0);
        }
        commandeClient.setClient(client);
        // Add required entity
        Commande commande;
        if (TestUtil.findAll(em, Commande.class).isEmpty()) {
            commande = CommandeResourceIT.createUpdatedEntity(em);
            em.persist(commande);
            em.flush();
        } else {
            commande = TestUtil.findAll(em, Commande.class).get(0);
        }
        commandeClient.setCommande(commande);
        return commandeClient;
    }

    @BeforeEach
    public void initTest() {
        commandeClient = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommandeClient() throws Exception {
        int databaseSizeBeforeCreate = commandeClientRepository.findAll().size();
        CommandeClient commandeClient =  this.createEntityPropre();

        // Create the CommandeClient

        // Validate the CommandeClient in the database
        List<CommandeClient> commandeClientList = commandeClientRepository.findAll();
        assertThat(commandeClientList).hasSize(databaseSizeBeforeCreate + 1);
        CommandeClient testCommandeClient = commandeClientList.get(commandeClientList.size() - 1);
        assertThat(testCommandeClient.getCodePromo()).isEqualTo(DEFAULT_CODE_PROMO);

        // Validate the id for MapsId, the ids must be same
        assertThat(testCommandeClient.getId()).isEqualTo(testCommandeClient.getCommande().getId());

        // Validate the CommandeClient in Elasticsearch
        verify(mockCommandeClientSearchRepository, times(1)).save(testCommandeClient);
    }

    @Test
    @Transactional
    public void createCommandeClientWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commandeClientRepository.findAll().size();

        // Create the CommandeClient with an existing ID
        commandeClient.setId(1L);
        CommandeClientDTO commandeClientDTO = commandeClientMapper.toDto(commandeClient);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommandeClientMockMvc
                .perform(post("/api/commande-clients").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(commandeClientDTO)))
                .andExpect(status().isBadRequest());

        // Validate the CommandeClient in the database
        List<CommandeClient> commandeClientList = commandeClientRepository.findAll();
        assertThat(commandeClientList).hasSize(databaseSizeBeforeCreate);

        // Validate the CommandeClient in Elasticsearch
        verify(mockCommandeClientSearchRepository, times(0)).save(commandeClient);
    }

    @Test
    @Transactional
    public void getAllCommandeClients() throws Exception {
        // Initialize the database
        CommandeClient commandeClient =  this.createEntityPropre();

        // Get all the commandeClientList
        restCommandeClientMockMvc.perform(get("/api/commande-clients?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(commandeClient.getId().intValue())))
                .andExpect(jsonPath("$.[*].codePromo").value(hasItem(DEFAULT_CODE_PROMO)));
    }

    @Test
    @Transactional
    public void getCommandeClient() throws Exception {
        // Initialize the database
        CommandeClient commandeClient =  this.createEntityPropre();

        // Get the commandeClient
        restCommandeClientMockMvc.perform(get("/api/commande-clients/{id}", commandeClient.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(commandeClient.getId().intValue()))
                .andExpect(jsonPath("$.codePromo").value(DEFAULT_CODE_PROMO));
    }

    @Test
    @Transactional
    public void getNonExistingCommandeClient() throws Exception {
        // Get the commandeClient
        restCommandeClientMockMvc.perform(get("/api/commande-clients/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommandeClient() throws Exception {
        // Initialize the database
        CommandeClient commandeClient =  this.createEntityPropre();

        int databaseSizeBeforeUpdate = commandeClientRepository.findAll().size();

        // Update the commandeClient
        CommandeClient updatedCommandeClient = commandeClientRepository.findById(commandeClient.getId()).get();
        // Disconnect from session so that the updates on updatedCommandeClient are not
        // directly saved in db
        em.detach(updatedCommandeClient);
        updatedCommandeClient.codePromo(UPDATED_CODE_PROMO);
        CommandeClientDTO commandeClientDTO = commandeClientMapper.toDto(updatedCommandeClient);

        restCommandeClientMockMvc.perform(put("/api/commande-clients").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(commandeClientDTO))).andExpect(status().isOk());

        // Validate the CommandeClient in the database
        List<CommandeClient> commandeClientList = commandeClientRepository.findAll();
        assertThat(commandeClientList).hasSize(databaseSizeBeforeUpdate);
        CommandeClient testCommandeClient = commandeClientList.get(commandeClientList.size() - 1);
        assertThat(testCommandeClient.getCodePromo()).isEqualTo(UPDATED_CODE_PROMO);

        // Validate the CommandeClient in Elasticsearch
        verify(mockCommandeClientSearchRepository, times(2)).save(testCommandeClient);
    }

    @Test
    @Transactional
    public void updateNonExistingCommandeClient() throws Exception {
        int databaseSizeBeforeUpdate = commandeClientRepository.findAll().size();

        // Create the CommandeClient
        CommandeClientDTO commandeClientDTO = commandeClientMapper.toDto(commandeClient);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommandeClientMockMvc
                .perform(put("/api/commande-clients").contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(commandeClientDTO)))
                .andExpect(status().isBadRequest());

        // Validate the CommandeClient in the database
        List<CommandeClient> commandeClientList = commandeClientRepository.findAll();
        assertThat(commandeClientList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CommandeClient in Elasticsearch
        verify(mockCommandeClientSearchRepository, times(0)).save(commandeClient);
    }

    @Test
    @Transactional
    public void deleteCommandeClient() throws Exception {
        // Initialize the database
        CommandeClient commandeClient =  this.createEntityPropre();

        int databaseSizeBeforeDelete = commandeClientRepository.findAll().size();

        // Delete the commandeClient
        restCommandeClientMockMvc
                .perform(
                        delete("/api/commande-clients/{id}", commandeClient.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommandeClient> commandeClientList = commandeClientRepository.findAll();
        assertThat(commandeClientList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CommandeClient in Elasticsearch
        verify(mockCommandeClientSearchRepository, times(1)).deleteById(commandeClient.getId());
    }

    @Test
    @Transactional
    public void searchCommandeClient() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        CommandeClient commandeClient =  this.createEntityPropre();
        when(mockCommandeClientSearchRepository.search(queryStringQuery("id:" + commandeClient.getId())))
                .thenReturn(Collections.singletonList(commandeClient));

        // Search the commandeClient
        restCommandeClientMockMvc.perform(get("/api/_search/commande-clients?query=id:" + commandeClient.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(commandeClient.getId().intValue())))
                .andExpect(jsonPath("$.[*].codePromo").value(hasItem(DEFAULT_CODE_PROMO)));
    }
}
