package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.CategorieFourniture;
import fr.baag.goodfood.repository.CategorieFournitureRepository;
import fr.baag.goodfood.repository.search.CategorieFournitureSearchRepository;
import fr.baag.goodfood.service.CategorieFournitureService;
import fr.baag.goodfood.service.dto.CategorieFournitureDTO;
import fr.baag.goodfood.service.mapper.CategorieFournitureMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CategorieFournitureResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CategorieFournitureResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private CategorieFournitureRepository categorieFournitureRepository;

    @Autowired
    private CategorieFournitureMapper categorieFournitureMapper;

    @Autowired
    private CategorieFournitureService categorieFournitureService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test package.
     *
     * @see fr.baag.goodfood.repository.search.CategorieFournitureSearchRepositoryMockConfiguration
     */
    @Autowired
    private CategorieFournitureSearchRepository mockCategorieFournitureSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategorieFournitureMockMvc;

    private CategorieFourniture categorieFourniture;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieFourniture createEntity(EntityManager em) {
        CategorieFourniture categorieFourniture = new CategorieFourniture()
            .type(DEFAULT_TYPE);
        return categorieFourniture;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieFourniture createUpdatedEntity(EntityManager em) {
        CategorieFourniture categorieFourniture = new CategorieFourniture()
            .type(UPDATED_TYPE);
        return categorieFourniture;
    }

    @BeforeEach
    public void initTest() {
        categorieFourniture = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategorieFourniture() throws Exception {
        int databaseSizeBeforeCreate = categorieFournitureRepository.findAll().size();
        // Create the CategorieFourniture
        CategorieFournitureDTO categorieFournitureDTO = categorieFournitureMapper.toDto(categorieFourniture);
        restCategorieFournitureMockMvc.perform(post("/api/categorie-fournitures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieFournitureDTO)))
            .andExpect(status().isCreated());

        // Validate the CategorieFourniture in the database
        List<CategorieFourniture> categorieFournitureList = categorieFournitureRepository.findAll();
        assertThat(categorieFournitureList).hasSize(databaseSizeBeforeCreate + 1);
        CategorieFourniture testCategorieFourniture = categorieFournitureList.get(categorieFournitureList.size() - 1);
        assertThat(testCategorieFourniture.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the CategorieFourniture in Elasticsearch
        verify(mockCategorieFournitureSearchRepository, times(1)).save(testCategorieFourniture);
    }

    @Test
    @Transactional
    public void createCategorieFournitureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categorieFournitureRepository.findAll().size();

        // Create the CategorieFourniture with an existing ID
        categorieFourniture.setId(1L);
        CategorieFournitureDTO categorieFournitureDTO = categorieFournitureMapper.toDto(categorieFourniture);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategorieFournitureMockMvc.perform(post("/api/categorie-fournitures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieFournitureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieFourniture in the database
        List<CategorieFourniture> categorieFournitureList = categorieFournitureRepository.findAll();
        assertThat(categorieFournitureList).hasSize(databaseSizeBeforeCreate);

        // Validate the CategorieFourniture in Elasticsearch
        verify(mockCategorieFournitureSearchRepository, times(0)).save(categorieFourniture);
    }


    @Test
    @Transactional
    public void getAllCategorieFournitures() throws Exception {
        // Initialize the database
        categorieFournitureRepository.saveAndFlush(categorieFourniture);

        // Get all the categorieFournitureList
        restCategorieFournitureMockMvc.perform(get("/api/categorie-fournitures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieFourniture.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }
    
    @Test
    @Transactional
    public void getCategorieFourniture() throws Exception {
        // Initialize the database
        categorieFournitureRepository.saveAndFlush(categorieFourniture);

        // Get the categorieFourniture
        restCategorieFournitureMockMvc.perform(get("/api/categorie-fournitures/{id}", categorieFourniture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categorieFourniture.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }
    @Test
    @Transactional
    public void getNonExistingCategorieFourniture() throws Exception {
        // Get the categorieFourniture
        restCategorieFournitureMockMvc.perform(get("/api/categorie-fournitures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategorieFourniture() throws Exception {
        // Initialize the database
        categorieFournitureRepository.saveAndFlush(categorieFourniture);

        int databaseSizeBeforeUpdate = categorieFournitureRepository.findAll().size();

        // Update the categorieFourniture
        CategorieFourniture updatedCategorieFourniture = categorieFournitureRepository.findById(categorieFourniture.getId()).get();
        // Disconnect from session so that the updates on updatedCategorieFourniture are not directly saved in db
        em.detach(updatedCategorieFourniture);
        updatedCategorieFourniture
            .type(UPDATED_TYPE);
        CategorieFournitureDTO categorieFournitureDTO = categorieFournitureMapper.toDto(updatedCategorieFourniture);

        restCategorieFournitureMockMvc.perform(put("/api/categorie-fournitures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieFournitureDTO)))
            .andExpect(status().isOk());

        // Validate the CategorieFourniture in the database
        List<CategorieFourniture> categorieFournitureList = categorieFournitureRepository.findAll();
        assertThat(categorieFournitureList).hasSize(databaseSizeBeforeUpdate);
        CategorieFourniture testCategorieFourniture = categorieFournitureList.get(categorieFournitureList.size() - 1);
        assertThat(testCategorieFourniture.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the CategorieFourniture in Elasticsearch
        verify(mockCategorieFournitureSearchRepository, times(1)).save(testCategorieFourniture);
    }

    @Test
    @Transactional
    public void updateNonExistingCategorieFourniture() throws Exception {
        int databaseSizeBeforeUpdate = categorieFournitureRepository.findAll().size();

        // Create the CategorieFourniture
        CategorieFournitureDTO categorieFournitureDTO = categorieFournitureMapper.toDto(categorieFourniture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategorieFournitureMockMvc.perform(put("/api/categorie-fournitures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieFournitureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieFourniture in the database
        List<CategorieFourniture> categorieFournitureList = categorieFournitureRepository.findAll();
        assertThat(categorieFournitureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CategorieFourniture in Elasticsearch
        verify(mockCategorieFournitureSearchRepository, times(0)).save(categorieFourniture);
    }

    @Test
    @Transactional
    public void deleteCategorieFourniture() throws Exception {
        // Initialize the database
        categorieFournitureRepository.saveAndFlush(categorieFourniture);

        int databaseSizeBeforeDelete = categorieFournitureRepository.findAll().size();

        // Delete the categorieFourniture
        restCategorieFournitureMockMvc.perform(delete("/api/categorie-fournitures/{id}", categorieFourniture.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CategorieFourniture> categorieFournitureList = categorieFournitureRepository.findAll();
        assertThat(categorieFournitureList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CategorieFourniture in Elasticsearch
        verify(mockCategorieFournitureSearchRepository, times(1)).deleteById(categorieFourniture.getId());
    }

    @Test
    @Transactional
    public void searchCategorieFourniture() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        categorieFournitureRepository.saveAndFlush(categorieFourniture);
        when(mockCategorieFournitureSearchRepository.search(queryStringQuery("id:" + categorieFourniture.getId())))
            .thenReturn(Collections.singletonList(categorieFourniture));

        // Search the categorieFourniture
        restCategorieFournitureMockMvc.perform(get("/api/_search/categorie-fournitures?query=id:" + categorieFourniture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieFourniture.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }
}
