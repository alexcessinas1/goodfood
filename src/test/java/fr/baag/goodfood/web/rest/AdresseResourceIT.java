package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Adresse;
import fr.baag.goodfood.repository.AdresseRepository;
import fr.baag.goodfood.repository.search.AdresseSearchRepository;
import fr.baag.goodfood.service.AdresseService;
import fr.baag.goodfood.service.dto.AdresseDTO;
import fr.baag.goodfood.service.mapper.AdresseMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AdresseResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AdresseResourceIT {

    private static final String DEFAULT_NUMERO_VOIE = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_VOIE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM_VOIE = "AAAAAAAAAA";
    private static final String UPDATED_NOM_VOIE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_POSTAL = "AAAAAAAAAA";
    private static final String UPDATED_CODE_POSTAL = "BBBBBBBBBB";

    private static final String DEFAULT_VILLE = "AAAAAAAAAA";
    private static final String UPDATED_VILLE = "BBBBBBBBBB";

    private static final String DEFAULT_COMPLEMENT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_COMPLEMENT_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_INFO_COMPLEMENTAIRE = "AAAAAAAAAA";
    private static final String UPDATED_INFO_COMPLEMENTAIRE = "BBBBBBBBBB";

    @Autowired
    private AdresseRepository adresseRepository;

    @Autowired
    private AdresseMapper adresseMapper;

    @Autowired
    private AdresseService adresseService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test package.
     *
     * @see fr.baag.goodfood.repository.search.AdresseSearchRepositoryMockConfiguration
     */
    @Autowired
    private AdresseSearchRepository mockAdresseSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdresseMockMvc;

    private Adresse adresse;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Adresse createEntity(EntityManager em) {
        Adresse adresse = new Adresse()
            .numeroVoie(DEFAULT_NUMERO_VOIE)
            .nomVoie(DEFAULT_NOM_VOIE)
            .codePostal(DEFAULT_CODE_POSTAL)
            .ville(DEFAULT_VILLE)
            .complementAdresse(DEFAULT_COMPLEMENT_ADRESSE)
            .infoComplementaire(DEFAULT_INFO_COMPLEMENTAIRE);
        return adresse;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Adresse createUpdatedEntity(EntityManager em) {
        Adresse adresse = new Adresse()
            .numeroVoie(UPDATED_NUMERO_VOIE)
            .nomVoie(UPDATED_NOM_VOIE)
            .codePostal(UPDATED_CODE_POSTAL)
            .ville(UPDATED_VILLE)
            .complementAdresse(UPDATED_COMPLEMENT_ADRESSE)
            .infoComplementaire(UPDATED_INFO_COMPLEMENTAIRE);
        return adresse;
    }

    @BeforeEach
    public void initTest() {
        adresse = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdresse() throws Exception {
        int databaseSizeBeforeCreate = adresseRepository.findAll().size();
        // Create the Adresse
        AdresseDTO adresseDTO = adresseMapper.toDto(adresse);
        restAdresseMockMvc.perform(post("/api/adresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adresseDTO)))
            .andExpect(status().isCreated());

        // Validate the Adresse in the database
        List<Adresse> adresseList = adresseRepository.findAll();
        assertThat(adresseList).hasSize(databaseSizeBeforeCreate + 1);
        Adresse testAdresse = adresseList.get(adresseList.size() - 1);
        assertThat(testAdresse.getNumeroVoie()).isEqualTo(DEFAULT_NUMERO_VOIE);
        assertThat(testAdresse.getNomVoie()).isEqualTo(DEFAULT_NOM_VOIE);
        assertThat(testAdresse.getCodePostal()).isEqualTo(DEFAULT_CODE_POSTAL);
        assertThat(testAdresse.getVille()).isEqualTo(DEFAULT_VILLE);
        assertThat(testAdresse.getComplementAdresse()).isEqualTo(DEFAULT_COMPLEMENT_ADRESSE);
        assertThat(testAdresse.getInfoComplementaire()).isEqualTo(DEFAULT_INFO_COMPLEMENTAIRE);

        // Validate the Adresse in Elasticsearch
        verify(mockAdresseSearchRepository, times(1)).save(testAdresse);
    }

    @Test
    @Transactional
    public void createAdresseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = adresseRepository.findAll().size();

        // Create the Adresse with an existing ID
        adresse.setId(1L);
        AdresseDTO adresseDTO = adresseMapper.toDto(adresse);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdresseMockMvc.perform(post("/api/adresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adresseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Adresse in the database
        List<Adresse> adresseList = adresseRepository.findAll();
        assertThat(adresseList).hasSize(databaseSizeBeforeCreate);

        // Validate the Adresse in Elasticsearch
        verify(mockAdresseSearchRepository, times(0)).save(adresse);
    }


    @Test
    @Transactional
    public void getAllAdresses() throws Exception {
        // Initialize the database
        adresseRepository.saveAndFlush(adresse);

        // Get all the adresseList
        restAdresseMockMvc.perform(get("/api/adresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adresse.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroVoie").value(hasItem(DEFAULT_NUMERO_VOIE)))
            .andExpect(jsonPath("$.[*].nomVoie").value(hasItem(DEFAULT_NOM_VOIE)))
            .andExpect(jsonPath("$.[*].codePostal").value(hasItem(DEFAULT_CODE_POSTAL)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].complementAdresse").value(hasItem(DEFAULT_COMPLEMENT_ADRESSE)))
            .andExpect(jsonPath("$.[*].infoComplementaire").value(hasItem(DEFAULT_INFO_COMPLEMENTAIRE)));
    }
    
    @Test
    @Transactional
    public void getAdresse() throws Exception {
        // Initialize the database
        adresseRepository.saveAndFlush(adresse);

        // Get the adresse
        restAdresseMockMvc.perform(get("/api/adresses/{id}", adresse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(adresse.getId().intValue()))
            .andExpect(jsonPath("$.numeroVoie").value(DEFAULT_NUMERO_VOIE))
            .andExpect(jsonPath("$.nomVoie").value(DEFAULT_NOM_VOIE))
            .andExpect(jsonPath("$.codePostal").value(DEFAULT_CODE_POSTAL))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE))
            .andExpect(jsonPath("$.complementAdresse").value(DEFAULT_COMPLEMENT_ADRESSE))
            .andExpect(jsonPath("$.infoComplementaire").value(DEFAULT_INFO_COMPLEMENTAIRE));
    }
    @Test
    @Transactional
    public void getNonExistingAdresse() throws Exception {
        // Get the adresse
        restAdresseMockMvc.perform(get("/api/adresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdresse() throws Exception {
        // Initialize the database
        adresseRepository.saveAndFlush(adresse);

        int databaseSizeBeforeUpdate = adresseRepository.findAll().size();

        // Update the adresse
        Adresse updatedAdresse = adresseRepository.findById(adresse.getId()).get();
        // Disconnect from session so that the updates on updatedAdresse are not directly saved in db
        em.detach(updatedAdresse);
        updatedAdresse
            .numeroVoie(UPDATED_NUMERO_VOIE)
            .nomVoie(UPDATED_NOM_VOIE)
            .codePostal(UPDATED_CODE_POSTAL)
            .ville(UPDATED_VILLE)
            .complementAdresse(UPDATED_COMPLEMENT_ADRESSE)
            .infoComplementaire(UPDATED_INFO_COMPLEMENTAIRE);
        AdresseDTO adresseDTO = adresseMapper.toDto(updatedAdresse);

        restAdresseMockMvc.perform(put("/api/adresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adresseDTO)))
            .andExpect(status().isOk());

        // Validate the Adresse in the database
        List<Adresse> adresseList = adresseRepository.findAll();
        assertThat(adresseList).hasSize(databaseSizeBeforeUpdate);
        Adresse testAdresse = adresseList.get(adresseList.size() - 1);
        assertThat(testAdresse.getNumeroVoie()).isEqualTo(UPDATED_NUMERO_VOIE);
        assertThat(testAdresse.getNomVoie()).isEqualTo(UPDATED_NOM_VOIE);
        assertThat(testAdresse.getCodePostal()).isEqualTo(UPDATED_CODE_POSTAL);
        assertThat(testAdresse.getVille()).isEqualTo(UPDATED_VILLE);
        assertThat(testAdresse.getComplementAdresse()).isEqualTo(UPDATED_COMPLEMENT_ADRESSE);
        assertThat(testAdresse.getInfoComplementaire()).isEqualTo(UPDATED_INFO_COMPLEMENTAIRE);

        // Validate the Adresse in Elasticsearch
        verify(mockAdresseSearchRepository, times(1)).save(testAdresse);
    }

    @Test
    @Transactional
    public void updateNonExistingAdresse() throws Exception {
        int databaseSizeBeforeUpdate = adresseRepository.findAll().size();

        // Create the Adresse
        AdresseDTO adresseDTO = adresseMapper.toDto(adresse);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdresseMockMvc.perform(put("/api/adresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adresseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Adresse in the database
        List<Adresse> adresseList = adresseRepository.findAll();
        assertThat(adresseList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Adresse in Elasticsearch
        verify(mockAdresseSearchRepository, times(0)).save(adresse);
    }

    @Test
    @Transactional
    public void deleteAdresse() throws Exception {
        // Initialize the database
        adresseRepository.saveAndFlush(adresse);

        int databaseSizeBeforeDelete = adresseRepository.findAll().size();

        // Delete the adresse
        restAdresseMockMvc.perform(delete("/api/adresses/{id}", adresse.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Adresse> adresseList = adresseRepository.findAll();
        assertThat(adresseList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Adresse in Elasticsearch
        verify(mockAdresseSearchRepository, times(1)).deleteById(adresse.getId());
    }

    @Test
    @Transactional
    public void searchAdresse() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        adresseRepository.saveAndFlush(adresse);
        when(mockAdresseSearchRepository.search(queryStringQuery("id:" + adresse.getId())))
            .thenReturn(Collections.singletonList(adresse));

        // Search the adresse
        restAdresseMockMvc.perform(get("/api/_search/adresses?query=id:" + adresse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adresse.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroVoie").value(hasItem(DEFAULT_NUMERO_VOIE)))
            .andExpect(jsonPath("$.[*].nomVoie").value(hasItem(DEFAULT_NOM_VOIE)))
            .andExpect(jsonPath("$.[*].codePostal").value(hasItem(DEFAULT_CODE_POSTAL)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].complementAdresse").value(hasItem(DEFAULT_COMPLEMENT_ADRESSE)))
            .andExpect(jsonPath("$.[*].infoComplementaire").value(hasItem(DEFAULT_INFO_COMPLEMENTAIRE)));
    }
}
