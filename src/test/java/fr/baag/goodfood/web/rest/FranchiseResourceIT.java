package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Franchise;
import fr.baag.goodfood.domain.Adresse;
import fr.baag.goodfood.domain.Produit;
import fr.baag.goodfood.repository.FranchiseRepository;
import fr.baag.goodfood.repository.ProduitRepository;
import fr.baag.goodfood.repository.search.FranchiseSearchRepository;
import fr.baag.goodfood.service.FranchiseService;
import fr.baag.goodfood.service.dto.FranchiseDTO;
import fr.baag.goodfood.service.mapper.FranchiseMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FranchiseResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class FranchiseResourceIT {

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_HORAIRE = "AAAAAAAAAA";
    private static final String UPDATED_HORAIRE = "BBBBBBBBBB";

    private static final String DEFAULT_SIRET = "AAAAAAAAAA";
    private static final String UPDATED_SIRET = "BBBBBBBBBB";

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Mock
    private FranchiseRepository franchiseRepositoryMock;

    @Autowired
    private FranchiseMapper franchiseMapper;

    @Mock
    private FranchiseService franchiseServiceMock;

    @Autowired
    private FranchiseService franchiseService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.FranchiseSearchRepositoryMockConfiguration
     */
    @Autowired
    private FranchiseSearchRepository mockFranchiseSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFranchiseMockMvc;

    private Franchise franchise;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Franchise createEntity(EntityManager em) {
        Franchise franchise = new Franchise().telephone(DEFAULT_TELEPHONE).email(DEFAULT_EMAIL).horaire(DEFAULT_HORAIRE)
                .siret(DEFAULT_SIRET);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        franchise.setAdresseFranchiseId(adresse);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createEntity(em);
            // em.persist(produit);
            // em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        franchise.getStockFranchises().add(produit);
        return franchise;
    }

    public static Franchise createEntitySansProduit(EntityManager em) {
        Franchise franchise = new Franchise().telephone(DEFAULT_TELEPHONE).email(DEFAULT_EMAIL).horaire(DEFAULT_HORAIRE)
                .siret(DEFAULT_SIRET);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        franchise.setAdresseFranchiseId(adresse);
        return franchise;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Franchise createUpdatedEntity(EntityManager em) {
        Franchise franchise = new Franchise().telephone(UPDATED_TELEPHONE).email(UPDATED_EMAIL).horaire(UPDATED_HORAIRE)
                .siret(UPDATED_SIRET);
        // Add required entity
        Adresse adresse;
        if (TestUtil.findAll(em, Adresse.class).isEmpty()) {
            adresse = AdresseResourceIT.createUpdatedEntity(em);
            em.persist(adresse);
            em.flush();
        } else {
            adresse = TestUtil.findAll(em, Adresse.class).get(0);
        }
        franchise.setAdresseFranchiseId(adresse);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createUpdatedEntity(em);
            // em.persist(produit);
            // em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        franchise.getStockFranchises().add(produit);
        return franchise;
    }

    @BeforeEach
    public void initTest() {
        franchise = createEntity(em);
    }

    @Test
    @Transactional
    public void createFranchise() throws Exception {
        int databaseSizeBeforeCreate = franchiseRepository.findAll().size();
        // Create the Franchise
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);
        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isCreated());

        // Validate the Franchise in the database
        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeCreate + 1);
        Franchise testFranchise = franchiseList.get(franchiseList.size() - 1);
        assertThat(testFranchise.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testFranchise.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testFranchise.getHoraire()).isEqualTo(DEFAULT_HORAIRE);
        assertThat(testFranchise.getSiret()).isEqualTo(DEFAULT_SIRET);

        // Validate the Franchise in Elasticsearch
        verify(mockFranchiseSearchRepository, times(1)).save(testFranchise);
    }

    @Test
    @Transactional
    public void createFranchiseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = franchiseRepository.findAll().size();

        // Create the Franchise with an existing ID
        franchise.setId(1L);
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        // Validate the Franchise in the database
        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeCreate);

        // Validate the Franchise in Elasticsearch
        verify(mockFranchiseSearchRepository, times(0)).save(franchise);
    }

    @Test
    @Transactional
    public void checkTelephoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = franchiseRepository.findAll().size();
        // set the field null
        franchise.setTelephone(null);

        // Create the Franchise, which fails.
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = franchiseRepository.findAll().size();
        // set the field null
        franchise.setEmail(null);

        // Create the Franchise, which fails.
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHoraireIsRequired() throws Exception {
        int databaseSizeBeforeTest = franchiseRepository.findAll().size();
        // set the field null
        franchise.setHoraire(null);

        // Create the Franchise, which fails.
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSiretIsRequired() throws Exception {
        int databaseSizeBeforeTest = franchiseRepository.findAll().size();
        // set the field null
        franchise.setSiret(null);

        // Create the Franchise, which fails.
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        restFranchiseMockMvc.perform(post("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFranchises() throws Exception {
        // Initialize the database
        franchiseRepository.save(franchise);

        // Get all the franchiseList
        restFranchiseMockMvc.perform(get("/api/franchises?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(franchise.getId().intValue())))
                .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
                .andExpect(jsonPath("$.[*].horaire").value(hasItem(DEFAULT_HORAIRE)))
                .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET)));
    }

    @SuppressWarnings({ "unchecked" })
    public void getAllFranchisesWithEagerRelationshipsIsEnabled() throws Exception {
        when(franchiseServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restFranchiseMockMvc.perform(get("/api/franchises?eagerload=true")).andExpect(status().isOk());

        verify(franchiseServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    public void getAllFranchisesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(franchiseServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restFranchiseMockMvc.perform(get("/api/franchises?eagerload=true")).andExpect(status().isOk());

        verify(franchiseServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getFranchise() throws Exception {
        franchise = createEntitySansProduit(em);
        // Initialize the database
        franchiseRepository.save(franchise);

        // Get the franchise
        restFranchiseMockMvc.perform(get("/api/franchises/{id}", franchise.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(franchise.getId().intValue()))
                .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
                .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
                .andExpect(jsonPath("$.horaire").value(DEFAULT_HORAIRE))
                .andExpect(jsonPath("$.siret").value(DEFAULT_SIRET));
    }

    @Test
    @Transactional
    public void getNonExistingFranchise() throws Exception {
        // Get the franchise
        restFranchiseMockMvc.perform(get("/api/franchises/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFranchise() throws Exception {
        // Initialize the database
        franchise = createEntitySansProduit(em);
        franchiseRepository.save(franchise);

        int databaseSizeBeforeUpdate = franchiseRepository.findAll().size();

        // Update the franchise
        Franchise updatedFranchise = franchiseRepository.findById(franchise.getId()).get();
        // Disconnect from session so that the updates on updatedFranchise are not
        // directly saved in db
        em.detach(updatedFranchise);
        updatedFranchise.telephone(UPDATED_TELEPHONE).email(UPDATED_EMAIL).horaire(UPDATED_HORAIRE)
                .siret(UPDATED_SIRET);
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(updatedFranchise);

        restFranchiseMockMvc.perform(put("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isOk());

        // Validate the Franchise in the database
        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeUpdate);
        Franchise testFranchise = franchiseList.get(franchiseList.size() - 1);
        assertThat(testFranchise.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testFranchise.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testFranchise.getHoraire()).isEqualTo(UPDATED_HORAIRE);
        assertThat(testFranchise.getSiret()).isEqualTo(UPDATED_SIRET);

        // Validate the Franchise in Elasticsearch
        verify(mockFranchiseSearchRepository, times(1)).save(testFranchise);
    }

    @Test
    @Transactional
    public void updateNonExistingFranchise() throws Exception {
        int databaseSizeBeforeUpdate = franchiseRepository.findAll().size();

        // Create the Franchise
        FranchiseDTO franchiseDTO = franchiseMapper.toDto(franchise);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFranchiseMockMvc.perform(put("/api/franchises").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(franchiseDTO))).andExpect(status().isBadRequest());

        // Validate the Franchise in the database
        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Franchise in Elasticsearch
        verify(mockFranchiseSearchRepository, times(0)).save(franchise);
    }

    @Test
    @Transactional
    public void deleteFranchise() throws Exception {
        // Initialize the database
        franchiseRepository.save(franchise);

        int databaseSizeBeforeDelete = franchiseRepository.findAll().size();

        // Delete the franchise
        restFranchiseMockMvc
                .perform(delete("/api/franchises/{id}", franchise.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Franchise> franchiseList = franchiseRepository.findAll();
        assertThat(franchiseList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Franchise in Elasticsearch
        verify(mockFranchiseSearchRepository, times(1)).deleteById(franchise.getId());
    }

    @Test
    @Transactional
    public void searchFranchise() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        franchiseRepository.save(franchise);
        when(mockFranchiseSearchRepository.search(queryStringQuery("id:" + franchise.getId()), PageRequest.of(0, 20)))
                .thenReturn(new PageImpl<>(Collections.singletonList(franchise), PageRequest.of(0, 1), 1));

        // Search the franchise
        restFranchiseMockMvc.perform(get("/api/_search/franchises?query=id:" + franchise.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(franchise.getId().intValue())))
                .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
                .andExpect(jsonPath("$.[*].horaire").value(hasItem(DEFAULT_HORAIRE)))
                .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET)));
    }
}
