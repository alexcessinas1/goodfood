package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Allergene;
import fr.baag.goodfood.repository.AllergeneRepository;
import fr.baag.goodfood.repository.search.AllergeneSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.baag.goodfood.domain.enumeration.AllergeneEnum;

/**
 * Integration tests for the {@link AllergeneResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AllergeneResourceIT {

    private static final AllergeneEnum DEFAULT_NOM = AllergeneEnum.ARACHIDE;
    private static final AllergeneEnum UPDATED_NOM = AllergeneEnum.LAIT;

    @Autowired
    private AllergeneRepository allergeneRepository;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.AllergeneSearchRepositoryMockConfiguration
     */
    @Autowired
    private AllergeneSearchRepository mockAllergeneSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAllergeneMockMvc;

    private Allergene allergene;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Allergene createEntity(EntityManager em) {
        Allergene allergene = new Allergene().nom(DEFAULT_NOM);
        return allergene;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Allergene createUpdatedEntity(EntityManager em) {
        Allergene allergene = new Allergene().nom(UPDATED_NOM);
        return allergene;
    }

    @BeforeEach
    public void initTest() {
        allergene = createEntity(em);
    }

    @Test
    @Transactional
    public void createAllergene() throws Exception {
        int databaseSizeBeforeCreate = allergeneRepository.findAll().size();
        // Create the Allergene
        restAllergeneMockMvc.perform(post("/api/allergenes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(allergene))).andExpect(status().isCreated());

        // Validate the Allergene in the database
        List<Allergene> allergeneList = allergeneRepository.findAll();
        assertThat(allergeneList).hasSize(databaseSizeBeforeCreate + 1);
        Allergene testAllergene = allergeneList.get(allergeneList.size() - 1);
        assertThat(testAllergene.getNom()).isEqualTo(DEFAULT_NOM);

        // Validate the Allergene in Elasticsearch
        verify(mockAllergeneSearchRepository, times(1)).save(testAllergene);
    }

    @Test
    @Transactional
    public void createAllergeneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = allergeneRepository.findAll().size();

        // Create the Allergene with an existing ID
        allergene.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAllergeneMockMvc.perform(post("/api/allergenes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(allergene))).andExpect(status().isBadRequest());

        // Validate the Allergene in the database
        List<Allergene> allergeneList = allergeneRepository.findAll();
        assertThat(allergeneList).hasSize(databaseSizeBeforeCreate);

        // Validate the Allergene in Elasticsearch
        verify(mockAllergeneSearchRepository, times(0)).save(allergene);
    }

    @Test
    @Transactional
    public void getAllAllergenes() throws Exception {
        // Initialize the database
        allergeneRepository.saveAndFlush(allergene);

        // Get all the allergeneList
        restAllergeneMockMvc.perform(get("/api/allergenes?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(allergene.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }

    @Test
    @Transactional
    public void getAllergene() throws Exception {
        // Initialize the database
        allergeneRepository.saveAndFlush(allergene);

        // Get the allergene
        restAllergeneMockMvc.perform(get("/api/allergenes/{id}", allergene.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(allergene.getId().intValue()))
                .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAllergene() throws Exception {
        // Get the allergene
        restAllergeneMockMvc.perform(get("/api/allergenes/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAllergene() throws Exception {
        // Initialize the database
        allergeneRepository.saveAndFlush(allergene);

        int databaseSizeBeforeUpdate = allergeneRepository.findAll().size();

        // Update the allergene
        Allergene updatedAllergene = allergeneRepository.findById(allergene.getId()).get();
        // Disconnect from session so that the updates on updatedAllergene are not
        // directly saved in db
        em.detach(updatedAllergene);
        updatedAllergene.nom(UPDATED_NOM);

        restAllergeneMockMvc.perform(put("/api/allergenes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(updatedAllergene))).andExpect(status().isOk());

        // Validate the Allergene in the database
        List<Allergene> allergeneList = allergeneRepository.findAll();
        assertThat(allergeneList).hasSize(databaseSizeBeforeUpdate);
        Allergene testAllergene = allergeneList.get(allergeneList.size() - 1);
        assertThat(testAllergene.getNom()).isEqualTo(UPDATED_NOM);

        // Validate the Allergene in Elasticsearch
        verify(mockAllergeneSearchRepository, times(1)).save(testAllergene);
    }

    @Test
    @Transactional
    public void updateNonExistingAllergene() throws Exception {
        int databaseSizeBeforeUpdate = allergeneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAllergeneMockMvc.perform(put("/api/allergenes").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(allergene))).andExpect(status().isBadRequest());

        // Validate the Allergene in the database
        List<Allergene> allergeneList = allergeneRepository.findAll();
        assertThat(allergeneList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Allergene in Elasticsearch
        verify(mockAllergeneSearchRepository, times(0)).save(allergene);
    }

    @Test
    @Transactional
    public void deleteAllergene() throws Exception {
        // Initialize the database
        allergeneRepository.saveAndFlush(allergene);

        int databaseSizeBeforeDelete = allergeneRepository.findAll().size();

        // Delete the allergene
        restAllergeneMockMvc
                .perform(delete("/api/allergenes/{id}", allergene.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Allergene> allergeneList = allergeneRepository.findAll();
        assertThat(allergeneList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Allergene in Elasticsearch
        verify(mockAllergeneSearchRepository, times(1)).deleteById(allergene.getId());
    }

    @Test
    @Transactional
    public void searchAllergene() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        allergeneRepository.saveAndFlush(allergene);
        when(mockAllergeneSearchRepository.search(queryStringQuery("id:" + allergene.getId())))
                .thenReturn(Collections.singletonList(allergene));

        // Search the allergene
        restAllergeneMockMvc.perform(get("/api/_search/allergenes?query=id:" + allergene.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(allergene.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }
}
