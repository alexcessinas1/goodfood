package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.Produit;
import fr.baag.goodfood.repository.AllergeneRepository;
import fr.baag.goodfood.repository.ProduitRepository;
import fr.baag.goodfood.repository.search.ProduitSearchRepository;
import fr.baag.goodfood.service.ProduitService;
import fr.baag.goodfood.service.dto.ProduitDTO;
import fr.baag.goodfood.service.mapper.ProduitMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.baag.goodfood.domain.enumeration.unite;
import fr.baag.goodfood.domain.enumeration.ProduitCategorie;
import fr.baag.goodfood.domain.enumeration.AllergeneEnum;
import fr.baag.goodfood.domain.Allergene;

/**
 * Integration tests for the {@link ProduitResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProduitResourceIT {

    private static final String DEFAULT_EID = "AAAAAAAAAA";
    private static final String UPDATED_EID = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final Float DEFAULT_QUANTITE = 1F;
    private static final Float UPDATED_QUANTITE = 2F;

    private static final unite DEFAULT_QUANTITE_UNITE = unite.LITRE;
    private static final unite UPDATED_QUANTITE_UNITE = unite.KG;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Float DEFAULT_PRIX_CATALOGUE = 1F;
    private static final Float UPDATED_PRIX_CATALOGUE = 2F;

    private static final Float DEFAULT_PRIX_STANDARD = 1F;
    private static final Float UPDATED_PRIX_STANDARD = 2F;

    private static final Boolean DEFAULT_VENTE_OUVERTE = false;
    private static final Boolean UPDATED_VENTE_OUVERTE = true;

    private static final String DEFAULT_ORIGINE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINE = "BBBBBBBBBB";

    private static final ProduitCategorie DEFAULT_PRODUIT_CATEGORIE = ProduitCategorie.BOISSON;
    private static final ProduitCategorie UPDATED_PRODUIT_CATEGORIE = ProduitCategorie.ALIMENTATION;

    private static final AllergeneEnum DEFAULT_ALLERGENES = AllergeneEnum.ARACHIDE;
    private static final AllergeneEnum UPDATED_ALLERGENES = AllergeneEnum.LAIT;

    private static final String DEFAULT_NUMERO_LOT = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_LOT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_LOT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_LOT = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private AllergeneRepository allergeneRepository;

    @Autowired
    private ProduitMapper produitMapper;

    @Autowired
    private ProduitService produitService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test
     * package.
     *
     * @see fr.baag.goodfood.repository.search.ProduitSearchRepositoryMockConfiguration
     */
    @Autowired
    private ProduitSearchRepository mockProduitSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProduitMockMvc;

    private Produit produit;
    private Allergene allergene;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Produit createEntity(EntityManager em) {

        Set<Allergene> als = new HashSet<>();
        Allergene al = createAllergeneEntity(em);

        // // al.setId(1L);
        // al.setNom(DEFAULT_ALLERGENES);
        als.add(al);
        Produit produit = new Produit().eid(DEFAULT_EID).nom(DEFAULT_NOM).quantite(DEFAULT_QUANTITE)
                .quantiteUnite(DEFAULT_QUANTITE_UNITE).description(DEFAULT_DESCRIPTION)
                .prixCatalogue(DEFAULT_PRIX_CATALOGUE).prixStandard(DEFAULT_PRIX_STANDARD)
                .venteOuverte(DEFAULT_VENTE_OUVERTE).origine(DEFAULT_ORIGINE)
                .produitCategorie(DEFAULT_PRODUIT_CATEGORIE).allergenes(als).numeroLot(DEFAULT_NUMERO_LOT)
                .dateLot(DEFAULT_DATE_LOT);
        return produit;
    }

    /**
     * Create an Allergene entity for this produit test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Allergene createAllergeneEntity(EntityManager em) {
        Allergene al = new Allergene();
        al.setId(1L);
        al.setNom(DEFAULT_ALLERGENES);
        return al;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Produit createUpdatedEntity(EntityManager em) {
        Set<Allergene> als = new HashSet<>();
        Allergene al = new Allergene();
        al.setNom(UPDATED_ALLERGENES);
        als.add(al);
        Produit produit = new Produit().eid(UPDATED_EID).nom(UPDATED_NOM).quantite(UPDATED_QUANTITE)
                .quantiteUnite(UPDATED_QUANTITE_UNITE).description(UPDATED_DESCRIPTION)
                .prixCatalogue(UPDATED_PRIX_CATALOGUE).prixStandard(UPDATED_PRIX_STANDARD)
                .venteOuverte(UPDATED_VENTE_OUVERTE).origine(UPDATED_ORIGINE)
                .produitCategorie(UPDATED_PRODUIT_CATEGORIE).allergenes(als).numeroLot(UPDATED_NUMERO_LOT)
                .dateLot(UPDATED_DATE_LOT);
        return produit;
    }

    @BeforeEach
    public void initTest() {
        // allergene = createAllergeneEntity(em);
        produit = createEntity(em);
    }

    @Test
    @Transactional
    public void createProduit() throws Exception {
        int databaseSizeBeforeCreate = produitRepository.findAll().size();
        // Create the Produit
        ProduitDTO produitDTO = produitMapper.toDto(produit);
        restProduitMockMvc.perform(post("/api/produits").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(produitDTO))).andExpect(status().isCreated());
        Set<Allergene> als = new HashSet<>();
        Allergene al = new Allergene();
        al.setNom(DEFAULT_ALLERGENES);
        als.add(al);
        // Validate the Produit in the database
        List<Produit> produitList = produitRepository.findAll();
        assertThat(produitList).hasSize(databaseSizeBeforeCreate + 1);
        Produit testProduit = produitList.get(produitList.size() - 1);
        assertThat(testProduit.getEid()).isEqualTo(DEFAULT_EID);
        assertThat(testProduit.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testProduit.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testProduit.getQuantiteUnite()).isEqualTo(DEFAULT_QUANTITE_UNITE);
        assertThat(testProduit.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProduit.getPrixCatalogue()).isEqualTo(DEFAULT_PRIX_CATALOGUE);
        assertThat(testProduit.getPrixStandard()).isEqualTo(DEFAULT_PRIX_STANDARD);
        assertThat(testProduit.isVenteOuverte()).isEqualTo(DEFAULT_VENTE_OUVERTE);
        assertThat(testProduit.getOrigine()).isEqualTo(DEFAULT_ORIGINE);
        assertThat(testProduit.getProduitCategorie()).isEqualTo(DEFAULT_PRODUIT_CATEGORIE);
        assertThat(testProduit.getAllergenes().size()).isEqualTo(als.size());
        assertThat(testProduit.getNumeroLot()).isEqualTo(DEFAULT_NUMERO_LOT);
        assertThat(testProduit.getDateLot()).isEqualTo(DEFAULT_DATE_LOT);

        // Validate the Produit in Elasticsearch
        verify(mockProduitSearchRepository, times(1)).save(testProduit);
    }

    @Test
    @Transactional
    public void createProduitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = produitRepository.findAll().size();

        // Create the Produit with an existing ID
        produit.setId(1L);
        ProduitDTO produitDTO = produitMapper.toDto(produit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProduitMockMvc.perform(post("/api/produits").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(produitDTO))).andExpect(status().isBadRequest());

        // Validate the Produit in the database
        List<Produit> produitList = produitRepository.findAll();
        assertThat(produitList).hasSize(databaseSizeBeforeCreate);

        // Validate the Produit in Elasticsearch
        verify(mockProduitSearchRepository, times(0)).save(produit);
    }

    @Test
    @Transactional
    public void getAllProduits() throws Exception {
        // Initialize the database
        produit = produitRepository.save(produit);

        // Get all the produitList
        restProduitMockMvc.perform(get("/api/produits?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(produit.getId().intValue())))
                .andExpect(jsonPath("$.[*].eid").value(hasItem(DEFAULT_EID)))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
                .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE.doubleValue())))
                .andExpect(jsonPath("$.[*].quantiteUnite").value(hasItem(DEFAULT_QUANTITE_UNITE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].prixCatalogue").value(hasItem(DEFAULT_PRIX_CATALOGUE.doubleValue())))
                .andExpect(jsonPath("$.[*].prixStandard").value(hasItem(DEFAULT_PRIX_STANDARD.doubleValue())))
                .andExpect(jsonPath("$.[*].venteOuverte").value(hasItem(DEFAULT_VENTE_OUVERTE.booleanValue())))
                .andExpect(jsonPath("$.[*].origine").value(hasItem(DEFAULT_ORIGINE)))
                .andExpect(jsonPath("$.[*].produitCategorie").value(hasItem(DEFAULT_PRODUIT_CATEGORIE.toString())))
                .andExpect(jsonPath("$.[*].allergenes[0].nom").value(hasItem(DEFAULT_ALLERGENES.toString())))
                .andExpect(jsonPath("$.[*].numeroLot").value(hasItem(DEFAULT_NUMERO_LOT)))
                .andExpect(jsonPath("$.[*].dateLot").value(hasItem(DEFAULT_DATE_LOT.toString())));
    }

    @Test
    @Transactional
    public void getProduit() throws Exception {
        // Initialize the database
        produit = produitRepository.save(produit);

        // Get the produit
        restProduitMockMvc.perform(get("/api/produits/{id}", produit.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(produit.getId().intValue()))
                .andExpect(jsonPath("$.eid").value(DEFAULT_EID)).andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
                .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE.doubleValue()))
                .andExpect(jsonPath("$.quantiteUnite").value(DEFAULT_QUANTITE_UNITE.toString()))
                .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
                .andExpect(jsonPath("$.prixCatalogue").value(DEFAULT_PRIX_CATALOGUE.doubleValue()))
                .andExpect(jsonPath("$.prixStandard").value(DEFAULT_PRIX_STANDARD.doubleValue()))
                .andExpect(jsonPath("$.venteOuverte").value(DEFAULT_VENTE_OUVERTE.booleanValue()))
                .andExpect(jsonPath("$.origine").value(DEFAULT_ORIGINE))
                .andExpect(jsonPath("$.produitCategorie").value(DEFAULT_PRODUIT_CATEGORIE.toString()))
                .andExpect(jsonPath("$.allergenes[0].nom").value(DEFAULT_ALLERGENES.toString()))
                .andExpect(jsonPath("$.numeroLot").value(DEFAULT_NUMERO_LOT))
                .andExpect(jsonPath("$.dateLot").value(DEFAULT_DATE_LOT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProduit() throws Exception {
        // Get the produit
        restProduitMockMvc.perform(get("/api/produits/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProduit() throws Exception {
        // Initialize the database
        produit = produitRepository.save(produit);

        int databaseSizeBeforeUpdate = produitRepository.findAll().size();

        // Update the produit
        Produit updatedProduit = produitRepository.findById(produit.getId()).get();
        Set<Allergene> als = new HashSet<>();
        Allergene al = new Allergene();
        al.setNom(UPDATED_ALLERGENES);
        al = allergeneRepository.save(al);
        als.add(al);
        // Disconnect from session so that the updates on updatedProduit are not
        // directly saved in db
        em.detach(updatedProduit);
        updatedProduit.eid(UPDATED_EID).nom(UPDATED_NOM).quantite(UPDATED_QUANTITE)
                .quantiteUnite(UPDATED_QUANTITE_UNITE).description(UPDATED_DESCRIPTION)
                .prixCatalogue(UPDATED_PRIX_CATALOGUE).prixStandard(UPDATED_PRIX_STANDARD)
                .venteOuverte(UPDATED_VENTE_OUVERTE).origine(UPDATED_ORIGINE)
                .produitCategorie(UPDATED_PRODUIT_CATEGORIE).allergenes(als).numeroLot(UPDATED_NUMERO_LOT)
                .dateLot(UPDATED_DATE_LOT);
        ProduitDTO produitDTO = produitMapper.toDto(updatedProduit);

        restProduitMockMvc.perform(put("/api/produits").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(produitDTO))).andExpect(status().isOk());

        // Validate the Produit in the database
        List<Produit> produitList = produitRepository.findAll();
        assertThat(produitList).hasSize(databaseSizeBeforeUpdate);
        Produit testProduit = produitList.get(produitList.size() - 1);
        assertThat(testProduit.getEid()).isEqualTo(UPDATED_EID);
        assertThat(testProduit.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testProduit.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testProduit.getQuantiteUnite()).isEqualTo(UPDATED_QUANTITE_UNITE);
        assertThat(testProduit.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProduit.getPrixCatalogue()).isEqualTo(UPDATED_PRIX_CATALOGUE);
        assertThat(testProduit.getPrixStandard()).isEqualTo(UPDATED_PRIX_STANDARD);
        assertThat(testProduit.isVenteOuverte()).isEqualTo(UPDATED_VENTE_OUVERTE);
        assertThat(testProduit.getOrigine()).isEqualTo(UPDATED_ORIGINE);
        assertThat(testProduit.getProduitCategorie()).isEqualTo(UPDATED_PRODUIT_CATEGORIE);
        assertThat(testProduit.getAllergenes()).isEqualTo(als);
        assertThat(testProduit.getNumeroLot()).isEqualTo(UPDATED_NUMERO_LOT);
        assertThat(testProduit.getDateLot()).isEqualTo(UPDATED_DATE_LOT);

        // Validate the Produit in Elasticsearch
        verify(mockProduitSearchRepository, times(1)).save(testProduit);
    }

    @Test
    @Transactional
    public void updateNonExistingProduit() throws Exception {
        int databaseSizeBeforeUpdate = produitRepository.findAll().size();

        // Create the Produit
        ProduitDTO produitDTO = produitMapper.toDto(produit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProduitMockMvc.perform(put("/api/produits").contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(produitDTO))).andExpect(status().isBadRequest());

        // Validate the Produit in the database
        List<Produit> produitList = produitRepository.findAll();
        assertThat(produitList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Produit in Elasticsearch
        verify(mockProduitSearchRepository, times(0)).save(produit);
    }

    @Test
    @Transactional
    public void deleteProduit() throws Exception {
        // Initialize the database
        produitRepository.save(produit);

        int databaseSizeBeforeDelete = produitRepository.findAll().size();

        // Delete the produit
        restProduitMockMvc.perform(delete("/api/produits/{id}", produit.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Produit> produitList = produitRepository.findAll();
        assertThat(produitList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Produit in Elasticsearch
        verify(mockProduitSearchRepository, times(1)).deleteById(produit.getId());
    }

    @Test
    @Transactional
    public void searchProduit() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        produitRepository.save(produit);
        when(mockProduitSearchRepository.search(queryStringQuery("id:" + produit.getId())))
                .thenReturn(Collections.singletonList(produit));

        // Search the produit
        restProduitMockMvc.perform(get("/api/_search/produits?query=id:" + produit.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(produit.getId().intValue())))
                .andExpect(jsonPath("$.[*].eid").value(hasItem(DEFAULT_EID)))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
                .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE.doubleValue())))
                .andExpect(jsonPath("$.[*].quantiteUnite").value(hasItem(DEFAULT_QUANTITE_UNITE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].prixCatalogue").value(hasItem(DEFAULT_PRIX_CATALOGUE.doubleValue())))
                .andExpect(jsonPath("$.[*].prixStandard").value(hasItem(DEFAULT_PRIX_STANDARD.doubleValue())))
                .andExpect(jsonPath("$.[*].venteOuverte").value(hasItem(DEFAULT_VENTE_OUVERTE.booleanValue())))
                .andExpect(jsonPath("$.[*].origine").value(hasItem(DEFAULT_ORIGINE)))
                .andExpect(jsonPath("$.[*].produitCategorie").value(hasItem(DEFAULT_PRODUIT_CATEGORIE.toString())))
                .andExpect(jsonPath("$.[*].allergenes[0].nom").value(hasItem(DEFAULT_ALLERGENES.toString())))
                .andExpect(jsonPath("$.[*].numeroLot").value(hasItem(DEFAULT_NUMERO_LOT)))
                .andExpect(jsonPath("$.[*].dateLot").value(hasItem(DEFAULT_DATE_LOT.toString())));
    }
}
