package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.CategorieBoisson;
import fr.baag.goodfood.repository.CategorieBoissonRepository;
import fr.baag.goodfood.repository.search.CategorieBoissonSearchRepository;
import fr.baag.goodfood.service.CategorieBoissonService;
import fr.baag.goodfood.service.dto.CategorieBoissonDTO;
import fr.baag.goodfood.service.mapper.CategorieBoissonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CategorieBoissonResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CategorieBoissonResourceIT {

    private static final Float DEFAULT_DEGRE_ALCOOL = 1F;
    private static final Float UPDATED_DEGRE_ALCOOL = 2F;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private CategorieBoissonRepository categorieBoissonRepository;

    @Autowired
    private CategorieBoissonMapper categorieBoissonMapper;

    @Autowired
    private CategorieBoissonService categorieBoissonService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test package.
     *
     * @see fr.baag.goodfood.repository.search.CategorieBoissonSearchRepositoryMockConfiguration
     */
    @Autowired
    private CategorieBoissonSearchRepository mockCategorieBoissonSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategorieBoissonMockMvc;

    private CategorieBoisson categorieBoisson;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieBoisson createEntity(EntityManager em) {
        CategorieBoisson categorieBoisson = new CategorieBoisson()
            .degreAlcool(DEFAULT_DEGRE_ALCOOL)
            .type(DEFAULT_TYPE);
        return categorieBoisson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieBoisson createUpdatedEntity(EntityManager em) {
        CategorieBoisson categorieBoisson = new CategorieBoisson()
            .degreAlcool(UPDATED_DEGRE_ALCOOL)
            .type(UPDATED_TYPE);
        return categorieBoisson;
    }

    @BeforeEach
    public void initTest() {
        categorieBoisson = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategorieBoisson() throws Exception {
        int databaseSizeBeforeCreate = categorieBoissonRepository.findAll().size();
        // Create the CategorieBoisson
        CategorieBoissonDTO categorieBoissonDTO = categorieBoissonMapper.toDto(categorieBoisson);
        restCategorieBoissonMockMvc.perform(post("/api/categorie-boissons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieBoissonDTO)))
            .andExpect(status().isCreated());

        // Validate the CategorieBoisson in the database
        List<CategorieBoisson> categorieBoissonList = categorieBoissonRepository.findAll();
        assertThat(categorieBoissonList).hasSize(databaseSizeBeforeCreate + 1);
        CategorieBoisson testCategorieBoisson = categorieBoissonList.get(categorieBoissonList.size() - 1);
        assertThat(testCategorieBoisson.getDegreAlcool()).isEqualTo(DEFAULT_DEGRE_ALCOOL);
        assertThat(testCategorieBoisson.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the CategorieBoisson in Elasticsearch
        verify(mockCategorieBoissonSearchRepository, times(1)).save(testCategorieBoisson);
    }

    @Test
    @Transactional
    public void createCategorieBoissonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categorieBoissonRepository.findAll().size();

        // Create the CategorieBoisson with an existing ID
        categorieBoisson.setId(1L);
        CategorieBoissonDTO categorieBoissonDTO = categorieBoissonMapper.toDto(categorieBoisson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategorieBoissonMockMvc.perform(post("/api/categorie-boissons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieBoissonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieBoisson in the database
        List<CategorieBoisson> categorieBoissonList = categorieBoissonRepository.findAll();
        assertThat(categorieBoissonList).hasSize(databaseSizeBeforeCreate);

        // Validate the CategorieBoisson in Elasticsearch
        verify(mockCategorieBoissonSearchRepository, times(0)).save(categorieBoisson);
    }


    @Test
    @Transactional
    public void getAllCategorieBoissons() throws Exception {
        // Initialize the database
        categorieBoissonRepository.saveAndFlush(categorieBoisson);

        // Get all the categorieBoissonList
        restCategorieBoissonMockMvc.perform(get("/api/categorie-boissons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieBoisson.getId().intValue())))
            .andExpect(jsonPath("$.[*].degreAlcool").value(hasItem(DEFAULT_DEGRE_ALCOOL.doubleValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }
    
    @Test
    @Transactional
    public void getCategorieBoisson() throws Exception {
        // Initialize the database
        categorieBoissonRepository.saveAndFlush(categorieBoisson);

        // Get the categorieBoisson
        restCategorieBoissonMockMvc.perform(get("/api/categorie-boissons/{id}", categorieBoisson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categorieBoisson.getId().intValue()))
            .andExpect(jsonPath("$.degreAlcool").value(DEFAULT_DEGRE_ALCOOL.doubleValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }
    @Test
    @Transactional
    public void getNonExistingCategorieBoisson() throws Exception {
        // Get the categorieBoisson
        restCategorieBoissonMockMvc.perform(get("/api/categorie-boissons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategorieBoisson() throws Exception {
        // Initialize the database
        categorieBoissonRepository.saveAndFlush(categorieBoisson);

        int databaseSizeBeforeUpdate = categorieBoissonRepository.findAll().size();

        // Update the categorieBoisson
        CategorieBoisson updatedCategorieBoisson = categorieBoissonRepository.findById(categorieBoisson.getId()).get();
        // Disconnect from session so that the updates on updatedCategorieBoisson are not directly saved in db
        em.detach(updatedCategorieBoisson);
        updatedCategorieBoisson
            .degreAlcool(UPDATED_DEGRE_ALCOOL)
            .type(UPDATED_TYPE);
        CategorieBoissonDTO categorieBoissonDTO = categorieBoissonMapper.toDto(updatedCategorieBoisson);

        restCategorieBoissonMockMvc.perform(put("/api/categorie-boissons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieBoissonDTO)))
            .andExpect(status().isOk());

        // Validate the CategorieBoisson in the database
        List<CategorieBoisson> categorieBoissonList = categorieBoissonRepository.findAll();
        assertThat(categorieBoissonList).hasSize(databaseSizeBeforeUpdate);
        CategorieBoisson testCategorieBoisson = categorieBoissonList.get(categorieBoissonList.size() - 1);
        assertThat(testCategorieBoisson.getDegreAlcool()).isEqualTo(UPDATED_DEGRE_ALCOOL);
        assertThat(testCategorieBoisson.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the CategorieBoisson in Elasticsearch
        verify(mockCategorieBoissonSearchRepository, times(1)).save(testCategorieBoisson);
    }

    @Test
    @Transactional
    public void updateNonExistingCategorieBoisson() throws Exception {
        int databaseSizeBeforeUpdate = categorieBoissonRepository.findAll().size();

        // Create the CategorieBoisson
        CategorieBoissonDTO categorieBoissonDTO = categorieBoissonMapper.toDto(categorieBoisson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategorieBoissonMockMvc.perform(put("/api/categorie-boissons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieBoissonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieBoisson in the database
        List<CategorieBoisson> categorieBoissonList = categorieBoissonRepository.findAll();
        assertThat(categorieBoissonList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CategorieBoisson in Elasticsearch
        verify(mockCategorieBoissonSearchRepository, times(0)).save(categorieBoisson);
    }

    @Test
    @Transactional
    public void deleteCategorieBoisson() throws Exception {
        // Initialize the database
        categorieBoissonRepository.saveAndFlush(categorieBoisson);

        int databaseSizeBeforeDelete = categorieBoissonRepository.findAll().size();

        // Delete the categorieBoisson
        restCategorieBoissonMockMvc.perform(delete("/api/categorie-boissons/{id}", categorieBoisson.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CategorieBoisson> categorieBoissonList = categorieBoissonRepository.findAll();
        assertThat(categorieBoissonList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CategorieBoisson in Elasticsearch
        verify(mockCategorieBoissonSearchRepository, times(1)).deleteById(categorieBoisson.getId());
    }

    @Test
    @Transactional
    public void searchCategorieBoisson() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        categorieBoissonRepository.saveAndFlush(categorieBoisson);
        when(mockCategorieBoissonSearchRepository.search(queryStringQuery("id:" + categorieBoisson.getId())))
            .thenReturn(Collections.singletonList(categorieBoisson));

        // Search the categorieBoisson
        restCategorieBoissonMockMvc.perform(get("/api/_search/categorie-boissons?query=id:" + categorieBoisson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieBoisson.getId().intValue())))
            .andExpect(jsonPath("$.[*].degreAlcool").value(hasItem(DEFAULT_DEGRE_ALCOOL.doubleValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }
}
