package fr.baag.goodfood.web.rest;

import fr.baag.goodfood.GoodFoodApp;
import fr.baag.goodfood.domain.CategorieAlimentation;
import fr.baag.goodfood.repository.CategorieAlimentationRepository;
import fr.baag.goodfood.repository.search.CategorieAlimentationSearchRepository;
import fr.baag.goodfood.service.CategorieAlimentationService;
import fr.baag.goodfood.service.dto.CategorieAlimentationDTO;
import fr.baag.goodfood.service.mapper.CategorieAlimentationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CategorieAlimentationResource} REST controller.
 */
@SpringBootTest(classes = GoodFoodApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CategorieAlimentationResourceIT {

    private static final Boolean DEFAULT_VEGAN = false;
    private static final Boolean UPDATED_VEGAN = true;

    private static final Boolean DEFAULT_VEGETARIEN = false;
    private static final Boolean UPDATED_VEGETARIEN = true;

    @Autowired
    private CategorieAlimentationRepository categorieAlimentationRepository;

    @Autowired
    private CategorieAlimentationMapper categorieAlimentationMapper;

    @Autowired
    private CategorieAlimentationService categorieAlimentationService;

    /**
     * This repository is mocked in the fr.baag.goodfood.repository.search test package.
     *
     * @see fr.baag.goodfood.repository.search.CategorieAlimentationSearchRepositoryMockConfiguration
     */
    @Autowired
    private CategorieAlimentationSearchRepository mockCategorieAlimentationSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCategorieAlimentationMockMvc;

    private CategorieAlimentation categorieAlimentation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieAlimentation createEntity(EntityManager em) {
        CategorieAlimentation categorieAlimentation = new CategorieAlimentation()
            .vegan(DEFAULT_VEGAN)
            .vegetarien(DEFAULT_VEGETARIEN);
        return categorieAlimentation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategorieAlimentation createUpdatedEntity(EntityManager em) {
        CategorieAlimentation categorieAlimentation = new CategorieAlimentation()
            .vegan(UPDATED_VEGAN)
            .vegetarien(UPDATED_VEGETARIEN);
        return categorieAlimentation;
    }

    @BeforeEach
    public void initTest() {
        categorieAlimentation = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategorieAlimentation() throws Exception {
        int databaseSizeBeforeCreate = categorieAlimentationRepository.findAll().size();
        // Create the CategorieAlimentation
        CategorieAlimentationDTO categorieAlimentationDTO = categorieAlimentationMapper.toDto(categorieAlimentation);
        restCategorieAlimentationMockMvc.perform(post("/api/categorie-alimentations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAlimentationDTO)))
            .andExpect(status().isCreated());

        // Validate the CategorieAlimentation in the database
        List<CategorieAlimentation> categorieAlimentationList = categorieAlimentationRepository.findAll();
        assertThat(categorieAlimentationList).hasSize(databaseSizeBeforeCreate + 1);
        CategorieAlimentation testCategorieAlimentation = categorieAlimentationList.get(categorieAlimentationList.size() - 1);
        assertThat(testCategorieAlimentation.isVegan()).isEqualTo(DEFAULT_VEGAN);
        assertThat(testCategorieAlimentation.isVegetarien()).isEqualTo(DEFAULT_VEGETARIEN);

        // Validate the CategorieAlimentation in Elasticsearch
        verify(mockCategorieAlimentationSearchRepository, times(1)).save(testCategorieAlimentation);
    }

    @Test
    @Transactional
    public void createCategorieAlimentationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categorieAlimentationRepository.findAll().size();

        // Create the CategorieAlimentation with an existing ID
        categorieAlimentation.setId(1L);
        CategorieAlimentationDTO categorieAlimentationDTO = categorieAlimentationMapper.toDto(categorieAlimentation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategorieAlimentationMockMvc.perform(post("/api/categorie-alimentations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAlimentationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieAlimentation in the database
        List<CategorieAlimentation> categorieAlimentationList = categorieAlimentationRepository.findAll();
        assertThat(categorieAlimentationList).hasSize(databaseSizeBeforeCreate);

        // Validate the CategorieAlimentation in Elasticsearch
        verify(mockCategorieAlimentationSearchRepository, times(0)).save(categorieAlimentation);
    }


    @Test
    @Transactional
    public void getAllCategorieAlimentations() throws Exception {
        // Initialize the database
        categorieAlimentationRepository.saveAndFlush(categorieAlimentation);

        // Get all the categorieAlimentationList
        restCategorieAlimentationMockMvc.perform(get("/api/categorie-alimentations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieAlimentation.getId().intValue())))
            .andExpect(jsonPath("$.[*].vegan").value(hasItem(DEFAULT_VEGAN.booleanValue())))
            .andExpect(jsonPath("$.[*].vegetarien").value(hasItem(DEFAULT_VEGETARIEN.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getCategorieAlimentation() throws Exception {
        // Initialize the database
        categorieAlimentationRepository.saveAndFlush(categorieAlimentation);

        // Get the categorieAlimentation
        restCategorieAlimentationMockMvc.perform(get("/api/categorie-alimentations/{id}", categorieAlimentation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(categorieAlimentation.getId().intValue()))
            .andExpect(jsonPath("$.vegan").value(DEFAULT_VEGAN.booleanValue()))
            .andExpect(jsonPath("$.vegetarien").value(DEFAULT_VEGETARIEN.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCategorieAlimentation() throws Exception {
        // Get the categorieAlimentation
        restCategorieAlimentationMockMvc.perform(get("/api/categorie-alimentations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategorieAlimentation() throws Exception {
        // Initialize the database
        categorieAlimentationRepository.saveAndFlush(categorieAlimentation);

        int databaseSizeBeforeUpdate = categorieAlimentationRepository.findAll().size();

        // Update the categorieAlimentation
        CategorieAlimentation updatedCategorieAlimentation = categorieAlimentationRepository.findById(categorieAlimentation.getId()).get();
        // Disconnect from session so that the updates on updatedCategorieAlimentation are not directly saved in db
        em.detach(updatedCategorieAlimentation);
        updatedCategorieAlimentation
            .vegan(UPDATED_VEGAN)
            .vegetarien(UPDATED_VEGETARIEN);
        CategorieAlimentationDTO categorieAlimentationDTO = categorieAlimentationMapper.toDto(updatedCategorieAlimentation);

        restCategorieAlimentationMockMvc.perform(put("/api/categorie-alimentations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAlimentationDTO)))
            .andExpect(status().isOk());

        // Validate the CategorieAlimentation in the database
        List<CategorieAlimentation> categorieAlimentationList = categorieAlimentationRepository.findAll();
        assertThat(categorieAlimentationList).hasSize(databaseSizeBeforeUpdate);
        CategorieAlimentation testCategorieAlimentation = categorieAlimentationList.get(categorieAlimentationList.size() - 1);
        assertThat(testCategorieAlimentation.isVegan()).isEqualTo(UPDATED_VEGAN);
        assertThat(testCategorieAlimentation.isVegetarien()).isEqualTo(UPDATED_VEGETARIEN);

        // Validate the CategorieAlimentation in Elasticsearch
        verify(mockCategorieAlimentationSearchRepository, times(1)).save(testCategorieAlimentation);
    }

    @Test
    @Transactional
    public void updateNonExistingCategorieAlimentation() throws Exception {
        int databaseSizeBeforeUpdate = categorieAlimentationRepository.findAll().size();

        // Create the CategorieAlimentation
        CategorieAlimentationDTO categorieAlimentationDTO = categorieAlimentationMapper.toDto(categorieAlimentation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategorieAlimentationMockMvc.perform(put("/api/categorie-alimentations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(categorieAlimentationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategorieAlimentation in the database
        List<CategorieAlimentation> categorieAlimentationList = categorieAlimentationRepository.findAll();
        assertThat(categorieAlimentationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CategorieAlimentation in Elasticsearch
        verify(mockCategorieAlimentationSearchRepository, times(0)).save(categorieAlimentation);
    }

    @Test
    @Transactional
    public void deleteCategorieAlimentation() throws Exception {
        // Initialize the database
        categorieAlimentationRepository.saveAndFlush(categorieAlimentation);

        int databaseSizeBeforeDelete = categorieAlimentationRepository.findAll().size();

        // Delete the categorieAlimentation
        restCategorieAlimentationMockMvc.perform(delete("/api/categorie-alimentations/{id}", categorieAlimentation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CategorieAlimentation> categorieAlimentationList = categorieAlimentationRepository.findAll();
        assertThat(categorieAlimentationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CategorieAlimentation in Elasticsearch
        verify(mockCategorieAlimentationSearchRepository, times(1)).deleteById(categorieAlimentation.getId());
    }

    @Test
    @Transactional
    public void searchCategorieAlimentation() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        categorieAlimentationRepository.saveAndFlush(categorieAlimentation);
        when(mockCategorieAlimentationSearchRepository.search(queryStringQuery("id:" + categorieAlimentation.getId())))
            .thenReturn(Collections.singletonList(categorieAlimentation));

        // Search the categorieAlimentation
        restCategorieAlimentationMockMvc.perform(get("/api/_search/categorie-alimentations?query=id:" + categorieAlimentation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categorieAlimentation.getId().intValue())))
            .andExpect(jsonPath("$.[*].vegan").value(hasItem(DEFAULT_VEGAN.booleanValue())))
            .andExpect(jsonPath("$.[*].vegetarien").value(hasItem(DEFAULT_VEGETARIEN.booleanValue())));
    }
}
