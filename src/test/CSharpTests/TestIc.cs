using Xunit;
using System;

namespace CSharpTests
{
    //TODO: Delete after IC is working
    public class TestIc
    {
        [Fact]
        public void TestAdditionOk()
        {
        //Given
        int a = 2;
        int b = 2;

        //When
        int c = a + b;

        //Then
        Assert.Equal(4,c);
        }

        [Fact]
        public void TestAdditionFailed()
        {
        //Given
        int a = 2;
        int b = 2;

        //When
        int c = a + b;

        //Then
        Assert.NotEqual(3,c);
        }
    }
}