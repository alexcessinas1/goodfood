Object.defineProperty(window, 'getComputedStyle', {
  value: () => ['-webkit-appearance'],
});

export const setupGoogleMock = () => {
  const google = {
    maps: {
      places: {
        AutocompleteService: class {},
        PlacesServiceStatus: {
          INVALID_REQUEST: 'INVALID_REQUEST',
          NOT_FOUND: 'NOT_FOUND',
          OK: 'OK',
          OVER_QUERY_LIMIT: 'OVER_QUERY_LIMIT',
          REQUEST_DENIED: 'REQUEST_DENIED',
          UNKNOWN_ERROR: 'UNKNOWN_ERROR',
          ZERO_RESULTS: 'ZERO_RESULTS',
        },
      },
      Geocoder: class {
        geocode = (): any => {};
      },
      TravelMode: class {},
      Marker: class {},
      Point: class {},
      GeocoderStatus: {
        ERROR: 'ERROR',
        INVALID_REQUEST: 'INVALID_REQUEST',
        OK: 'OK',
        OVER_QUERY_LIMIT: 'OVER_QUERY_LIMIT',
        REQUEST_DENIED: 'REQUEST_DENIED',
        UNKNOWN_ERROR: 'UNKNOWN_ERROR',
        ZERO_RESULTS: 'ZERO_RESULTS',
      },
      InfoWindow: class {},
    },
  };
  // @ts-ignore
  global.window.google = google;
};

export const setupNavigator = () => {
  const navigator = {
    geolocation: {
      getCurrentPosition(successCallback: any): void {
        successCallback({
          coords: {
            latitude: 15,
            longitude: 15,
            accuracy: 12,
            altitude: 18,
            altitudeAccuracy: null,
            heading: null,
            speed: null,
          },
          timestamp: 15,
        });
      },
    },
  };
  // @ts-ignore
  global.window.navigator = navigator;
};
