import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  EmployeComponentsPage,
  /* EmployeDeleteDialog, */
  EmployeUpdatePage,
} from './employe.page-object';

const expect = chai.expect;

describe('Employe e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let employeComponentsPage: EmployeComponentsPage;
  let employeUpdatePage: EmployeUpdatePage;
  /* let employeDeleteDialog: EmployeDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Employes', async () => {
    await navBarPage.goToEntity('employe');
    employeComponentsPage = new EmployeComponentsPage();
    await browser.wait(ec.visibilityOf(employeComponentsPage.title), 5000);
    expect(await employeComponentsPage.getTitle()).to.eq('goodFoodApp.employe.home.title');
    await browser.wait(ec.or(ec.visibilityOf(employeComponentsPage.entities), ec.visibilityOf(employeComponentsPage.noResult)), 1000);
  });

  it('should load create Employe page', async () => {
    await employeComponentsPage.clickOnCreateButton();
    employeUpdatePage = new EmployeUpdatePage();
    expect(await employeUpdatePage.getPageTitle()).to.eq('goodFoodApp.employe.home.createOrEditLabel');
    await employeUpdatePage.cancel();
  });

  /* it('should create and save Employes', async () => {
        const nbButtonsBeforeCreate = await employeComponentsPage.countDeleteButtons();

        await employeComponentsPage.clickOnCreateButton();

        await promise.all([
            employeUpdatePage.posteSelectLastOption(),
            employeUpdatePage.setNotesInput('notes'),
            employeUpdatePage.franchiseSelectLastOption(),
            employeUpdatePage.adresseSelectLastOption(),
            employeUpdatePage.personnePhysiqueSelectLastOption(),
            employeUpdatePage.personneMoraleSelectLastOption(),
        ]);

        expect(await employeUpdatePage.getNotesInput()).to.eq('notes', 'Expected Notes value to be equals to notes');

        await employeUpdatePage.save();
        expect(await employeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await employeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Employe', async () => {
        const nbButtonsBeforeDelete = await employeComponentsPage.countDeleteButtons();
        await employeComponentsPage.clickOnLastDeleteButton();

        employeDeleteDialog = new EmployeDeleteDialog();
        expect(await employeDeleteDialog.getDialogTitle())
            .to.eq('goodFoodApp.employe.delete.question');
        await employeDeleteDialog.clickOnConfirmButton();

        expect(await employeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
