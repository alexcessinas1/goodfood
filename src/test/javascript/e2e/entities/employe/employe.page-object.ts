import { element, by, ElementFinder } from 'protractor';

export class EmployeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-employe div table .btn-danger'));
  title = element.all(by.css('jhi-employe div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EmployeUpdatePage {
  pageTitle = element(by.id('jhi-employe-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  posteSelect = element(by.id('field_poste'));
  notesInput = element(by.id('field_notes'));

  franchiseSelect = element(by.id('field_franchise'));
  adresseSelect = element(by.id('field_adresse'));
  personnePhysiqueSelect = element(by.id('field_personnePhysique'));
  personneMoraleSelect = element(by.id('field_personneMorale'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPosteSelect(poste: string): Promise<void> {
    await this.posteSelect.sendKeys(poste);
  }

  async getPosteSelect(): Promise<string> {
    return await this.posteSelect.element(by.css('option:checked')).getText();
  }

  async posteSelectLastOption(): Promise<void> {
    await this.posteSelect.all(by.tagName('option')).last().click();
  }

  async setNotesInput(notes: string): Promise<void> {
    await this.notesInput.sendKeys(notes);
  }

  async getNotesInput(): Promise<string> {
    return await this.notesInput.getAttribute('value');
  }

  async franchiseSelectLastOption(): Promise<void> {
    await this.franchiseSelect.all(by.tagName('option')).last().click();
  }

  async franchiseSelectOption(option: string): Promise<void> {
    await this.franchiseSelect.sendKeys(option);
  }

  getFranchiseSelect(): ElementFinder {
    return this.franchiseSelect;
  }

  async getFranchiseSelectedOption(): Promise<string> {
    return await this.franchiseSelect.element(by.css('option:checked')).getText();
  }

  async adresseSelectLastOption(): Promise<void> {
    await this.adresseSelect.all(by.tagName('option')).last().click();
  }

  async adresseSelectOption(option: string): Promise<void> {
    await this.adresseSelect.sendKeys(option);
  }

  getAdresseSelect(): ElementFinder {
    return this.adresseSelect;
  }

  async getAdresseSelectedOption(): Promise<string> {
    return await this.adresseSelect.element(by.css('option:checked')).getText();
  }

  async personnePhysiqueSelectLastOption(): Promise<void> {
    await this.personnePhysiqueSelect.all(by.tagName('option')).last().click();
  }

  async personnePhysiqueSelectOption(option: string): Promise<void> {
    await this.personnePhysiqueSelect.sendKeys(option);
  }

  getPersonnePhysiqueSelect(): ElementFinder {
    return this.personnePhysiqueSelect;
  }

  async getPersonnePhysiqueSelectedOption(): Promise<string> {
    return await this.personnePhysiqueSelect.element(by.css('option:checked')).getText();
  }

  async personneMoraleSelectLastOption(): Promise<void> {
    await this.personneMoraleSelect.all(by.tagName('option')).last().click();
  }

  async personneMoraleSelectOption(option: string): Promise<void> {
    await this.personneMoraleSelect.sendKeys(option);
  }

  getPersonneMoraleSelect(): ElementFinder {
    return this.personneMoraleSelect;
  }

  async getPersonneMoraleSelectedOption(): Promise<string> {
    return await this.personneMoraleSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EmployeDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-employe-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-employe'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
