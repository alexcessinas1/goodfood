import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProduitComponentsPage, ProduitDeleteDialog, ProduitUpdatePage } from './produit.page-object';

const expect = chai.expect;

describe('Produit e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let produitComponentsPage: ProduitComponentsPage;
  let produitUpdatePage: ProduitUpdatePage;
  let produitDeleteDialog: ProduitDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Produits', async () => {
    await navBarPage.goToEntity('produit');
    produitComponentsPage = new ProduitComponentsPage();
    await browser.wait(ec.visibilityOf(produitComponentsPage.title), 5000);
    expect(await produitComponentsPage.getTitle()).to.eq('goodFoodApp.produit.home.title');
    await browser.wait(ec.or(ec.visibilityOf(produitComponentsPage.entities), ec.visibilityOf(produitComponentsPage.noResult)), 1000);
  });

  it('should load create Produit page', async () => {
    await produitComponentsPage.clickOnCreateButton();
    produitUpdatePage = new ProduitUpdatePage();
    expect(await produitUpdatePage.getPageTitle()).to.eq('goodFoodApp.produit.home.createOrEditLabel');
    await produitUpdatePage.cancel();
  });

  it('should create and save Produits', async () => {
    const nbButtonsBeforeCreate = await produitComponentsPage.countDeleteButtons();

    await produitComponentsPage.clickOnCreateButton();

    await promise.all([
      produitUpdatePage.setEidInput('eid'),
      produitUpdatePage.setNomInput('nom'),
      produitUpdatePage.setQuantiteInput('5'),
      produitUpdatePage.quantiteUniteSelectLastOption(),
      produitUpdatePage.setDescriptionInput('description'),
      produitUpdatePage.setPrixCatalogueInput('5'),
      produitUpdatePage.setPrixStandardInput('5'),
      produitUpdatePage.setOrigineInput('origine'),
      produitUpdatePage.produitCategorieSelectLastOption(),
      produitUpdatePage.allergenesSelectLastOption(),
      produitUpdatePage.setNumeroLotInput('numeroLot'),
      produitUpdatePage.setDateLotInput('2000-12-31'),
      produitUpdatePage.boissonSelectLastOption(),
      produitUpdatePage.alimentSelectLastOption(),
      produitUpdatePage.fournitureSelectLastOption(),
    ]);

    expect(await produitUpdatePage.getEidInput()).to.eq('eid', 'Expected Eid value to be equals to eid');
    expect(await produitUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
    expect(await produitUpdatePage.getQuantiteInput()).to.eq('5', 'Expected quantite value to be equals to 5');
    expect(await produitUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await produitUpdatePage.getPrixCatalogueInput()).to.eq('5', 'Expected prixCatalogue value to be equals to 5');
    expect(await produitUpdatePage.getPrixStandardInput()).to.eq('5', 'Expected prixStandard value to be equals to 5');
    const selectedVenteOuverte = produitUpdatePage.getVenteOuverteInput();
    if (await selectedVenteOuverte.isSelected()) {
      await produitUpdatePage.getVenteOuverteInput().click();
      expect(await produitUpdatePage.getVenteOuverteInput().isSelected(), 'Expected venteOuverte not to be selected').to.be.false;
    } else {
      await produitUpdatePage.getVenteOuverteInput().click();
      expect(await produitUpdatePage.getVenteOuverteInput().isSelected(), 'Expected venteOuverte to be selected').to.be.true;
    }
    expect(await produitUpdatePage.getOrigineInput()).to.eq('origine', 'Expected Origine value to be equals to origine');
    expect(await produitUpdatePage.getNumeroLotInput()).to.eq('numeroLot', 'Expected NumeroLot value to be equals to numeroLot');
    expect(await produitUpdatePage.getDateLotInput()).to.eq('2000-12-31', 'Expected dateLot value to be equals to 2000-12-31');

    await produitUpdatePage.save();
    expect(await produitUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await produitComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Produit', async () => {
    const nbButtonsBeforeDelete = await produitComponentsPage.countDeleteButtons();
    await produitComponentsPage.clickOnLastDeleteButton();

    produitDeleteDialog = new ProduitDeleteDialog();
    expect(await produitDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.produit.delete.question');
    await produitDeleteDialog.clickOnConfirmButton();

    expect(await produitComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
