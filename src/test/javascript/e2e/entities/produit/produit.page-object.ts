import { element, by, ElementFinder } from 'protractor';

export class ProduitComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-produit div table .btn-danger'));
  title = element.all(by.css('jhi-produit div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProduitUpdatePage {
  pageTitle = element(by.id('jhi-produit-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  eidInput = element(by.id('field_eid'));
  nomInput = element(by.id('field_nom'));
  quantiteInput = element(by.id('field_quantite'));
  quantiteUniteSelect = element(by.id('field_quantiteUnite'));
  descriptionInput = element(by.id('field_description'));
  prixCatalogueInput = element(by.id('field_prixCatalogue'));
  prixStandardInput = element(by.id('field_prixStandard'));
  venteOuverteInput = element(by.id('field_venteOuverte'));
  origineInput = element(by.id('field_origine'));
  produitCategorieSelect = element(by.id('field_produitCategorie'));
  allergenesSelect = element(by.id('field_allergenes'));
  numeroLotInput = element(by.id('field_numeroLot'));
  dateLotInput = element(by.id('field_dateLot'));

  boissonSelect = element(by.id('field_boisson'));
  alimentSelect = element(by.id('field_aliment'));
  fournitureSelect = element(by.id('field_fourniture'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setEidInput(eid: string): Promise<void> {
    await this.eidInput.sendKeys(eid);
  }

  async getEidInput(): Promise<string> {
    return await this.eidInput.getAttribute('value');
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setQuantiteInput(quantite: string): Promise<void> {
    await this.quantiteInput.sendKeys(quantite);
  }

  async getQuantiteInput(): Promise<string> {
    return await this.quantiteInput.getAttribute('value');
  }

  async setQuantiteUniteSelect(quantiteUnite: string): Promise<void> {
    await this.quantiteUniteSelect.sendKeys(quantiteUnite);
  }

  async getQuantiteUniteSelect(): Promise<string> {
    return await this.quantiteUniteSelect.element(by.css('option:checked')).getText();
  }

  async quantiteUniteSelectLastOption(): Promise<void> {
    await this.quantiteUniteSelect.all(by.tagName('option')).last().click();
  }

  async setDescriptionInput(description: string): Promise<void> {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput(): Promise<string> {
    return await this.descriptionInput.getAttribute('value');
  }

  async setPrixCatalogueInput(prixCatalogue: string): Promise<void> {
    await this.prixCatalogueInput.sendKeys(prixCatalogue);
  }

  async getPrixCatalogueInput(): Promise<string> {
    return await this.prixCatalogueInput.getAttribute('value');
  }

  async setPrixStandardInput(prixStandard: string): Promise<void> {
    await this.prixStandardInput.sendKeys(prixStandard);
  }

  async getPrixStandardInput(): Promise<string> {
    return await this.prixStandardInput.getAttribute('value');
  }

  getVenteOuverteInput(): ElementFinder {
    return this.venteOuverteInput;
  }

  async setOrigineInput(origine: string): Promise<void> {
    await this.origineInput.sendKeys(origine);
  }

  async getOrigineInput(): Promise<string> {
    return await this.origineInput.getAttribute('value');
  }

  async setProduitCategorieSelect(produitCategorie: string): Promise<void> {
    await this.produitCategorieSelect.sendKeys(produitCategorie);
  }

  async getProduitCategorieSelect(): Promise<string> {
    return await this.produitCategorieSelect.element(by.css('option:checked')).getText();
  }

  async produitCategorieSelectLastOption(): Promise<void> {
    await this.produitCategorieSelect.all(by.tagName('option')).last().click();
  }

  async setAllergenesSelect(allergenes: string): Promise<void> {
    await this.allergenesSelect.sendKeys(allergenes);
  }

  async getAllergenesSelect(): Promise<string> {
    return await this.allergenesSelect.element(by.css('option:checked')).getText();
  }

  async allergenesSelectLastOption(): Promise<void> {
    await this.allergenesSelect.all(by.tagName('option')).last().click();
  }

  async setNumeroLotInput(numeroLot: string): Promise<void> {
    await this.numeroLotInput.sendKeys(numeroLot);
  }

  async getNumeroLotInput(): Promise<string> {
    return await this.numeroLotInput.getAttribute('value');
  }

  async setDateLotInput(dateLot: string): Promise<void> {
    await this.dateLotInput.sendKeys(dateLot);
  }

  async getDateLotInput(): Promise<string> {
    return await this.dateLotInput.getAttribute('value');
  }

  async boissonSelectLastOption(): Promise<void> {
    await this.boissonSelect.all(by.tagName('option')).last().click();
  }

  async boissonSelectOption(option: string): Promise<void> {
    await this.boissonSelect.sendKeys(option);
  }

  getBoissonSelect(): ElementFinder {
    return this.boissonSelect;
  }

  async getBoissonSelectedOption(): Promise<string> {
    return await this.boissonSelect.element(by.css('option:checked')).getText();
  }

  async alimentSelectLastOption(): Promise<void> {
    await this.alimentSelect.all(by.tagName('option')).last().click();
  }

  async alimentSelectOption(option: string): Promise<void> {
    await this.alimentSelect.sendKeys(option);
  }

  getAlimentSelect(): ElementFinder {
    return this.alimentSelect;
  }

  async getAlimentSelectedOption(): Promise<string> {
    return await this.alimentSelect.element(by.css('option:checked')).getText();
  }

  async fournitureSelectLastOption(): Promise<void> {
    await this.fournitureSelect.all(by.tagName('option')).last().click();
  }

  async fournitureSelectOption(option: string): Promise<void> {
    await this.fournitureSelect.sendKeys(option);
  }

  getFournitureSelect(): ElementFinder {
    return this.fournitureSelect;
  }

  async getFournitureSelectedOption(): Promise<string> {
    return await this.fournitureSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProduitDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-produit-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-produit'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
