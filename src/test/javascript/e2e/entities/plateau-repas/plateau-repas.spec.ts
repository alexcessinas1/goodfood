import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PlateauRepasComponentsPage, PlateauRepasDeleteDialog, PlateauRepasUpdatePage } from './plateau-repas.page-object';

const expect = chai.expect;

describe('PlateauRepas e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let plateauRepasComponentsPage: PlateauRepasComponentsPage;
  let plateauRepasUpdatePage: PlateauRepasUpdatePage;
  let plateauRepasDeleteDialog: PlateauRepasDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load PlateauRepas', async () => {
    await navBarPage.goToEntity('plateau-repas');
    plateauRepasComponentsPage = new PlateauRepasComponentsPage();
    await browser.wait(ec.visibilityOf(plateauRepasComponentsPage.title), 5000);
    expect(await plateauRepasComponentsPage.getTitle()).to.eq('goodFoodApp.plateauRepas.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(plateauRepasComponentsPage.entities), ec.visibilityOf(plateauRepasComponentsPage.noResult)),
      1000
    );
  });

  it('should load create PlateauRepas page', async () => {
    await plateauRepasComponentsPage.clickOnCreateButton();
    plateauRepasUpdatePage = new PlateauRepasUpdatePage();
    expect(await plateauRepasUpdatePage.getPageTitle()).to.eq('goodFoodApp.plateauRepas.home.createOrEditLabel');
    await plateauRepasUpdatePage.cancel();
  });

  it('should create and save PlateauRepas', async () => {
    const nbButtonsBeforeCreate = await plateauRepasComponentsPage.countDeleteButtons();

    await plateauRepasComponentsPage.clickOnCreateButton();

    await promise.all([
      plateauRepasUpdatePage.setPrixHTInput('5'),
      plateauRepasUpdatePage.setTvaInput('5'),
      plateauRepasUpdatePage.setCompositionInput('composition'),
      plateauRepasUpdatePage.boissonSelectLastOption(),
    ]);

    expect(await plateauRepasUpdatePage.getPrixHTInput()).to.eq('5', 'Expected prixHT value to be equals to 5');
    expect(await plateauRepasUpdatePage.getTvaInput()).to.eq('5', 'Expected tva value to be equals to 5');
    expect(await plateauRepasUpdatePage.getCompositionInput()).to.eq(
      'composition',
      'Expected Composition value to be equals to composition'
    );
    const selectedCouvert = plateauRepasUpdatePage.getCouvertInput();
    if (await selectedCouvert.isSelected()) {
      await plateauRepasUpdatePage.getCouvertInput().click();
      expect(await plateauRepasUpdatePage.getCouvertInput().isSelected(), 'Expected couvert not to be selected').to.be.false;
    } else {
      await plateauRepasUpdatePage.getCouvertInput().click();
      expect(await plateauRepasUpdatePage.getCouvertInput().isSelected(), 'Expected couvert to be selected').to.be.true;
    }
    const selectedServiette = plateauRepasUpdatePage.getServietteInput();
    if (await selectedServiette.isSelected()) {
      await plateauRepasUpdatePage.getServietteInput().click();
      expect(await plateauRepasUpdatePage.getServietteInput().isSelected(), 'Expected serviette not to be selected').to.be.false;
    } else {
      await plateauRepasUpdatePage.getServietteInput().click();
      expect(await plateauRepasUpdatePage.getServietteInput().isSelected(), 'Expected serviette to be selected').to.be.true;
    }

    await plateauRepasUpdatePage.save();
    expect(await plateauRepasUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await plateauRepasComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last PlateauRepas', async () => {
    const nbButtonsBeforeDelete = await plateauRepasComponentsPage.countDeleteButtons();
    await plateauRepasComponentsPage.clickOnLastDeleteButton();

    plateauRepasDeleteDialog = new PlateauRepasDeleteDialog();
    expect(await plateauRepasDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.plateauRepas.delete.question');
    await plateauRepasDeleteDialog.clickOnConfirmButton();

    expect(await plateauRepasComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
