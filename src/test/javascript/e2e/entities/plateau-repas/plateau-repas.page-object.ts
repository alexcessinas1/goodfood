import { element, by, ElementFinder } from 'protractor';

export class PlateauRepasComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-plateau-repas div table .btn-danger'));
  title = element.all(by.css('jhi-plateau-repas div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class PlateauRepasUpdatePage {
  pageTitle = element(by.id('jhi-plateau-repas-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  prixHTInput = element(by.id('field_prixHT'));
  tvaInput = element(by.id('field_tva'));
  compositionInput = element(by.id('field_composition'));
  couvertInput = element(by.id('field_couvert'));
  servietteInput = element(by.id('field_serviette'));

  boissonSelect = element(by.id('field_boisson'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPrixHTInput(prixHT: string): Promise<void> {
    await this.prixHTInput.sendKeys(prixHT);
  }

  async getPrixHTInput(): Promise<string> {
    return await this.prixHTInput.getAttribute('value');
  }

  async setTvaInput(tva: string): Promise<void> {
    await this.tvaInput.sendKeys(tva);
  }

  async getTvaInput(): Promise<string> {
    return await this.tvaInput.getAttribute('value');
  }

  async setCompositionInput(composition: string): Promise<void> {
    await this.compositionInput.sendKeys(composition);
  }

  async getCompositionInput(): Promise<string> {
    return await this.compositionInput.getAttribute('value');
  }

  getCouvertInput(): ElementFinder {
    return this.couvertInput;
  }

  getServietteInput(): ElementFinder {
    return this.servietteInput;
  }

  async boissonSelectLastOption(): Promise<void> {
    await this.boissonSelect.all(by.tagName('option')).last().click();
  }

  async boissonSelectOption(option: string): Promise<void> {
    await this.boissonSelect.sendKeys(option);
  }

  getBoissonSelect(): ElementFinder {
    return this.boissonSelect;
  }

  async getBoissonSelectedOption(): Promise<string> {
    return await this.boissonSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class PlateauRepasDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-plateauRepas-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-plateauRepas'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
