import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  PersonneMoraleComponentsPage,
  /* PersonneMoraleDeleteDialog, */
  PersonneMoraleUpdatePage,
} from './personne-morale.page-object';

const expect = chai.expect;

describe('PersonneMorale e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let personneMoraleComponentsPage: PersonneMoraleComponentsPage;
  let personneMoraleUpdatePage: PersonneMoraleUpdatePage;
  /* let personneMoraleDeleteDialog: PersonneMoraleDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load PersonneMorales', async () => {
    await navBarPage.goToEntity('personne-morale');
    personneMoraleComponentsPage = new PersonneMoraleComponentsPage();
    await browser.wait(ec.visibilityOf(personneMoraleComponentsPage.title), 5000);
    expect(await personneMoraleComponentsPage.getTitle()).to.eq('goodFoodApp.personneMorale.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(personneMoraleComponentsPage.entities), ec.visibilityOf(personneMoraleComponentsPage.noResult)),
      1000
    );
  });

  it('should load create PersonneMorale page', async () => {
    await personneMoraleComponentsPage.clickOnCreateButton();
    personneMoraleUpdatePage = new PersonneMoraleUpdatePage();
    expect(await personneMoraleUpdatePage.getPageTitle()).to.eq('goodFoodApp.personneMorale.home.createOrEditLabel');
    await personneMoraleUpdatePage.cancel();
  });

  /* it('should create and save PersonneMorales', async () => {
        const nbButtonsBeforeCreate = await personneMoraleComponentsPage.countDeleteButtons();

        await personneMoraleComponentsPage.clickOnCreateButton();

        await promise.all([
            personneMoraleUpdatePage.setRaisonSocialeInput('raisonSociale'),
            personneMoraleUpdatePage.setSiretInput('siret'),
            personneMoraleUpdatePage.userSelectLastOption(),
        ]);

        expect(await personneMoraleUpdatePage.getRaisonSocialeInput()).to.eq('raisonSociale', 'Expected RaisonSociale value to be equals to raisonSociale');
        expect(await personneMoraleUpdatePage.getSiretInput()).to.eq('siret', 'Expected Siret value to be equals to siret');

        await personneMoraleUpdatePage.save();
        expect(await personneMoraleUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await personneMoraleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last PersonneMorale', async () => {
        const nbButtonsBeforeDelete = await personneMoraleComponentsPage.countDeleteButtons();
        await personneMoraleComponentsPage.clickOnLastDeleteButton();

        personneMoraleDeleteDialog = new PersonneMoraleDeleteDialog();
        expect(await personneMoraleDeleteDialog.getDialogTitle())
            .to.eq('goodFoodApp.personneMorale.delete.question');
        await personneMoraleDeleteDialog.clickOnConfirmButton();

        expect(await personneMoraleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
