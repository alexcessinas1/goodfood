import { element, by, ElementFinder } from 'protractor';

export class CommandeClientComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-commande-client div table .btn-danger'));
  title = element.all(by.css('jhi-commande-client div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CommandeClientUpdatePage {
  pageTitle = element(by.id('jhi-commande-client-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  codePromoInput = element(by.id('field_codePromo'));

  clientSelect = element(by.id('field_client'));
  employeSelect = element(by.id('field_employe'));
  commandeSelect = element(by.id('field_commande'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCodePromoInput(codePromo: string): Promise<void> {
    await this.codePromoInput.sendKeys(codePromo);
  }

  async getCodePromoInput(): Promise<string> {
    return await this.codePromoInput.getAttribute('value');
  }

  async clientSelectLastOption(): Promise<void> {
    await this.clientSelect.all(by.tagName('option')).last().click();
  }

  async clientSelectOption(option: string): Promise<void> {
    await this.clientSelect.sendKeys(option);
  }

  getClientSelect(): ElementFinder {
    return this.clientSelect;
  }

  async getClientSelectedOption(): Promise<string> {
    return await this.clientSelect.element(by.css('option:checked')).getText();
  }

  async employeSelectLastOption(): Promise<void> {
    await this.employeSelect.all(by.tagName('option')).last().click();
  }

  async employeSelectOption(option: string): Promise<void> {
    await this.employeSelect.sendKeys(option);
  }

  getEmployeSelect(): ElementFinder {
    return this.employeSelect;
  }

  async getEmployeSelectedOption(): Promise<string> {
    return await this.employeSelect.element(by.css('option:checked')).getText();
  }

  async commandeSelectLastOption(): Promise<void> {
    await this.commandeSelect.all(by.tagName('option')).last().click();
  }

  async commandeSelectOption(option: string): Promise<void> {
    await this.commandeSelect.sendKeys(option);
  }

  getCommandeSelect(): ElementFinder {
    return this.commandeSelect;
  }

  async getCommandeSelectedOption(): Promise<string> {
    return await this.commandeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CommandeClientDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-commandeClient-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-commandeClient'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
