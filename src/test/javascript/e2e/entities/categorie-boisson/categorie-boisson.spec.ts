import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CategorieBoissonComponentsPage, CategorieBoissonDeleteDialog, CategorieBoissonUpdatePage } from './categorie-boisson.page-object';

const expect = chai.expect;

describe('CategorieBoisson e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let categorieBoissonComponentsPage: CategorieBoissonComponentsPage;
  let categorieBoissonUpdatePage: CategorieBoissonUpdatePage;
  let categorieBoissonDeleteDialog: CategorieBoissonDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CategorieBoissons', async () => {
    await navBarPage.goToEntity('categorie-boisson');
    categorieBoissonComponentsPage = new CategorieBoissonComponentsPage();
    await browser.wait(ec.visibilityOf(categorieBoissonComponentsPage.title), 5000);
    expect(await categorieBoissonComponentsPage.getTitle()).to.eq('goodFoodApp.categorieBoisson.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(categorieBoissonComponentsPage.entities), ec.visibilityOf(categorieBoissonComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CategorieBoisson page', async () => {
    await categorieBoissonComponentsPage.clickOnCreateButton();
    categorieBoissonUpdatePage = new CategorieBoissonUpdatePage();
    expect(await categorieBoissonUpdatePage.getPageTitle()).to.eq('goodFoodApp.categorieBoisson.home.createOrEditLabel');
    await categorieBoissonUpdatePage.cancel();
  });

  it('should create and save CategorieBoissons', async () => {
    const nbButtonsBeforeCreate = await categorieBoissonComponentsPage.countDeleteButtons();

    await categorieBoissonComponentsPage.clickOnCreateButton();

    await promise.all([categorieBoissonUpdatePage.setDegreAlcoolInput('5'), categorieBoissonUpdatePage.setTypeInput('type')]);

    expect(await categorieBoissonUpdatePage.getDegreAlcoolInput()).to.eq('5', 'Expected degreAlcool value to be equals to 5');
    expect(await categorieBoissonUpdatePage.getTypeInput()).to.eq('type', 'Expected Type value to be equals to type');

    await categorieBoissonUpdatePage.save();
    expect(await categorieBoissonUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await categorieBoissonComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CategorieBoisson', async () => {
    const nbButtonsBeforeDelete = await categorieBoissonComponentsPage.countDeleteButtons();
    await categorieBoissonComponentsPage.clickOnLastDeleteButton();

    categorieBoissonDeleteDialog = new CategorieBoissonDeleteDialog();
    expect(await categorieBoissonDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.categorieBoisson.delete.question');
    await categorieBoissonDeleteDialog.clickOnConfirmButton();

    expect(await categorieBoissonComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
