import { element, by, ElementFinder } from 'protractor';

export class CategorieBoissonComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-categorie-boisson div table .btn-danger'));
  title = element.all(by.css('jhi-categorie-boisson div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CategorieBoissonUpdatePage {
  pageTitle = element(by.id('jhi-categorie-boisson-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  degreAlcoolInput = element(by.id('field_degreAlcool'));
  typeInput = element(by.id('field_type'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDegreAlcoolInput(degreAlcool: string): Promise<void> {
    await this.degreAlcoolInput.sendKeys(degreAlcool);
  }

  async getDegreAlcoolInput(): Promise<string> {
    return await this.degreAlcoolInput.getAttribute('value');
  }

  async setTypeInput(type: string): Promise<void> {
    await this.typeInput.sendKeys(type);
  }

  async getTypeInput(): Promise<string> {
    return await this.typeInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CategorieBoissonDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-categorieBoisson-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-categorieBoisson'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
