import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AllergeneComponentsPage, AllergeneDeleteDialog, AllergeneUpdatePage } from './allergene.page-object';

const expect = chai.expect;

describe('Allergene e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let allergeneComponentsPage: AllergeneComponentsPage;
  let allergeneUpdatePage: AllergeneUpdatePage;
  let allergeneDeleteDialog: AllergeneDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Allergenes', async () => {
    await navBarPage.goToEntity('allergene');
    allergeneComponentsPage = new AllergeneComponentsPage();
    await browser.wait(ec.visibilityOf(allergeneComponentsPage.title), 5000);
    expect(await allergeneComponentsPage.getTitle()).to.eq('goodFoodApp.allergene.home.title');
    await browser.wait(ec.or(ec.visibilityOf(allergeneComponentsPage.entities), ec.visibilityOf(allergeneComponentsPage.noResult)), 1000);
  });

  it('should load create Allergene page', async () => {
    await allergeneComponentsPage.clickOnCreateButton();
    allergeneUpdatePage = new AllergeneUpdatePage();
    expect(await allergeneUpdatePage.getPageTitle()).to.eq('goodFoodApp.allergene.home.createOrEditLabel');
    await allergeneUpdatePage.cancel();
  });

  it('should create and save Allergenes', async () => {
    const nbButtonsBeforeCreate = await allergeneComponentsPage.countDeleteButtons();

    await allergeneComponentsPage.clickOnCreateButton();

    await promise.all([allergeneUpdatePage.nomSelectLastOption()]);

    await allergeneUpdatePage.save();
    expect(await allergeneUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await allergeneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Allergene', async () => {
    const nbButtonsBeforeDelete = await allergeneComponentsPage.countDeleteButtons();
    await allergeneComponentsPage.clickOnLastDeleteButton();

    allergeneDeleteDialog = new AllergeneDeleteDialog();
    expect(await allergeneDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.allergene.delete.question');
    await allergeneDeleteDialog.clickOnConfirmButton();

    expect(await allergeneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
