import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  CategorieFournitureComponentsPage,
  CategorieFournitureDeleteDialog,
  CategorieFournitureUpdatePage,
} from './categorie-fourniture.page-object';

const expect = chai.expect;

describe('CategorieFourniture e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let categorieFournitureComponentsPage: CategorieFournitureComponentsPage;
  let categorieFournitureUpdatePage: CategorieFournitureUpdatePage;
  let categorieFournitureDeleteDialog: CategorieFournitureDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CategorieFournitures', async () => {
    await navBarPage.goToEntity('categorie-fourniture');
    categorieFournitureComponentsPage = new CategorieFournitureComponentsPage();
    await browser.wait(ec.visibilityOf(categorieFournitureComponentsPage.title), 5000);
    expect(await categorieFournitureComponentsPage.getTitle()).to.eq('goodFoodApp.categorieFourniture.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(categorieFournitureComponentsPage.entities), ec.visibilityOf(categorieFournitureComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CategorieFourniture page', async () => {
    await categorieFournitureComponentsPage.clickOnCreateButton();
    categorieFournitureUpdatePage = new CategorieFournitureUpdatePage();
    expect(await categorieFournitureUpdatePage.getPageTitle()).to.eq('goodFoodApp.categorieFourniture.home.createOrEditLabel');
    await categorieFournitureUpdatePage.cancel();
  });

  it('should create and save CategorieFournitures', async () => {
    const nbButtonsBeforeCreate = await categorieFournitureComponentsPage.countDeleteButtons();

    await categorieFournitureComponentsPage.clickOnCreateButton();

    await promise.all([categorieFournitureUpdatePage.setTypeInput('type')]);

    expect(await categorieFournitureUpdatePage.getTypeInput()).to.eq('type', 'Expected Type value to be equals to type');

    await categorieFournitureUpdatePage.save();
    expect(await categorieFournitureUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await categorieFournitureComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CategorieFourniture', async () => {
    const nbButtonsBeforeDelete = await categorieFournitureComponentsPage.countDeleteButtons();
    await categorieFournitureComponentsPage.clickOnLastDeleteButton();

    categorieFournitureDeleteDialog = new CategorieFournitureDeleteDialog();
    expect(await categorieFournitureDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.categorieFourniture.delete.question');
    await categorieFournitureDeleteDialog.clickOnConfirmButton();

    expect(await categorieFournitureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
