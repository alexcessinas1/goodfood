import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  CategorieAlimentationComponentsPage,
  CategorieAlimentationDeleteDialog,
  CategorieAlimentationUpdatePage,
} from './categorie-alimentation.page-object';

const expect = chai.expect;

describe('CategorieAlimentation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let categorieAlimentationComponentsPage: CategorieAlimentationComponentsPage;
  let categorieAlimentationUpdatePage: CategorieAlimentationUpdatePage;
  let categorieAlimentationDeleteDialog: CategorieAlimentationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CategorieAlimentations', async () => {
    await navBarPage.goToEntity('categorie-alimentation');
    categorieAlimentationComponentsPage = new CategorieAlimentationComponentsPage();
    await browser.wait(ec.visibilityOf(categorieAlimentationComponentsPage.title), 5000);
    expect(await categorieAlimentationComponentsPage.getTitle()).to.eq('goodFoodApp.categorieAlimentation.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(categorieAlimentationComponentsPage.entities), ec.visibilityOf(categorieAlimentationComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CategorieAlimentation page', async () => {
    await categorieAlimentationComponentsPage.clickOnCreateButton();
    categorieAlimentationUpdatePage = new CategorieAlimentationUpdatePage();
    expect(await categorieAlimentationUpdatePage.getPageTitle()).to.eq('goodFoodApp.categorieAlimentation.home.createOrEditLabel');
    await categorieAlimentationUpdatePage.cancel();
  });

  it('should create and save CategorieAlimentations', async () => {
    const nbButtonsBeforeCreate = await categorieAlimentationComponentsPage.countDeleteButtons();

    await categorieAlimentationComponentsPage.clickOnCreateButton();

    await promise.all([]);

    const selectedVegan = categorieAlimentationUpdatePage.getVeganInput();
    if (await selectedVegan.isSelected()) {
      await categorieAlimentationUpdatePage.getVeganInput().click();
      expect(await categorieAlimentationUpdatePage.getVeganInput().isSelected(), 'Expected vegan not to be selected').to.be.false;
    } else {
      await categorieAlimentationUpdatePage.getVeganInput().click();
      expect(await categorieAlimentationUpdatePage.getVeganInput().isSelected(), 'Expected vegan to be selected').to.be.true;
    }
    const selectedVegetarien = categorieAlimentationUpdatePage.getVegetarienInput();
    if (await selectedVegetarien.isSelected()) {
      await categorieAlimentationUpdatePage.getVegetarienInput().click();
      expect(await categorieAlimentationUpdatePage.getVegetarienInput().isSelected(), 'Expected vegetarien not to be selected').to.be.false;
    } else {
      await categorieAlimentationUpdatePage.getVegetarienInput().click();
      expect(await categorieAlimentationUpdatePage.getVegetarienInput().isSelected(), 'Expected vegetarien to be selected').to.be.true;
    }

    await categorieAlimentationUpdatePage.save();
    expect(await categorieAlimentationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await categorieAlimentationComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CategorieAlimentation', async () => {
    const nbButtonsBeforeDelete = await categorieAlimentationComponentsPage.countDeleteButtons();
    await categorieAlimentationComponentsPage.clickOnLastDeleteButton();

    categorieAlimentationDeleteDialog = new CategorieAlimentationDeleteDialog();
    expect(await categorieAlimentationDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.categorieAlimentation.delete.question');
    await categorieAlimentationDeleteDialog.clickOnConfirmButton();

    expect(await categorieAlimentationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
