import { element, by, ElementFinder } from 'protractor';

export class CategorieAlimentationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-categorie-alimentation div table .btn-danger'));
  title = element.all(by.css('jhi-categorie-alimentation div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CategorieAlimentationUpdatePage {
  pageTitle = element(by.id('jhi-categorie-alimentation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  veganInput = element(by.id('field_vegan'));
  vegetarienInput = element(by.id('field_vegetarien'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  getVeganInput(): ElementFinder {
    return this.veganInput;
  }

  getVegetarienInput(): ElementFinder {
    return this.vegetarienInput;
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CategorieAlimentationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-categorieAlimentation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-categorieAlimentation'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
