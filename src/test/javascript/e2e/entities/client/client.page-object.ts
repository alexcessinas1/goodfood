import { element, by, ElementFinder } from 'protractor';

export class ClientComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-client div table .btn-danger'));
  title = element.all(by.css('jhi-client div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ClientUpdatePage {
  pageTitle = element(by.id('jhi-client-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  personneTypeSelect = element(by.id('field_personneType'));

  adresseLivraisonSelect = element(by.id('field_adresseLivraison'));
  adresseFacturationSelect = element(by.id('field_adresseFacturation'));
  personnePhysiqueSelect = element(by.id('field_personnePhysique'));
  personneMoraleSelect = element(by.id('field_personneMorale'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPersonneTypeSelect(personneType: string): Promise<void> {
    await this.personneTypeSelect.sendKeys(personneType);
  }

  async getPersonneTypeSelect(): Promise<string> {
    return await this.personneTypeSelect.element(by.css('option:checked')).getText();
  }

  async personneTypeSelectLastOption(): Promise<void> {
    await this.personneTypeSelect.all(by.tagName('option')).last().click();
  }

  async adresseLivraisonSelectLastOption(): Promise<void> {
    await this.adresseLivraisonSelect.all(by.tagName('option')).last().click();
  }

  async adresseLivraisonSelectOption(option: string): Promise<void> {
    await this.adresseLivraisonSelect.sendKeys(option);
  }

  getAdresseLivraisonSelect(): ElementFinder {
    return this.adresseLivraisonSelect;
  }

  async getAdresseLivraisonSelectedOption(): Promise<string> {
    return await this.adresseLivraisonSelect.element(by.css('option:checked')).getText();
  }

  async adresseFacturationSelectLastOption(): Promise<void> {
    await this.adresseFacturationSelect.all(by.tagName('option')).last().click();
  }

  async adresseFacturationSelectOption(option: string): Promise<void> {
    await this.adresseFacturationSelect.sendKeys(option);
  }

  getAdresseFacturationSelect(): ElementFinder {
    return this.adresseFacturationSelect;
  }

  async getAdresseFacturationSelectedOption(): Promise<string> {
    return await this.adresseFacturationSelect.element(by.css('option:checked')).getText();
  }

  async personnePhysiqueSelectLastOption(): Promise<void> {
    await this.personnePhysiqueSelect.all(by.tagName('option')).last().click();
  }

  async personnePhysiqueSelectOption(option: string): Promise<void> {
    await this.personnePhysiqueSelect.sendKeys(option);
  }

  getPersonnePhysiqueSelect(): ElementFinder {
    return this.personnePhysiqueSelect;
  }

  async getPersonnePhysiqueSelectedOption(): Promise<string> {
    return await this.personnePhysiqueSelect.element(by.css('option:checked')).getText();
  }

  async personneMoraleSelectLastOption(): Promise<void> {
    await this.personneMoraleSelect.all(by.tagName('option')).last().click();
  }

  async personneMoraleSelectOption(option: string): Promise<void> {
    await this.personneMoraleSelect.sendKeys(option);
  }

  getPersonneMoraleSelect(): ElementFinder {
    return this.personneMoraleSelect;
  }

  async getPersonneMoraleSelectedOption(): Promise<string> {
    return await this.personneMoraleSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ClientDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-client-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-client'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
