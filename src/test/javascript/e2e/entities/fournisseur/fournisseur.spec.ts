import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  FournisseurComponentsPage,
  /* FournisseurDeleteDialog, */
  FournisseurUpdatePage,
} from './fournisseur.page-object';

const expect = chai.expect;

describe('Fournisseur e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fournisseurComponentsPage: FournisseurComponentsPage;
  let fournisseurUpdatePage: FournisseurUpdatePage;
  /* let fournisseurDeleteDialog: FournisseurDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Fournisseurs', async () => {
    await navBarPage.goToEntity('fournisseur');
    fournisseurComponentsPage = new FournisseurComponentsPage();
    await browser.wait(ec.visibilityOf(fournisseurComponentsPage.title), 5000);
    expect(await fournisseurComponentsPage.getTitle()).to.eq('goodFoodApp.fournisseur.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fournisseurComponentsPage.entities), ec.visibilityOf(fournisseurComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Fournisseur page', async () => {
    await fournisseurComponentsPage.clickOnCreateButton();
    fournisseurUpdatePage = new FournisseurUpdatePage();
    expect(await fournisseurUpdatePage.getPageTitle()).to.eq('goodFoodApp.fournisseur.home.createOrEditLabel');
    await fournisseurUpdatePage.cancel();
  });

  /* it('should create and save Fournisseurs', async () => {
        const nbButtonsBeforeCreate = await fournisseurComponentsPage.countDeleteButtons();

        await fournisseurComponentsPage.clickOnCreateButton();

        await promise.all([
            fournisseurUpdatePage.setPrenomInput('prenom'),
            fournisseurUpdatePage.setNomInput('nom'),
            fournisseurUpdatePage.setEmailInput('email'),
            fournisseurUpdatePage.setTitreInput('titre'),
            fournisseurUpdatePage.setTelEntrepriseInput('telEntreprise'),
            fournisseurUpdatePage.setTelMaisonInput('telMaison'),
            fournisseurUpdatePage.setTelMobileInput('telMobile'),
            fournisseurUpdatePage.setNumeroFaxInput('numeroFax'),
            fournisseurUpdatePage.setPageWebInput('pageWeb'),
            fournisseurUpdatePage.setNotesInput('notes'),
            fournisseurUpdatePage.idAdresseSelectLastOption(),
        ]);

        expect(await fournisseurUpdatePage.getPrenomInput()).to.eq('prenom', 'Expected Prenom value to be equals to prenom');
        expect(await fournisseurUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
        expect(await fournisseurUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await fournisseurUpdatePage.getTitreInput()).to.eq('titre', 'Expected Titre value to be equals to titre');
        expect(await fournisseurUpdatePage.getTelEntrepriseInput()).to.eq('telEntreprise', 'Expected TelEntreprise value to be equals to telEntreprise');
        expect(await fournisseurUpdatePage.getTelMaisonInput()).to.eq('telMaison', 'Expected TelMaison value to be equals to telMaison');
        expect(await fournisseurUpdatePage.getTelMobileInput()).to.eq('telMobile', 'Expected TelMobile value to be equals to telMobile');
        expect(await fournisseurUpdatePage.getNumeroFaxInput()).to.eq('numeroFax', 'Expected NumeroFax value to be equals to numeroFax');
        expect(await fournisseurUpdatePage.getPageWebInput()).to.eq('pageWeb', 'Expected PageWeb value to be equals to pageWeb');
        expect(await fournisseurUpdatePage.getNotesInput()).to.eq('notes', 'Expected Notes value to be equals to notes');

        await fournisseurUpdatePage.save();
        expect(await fournisseurUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await fournisseurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Fournisseur', async () => {
        const nbButtonsBeforeDelete = await fournisseurComponentsPage.countDeleteButtons();
        await fournisseurComponentsPage.clickOnLastDeleteButton();

        fournisseurDeleteDialog = new FournisseurDeleteDialog();
        expect(await fournisseurDeleteDialog.getDialogTitle())
            .to.eq('goodFoodApp.fournisseur.delete.question');
        await fournisseurDeleteDialog.clickOnConfirmButton();

        expect(await fournisseurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
