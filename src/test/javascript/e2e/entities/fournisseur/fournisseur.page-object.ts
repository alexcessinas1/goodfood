import { element, by, ElementFinder } from 'protractor';

export class FournisseurComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-fournisseur div table .btn-danger'));
  title = element.all(by.css('jhi-fournisseur div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FournisseurUpdatePage {
  pageTitle = element(by.id('jhi-fournisseur-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  prenomInput = element(by.id('field_prenom'));
  nomInput = element(by.id('field_nom'));
  emailInput = element(by.id('field_email'));
  titreInput = element(by.id('field_titre'));
  telEntrepriseInput = element(by.id('field_telEntreprise'));
  telMaisonInput = element(by.id('field_telMaison'));
  telMobileInput = element(by.id('field_telMobile'));
  numeroFaxInput = element(by.id('field_numeroFax'));
  pageWebInput = element(by.id('field_pageWeb'));
  notesInput = element(by.id('field_notes'));

  idAdresseSelect = element(by.id('field_idAdresse'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPrenomInput(prenom: string): Promise<void> {
    await this.prenomInput.sendKeys(prenom);
  }

  async getPrenomInput(): Promise<string> {
    return await this.prenomInput.getAttribute('value');
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setTitreInput(titre: string): Promise<void> {
    await this.titreInput.sendKeys(titre);
  }

  async getTitreInput(): Promise<string> {
    return await this.titreInput.getAttribute('value');
  }

  async setTelEntrepriseInput(telEntreprise: string): Promise<void> {
    await this.telEntrepriseInput.sendKeys(telEntreprise);
  }

  async getTelEntrepriseInput(): Promise<string> {
    return await this.telEntrepriseInput.getAttribute('value');
  }

  async setTelMaisonInput(telMaison: string): Promise<void> {
    await this.telMaisonInput.sendKeys(telMaison);
  }

  async getTelMaisonInput(): Promise<string> {
    return await this.telMaisonInput.getAttribute('value');
  }

  async setTelMobileInput(telMobile: string): Promise<void> {
    await this.telMobileInput.sendKeys(telMobile);
  }

  async getTelMobileInput(): Promise<string> {
    return await this.telMobileInput.getAttribute('value');
  }

  async setNumeroFaxInput(numeroFax: string): Promise<void> {
    await this.numeroFaxInput.sendKeys(numeroFax);
  }

  async getNumeroFaxInput(): Promise<string> {
    return await this.numeroFaxInput.getAttribute('value');
  }

  async setPageWebInput(pageWeb: string): Promise<void> {
    await this.pageWebInput.sendKeys(pageWeb);
  }

  async getPageWebInput(): Promise<string> {
    return await this.pageWebInput.getAttribute('value');
  }

  async setNotesInput(notes: string): Promise<void> {
    await this.notesInput.sendKeys(notes);
  }

  async getNotesInput(): Promise<string> {
    return await this.notesInput.getAttribute('value');
  }

  async idAdresseSelectLastOption(): Promise<void> {
    await this.idAdresseSelect.all(by.tagName('option')).last().click();
  }

  async idAdresseSelectOption(option: string): Promise<void> {
    await this.idAdresseSelect.sendKeys(option);
  }

  getIdAdresseSelect(): ElementFinder {
    return this.idAdresseSelect;
  }

  async getIdAdresseSelectedOption(): Promise<string> {
    return await this.idAdresseSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FournisseurDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fournisseur-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fournisseur'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
