import { element, by, ElementFinder } from 'protractor';

export class CommandeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-commande div table .btn-danger'));
  title = element.all(by.css('jhi-commande div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CommandeUpdatePage {
  pageTitle = element(by.id('jhi-commande-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  prixTotalInput = element(by.id('field_prixTotal'));
  dateCommandeInput = element(by.id('field_dateCommande'));
  dateStatutInput = element(by.id('field_dateStatut'));
  statutSelect = element(by.id('field_statut'));
  aLivrerInput = element(by.id('field_aLivrer'));
  datePaiementInput = element(by.id('field_datePaiement'));

  idClientSelect = element(by.id('field_idClient'));
  idFranchiseSelect = element(by.id('field_idFranchise'));
  idFournisseurSelect = element(by.id('field_idFournisseur'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPrixTotalInput(prixTotal: string): Promise<void> {
    await this.prixTotalInput.sendKeys(prixTotal);
  }

  async getPrixTotalInput(): Promise<string> {
    return await this.prixTotalInput.getAttribute('value');
  }

  async setDateCommandeInput(dateCommande: string): Promise<void> {
    await this.dateCommandeInput.sendKeys(dateCommande);
  }

  async getDateCommandeInput(): Promise<string> {
    return await this.dateCommandeInput.getAttribute('value');
  }

  async setDateStatutInput(dateStatut: string): Promise<void> {
    await this.dateStatutInput.sendKeys(dateStatut);
  }

  async getDateStatutInput(): Promise<string> {
    return await this.dateStatutInput.getAttribute('value');
  }

  async setStatutSelect(statut: string): Promise<void> {
    await this.statutSelect.sendKeys(statut);
  }

  async getStatutSelect(): Promise<string> {
    return await this.statutSelect.element(by.css('option:checked')).getText();
  }

  async statutSelectLastOption(): Promise<void> {
    await this.statutSelect.all(by.tagName('option')).last().click();
  }

  getALivrerInput(): ElementFinder {
    return this.aLivrerInput;
  }

  async setDatePaiementInput(datePaiement: string): Promise<void> {
    await this.datePaiementInput.sendKeys(datePaiement);
  }

  async getDatePaiementInput(): Promise<string> {
    return await this.datePaiementInput.getAttribute('value');
  }

  async idClientSelectLastOption(): Promise<void> {
    await this.idClientSelect.all(by.tagName('option')).last().click();
  }

  async idClientSelectOption(option: string): Promise<void> {
    await this.idClientSelect.sendKeys(option);
  }

  getIdClientSelect(): ElementFinder {
    return this.idClientSelect;
  }

  async getIdClientSelectedOption(): Promise<string> {
    return await this.idClientSelect.element(by.css('option:checked')).getText();
  }

  async idFranchiseSelectLastOption(): Promise<void> {
    await this.idFranchiseSelect.all(by.tagName('option')).last().click();
  }

  async idFranchiseSelectOption(option: string): Promise<void> {
    await this.idFranchiseSelect.sendKeys(option);
  }

  getIdFranchiseSelect(): ElementFinder {
    return this.idFranchiseSelect;
  }

  async getIdFranchiseSelectedOption(): Promise<string> {
    return await this.idFranchiseSelect.element(by.css('option:checked')).getText();
  }

  async idFournisseurSelectLastOption(): Promise<void> {
    await this.idFournisseurSelect.all(by.tagName('option')).last().click();
  }

  async idFournisseurSelectOption(option: string): Promise<void> {
    await this.idFournisseurSelect.sendKeys(option);
  }

  getIdFournisseurSelect(): ElementFinder {
    return this.idFournisseurSelect;
  }

  async getIdFournisseurSelectedOption(): Promise<string> {
    return await this.idFournisseurSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CommandeDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-commande-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-commande'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
