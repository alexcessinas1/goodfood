import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  PersonnePhysiqueComponentsPage,
  /* PersonnePhysiqueDeleteDialog, */
  PersonnePhysiqueUpdatePage,
} from './personne-physique.page-object';

const expect = chai.expect;

describe('PersonnePhysique e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let personnePhysiqueComponentsPage: PersonnePhysiqueComponentsPage;
  let personnePhysiqueUpdatePage: PersonnePhysiqueUpdatePage;
  /* let personnePhysiqueDeleteDialog: PersonnePhysiqueDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load PersonnePhysiques', async () => {
    await navBarPage.goToEntity('personne-physique');
    personnePhysiqueComponentsPage = new PersonnePhysiqueComponentsPage();
    await browser.wait(ec.visibilityOf(personnePhysiqueComponentsPage.title), 5000);
    expect(await personnePhysiqueComponentsPage.getTitle()).to.eq('goodFoodApp.personnePhysique.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(personnePhysiqueComponentsPage.entities), ec.visibilityOf(personnePhysiqueComponentsPage.noResult)),
      1000
    );
  });

  it('should load create PersonnePhysique page', async () => {
    await personnePhysiqueComponentsPage.clickOnCreateButton();
    personnePhysiqueUpdatePage = new PersonnePhysiqueUpdatePage();
    expect(await personnePhysiqueUpdatePage.getPageTitle()).to.eq('goodFoodApp.personnePhysique.home.createOrEditLabel');
    await personnePhysiqueUpdatePage.cancel();
  });

  /* it('should create and save PersonnePhysiques', async () => {
        const nbButtonsBeforeCreate = await personnePhysiqueComponentsPage.countDeleteButtons();

        await personnePhysiqueComponentsPage.clickOnCreateButton();

        await promise.all([
            personnePhysiqueUpdatePage.setDateNaissanceInput('2000-12-31'),
            personnePhysiqueUpdatePage.userSelectLastOption(),
        ]);

        expect(await personnePhysiqueUpdatePage.getDateNaissanceInput()).to.eq('2000-12-31', 'Expected dateNaissance value to be equals to 2000-12-31');

        await personnePhysiqueUpdatePage.save();
        expect(await personnePhysiqueUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await personnePhysiqueComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last PersonnePhysique', async () => {
        const nbButtonsBeforeDelete = await personnePhysiqueComponentsPage.countDeleteButtons();
        await personnePhysiqueComponentsPage.clickOnLastDeleteButton();

        personnePhysiqueDeleteDialog = new PersonnePhysiqueDeleteDialog();
        expect(await personnePhysiqueDeleteDialog.getDialogTitle())
            .to.eq('goodFoodApp.personnePhysique.delete.question');
        await personnePhysiqueDeleteDialog.clickOnConfirmButton();

        expect(await personnePhysiqueComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
