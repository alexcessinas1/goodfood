import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AdresseComponentsPage, AdresseDeleteDialog, AdresseUpdatePage } from './adresse.page-object';

const expect = chai.expect;

describe('Adresse e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let adresseComponentsPage: AdresseComponentsPage;
  let adresseUpdatePage: AdresseUpdatePage;
  let adresseDeleteDialog: AdresseDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Adresses', async () => {
    await navBarPage.goToEntity('adresse');
    adresseComponentsPage = new AdresseComponentsPage();
    await browser.wait(ec.visibilityOf(adresseComponentsPage.title), 5000);
    expect(await adresseComponentsPage.getTitle()).to.eq('goodFoodApp.adresse.home.title');
    await browser.wait(ec.or(ec.visibilityOf(adresseComponentsPage.entities), ec.visibilityOf(adresseComponentsPage.noResult)), 1000);
  });

  it('should load create Adresse page', async () => {
    await adresseComponentsPage.clickOnCreateButton();
    adresseUpdatePage = new AdresseUpdatePage();
    expect(await adresseUpdatePage.getPageTitle()).to.eq('goodFoodApp.adresse.home.createOrEditLabel');
    await adresseUpdatePage.cancel();
  });

  it('should create and save Adresses', async () => {
    const nbButtonsBeforeCreate = await adresseComponentsPage.countDeleteButtons();

    await adresseComponentsPage.clickOnCreateButton();

    await promise.all([
      adresseUpdatePage.setNumeroVoieInput('numeroVoie'),
      adresseUpdatePage.setNomVoieInput('nomVoie'),
      adresseUpdatePage.setCodePostalInput('codePostal'),
      adresseUpdatePage.setVilleInput('ville'),
      adresseUpdatePage.setComplementAdresseInput('complementAdresse'),
      adresseUpdatePage.setInfoComplementaireInput('infoComplementaire'),
    ]);

    expect(await adresseUpdatePage.getNumeroVoieInput()).to.eq('numeroVoie', 'Expected NumeroVoie value to be equals to numeroVoie');
    expect(await adresseUpdatePage.getNomVoieInput()).to.eq('nomVoie', 'Expected NomVoie value to be equals to nomVoie');
    expect(await adresseUpdatePage.getCodePostalInput()).to.eq('codePostal', 'Expected CodePostal value to be equals to codePostal');
    expect(await adresseUpdatePage.getVilleInput()).to.eq('ville', 'Expected Ville value to be equals to ville');
    expect(await adresseUpdatePage.getComplementAdresseInput()).to.eq(
      'complementAdresse',
      'Expected ComplementAdresse value to be equals to complementAdresse'
    );
    expect(await adresseUpdatePage.getInfoComplementaireInput()).to.eq(
      'infoComplementaire',
      'Expected InfoComplementaire value to be equals to infoComplementaire'
    );

    await adresseUpdatePage.save();
    expect(await adresseUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await adresseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Adresse', async () => {
    const nbButtonsBeforeDelete = await adresseComponentsPage.countDeleteButtons();
    await adresseComponentsPage.clickOnLastDeleteButton();

    adresseDeleteDialog = new AdresseDeleteDialog();
    expect(await adresseDeleteDialog.getDialogTitle()).to.eq('goodFoodApp.adresse.delete.question');
    await adresseDeleteDialog.clickOnConfirmButton();

    expect(await adresseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
