import { element, by, ElementFinder } from 'protractor';

export class AdresseComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-adresse div table .btn-danger'));
  title = element.all(by.css('jhi-adresse div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AdresseUpdatePage {
  pageTitle = element(by.id('jhi-adresse-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  numeroVoieInput = element(by.id('field_numeroVoie'));
  nomVoieInput = element(by.id('field_nomVoie'));
  codePostalInput = element(by.id('field_codePostal'));
  villeInput = element(by.id('field_ville'));
  complementAdresseInput = element(by.id('field_complementAdresse'));
  infoComplementaireInput = element(by.id('field_infoComplementaire'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNumeroVoieInput(numeroVoie: string): Promise<void> {
    await this.numeroVoieInput.sendKeys(numeroVoie);
  }

  async getNumeroVoieInput(): Promise<string> {
    return await this.numeroVoieInput.getAttribute('value');
  }

  async setNomVoieInput(nomVoie: string): Promise<void> {
    await this.nomVoieInput.sendKeys(nomVoie);
  }

  async getNomVoieInput(): Promise<string> {
    return await this.nomVoieInput.getAttribute('value');
  }

  async setCodePostalInput(codePostal: string): Promise<void> {
    await this.codePostalInput.sendKeys(codePostal);
  }

  async getCodePostalInput(): Promise<string> {
    return await this.codePostalInput.getAttribute('value');
  }

  async setVilleInput(ville: string): Promise<void> {
    await this.villeInput.sendKeys(ville);
  }

  async getVilleInput(): Promise<string> {
    return await this.villeInput.getAttribute('value');
  }

  async setComplementAdresseInput(complementAdresse: string): Promise<void> {
    await this.complementAdresseInput.sendKeys(complementAdresse);
  }

  async getComplementAdresseInput(): Promise<string> {
    return await this.complementAdresseInput.getAttribute('value');
  }

  async setInfoComplementaireInput(infoComplementaire: string): Promise<void> {
    await this.infoComplementaireInput.sendKeys(infoComplementaire);
  }

  async getInfoComplementaireInput(): Promise<string> {
    return await this.infoComplementaireInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AdresseDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-adresse-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-adresse'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
