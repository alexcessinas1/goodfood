import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  FranchiseComponentsPage,
  /* FranchiseDeleteDialog, */
  FranchiseUpdatePage,
} from './franchise.page-object';

const expect = chai.expect;

describe('Franchise e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let franchiseComponentsPage: FranchiseComponentsPage;
  let franchiseUpdatePage: FranchiseUpdatePage;
  /* let franchiseDeleteDialog: FranchiseDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Franchises', async () => {
    await navBarPage.goToEntity('franchise');
    franchiseComponentsPage = new FranchiseComponentsPage();
    await browser.wait(ec.visibilityOf(franchiseComponentsPage.title), 5000);
    expect(await franchiseComponentsPage.getTitle()).to.eq('goodFoodApp.franchise.home.title');
    await browser.wait(ec.or(ec.visibilityOf(franchiseComponentsPage.entities), ec.visibilityOf(franchiseComponentsPage.noResult)), 1000);
  });

  it('should load create Franchise page', async () => {
    await franchiseComponentsPage.clickOnCreateButton();
    franchiseUpdatePage = new FranchiseUpdatePage();
    expect(await franchiseUpdatePage.getPageTitle()).to.eq('goodFoodApp.franchise.home.createOrEditLabel');
    await franchiseUpdatePage.cancel();
  });

  /* it('should create and save Franchises', async () => {
        const nbButtonsBeforeCreate = await franchiseComponentsPage.countDeleteButtons();

        await franchiseComponentsPage.clickOnCreateButton();

        await promise.all([
            franchiseUpdatePage.setTelephoneInput('telephone'),
            franchiseUpdatePage.setEmailInput('email'),
            franchiseUpdatePage.setHoraireInput('horaire'),
            franchiseUpdatePage.setSiretInput('siret'),
            franchiseUpdatePage.adresseFranchiseIdSelectLastOption(),
            // franchiseUpdatePage.stockFranchiseSelectLastOption(),
        ]);

        expect(await franchiseUpdatePage.getTelephoneInput()).to.eq('telephone', 'Expected Telephone value to be equals to telephone');
        expect(await franchiseUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await franchiseUpdatePage.getHoraireInput()).to.eq('horaire', 'Expected Horaire value to be equals to horaire');
        expect(await franchiseUpdatePage.getSiretInput()).to.eq('siret', 'Expected Siret value to be equals to siret');

        await franchiseUpdatePage.save();
        expect(await franchiseUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await franchiseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Franchise', async () => {
        const nbButtonsBeforeDelete = await franchiseComponentsPage.countDeleteButtons();
        await franchiseComponentsPage.clickOnLastDeleteButton();

        franchiseDeleteDialog = new FranchiseDeleteDialog();
        expect(await franchiseDeleteDialog.getDialogTitle())
            .to.eq('goodFoodApp.franchise.delete.question');
        await franchiseDeleteDialog.clickOnConfirmButton();

        expect(await franchiseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
