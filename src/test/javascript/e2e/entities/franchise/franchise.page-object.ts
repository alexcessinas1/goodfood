import { element, by, ElementFinder } from 'protractor';

export class FranchiseComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-franchise div table .btn-danger'));
  title = element.all(by.css('jhi-franchise div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FranchiseUpdatePage {
  pageTitle = element(by.id('jhi-franchise-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  telephoneInput = element(by.id('field_telephone'));
  emailInput = element(by.id('field_email'));
  horaireInput = element(by.id('field_horaire'));
  siretInput = element(by.id('field_siret'));

  adresseFranchiseIdSelect = element(by.id('field_adresseFranchiseId'));
  stockFranchiseSelect = element(by.id('field_stockFranchise'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTelephoneInput(telephone: string): Promise<void> {
    await this.telephoneInput.sendKeys(telephone);
  }

  async getTelephoneInput(): Promise<string> {
    return await this.telephoneInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setHoraireInput(horaire: string): Promise<void> {
    await this.horaireInput.sendKeys(horaire);
  }

  async getHoraireInput(): Promise<string> {
    return await this.horaireInput.getAttribute('value');
  }

  async setSiretInput(siret: string): Promise<void> {
    await this.siretInput.sendKeys(siret);
  }

  async getSiretInput(): Promise<string> {
    return await this.siretInput.getAttribute('value');
  }

  async adresseFranchiseIdSelectLastOption(): Promise<void> {
    await this.adresseFranchiseIdSelect.all(by.tagName('option')).last().click();
  }

  async adresseFranchiseIdSelectOption(option: string): Promise<void> {
    await this.adresseFranchiseIdSelect.sendKeys(option);
  }

  getAdresseFranchiseIdSelect(): ElementFinder {
    return this.adresseFranchiseIdSelect;
  }

  async getAdresseFranchiseIdSelectedOption(): Promise<string> {
    return await this.adresseFranchiseIdSelect.element(by.css('option:checked')).getText();
  }

  async stockFranchiseSelectLastOption(): Promise<void> {
    await this.stockFranchiseSelect.all(by.tagName('option')).last().click();
  }

  async stockFranchiseSelectOption(option: string): Promise<void> {
    await this.stockFranchiseSelect.sendKeys(option);
  }

  getStockFranchiseSelect(): ElementFinder {
    return this.stockFranchiseSelect;
  }

  async getStockFranchiseSelectedOption(): Promise<string> {
    return await this.stockFranchiseSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FranchiseDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-franchise-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-franchise'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
